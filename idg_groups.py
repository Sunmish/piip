#! /usr/bin/env python

import os
from argparse import ArgumentParser
import math
import numpy as np
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.io import fits

from subprocess import Popen
from sklearn.cluster import KMeans

from matplotlib.colors import rgb2hex
from matplotlib.cm import get_cmap 


def get_args():

    ps = ArgumentParser()
    ps.add_argument("obslists", nargs="*", type=str, help="Lists of obsids.")
    ps.add_argument("-o", "--outname", default=None)
    ps.add_argument("-N", "--max_obs_per_group", 
        default=24, 
        type=int,
        help="Maximum number of observations per group."
    )
    ps.add_argument("-c", "--cmap",
        default="Spectral_r",
        type=str,
        help="Colourmap for output region files."
    )
    ps.add_argument("-C", "--coords", nargs=2,
        type=float,
        default=None,
        help="Reference coords to ensure cluster closest to target is full."
    )

    return ps.parse_args()


def mean_coordinates(ra, dec):
    """Mean RA,DEC from a list."""

    mean_dec = np.mean(dec)
    mean_ra = np.mean(
        np.degrees(
                np.arctan2(
                    (np.sum(np.sin(np.radians(ra)))/len(ra)), 
                    (np.sum(np.cos(np.radians(ra)))/len(ra))
                )
            )
        )

    return mean_ra, mean_dec


def make_metafits(obsid):
    """Download metafits file for a given obsid."""

    with open("make_metafits.log", "a+") as log:
        Popen(
            "wget http://ws.mwatelescope.org/metadata/fits/?obs_id={0} -O {0}.metafits".format(obsid),
            stdout=log, 
            stderr=log, 
            shell=True
        ).wait()


def open_obslist(obslist):
    """Open obsid list file."""

    obsids = []
    antennas = []
    with open(obslist) as f:
        lines = f.readlines()
        for line in lines:
            bits = line.split()
            obsid = bits[0]
            if len(obsid) == 10:
                print(bits,int(obsid),obsid)
                obsids.append(int(obsid))
                if len(bits) > 1:
                    antennas.append(bits[1])
                else:
                    antennas.append("")
    
    return obsids, antennas


def get_obsid_coords(obsids):

    metafits = []
    for obsid in obsids:
        if not os.path.exists("{}.metafits".format(obsid)):
            make_metafits(obsid)
        try:
            metafits.append(fits.getheader("{}.metafits".format(obsid)))
        except OSError:
            print(obsid)
            raise

    radec = SkyCoord(
        ra=np.asarray([mf["RA"] for mf in metafits])*u.deg,
        dec=np.asarray([mf["DEC"] for mf in metafits])*u.deg
    )

    ra = [mf["RA"] for mf in metafits]
    dec = [mf["DEC"] for mf in metafits]

    return ra, dec


def get_n_groups(obsids, max_obs_per_group=24):

    n = len(np.hstack(obsids))
    n_groups = int(math.ceil(n / max_obs_per_group))
    print("Selecting {} groups, max {} per group (of {} obsids)".format(
        n_groups, max_obs_per_group, n))

    return n_groups
    

def get_group_locations(ra, dec, n_groups):
    """Get group locations from clustering."""

    rads = np.array(
        [
            np.unwrap(np.radians(ra)),
            np.asarray(np.radians(dec))
        ]
    ).T

    db = KMeans(
        n_clusters=int(n_groups),
        n_init=100,
        tol=1e-2
    ).fit(rads)

    group_coords = np.asarray([
        SkyCoord(
            ra=np.degrees(db.cluster_centers_[i][0])*u.deg,
            dec=np.degrees(db.cluster_centers_[i][1])*u.deg
        ) for i in range(len(db.cluster_centers_))
    ])

    return group_coords


def make_even_groups(obsids, ra, dec, group_coords, n_groups, antennas,
    ref_coords=None):
    """Re-distribute obsids to make more even groupings."""

    groups_obsids = {}
    groups_antennas = {}
    groups_obsids_ra = {}
    groups_obsids_dec = {}

    max_n = [
        int(math.ceil(len(obsid_list)/n_groups)) for obsid_list in obsids
    ]

    used_obsids = []

    if ref_coords is not None:
        # Start from the closest cluster, since the cluster farthest away
        # will likely have a few obsids less.
        gdx = order_groups(ref_coords, group_coords)
    else:
        gdx = range(len(group_coords))

    for i in gdx:

        groups_obsids[i] = []
        groups_antennas[i] = []
        groups_obsids_ra[i] = []
        groups_obsids_dec[i] = []

        for j in range(len(obsids)):


            ra[j] = np.asarray([ra[j][n] for n in range(len(ra[j])) if obsids[j][n] not in used_obsids])
            dec[j] = np.asarray([dec[j][n] for n in range(len(dec[j])) if obsids[j][n] not in used_obsids])

            radec = SkyCoord(
                ra=ra[j]*u.deg,
                dec=dec[j]*u.deg
            )
            antennas[j] = np.array([
                antennas[j][n] for n in range(len(antennas[j])) if obsids[j][n] not in used_obsids
            ])
            obsids[j] = np.array([
                obsid for obsid in obsids[j] if obsid not in used_obsids
            ])

            seps = radec.separation(group_coords[i])
            print(seps[np.argsort(seps.value)].value)

            idx = np.argsort(seps.value)[0:max_n[j]]

            for k in idx:
                if obsids[j][k] not in used_obsids:
                    groups_obsids[i].append(obsids[j][k])
                    groups_obsids_ra[i].append(ra[j][k])
                    groups_obsids_dec[i].append(dec[j][k])
                    groups_antennas[i].append(antennas[j][k])
                    used_obsids.append(obsids[j][k])


    for i in range(len(group_coords)):

        print("Group {}: {} obsids".format(i, len(groups_obsids[i])))

    return groups_obsids, groups_antennas, groups_obsids_ra, groups_obsids_dec


def order_groups(ref_coords, group_coords):

    radec = SkyCoord(ra=ref_coords[0]*u.deg, dec=ref_coords[1]*u.deg)
    
    gcoords = SkyCoord(
        ra=np.array([k.ra.value for k in group_coords])*u.deg,
        dec=np.array([k.dec.value for k in group_coords])*u.deg
    )
    seps = radec.separation(gcoords)
    gdx = np.argsort(seps.value)
    # print(seps[gdx])
    return gdx


def make_region(obslist, color, dash=False):
    outname = obslist.replace(".txt", "") + ".reg"
    color = rgb2hex(color)
    if dash:
        dash = "-d"
    else:
        dash = ""
    cmd = "obs2reg {obslist} -k -o {outname} -c '{color}' {dash}".format(
        obslist=obslist, outname=outname, color=color, dash=dash
    )
    Popen(cmd, shell=True).wait()


def write_out_groups(group_obsids, groups_antennas, outname,
    cmap="Spectral_r"):

    cmap = get_cmap(cmap)

    colors = [cmap(i/len(group_obsids.keys())) \
        for i in range(len(group_obsids.keys()))]

    for g in group_obsids.keys():

        mwa1_name = "{}_mwa1_group{}.txt".format(
            outname, g
        )
        mwa2_name = "{}_mwa2_group{}.txt".format(
            outname, g
        )

        mwa1 = open(mwa1_name, "w+")
        mwa2 = open(mwa2_name, "w+")

        for i in range(len(group_obsids[g])):

            if group_obsids[g][i] > 1154135392:

                mwa2.write("{} {}\n".format(
                    group_obsids[g][i],
                    groups_antennas[g][i]
                ))

            else:

                mwa1.write("{} {}\n".format(
                    group_obsids[g][i],
                    groups_antennas[g][i]
                ))


        mwa1.flush()
        mwa1.close()

        mwa2.flush()
        mwa2.close()

        make_region(mwa1_name, colors[g], dash=True)
        make_region(mwa2_name, colors[g], dash=False)

        
def make_groups(obslists, max_obs_per_group=24,
        ref_coords=None,
        outname=None,
        cmap="Spectral_r"):

    all_obsids, all_antennas, all_radec = [], [], []
    all_ra = []
    all_dec = []

    for i in range(len(obslists)):

        obsids, antennas = open_obslist(obslists[i])
        all_obsids.append(obsids)
        all_antennas.append(antennas)

        ra, dec = get_obsid_coords(obsids)
        all_ra.append(ra)
        all_dec.append(dec)
    
    
    n_groups = get_n_groups(
        obsids=all_obsids,
        max_obs_per_group=max_obs_per_group
    )

    group_locations = get_group_locations(
        ra=np.hstack(all_ra), 
        dec=np.hstack(all_dec),
        n_groups=n_groups
    )

    groups_obsids, groups_antennas, groups_obsids_ra, groups_obsids_dec = make_even_groups(
        obsids=all_obsids, 
        ra=all_ra,
        dec=all_dec, 
        group_coords=group_locations, 
        n_groups=n_groups, 
        antennas=all_antennas,
        ref_coords=ref_coords
    )

    if outname is not None:
        write_out_groups(
            group_obsids=groups_obsids, 
            groups_antennas=groups_antennas, 
            outname=outname,
            cmap=cmap)

    


def cli(args):

    make_groups(
        obslists=args.obslists,
        max_obs_per_group=args.max_obs_per_group,
        ref_coords=args.coords,
        outname=args.outname,
        cmap=args.cmap
    )


if __name__ == "__main__":
    cli(get_args())




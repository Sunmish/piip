#!/bin/bash

#SBATCH --account=mwasci
#SBATCH --partition=workq
#SBATCH --time=06:00:00
#SBATCH --nodes=1

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh


USE_OLD=false
SELFCAL_CHAN_AVG=1

FLAG=true
FLAG_SELFCAL=true
FLAG_AO=true
FLAG_TILE=true
FLAG_CHANNEL=true
FLAG_QUACK=true
FLAG_BASELINES=true
SELFCAL_NITER=100000
SELFCAL_BRIGGS="0.0"
SELFCAL_MINUV=100
SELFCAL_CASA=false
SELFCAL_XXYY=false
SELFCAL_SIGMA1=5
SELFCAL_SIGMA2=1
SELFCAL_CHANNELS=4
SELFCAL_CASA_SOLINT="120s"
SELFCAL_CASA_CALMODE="p"
SELFCAL_CASA_REFANT="0"
SELFCAL_SOLNORM=true
SELFCAL_EXTRA_OPT=
CASA_LOCATION=casa
TMP_DIR=
USE_JOB_FOR_TMP=true
ARRAY_OFFSET=0
BIND_PATH=
REMOTE_PROCESSED=
QUICK_LOOK_VMAX=1
QUICK_LOOK_VMIN=-0.1

CALIBRATION_COLUMN="DATA"
SELFCAL_COLUMN="DATA"

start_time=$(date +%s)

opts=$(getopt \
     -o :hd:O:s:f:C:m:a:wuT:A: \
     --long help,processing_dir:,obsid_list:,scale:,field:,calibrator:,minuv:,obsnum:,nominw,useold,flagtiles:,array_offset: \
     --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piipselfcal
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done


# after configuration file loading:
set -x

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipselfcal ;;
        -d|--processing_dir) INDIR=$2 ; shift 2 ;;
        -O|--obsid_list) obsid_list=$2 ; shift 2 ;;
        -s|--scale) SCALE=$2 ; shift 2 ;;
        -f|--field) field=$2 ; shift 2 ;;
        -C|--calibrator) calibrator=$2 ; shift 2 ;;
        -m|--minuv) SELFCAL_MINUV=$2 ; shift 2 ;;
        -w|--nominw) MINW=false ; shift ;;
        -a|--obsnum) obsnum=$2 ; shift 2 ;;
        -U|--useold) USE_OLD=true ; shift ;;
        -T|--flagtiles) flagtiles=$2 ; shift 2 ;;
        -A|--array_offset) ARRAY_OFFSET=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR: $1 $2" ; usage_piipselfcal ;;
    esac
done

check_required "--processing_dir" "-d" $INDIR

if [ -z ${TMP_DIR} ] || [ "${TMP_DIR}" == "./" ]; then
    TMP_DIR="./"
    USING_TMP=false
else
    TMP_DIR="${TMP_DIR}/"
    if $USE_JOB_FOR_TMP; then
        TMP_DIR="${TMP_DIR}/${SLURM_JOB_ID}/"
    fi 
    if [ ! -e ${TMP_DIR} ]; then
        mkdir -p ${TMP_DIR} || exit 1
    fi
    USING_TMP=true
fi

# Set obsid from array:
if [ -z ${SLURM_ARRAY_TASK_ID} ]; then
    check_required "--obsnum" "-a" ${obsnum}
else
    obsnum=$((${SLURM_ARRAY_TASK_ID} + ${ARRAY_OFFSET}))
    
fi

if [ ${#obsnum} = 10 ]; then
    obsid=${obsnum}
else
    check_required "--obsid_list" "-o" $obsid_list
    obsid=$(sed "${obsnum}q;d" ${obsid_list} | awk '{print $1}')
fi

check_obsid ${obsid}
if [ ! -e ${INDIR}/${obsid} ]; then
    echo "ERROR: missing working directory - has calibration been done?"
    exit 1
fi

if [ ! -z ${config} ]; then
    cp ${config} ${INDIR}/${obsid}/
fi
cd ${INDIR}/${obsid} || exit 1


if ${USING_TMP}; then
    BIND_PATH="${TMP_DIR},${BIND_PATH}"
fi

if [ -z ${CONTAINER} ]; then
    container=""
else
    if [ ! -z ${BIND_PATH} ]; then
        BIND_PATH="-B ${BIND_PATH}"
    fi
    container="singularity exec ${BIND_PATH} ${CONTAINER}"
fi 

# Move MS into temp directory if not already there:
if ${USING_TMP}; then
    if [ -e ${TMP_DIR}${obsid}.ms ]; then 
        rm -r ${TMP_DIR}${obsid}.ms
    fi

    if [ ! -z ${REMOTE_PROCESSED} ]; then
        rclone copy --local-no-set-modtime ${REMOTE_PROCESSED}/${obsid}_ms.tar ${TMP_DIR}
        tar -mxvf ${TMP_DIR}${obsid}_ms.tar -C ${TMP_DIR} && rm ${TMP_DIR}${obsid}_ms.tar

    else
        cp -r ${obsid}.ms ${TMP_DIR}
    fi
fi

chan=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "CENTCHAN")

if [ ! -z ${field} ] && [ ! -z ${DBFILE} ]; then
    d_format=$(date +%F:%H:%M:%S)
    if [ ${#obsnum} -lt 10 ]; then
        extraOptions="--selection=${obsnum}"
    else
        extraOptions=""
    fi
    $container ${PIIPPATH}/field_manager.py -m "update_snapshot_status" \
        -d ${DBFILE} \
        --field=${field} \
        --chan=${chan} \
        --status="start-preimage-5-[${d_format}]" \
        --overwrite \
        --obsid=${obsid} ${extraOptions}
fi

# Allow overwriting of DATA column:
if [ ${CALIBRATION_COLUMN} = "DATA" ]; then
    calcopy="-nocopy"
    datacolumn="data"
else
    calcopy="-copy"
    datacolumn="corrected"
fi

scale=$(echo "${SCALE}/${chan}" | bc -l)
freq=$(echo "${chan}*1.28" | bc -l)

# Change phase centre to make sure we don't image too much beyond the horizon.
# The phase centre will be defined as the location of the peak of the main lobe
# of the primary beam, rather than the "pointing" centre. This differs as you
# point off-zenith.
PERCENT=0.05
$container get_beam_lobes ${obsid}.metafits -p ${PERCENT} -mM > ${obsid}_beamlobes.txt
mainlobe=$(grep "main" < ${obsid}_beamlobes.txt)
ra_beam=$(echo "$mainlobe" | awk '{print $2}')
dec_beam=$(echo "$mainlobe" | awk '{print $3}')
radec="${ra_beam} ${dec_beam}"
radec_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${radec} -d "hms")


# Ensure that we are at the pointing centre,
$container chgcentre ${TMP_DIR}${obsid}.ms ${radec_hmsdms}


# ---------------------------------------------------------------------------- #

# We will flag again in case calibration has revealed any bad tiles:
# Flag specific tiles and auto-flag some channels:
if ${FLAG} && ${FLAG_SELFCAL}; then

    if [ ! -z ${flagtiles} ]; then
        flagExtras="--tiles=${flagtiles}"
    fi 

    ${PIIPPATH}/piipflag.sh ${config} --ms=${TMP_DIR}${obsid}.ms \
        --datacolumn=${CALIBRATION_COLUMN} \
        --obsid_list="${obsid_list}" \
        --nobaseline ${flagExtras} \
        --obsid=${obsid}

fi

scale=$(echo "${SCALE}/${chan}" | bc -l)
rad=$(echo "${scale}*${IMSIZE}/2.0" | bc -l)
minuv=$($container ${PIIPPATH}/get_baselines_from_lambdas.py ${freq} ${SELFCAL_MINUV})


if $SELFCAL_XXYY; then
    selfcal_pol="xx,yy"
else
    selfcal_pol="I"
fi

# If required that we shift to the minimum w-term position so that 
# WSClean will complete within a reasonable time frame:
if ${MINW}; then
    $container chgcentre -minw -shiftback ${obsid}.ms


    nwlayerSize=$($container python -c "print(int(${IMSIZE}*1.5))")

    $container wsclean -name ${TMP_DIR}${obsid}_initial \
            -size ${IMSIZE} ${IMSIZE} \
            -niter ${SELFCAL_NITER} \
            -pol I \
            -join-channels \
            -weight briggs ${SELFCAL_BRIGGS} \
            -scale ${scale:0:8} \
            -auto-threshold ${SELFCAL_SIGMA2} \
            -auto-mask ${SELFCAL_SIGMA1} \
            -abs-mem ${ABSMEM} \
            -padding 1.5 \
            -mgain 0.8 \
            -no-mf-weighting \
            -nwlayers-for-size ${nwlayerSize} ${nwlayerSize} \
            -data-column ${CALIBRATION_COLUMN} \
            -channels-out ${SELFCAL_CHANNELS} \
            -j ${NCPUS} \
            -minuv-l ${SELFCAL_MINUV} \
            -temp-dir ${TMP_DIR} \
            ${TMP_DIR}${obsid}.ms

else


    $container wsclean -name ${TMP_DIR}${obsid}_initial \
        -size ${IMSIZE} ${IMSIZE} \
        -niter ${SELFCAL_NITER} \
        -pol ${selfcal_pol} \
        -weight briggs ${SELFCAL_BRIGGS} \
        -scale ${scale:0:8} \
        -auto-threshold ${SELFCAL_SIGMA2} \
        -auto-mask ${SELFCAL_SIGMA1} \
        -abs-mem ${ABSMEM} \
        -padding 1.5 \
        -mgain 0.8 \
        -data-column ${CALIBRATION_COLUMN} \
        -channels-out ${SELFCAL_CHANNELS} \
        -j ${NCPUS} \
        -use-wgridder \
        -join-channels \
        -no-mf-weighting \
        -nmiter 5 \
        -minuv-l ${SELFCAL_MINUV} \
        -temp-dir ${TMP_DIR} \
        ${SELFCAL_EXTRA_OPTS} ${TMP_DIR}${obsid}.ms


fi

if ${USING_TMP}; then
    cp ${TMP_DIR}${obsid}_initial*.fits .
fi

if $SELFCAL_XXYY; then
    
    for stokes in XX YY; do
    ${container} ${PIIPPATH}/quick_look.py  "${obsid}_initial-MFS-${stokes}-image.fits" \
            --cmap cubehelix \
            --inset \
            --vmin ${QUICK_LOOK_VMAX} \
            --vmin ${QUICK_LOOK_VMIN} \
            --rms
    done

    modelflux=$($container $PIIPPATH/get_model_flux.py ${obsid}_initial-XX-MFS-model.fits)

else
    ${container} ${PIIPPATH}/quick_look.py  "${obsid}_initial-MFS-image.fits" \
        --cmap cubehelix \
        --inset \
        --vmin ${QUICK_LOOK_VMAX} \
        --vmin ${QUICK_LOOK_VMIN} \
        --rms

    modelflux=$($container $PIIPPATH/get_model_flux.py ${obsid}_initial-MFS-model.fits)
fi

if $SELFCAL_CASA; then

    if [ -z ${CONTAINER} ]; then
        casa_bin=${CASA_LOCATION}
    else
        casa_bin="${container} casa"
    fi

    casa_bin=${CASA_LOCATION}
    # TODO bind /home/ for singularity otherwise cASA fails

    # TODO check refant not in flagged ants

    ${casa_bin} --nogui --agg --nologger -c "gaincal( \
        vis='${TMP_DIR}${obsid}.ms', \
        caltable='${TMP_DIR}${obsid}_selfcal.gcal',
        solint='${SELFCAL_CASA_SOLINT}', \
        calmode='${SELFCAL_CASA_CALMODE}', \
        uvrange='>${SELFCAL_MINUV}lambda', \
        solnorm=True, \
        refant='${SELFCAL_CASA_REFANT}')"

     ${container} $PIIPPATH/ms_apply_gains.py \
        ${TMP_DIR}${obsid}.ms ${TMP_DIR}${obsid}_selfcal.gcal \
        --column=DATA \
        --output_column=${SELFCAL_COLUMN} \
        --verbose

    if $USING_TMP; then
        rsync -av ${TMP_DIR}${obsid}_selfcal.gcal .
        rm -r ${TMP_DIR}${obsid}.selfcal.gcal
    fi 

    applycal=true

else
    # check flux in model and set intervals out accordingly:
    if [ -z ${SELFCAL_TSTEPS} ]; then

        # this needs to be scaled based on frequency - so convert to 88 MHz reference:
        threshold=$($container ${PIIPPATH}/get_scaled_flux.py 88.0 -f ${freq} -a -0.77 -S ${modelflux} -i)

        # these settings are for now trial-and-error:
        if [ ${threshold} -lt 100 ]; then
            # probably shouldn't be self-calibrating...
            SELFCAL_TSTEPS=999
        elif [ ${threshold} -lt 300 ]; then
            SELFCAL_TSTEPS=15
        elif [ ${threshold} -lt 1000 ]; then
            SELFCAL_TSTEPS=10
        elif [ ${threshold} -lt 5000 ]; then
            # e.g. CenA fields
            SELFCAL_TSTEPS=6
        else
            # the SUN/CygA
            SELFCAL_TSTEPS=1
        fi
    fi

    $container calibrate -j ${NCPUS} \
                -absmem ${ABSMEM} \
                -minuv ${minuv} \
                -t ${SELFCAL_TSTEPS} \
                -ch ${SELFCAL_CHAN_AVG} \
                -datacolumn ${SELFCAL_COLUMN} \
                ${TMP_DIR}${obsid}.ms \
                ${obsid}_selfcal.bin > ${obsid}_calibrate3.log 2>&1

    applycal=true
    status="selfcal"

    for amp in 2 50; do
        $container ${PIIPPATH}/plot_solutions.py ${obsid}_selfcal.bin \
            --refant=-1 \
            --outname="./aocal_plots/${obsid}_selfcal_${amp}" \
            --amp_max=${amp} \
            --metafits=${obsid}.metafits
    done


    if $applycal; then

        flaggedchans=$(grep "gains to NaN" ${obsid}_calibrate3.log | \
                    awk '{printf("%03d\n",$2)}' | \
                    sort | \
                    uniq | \
                    wc -l)



        if [[ ${flaggedchans} -gt 350 || ! -s ${obsid}_selfcal.bin ]]; then
            echo "WARNING: > 350 channels flagged (of 768) - no selfcal."
            echo "" > no_selfcal_done.log
            status="noselfcal"
        else
            $container applysolutions -datacolumn ${SELFCAL_COLUMN} ${calcopy} \
                        ${TMP_DIR}${obsid}.ms \
                        ${obsid}_selfcal.bin

        fi
    
    fi

fi

# Now remove all the useless images:
files=$(find . -type f -name "${obsid}*.fits" -not \
    \( -name "*MFS*-image*.fits" -or -name "*MFS*-model*.fits" \))
for file in ${files[@]}; do
    if [ -e ${file} ]; then
        rm ${file}
    fi
done

if ${USING_TMP}; then
    
    if [ ! -z ${REMOTE_PROCESSED} ]; then
        tar -cf ${TMP_DIR}${obsid}_ms.tar -C ${TMP_DIR} ${obsid}.ms
        rclone copy ${TMP_DIR}${obsid}_ms.tar ${REMOTE_PROCESSED}
    else

        rsync -av ${TMP_DIR}${obsid}.ms .

    fi

    rm -r ${TMP_DIR}${obsid}.ms

elif [ ! -z ${REMOTE_PROCESSED} ]; then
    tar -cf ${TMP_DIR}${obsid}_ms.tar -C ${TMP_DIR} ${obsid}.ms
    rclone copy ${TMP_DIR}${obsid}_ms.tar ${REMOTE_PROCESSED}
fi

# ---------------------------------------------------------------------------- #

end_time=$(date +%s)
duration=$(echo "${end_time}-${start_time}" | bc -l)
echo "INFO: Total time taken: ${duration}"

if [ ! -z ${SLURM_ARRAY_TASK_ID} ]; then
    mv ${LOG_LOCATION}/${SLURM_JOB_NAME}.o${SLURM_ARRAY_JOB_ID}_${obsnum} \
        ${LOG_LOCATION}/${SLURM_JOB_NAME}.e${SLURM_ARRAY_JOB_ID}_${obsnum} .
fi

if [ ! -z "${field}" ] && [ ! -z ${DBFILE} ]; then
    d_format=$(date +%F:%H:%M:%S)
    $container ${PIIPPATH}/field_manager.py -m "update_snapshot_status" \
        -d ${DBFILE} \
        --field=${field} \
        --chan=${chan} \
        --status="${status}-5-[${d_format}]" \
        --overwrite \
        --obsid=${obsid} ${extraOptions}

fi

exit 0






















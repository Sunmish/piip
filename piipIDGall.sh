#!/bin/bash
#SBATCH --account=mwasci
#SBATCH --partition=gpuq
#SBATCH --time=24:00:00
#SBATCH --nodes=1
#SBATCH --cpus-per-gpu=38
#SBATCH --gres=gpu:1 
#SBATCH --job-name=IDGall
#SBATCH --mem=370GB
#SBATCH --tmp=890GB
#SBATCH --output=/astro/mwasci/duchesst/G0045/processing/logs/%x.o%A_%a
#SBATCH --error=/astro/mwasci/duchesst/G0045/processing/logs/%x.e%A_%a

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh


IMSIZE=9000
WEIGHT1="r0.5"
WEIGHT2="uniform"
ROBUST="uniform"
KERNEL=55  # arbitrary selection that seems to work?
DIAGONALS=false 
USE_OLD=false
WINDOW="rectangular"
SAVE_ATERMS=false
SUBTRACT=false
INTERP="rbf"

# TODO merge these two containers
CONTAINER1="singularity run --bind /nvmetmp/ /astro/mwasci/duchesst/data_processing3.img"
CONTAINER2="singularity run --nv --bind /nvmetmp/ /astro/mwasci/duchesst/gpu_stuff.img"

start_time=$(date)

opts=$(getopt \
    -o :ho:M:c:d:i:k:r:w:W:DUf:SsI: \
    --long help,outbase:,matchlist:,chan:,processing_dir:,indir:,kernel:,robust:,robust1:,robust2:,diagonals,use_old,window:,save_aterms,subtract,interp: \
    --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piipimage
    exit 1
}


for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

set -x

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipIDGpair ;;
        -o|--outbase) outbase=$2 ; shift 2 ;;
        -M|--matchlist) matchlist=$2 ; shift 2 ;;
        -c|--chan) chan=$2 ; shift 2 ;;
        -d|--processing_dir) outdir=$2 ; shift 2 ;;
        -i|--indir) indir=$2 ; shift 2 ;;
        -k|--kernel) KERNEL=$2 ; shift 2 ;;
        -r|--robust) ROBUST=$2 ; shift 2 ;;
        -w|--weight1) WEIGHT1=$2 ; shift 2 ;;
        -W|--weight2) WEIGHT2=$2 ; shift 2 ;;
        -D|--diagonals) DIAGONALS=true ; shift ;;
        -U|--use_old) USE_OLD=true ; shift ;;
        -f|--window) WINDOW=$2 ; shift 2 ;;
        -S|--save_aterms) SAVE_ATERMS=true ; shift ;;
        -s|--subtract) SUBTRACT=true ; shift ;;
        -I|--interp) INTERP=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR: " ; usage_piipimage ;;
    esac
done

NCPUS=$SLURM_CPUS_PER_GPU
ABSMEM=$(echo "$SLURM_MEM_PER_NODE/1024" | bc -l)

check_required "--outbase" "-o" $outbase
check_required "--matchlist" "-M" $matchlist
check_required "--chan" "-c" $chan
check_required "--processing_dir" "-d" $outdir
check_required "--indir" "-i" $indir

if [ "${ROBUST}" == "uniform" ]; then
    weight="uniform"
    weight_dir="uniform"
elif [ "${ROBUST}" == "natural" ]; then
    weight="natural"
    weight_dir="natural"
else
    weight="briggs ${ROBUST}"
    weight_dir="r${ROBUST}"
fi

if $SAVE_ATERMS; then
    saveAterms="-save-aterms"
else
    saveAterms=""
fi

if [ ! -e ${outdir} ]; then
    mkdir $outdir
fi
if [ ! -e ${outdir} ]; then
    echo "ERROR: output directory not available!"
    exit 1
fi

cd ${outdir}

phase1obs=""
phase2obs=""
allmetafits=""
mses=()
cfgs=()
while read line; do

    mwa1=$(echo "$line" | awk '{print $2}')
    mwa2=$(echo "$line" | awk '{print $1}')
    if [ ! -z ${mwa1} ]; then
        phase1obs="${phase1obs} ${mwa1}.ms"
        allmetafits="${allmetafits} ${mwa1}.metafits"
        allobs="${allobs} ${mwa1}.ms"
    fi
    phase2obs="${phase2obs} ${mwa2}.ms"
    allmetafits="${allmetafits} ${mwa2}.metafits"
    allobs="${allobs} ${mwa2}.ms"
    mwa1=
done < $matchlist

for obs in ${phase1obs[@]}; do
    obsid=$(echo ${obs} | sed "s/.ms//")
    if [ ! -e ${obsid}.ms ] || [ ! $USE_OLD ]; then
        cp -r ${indir}/${obsid}/${obsid}.m* .
        cp -r ${indir}/${obsid}/image_${WEIGHT2}/*${WEIGHT2}_comp.vot ${obsid}.vot
        cp -r ${indir}/${obsid}/image_${WEIGHT2}/*_cf.fits ${obsid}_cf.fits
        # TODO: add subbands for frequency dependence. Need to enable subbands in piipimage.sh
        cp -r ${indir}/${obsid}/image_${WEIGHT2}/${obsid}_deep-MFS-model.fits ${obsid}-model.fits
        cfg=$(ls -t ${indir}/${obsid}/image_${WEIGHT2}/*cfg* | head -1)
        cp ${cfg} ${obsid}.cfg
    fi
done

for obs in ${phase2obs[@]}; do
    obsid=$(echo ${obs} | sed "s/.ms//")
    if [ ! -e ${obsid}.ms ] || [ ! $USE_OLD ]; then
        cp -r ${indir}/${obsid}/${obsid}.m* .
        cp -r ${indir}/${obsid}/image_${WEIGHT1}/*${WEIGHT1}_comp.vot ${obsid}.vot
        cp -r ${indir}/${obsid}/image_${WEIGHT1}/*_cf.fits ${obsid}_cf.fits
        cp -r ${indir}/${obsid}/image_${WEIGHT1}/${obsid}_deep-MFS-model.fits ${obsid}-model.fits
        cfg=$(ls -t ${indir}/${obsid}/image_${WEIGHT1}/*cfg* | head -1)
        cp ${cfg} ${obsid}.cfg
    fi
    if [ -z ${reference} ]; then
        reference=${obsid}
    fi
done

radec=$($CONTAINER1 ${PIIPPATH}/get_central_coordinates.py ${allmetafits})
radec_hms=$($CONTAINER1 ${PIIPPATH}/dd_hms_dms.py ${radec} -d "hms")

dldmscreens=""
diagonalscreens=""

ref=$reference
freq=$($CONTAINER1 ${PIIPPATH}/get_header_key.py ${ref}.metafits FREQCENT)
freqMHz=$(echo "${freq}*1000000" | bc -l)
scale=$(echo "0.5/${chan}" | bc -l)

$CONTAINER1 get_beam_lobes ${reference}.metafits -p 0.05 -Mm > ${reference}_beamlobes.txt
if (( $( wc -l < ${reference}_beamlobes.txt ) )); then
    while read line; do
        beam_size=$(echo "$line" | awk '{print $4}')
        SMOOTHING=$(echo "$beam_size / 4.0" | bc -l)
    done < ${reference}_beamlobes.txt
else    
    # roughly size of the 216-MHz beam:
    SMOOTHING=22
fi

fov=$(echo "0.95*${scale}*${IMSIZE}/2" | bc -l)
${CONTAINER1} MIMAS +c $radec $fov -o ${outbase}_fov.mim

for m in ${allobs[@]}; do

    obsid=$(echo ${m} | sed "s/.ms//")

    $CONTAINER1 aoflagger -column ${CALIBRATION_COLUMN} ${obsid}.ms &> ${obsid}_aoflagging.log

    if $SUBTRACT; then

        crval1=$($CONTAINER1 ${PIIPPATH}/get_header_key.py ${obsid}-model.fits "CRVAL1")
        crval2=$($CONTAINER1 ${PIIPPATH}/get_header_key.py ${obsid}-model.fits "CRVAL2")
        originalimsize=$(${CONTAINER1} ${PIIPPATH}/get_header_key.py ${obsid}-model.fits "NAXIS1")
        originalscale=$(${CONTAINER1} ${PIIPPATH}/get_header_key.py ${obsid}-model.fits "CDELT2")
        originalhmsdms=$($CONTAINER1 ${PIIPPATH}/dd_hms_dms.py ${crval1} ${crval2} -d "hms")
        ${CONTAINER1} chgcentre $m $originalhmsdms

        ${CONTAINER1} MIMAS --negate --maskimage ${outbase}_fov.mim ${obsid}-model.fits ${obsid}-masked-model.fits 
        ${CONTAINER1} ${PIIPPATH}/fits_set_nan_zero.py ${obsid}-masked-model.fits

        ${CONTAINER1} wsclean \
            -scale ${originalscale} \
            -size ${originalimsize} ${originalimsize} \
            -predict \
            -name ${obsid}-masked \
            -use-wgridder \
            ${m}

        ${CONTAINER1} subtrmodel \
            -usemodelcol \
            -datacolumn $CALIBRATION_COLUMN \
            "model" $m

    fi

    ${CONTAINER1} chgcentre ${m} ${radec_hms}

done


if [ ! -e ${reference}_template-dirty.fits ]; then
# get template image for a-term screens:
    ${CONTAINER2} wsclean \
        -niter 0 \
        -name ${reference}_template \
        -size ${IMSIZE} ${IMSIZE} \
        -pol I \
        -scale ${scale} \
        -abs-mem ${ABSMEM} \
        -j ${NCPUS} \
        -log-time \
        -verbose \
        -use-idg \
        -idg-mode hybrid \
        -temp-dir /nvmetmp/ \
        ${reference}.ms
fi

template_image=${reference}_template-dirty
pad=0.2
smallkernel=$(echo "(2*${KERNEL}-2*${pad}*${KERNEL})" | bc -l)
smallkernel=${smallkernel%.*}
abspad=$(echo "2*${KERNEL}-${smallkernel}" | bc -l)

bmaj=$($CONTAINER1 ${PIIPPATH}/get_header_key.py ${reference}_cf.fits BMAJ)


for m in ${allobs[@]}; do

    obsid=$(echo ${m} | sed "s/.ms//")
    if [ ! -e ${obsid}_dldm.fits ] || [ ! $USE_OLD ]; then

        if [ "${obsid}" != "${reference}" ]; then

            


            # separation=$(echo "${bmaj}*3" | bc -l)

            # we need unfortunately a very strict match but also as many sources as possible...
            xmcat=${obsid}_${outbase}.fits
            ${CONTAINER1} match_catalogues ${obsid}.vot ${reference}.vot \
                --separation ${bmaj} \
                --exclusion_zone ${bmaj} \
                --threshold 0.0 \
                --nmax 5000 \
                --outname ${xmcat}  \
                --ra1 ra --dec1 dec \
                --ra2 ra --dec2 dec \
                --flux_key int_flux \
                --eflux_key err_int_flux \
                -f1 int_flux peak_flux \
                -f2 int_flux peak_flux \
                --localrms local_rms \
                --ratio 1.2
            
            # need to add nearest to normal fits_warp
            smooth=$(echo "0.2/${scale}" | bc -l)
            ${CONTAINER1} /astro/mwasci/duchesst/fits_warp2.py \
                --infits ${template_image}.fits \
                --suffix warp \
                --ra1 old_ra \
                --dec1 old_dec \
                --ra2 ra \
                --dec2 dec \
                --smooth ${smooth} \
                --plot \
                --testimage \
                --xm ${xmcat} \
                --interpolat $INTERP \

            mv ${template_image}_delx.fits ${obsid}_delx.fits
            mv ${template_image}_dely.fits ${obsid}_dely.fits
        
            if $DIAGONALS; then
                # TODO generalise smoothing, threshold, etc

                $CONTAINER1 flux_warp ${xmcat} ${template_image}.fits \
                    --threshold 0.5 \
                    --nmax 1000 \
                    --smooth ${SMOOTHING} \
                    --mode "linear_rbf" \
                    --outname ${obsid}_rbf.fits \
                    --ignore \
                    --ref_freq ${freqMHz} \
                    --ref_flux_key "int_flux" \
                    --alpha 0.0 \
                    --ra_key old_ra \
                    --dec_key old_dec \
                    -l "lcoal_rms" \
                    
                diagonals="-d ${obsid}_rbf_cf.fits"

            fi

        
            ${CONTAINER1} ${PIIPPATH}/make_aterm_files.py \
                -x ${obsid}_delx.fits \
                -y ${obsid}_dely.fits \
                --dfl ${smallkernel} \
                --dfd ${smallkernel} \
                --pad 0.2 \
                --abs-pad ${abspad} \
                -o ${obsid} \
                -m ${obsid}.metafits \
                ${diagonals}
        
        else
            
            # make dl,dm with all zeroes:
            ${CONTAINER1} ${PIIPPATH}/make_aterm_files.py \
                -x ${template_image}.fits \
                -y ${template_image}.fits \
                --dfl ${smallkernel} \
                --dfd ${smallkernel} \
                --pad 0.2 \
                --abs-pad ${abspad} \
                -o ${obsid} \
                -m ${obsid}.metafits \
                --zeroes \
                --ones \
                -d ${template_image}.fits

        fi

    fi

    dldmscreens="${dldmscreens} ${obsid}_dldm.fits"
    diagonalscreens="${diagonalscreens} ${obsid}_diag.fits"

done

if $DIAGONALS; then
    diag="--diag ${diagonalscreens}"
else
    diag=""
fi

# recreate aterm config in case wanting to try different params?
${CONTAINER1} ${PIIPPATH}/make_aterm_config.py \
    --beam \
    -o "${outbase}_aterm.cfg" \
    --dldm ${dldmscreens} \
    --window $WINDOW \
    $diag


stokesIbase=${outbase}_${weight_dir}_k${KERNEL}

${CONTAINER2} wsclean \
    -niter 1000000 \
    -nmiter 10 \
    -auto-threshold 1 \
    -auto-mask 3 \
    -name ${stokesIbase} \
    -size ${IMSIZE} ${IMSIZE} \
    -mgain 0.8 \
    -pol I \
    -weight ${weight} \
    -minuv-l 35 \
    -scale ${scale} \
    -abs-mem ${ABSMEM} \
    -j ${NCPUS} \
    -log-time \
    -verbose \
    -use-idg \
    -idg-mode hybrid \
    -channels-out 4 \
    -join-channels \
    -no-mf-weighting \
    -mwa-path ${BEAM_PATH} \
    -temp-dir /nvmetmp/ \
    -clean-border 2 \
    -aterm-config ${outbase}_aterm.cfg \
    -aterm-kernel-size ${KERNEL} \
    ${saveAterms} ${phase1obs} ${phase2obs}

image=${stokesIbase}-MFS-image-pb.fits
catalogue=${stokesIbase}-MFS-image-pb_comp.vot
residual=${stokesIbase}-MFS-residual-pb.fits
model=${stokesIbase}-MFS-model-pb.fits
maxsep=$(echo "${scale}*10" | bc -l)
smooth=$(echo "1.0/${scale}" | bc -l)
bmaj=$($CONTAINER1 ${PIIPPATH}/get_header_key.py $image "BMAJ")
zone=$(echo "${bmaj}*2" | bc -l)

$CONTAINER1 ${PIIPPATH}/piipaegean.sh ${image}

$CONTAINER1 match_catalogues ${catalogue} ${SKYMODEL} \
    --separation $maxsep \
    --exclusion_zone $zone \
    --threshold 0.0 \
    --nmax 1000 \
    --outname ${stokesIbase}_matched.fits \
    --localrms "local_rms" \
    --ratio 1.2

$CONTAINER1 fits_warp.py \
    --xm ${stokesIbase}_matched.fits \
    --infits ${image} \
    --suffix warp \
    --ra1 old_ra \
    --dec1 old_dec \
    --ra2 ra \
    --dec2 dec \
    --smooth ${smooth} \
    --plot

image=${stokesIbase}-MFS-image-pb_warp.fits

freq=$($CONTAINER1 ${PIIPPATH}/get_header_key.py ${ref}.metafits FREQCENT)
freqHz=$(echo "${freq}*1000000" | bc -l)
$CONTAINER1 ${PIIPPATH}/update_header_key.py ${image} "FREQ" ${freqHz}



threshold=$($CONTAINER1 ${PIIPPATH}/get_scaled_flux.py ${freq} -f 88.0 -S 0.5 -a -0.77)
$CONTAINER1 flux_warp ${stokesIbase}_matched.fits $image \
    --threshold ${threshold} \
    --nmax 1000 \
    --smooth ${SMOOTHING} \
    --test_subset \
    --test_fraction 0.2 \
    --residual \
    --mode "linear_rbf" \
    --outname ${stokesIbase}-MFS-image-pb_rbf.fits \
    --ignore \
    -A "alpha_c" \
    -a "beta_c" \
    -c "gamma_c" \
    --ra_key old_ra \
    --dec_key old_dec \
    -l "local_rms" \
    --plot \
    --nolatex \
    --additional_images ${model} ${residual} \
    --additional_outname ${stokesIbase}-MFS-model-pb_rbf.fits ${stokesIbase}-MFS-residual-pb_rbf.fits 




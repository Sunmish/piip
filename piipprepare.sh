#!/bin/bash

#SBATCH --account=mwasci
#SBATCH --partition=workq
#SBATCH --time=06:00:00
#SBATCH --nodes=1

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

TMP_DIR=
USE_JOB_FOR_TMP=true
BIND_PATH=
RA_DIR=
DEC_DIR=
OUTBASE=idg
MAX_MATCHED_SOURCES=400
SMOOTHING=
USE_OLD=false
IMSIZE=9000
SCALE=0.5
WEIGHT1="r0.5"
WEIGHT2="uniform"
KERNEL=256
DIAGONALS=false
WINDOW="rectangular"
SUBTRACT=false
INTERP="rbf"
CLEAN_REGION=
TIME_AVG=
CHAN_AVG=
APPLY_BEAM_MS=false
REWEIGHT_DATA=false
ASSUME_XXYY_EQUAL=false
APPLY_RMS_WEIGHT=false
DROP_ANTS=false
MS_OUTNAME=

CALIBRATION_COLUMN="CORRECTED_DATA"

start_time=$(date +%s)

set -x

opt=$1
if [[ "${opt}" == *".cfg"* ]]; then
    config=$(echo "${opt}" | sed "s/'//g")
    echo "INFO: using config file ${config}"
    source ${config}
fi

opts=$(getopt \
     -o :hd:O:r:s:f:a:A:k:f:S:F:t:u:E:z:BM: \
     --long help,processing_dir:,obsid_list:,robust:,scale:,field:,obsnum:,array_offset:,kernel:,window:,subtract,fov:,ra:,dec:,exclude:,region:,applybeam,msoutname: \
     --name "$(basename "$0")" \
     -- "$@"
)

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipimage ;;
        -d|--processing_dir) INDIR=$2 ; shift 2 ;;
        -O|--obsid_list) obsid_list=$2 ; shift 2 ;;
        -r|--robust) robust=$2 ; shift 2 ;;
        -s|--scale) scale=$2 ; shift 2 ;;
        -f|--field) field=$2 ; shift 2 ;;
        -a|--obsnum) obsnum=$2 ; shift 2 ;;
        -A|--array_offset) ARRAY_OFFSET=$2 ; shift 2 ;;
        -k|--kernel) KERNEL=$2 ; shift 2 ;;
        -f|--window) WINDOW=$2 ; shift 2 ;;
        -S|--subtract) SUBTRACT=true ; shift ;;
        -F|--fov) fov=$2 ; shift 2 ;;
        -t|--ra) RA_DIR=$2 ; shift 2 ;;
        -u|--dec) DEC_DIR=$2 ; shift 2 ;;
        -E|--exclude) extraMask=$2 ; shift 2 ;;
        -z|--region) CLEAN_REGION=$2 ; shift 2 ;;
        -B|--applybeam) APPLY_BEAM_MS=$2 ; shift 2 ;;
        _M|--msoutname) MS_OUTNAME=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR: " ; usage_piipimage ;;
    esac
done

check_required "--processing_dir" "-d" $INDIR
check_required "--robust" "-r" $robust

if [ -z ${TMP_DIR} ] || [ "${TMP_DIR}" == "./" ]; then
    TMP_DIR="./"
    USING_TMP=false
else
    TMP_DIR="${TMP_DIR}/"
    if $USE_JOB_FOR_TMP; then
        TMP_DIR="${TMP_DIR}/${SLURM_JOB_ID}/"
    fi 
    if [ ! -e ${TMP_DIR} ]; then
        mkdir -p ${TMP_DIR} || exit 1
    fi
    USING_TMP=true
fi

if ${USING_TMP}; then
    BIND_PATH="${TMP_DIR},${BIND_PATH}"
fi

if [ -z ${CONTAINER} ]; then
    container=""
else
    if [ ! -z ${BIND_PATH} ]; then
        BIND_PATH="-B ${BIND_PATH}"
    fi
    container="singularity exec ${BIND_PATH} ${CONTAINER}"
fi 

if [ -z ${CASA_LOCATION} ]; then
    CASA_LOCATION="${container} casa"
fi

if [ "${CALIBRATION_COLUMN}" = "DATA" ]; then
    casa_datacolumn="data"
elif [ "${CALIBRATION_COLUMN}" = "CORRECTED_DATA" ]; then
    casa_datacolumn="corrected"
else
    echo "Only DATA and CORRECTED_DATA are possible - exiting."
    exit 1
fi

# Set obsid from array:
if [ -z ${SLURM_ARRAY_TASK_ID} ]; then
    check_required "--obsnum" "-a" ${obsnum}
else
    obsnum=$((${SLURM_ARRAY_TASK_ID} + ${ARRAY_OFFSET}))
    
fi

if [ ${#obsnum} = 10 ]; then
    obsid=${obsnum}
else
    check_required "--obsid_list" "-o" $obsid_list
    obsid=$(sed "${obsnum}q;d" ${obsid_list} | awk '{print $1}')
fi

check_obsid ${obsid}

if [ "${robust}" == "uniform" ]; then
    weight="uniform"
    weight_dir="uniform"
elif [ "${robust}" == "natural" ]; then
    weight="natural"
    weight_dir="natural"
else
    weight="briggs ${robust}"
    weight_dir="r${robust}"
fi

cd ${INDIR}/${obsid}/image_${weight_dir} || exit 1

if [ ! -e ${obsid}.ms ]; then
    echo "No MeasurementSet present - imaging has to be done with PREPARE_FOR_IDG=true."
    exit 1
fi

if [ ! -e ${obsid}.metafits ]; then
    wget http://ws.mwatelescope.org/metadata/fits/?obs_id=${obsid} -O ${obsid}.metafits
fi

chan=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "CENTCHAN")
freq=$(echo "${chan}*1.28" | bc -l)

if [ ! -z ${field} ] && [ ! -z ${DBFILE} ] && $DOIMAGE; then
    d_format=$(date +%F:%H:%M:%S)
    if [ ${#obsnum} -lt 10 ]; then
        extraOptions="--selection=${obsnum}"
    else
        extraOptions=""
    fi
    $container ${PIIPPATH}/field_manager.py -m "update_snapshot_status" \
        -d ${DBFILE} \
        --field=${field} \
        --chan=${chan} \
        --status="start-prep-8r${robust}-[${d_format}]" \
        --overwrite \
        --obsid=${obsid} ${extraOptions}
fi

# Allow overwriting of DATA column:
if [ ${CALIBRATION_COLUMN} = "DATA" ]; then
    datacolumn="data"
else
    datacolumn="corrected"
fi
# ---------------------------------------------------------------------------- #

if ${USING_TMP}; then

    rsync -av ${obsid}.m* ${TMP_DIR}/
    cp ${obsid}_deep-MFS-image.fits ${TMP_DIR}/
    cp ${obsid}_deep-????-model.fits ${TMP_DIR}/
    cp ${obsid}_deep-MFS-model.fits ${TMP_DIR}/

fi

deep_image=${TMP_DIR}${obsid}_deep-MFS-image.fits
deep_model=${TMP_DIR}${obsid}_deep-MFS-model.fits

if $DROP_ANTS; then
$container ${PIIPPATH}/ms_drop_ants.py ${TMP_DIR}${obsid}.ms -a 128 129 130 131 132 133 134 135
fi

radec="${RA_DIR} ${DEC_DIR}"
radec_hms=$($container ${PIIPPATH}/dd_hms_dms.py ${radec} -d "hms")

if [ -e ${obsid}_beamlobes.txt ]; then
    $container get_beam_lobes ${obsid}.metafits -p 0.05 -Mm > ${obsid}_beamlobes.txt
fi

if (( $( wc -l < ${obsid}_beamlobes.txt ) )); then
    while read line; do
        beam_size=$(echo "$line" | awk '{print $4}')
        SMOOTHING=$(echo "$beam_size / 4.0" | bc -l)
    done < ${obsid}_beamlobes.txt
else    
    # roughly size of the 216-MHz beam:
    SMOOTHING=22
fi

if [ -z ${fov} ]; then
    fov=$(echo "0.95*${scale}*${IMSIZE}/2" | bc -l)
else
    fov=$(echo "${fov}/2" | bc -l)
fi

if [ -z ${CLEAN_REGION} ]; then
    if [ ! -z ${extraMask} ]; then
        circles=""
        while read line; do
            extraRA=$(echo "$line" | awk '{print $1}')
            extraDEC=$(echo "$line" | awk '{print $2}')
            extraFOV=$(echo "$line" | awk '{print $3}')
            if [ ! -z ${extraRA} ] && [ ! -z ${extraDEC} ] && [ ! -z ${extraFOV} ]; then
                circles="${circles} -c ${extraRA} ${extraDEC} ${extraFOV}"
            fi
        done < $extraMask
    fi
    ${container} MIMAS -depth 16 +c $radec $fov $circles -o ${OUTBASE}_fov.mim
fi

MS=${TMP_DIR}${obsid}.ms

crval1=$($container ${PIIPPATH}/get_header_key.py ${deep_model} "CRVAL1")
crval2=$($container ${PIIPPATH}/get_header_key.py ${deep_model} "CRVAL2")
originalimsize=$(${container} ${PIIPPATH}/get_header_key.py ${deep_model} "NAXIS1")
originalscale=$(${container} ${PIIPPATH}/get_header_key.py ${deep_model} "CDELT2")
originalhmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${crval1} ${crval2} -d "hms")
${container} chgcentre ${MS} $originalhmsdms

if [ -e ${TMP_DIR}${obsid}_deep-0000-model.fits ]; then
    for subband in 0000 0001 0002 0003; do
        if [ -z ${CLEAN_REGION} ]; then
            ${container} MIMAS --negate --maskimage ${OUTBASE}_fov.mim ${TMP_DIR}${obsid}_deep-${subband}-model.fits ${TMP_DIR}${obsid}-masked-${subband}-model.fits 
            ${container} ${PIIPPATH}/fits_set_nan_zero.py ${TMP_DIR}${obsid}-masked-${subband}-model.fits
        else
            # Arbitrary region mask instead - use a DS9 region file with e.g. large circle to CLEAN:
            ${container} ${PIIPPATH}/mask_region.py \
                ${CLEAN_MASK} ${TMP_DIR}${obsid}_deep-${subband}-model.fits \
                -o ${TMP_DIR}${obsid}-masked-${subband}-model.fits -bMn -m 1 -f 0
        fi
    done

    ${container} wsclean \
        -scale ${originalscale} \
        -size ${originalimsize} ${originalimsize} \
        -predict \
        -name ${TMP_DIR}${obsid}-masked \
        -use-wgridder \
        -channels-out 4 \
        ${MS}

    
else
    
    if [ -z ${CLEAN_REGION} ]; then
        ${container} MIMAS --negate --maskimage ${OUTBASE}_fov.mim ${deep_model} ${TMP_DIR}${obsid}-masked-model.fits 
        ${container} ${PIIPPATH}/fits_set_nan_zero.py ${TMP_DIR}${obsid}-masked-model.fits
    else
        ${container} ${PIIPPATH}/mask_region.py \
            ${CLEAN_MASK} ${deep_model} \
            -o ${TMP_DIR}${obsid}-masked-model.fits -bMn -m 1 -f 0
    fi
    
    ${container} wsclean \
        -scale ${originalscale} \
        -size ${originalimsize} ${originalimsize} \
        -predict \
        -name ${TMP_DIR}${obsid}-masked \
        -use-wgridder \
        ${MS}

fi


${container} subtrmodel \
-usemodelcol \
-datacolumn $CALIBRATION_COLUMN \
"model" ${MS}


${container} BANE --cores 1 ${deep_image}
${container} aegean --find --autoload \
    --seedclip 10 \
    --floodclip 4 \
    --nocov \
    --table=${deep_image/.fits/.vot},${deep_image/.fits/.reg} \
    ${deep_image}

if [ ! -z ${MS_OUTNAME} ]; then

    cp -r ${MS} ${MS/.ms/}${MS_OUTNAME}.ms
    MS=${MS/.ms/}${MS_OUTNAME}.ms

fi

${container} chgcentre ${MS} ${radec_hms}



if [ -e ${MS/.ms/.sub.ms} ]; then
    rm -r ${MS/.ms/.sub.ms}
fi


${CASA_LOCATION} --nogui \
    --agg \
    --nologger \
    -c \
    "split(vis='${MS}', datacolumn='${casa_datacolumn}', outputvis='${MS/.ms/}.sub.ms')"

MS=${MS/.ms/}.sub.ms

if [ ! -z ${TIME_AVG} ] && [ ! -z ${CHAN_AVG} ]; then
        ${CASA_LOCATION} --nogui --nologger --agg -c \
        "mstransform(vis='${MS}', outputvis='${MS/.ms/.avg.ms}', chanaverage=True, chanbin=${CHAN_AVG}, timeaverage=True, timebin='${TIME_AVG}', datacolumn='data')"
    
    rm -r ${MS}
    mv ${MS/.ms/.avg.ms} ${MS}
fi


if ${APPLY_BEAM_MS}; then

    cp -r ${MS} ${MS/.ms/.pb.ms}
    if ${ASSUME_XXYY_EQUAL}; then
        OPT_ASSUME_XXYY_EQUAL="--use_stokes_i"
    else
        OPT_ASSUME_XXYY_EQUAL=""
    fi

    if $APPLY_RMS_WEIGHT; then
        IMAGE=${obsid}_deep-MFS-residual-pb.fits
        std=$(${container} ${PIIPPATH}/get_std_of_image.py ${IMAGE} -p)
        OPT_RMS_WEIGHT="--rms ${std}"
    fi

    ${container} ${PIIPPATH}/ms_apply_beam.py ${MS/.ms/.pb.ms} ${obsid}.metafits --apply-weights ${OPT_ASSUME_XXYY_EQUAL} ${OPT_RMS_WEIGHT}

fi

# if [ ! -e ${obsid}.sub.ms ]; then
rsync -av ${MS} .
rsync -av ${MS/.ms/.pb.ms} .
rsync -av ${TMP_DIR}${obsid}*masked*.fits .
rsync -av ${TMP_DIR}*comp.vot .
# fi

if [ -e ${obsid}.sub.ms ]; then
    tar -cvf ${obsid}.sub.ms.tar.gz ${obsid}.sub.ms
fi
if [ -e ${obsid}.sub.pb.ms ]; then
    tar -cvf ${obsid}.sub.pb.ms.tar.gz ${obsid}.sub.pb.ms
fi


end_time=$(date +%s)
duration=$(echo "${end_time}-${start_time}" | bc -l)
echo "Total time taken: ${duration}"

if [ ! -z ${SLURM_ARRAY_TASK_ID} ]; then
    mv ${LOG_LOCATION}/${SLURM_JOB_NAME}.o${SLURM_ARRAY_JOB_ID}_${obsnum} \
        ${LOG_LOCATION}/${SLURM_JOB_NAME}.e${SLURM_ARRAY_JOB_ID}_${obsnum} .
fi

if [ ! -z ${field} ] && [ ! -z ${DBFILE} ]; then
    d_format=$(date +%F:%H:%M:%S)
    $container ${PIIPPATH}/field_manager.py -m "update_snapshot_status" \
        -d ${DBFILE} \
        --field=${field} \
        --chan=${chan} \
        --obsid=${obsid} \
        --status="prep-8r${robust}-[${d_format}]" \
        --overwrite ${extraOptions}
fi

exit 0

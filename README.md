The MWA Phase II Pipeline (`piip`). *Documentation in progress!* 

For calibration and imaging of MWA 2-minute snapshots in continuum mode, and finally performing stacking/mosaicking on the resultant images. The intended use is on the Pawsey Supercomputer using SLURM, but can be run locally one observation ID at a time. Despite the name, Phase I MWA data can be processed nicely as well.

## Usage
The general usage of the pipeline is through the `piip` script, which will chain the various jobs (calibration, self-calibration, and imaging) together, either submitting array jobs to the SLURM job scheduler or by processing a single observation ID if running locally on a normal system. 

The basic usage for `piip` is:
```
piip ${CUSTOM_CONFIG_FILE} -i ${INPUT_MS_DIRECTORY} -d ${PROCESSING_DIRECTORY} -O ${OBS_ID_LIST} -s ${SELECTION} -c ${CHANNEL_NUMBER}  -r ${BRIGGS_ROBUST_WEIGHTING} 
```

1. `${CUSTOM_CONFIG_FILE}` is a specific configuration file (edited version of `piipconfig.cfg`) for this specific set of processing.
2. `${INPUT_MS_DIRECTORY}` contains the MeasurementSets or the zipped MeasurementSets output from ASVO. This should be different from the processing directory to avoid errant overwriting and deleting. 
3. `${PROCESSING_DIRECTORY}` is the output processing directory.
4. `${OBS_ID_LIST}` is the text file list of observation IDs.
4. `${SELECTION}` is the selection within that list (e.g. "1-3" or "1,5-10", "1", etc.). 
5. `${CHANNEL_NUMBER}` is the central channel of the observation. 
6. `${BRIGGS_ROBUST_WEIGHTING}` is the robustness parameter in the 'Briggs' imaging weighting scheme, or 'uniform' or 'natural'. Multiple weightings can be supplied as a comma-separated list.

## Directory structure
When running `piip` a specific directory structure is used for the input and processing directories (note that these should be different otherwise they will be overwritten if not careful with options). 
For the *input* directory (i.e. the location of your MeasurementSets or .zip output from ASVO):
```
> INDIR/
>> OBSID/
>>> OBSID_ms.zip or (OBSID.ms and OBSID.metafits)
```
For the *processing directory*:
```
> PROCESSING_DIR/
>> OBSID/
>>> OBSID.ms + calibration output
>>> IMAGE_WEIGHTING1/
>>>> images for image weighting 1
...
>>> IMAGE_WEIGHTINGN/
>>>> images for image weighting N
```

## Requirements
This pipeline requires many software packages (as it is a pipeline...). The full list of required packages is listed here: 

1. [`casacore`](https://github.com/casacore/casacore)
2. [`python-casacore`](https://github.com/casacore/python-casacore)
3. `mwa-reduce`: Not publicly available.^
4. [`WSClean`](https://sourceforge.net/p/wsclean/wiki/Home/)
5. [`AOFlagger`](https://sourceforge.net/p/aoflagger/wiki/Home/) 
6. [`mwa_pb`](https://github.com/MWATelescope/mwa_pb)
7. [`Aegean`](https://github.com/PaulHancock/Aegean)
8. [`skymodel`](https://github.com/Sunmish/skymodel)
9. [`flux_warp`](https://gitlab.com/Sunmish/flux_warp)
10. [`fits_warp`](https://github.com/nhurleywalker/fits_warp)
11. [`(CASA)`](https://casa.nrao.edu/): only required for flagging.
12. [`(miriad)`](https://www.atnf.csiro.au/computing/software/miriad/): only required for mosaicking/stacking - not actually part of the pipeline.

All of these softwares can be packaged into a singularity container, which can be supplied in the configuration file as `CONTAINER=$CONTAINER_PATH` which will be run as, e.g. `singularity run $CONTAINER wsclean ...`. All software needs to be in the same container in this case. A container for this purpose can be provided if processing with the Pawsey supercomputer (e.g. on the MWA cluster Garrawarla).

^Note that calibration is performed with the tool `calibrate`, which using the full-Jones `Mitchcal` algorithrm developed specifically for MWA calibration. The software and algorithm are described by [Offringa et al. (2016)](https://ui.adsabs.harvard.edu/abs/2016MNRAS.458.1057O/abstract). 

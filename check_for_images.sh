#!/bin/bash 

source piipconfig.cfg
source piipfunctions.sh

subband=false

usage() {
    echo "usage: $(basename "$0") -f FIELD -c CHANNEL -S SUFFIX -l LOCATION -r ROBUST -s SUBBAND -o OBSLIST"
    1>&2
    exit 1
}

opts=$(getopt \
    -o :hf:c:S:l:r:so: \
    --long help,field:,chan:,suffix:,location:,robust:,subband,obslist: \
    --name "$(basename "$0")" \
    -- "$@"
)

[ $? -eq 0 ] || {
    echo "Unknown option provided - please check inputs!"
    usage
    exit 1
}

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage ;;
        -f|--field) field=$2 ; shift 2 ;;
        -c|--chan) chan=$2 ; shift 2 ;;
        -S|--suffix) suffix=$2 ; shift 2 ;;
        -l|--location) location=$2 ; shift 2 ;;
        -r|--robust) robust=$2 ; shift 2 ;;
        -s|--subband) subband=true ; shift ;;
        -o|--obslist) obslist=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "Unknown option/parameter: ${1}" ; usage ;;
    esac
done

check_required "--field" "-f" $field
check_required "--chan" "-c" $chan
check_required "--location" "-l" $location

if [ -z ${robust} ]; then
    robust=("0.0" "1.0")
else
    robust=("${robust}")
fi

if ! $subband; then
    subbands=("")
else
    subbands=("-0" "-1" "-2" "-3")
fi

if [ -z ${obslist} ]; then
    obslist=${OBSLISTS}/FIELD${field}_${chan}.txt
fi

while read line; do

    obsid=$(echo "$line" | awk '{print $1}')

    for r in ${robust[@]}; do
        for c in ${subbands[@]}; do
            get_frequency_from_channel ${chan}${c}
            echo $FREQ
            name=${location}/${obsid}/image_r${r}/${obsid}_${FREQ}MHz_r${r}${suffix}.fits
            if [ -e ${name} ]; then
                echo "${name} found."
            else
                echo "WARNING ${name} not found"
            fi
        done
    done 


done < ${obslist}


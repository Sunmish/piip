#! /usr/bin/env python

from argparse import ArgumentParser
from casacore.tables import table

import numpy as np

import warnings
warnings.filterwarnings("ignore")

from skymodel.get_beam import beam_value
from skymodel.parsers import parse_metafits

from tqdm import trange

_description = """
Apply single-valued primary beam correction to visibilities.
Assumes a very small FoV (e.g. <1 deg) and will apply to each channel.
For use on frequency-averaged, direction-extracted data.
"""


def get_args():
    ps = ArgumentParser(description=_description)
    ps.add_argument("measurementset", type=str, help="MeasurementSet name.")
    ps.add_argument("metafits", type=str, help="Metafits file name.")
    ps.add_argument("-w", "--apply-weights", "--apply_weights", dest="apply_weights", action="store_true")
    ps.add_argument("-r", "--rms", type=float, default=None)
    ps.add_argument("-i", "--use_stokes_i", action="store_true")
    return ps.parse_args()


def apply_beam(measurementset, metafits, apply_weights=False, rms=None, assume_yy_is_xx=False):
    """Apply XX,YY beam values to MS phase centre. Assumes very very small FoV."""

    t, delays, _, _ = parse_metafits(metafits)

    with table(measurementset, readonly=False, ack=False) as ms:

        ra0, dec0 = ms.FIELD.getcell("PHASE_DIR", 0)[0]
        ra0 = np.degrees(ra0)
        dec0 = np.degrees(dec0)
        print(ra0, dec0)

        data = np.cdouble(ms.getcol("DATA"))

        freqs = ms.SPECTRAL_WINDOW.getcell("CHAN_FREQ", 0)
        midfreq = (min(freqs) + max(freqs)) / 2.

        xx_freq = np.full((len(freqs),), np.nan)
        yy_freq = np.full((len(freqs),), np.nan)


        pbar = trange(len(freqs))
        for i in pbar:
            xx, yy = beam_value(ra0, dec0, t, delays, freqs[i])
            pbar.set_description("{} {}".format(xx, yy))
            if assume_yy_is_xx:
                ii = 0.5*(xx+yy)
                xx = ii
                yy = ii
            xx_freq[i] = xx
            yy_freq[i] = yy


        xx_mid, yy_mid = beam_value(ra0, dec0, t, delays, midfreq)
        if rms is None:
            weight_pb_mid = (xx_mid + yy_mid)**4
            sigma_pb_mid = 1/(xx_mid + yy_mid)**2
        else:
            weight_pb_mid = (xx_mid + yy_mid)**2 / (rms**2)
            sigma_pb_mid = rms**2

        if rms is None:
            weight_pb = (xx_freq + yy_freq)**4
            sigma_pb = 1/(xx_freq + yy_freq)**2
        
        else:
            weight_pb = (xx_freq + yy_freq)**2 / (rms**2)
            sigma_pb = rms**2
        

        data[:, :, 0] /= xx_freq
        data[:, :, 3] /= yy_freq

        print(weight_pb)
        print(sigma_pb)
        if apply_weights:
            weight_spectrum = ms.getcol("WEIGHT_SPECTRUM")
            weight = ms.getcol("WEIGHT")
            sigma_spectrum = ms.getcol("SIGMA_SPECTRUM")
            sigma = ms.getcol("SIGMA")
            print(sigma_spectrum)
            for i in range(4):
                weight_spectrum[:, :, i] *= weight_pb
                sigma_spectrum[:, :, i] *= sigma_pb
            weight *= weight_pb_mid
            sigma *= sigma_pb_mid
            print(sigma_spectrum)
            ms.putcol("WEIGHT_SPECTRUM", weight_spectrum)
            ms.putcol("WEIGHT", weight)
            ms.putcol("SIGMA_SPECTRUM", sigma_spectrum)
            ms.putcol("SIGMA", sigma)

        ms.putcol("DATA", data)

        ms.flush()
    

def cli(args):
    apply_beam(
        measurementset=args.measurementset, 
        metafits=args.metafits, 
        rms=args.rms,
        apply_weights=args.apply_weights,
        assume_yy_is_xx=args.use_stokes_i)


if __name__ == "__main__":
    cli(get_args())
#! /usr/bin/env python

from argparse import ArgumentParser

import sys


import numpy as np
# from numpy.linalg import inv  # slower than below function
from casacore.tables import table, maketabdesc, makecoldesc

from numba import jit

@jit(nopython=True)
def apply(data : np.ndarray, 
    ant1 : np.ndarray, 
    ant2 : np.ndarray, 
    time : np.ndarray, g_cparams_xx, g_cparams_yy, g_times, 
    unapply=False,
    verbose=False) -> np.ndarray:
    """Apply/unapply closest-in-time gains to `data`.

    
    """

    def inv(A):
        detA = (A[0, 0]*A[1, 1]) - (A[0, 1]*A[1, 0])
        invA = A.copy()
        invA[0, 0] = A[1, 1] / detA
        invA[0, 1] = -A[0, 1] / detA
        invA[1, 0] = -A[1, 0] / detA
        invA[1, 1] = A[0, 0] / detA
        return invA
    

    for n in range(len(data)):

        idx = np.argmin(np.abs(g_times-time[n]))

        for i in range(data[n].shape[0]):
            
            gains_a = np.zeros((2,2), dtype=np.cdouble)
            gains_a[0, 0] = g_cparams_xx[ant1[n], idx]
            gains_a[1, 1] = g_cparams_yy[ant1[n], idx]
            
            gains_b = np.zeros((2,2), dtype=np.cdouble)
            gains_b[0, 0] = g_cparams_xx[ant2[n], idx]
            gains_b[1, 1] = g_cparams_yy[ant2[n], idx]

            # just use reshape?
            data_i = np.zeros((2,2), dtype=np.cdouble)
            data_i[0, 0] = data[n][i][0]
            data_i[0, 1] = data[n][i][1]
            data_i[1, 0] = data[n][i][2]
            data_i[1, 1] = data[n][i][3]

            if unapply:
                data_i_c = np.dot(np.dot(gains_a, data_i), gains_b.conj().T)
        
            else:
                data_i_c = np.dot(np.dot(inv(gains_a), data_i), inv(gains_b.conj().T))
                
            data[n][i][0] = data_i_c[0, 0]
            data[n][i][3] = data_i_c[1, 1]


    return data

def ms_apply_gains(msname: str, gains: str, 
    column : str = "DATA", 
    output_column : str ="CORRECTED_DATA", 
    unapply : bool = False,
    verbose : bool = False) -> None:
    """

    Args:
    

    """

    ms = table(msname, readonly=False, ack=False)

    if column not in ms.colnames():
        ms.close()
        raise RuntimeError("No column in {} of name {}".format(msname, column))

    if output_column != column:
        if output_column in ms.colnames() and not unapply:
            ms.removecols(output_column)
            ms.flush()
        cdesc = ms.getcoldesc(column)
        dminfo = ms.getdminfo(column)
        dminfo["NAME"] = output_column
        cdesc["comment"] = "The {} column".format(output_column)
        ms.addcols(maketabdesc(makecoldesc(output_column, cdesc)), dminfo)
        data_ = ms.getcol(column)
        ms.putcol(output_column, data_)
    
    column = output_column

    g = table(gains, readonly=True, ack=False)
    g_times1 = g.getcol("TIME")
    g_ants1 = g.getcol("ANTENNA1")
    g_cparams1 = g.getcol("CPARAM")
    g_times = g_times1[np.where(g_ants1 == 0)[0]]
    g_ants = set(g_ants1)
    g_cparams_xx = np.ones((len(g_ants), len(g_times)), dtype=np.cdouble)
    g_cparams_yy = np.ones((len(g_ants), len(g_times)), dtype=np.cdouble)
    for antenna in g_ants:
        g_cparams_xx[antenna, :] = g_cparams1[np.where(g_ants1 == antenna)[0]][:, 0, 0]
        g_cparams_yy[antenna, :] = g_cparams1[np.where(g_ants1 == antenna)[0]][:, 0, 1]

    
    # this may be memory intensive for large datasets...
    data = ms.getcol(column)
    ant1 = ms.getcol("ANTENNA1")
    ant2 = ms.getcol("ANTENNA2")
    time = ms.getcol("TIME")

    data = apply(data, ant1, ant2, time, g_cparams_xx, g_cparams_yy, g_times, 
        unapply=unapply,
        verbose=verbose)

    ms.putcol(column, data)

    ms.flush()
    ms.close()
    g.close()


def main():
    ps = ArgumentParser(description="A small program to *simply* apply *simple* CASA-generated gain tables.",
        epilog="Seriously, this is very *simple* - no checking is made and gains are just plonked right in there for the nearest time interval.")
    ps.add_argument("msname")
    ps.add_argument("gains", help="Name of gains table. Must be simple in that it will be mapped to all channels.")
    ps.add_argument("-c", "--column", default="DATA")
    ps.add_argument("-C", "--output_column", "--output-column", dest="output", default="CORRECTED_DATA")
    ps.add_argument("-u", "--unapply", dest="unapply", action="store_true")
    ps.add_argument("-v", "--verbose", action="store_true")
    args = ps.parse_args()

    ms_apply_gains(args.msname, args.gains, args.column, args.output, 
        unapply=args.unapply,
        verbose=args.verbose)

if __name__ == "__main__":
    main()






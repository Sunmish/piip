#! /usr/bin/env python

from argparse import ArgumentParser
import numpy as np
from astropy.io import fits

def add(image1, image2, outname):
    """Add image2 to image1, saving as outname."""

    fits1 = fits.open(image1)
    fits2 = fits.open(image2)

    data3 = np.squeeze(fits1[0].data) + np.squeeze(fits2[0].data)
    if "CRVAL3" in fits1[0].header.keys():
        fits1[0].header["FREQ"] = fits1[0].header["CRVAL3"]

    fits.writeto(outname, data3, fits1[0].header, overwrite=True)


def main():
    ps = ArgumentParser()
    ps.add_argument("image1")
    ps.add_argument("image2")
    ps.add_argument("-o", "--outname", default=None)
    args = ps.parse_args()

    if args.outname is None:
        args.outname = args.image1.replace(".fits", "") + "_" + args.image2

    add(args.image1, args.image2, args.outname)

if __name__ == "__main__":
    main()



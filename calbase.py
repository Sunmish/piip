#! /usr/bin/env python

import numpy as np
import sqlite3
import os

import argparse

try:
    from astropy.time import Time
except ImportError:
    pass

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s", \
                    level=logging.DEBUG)



def read_obslist(*args):
    """Read in obs list(s) in the format: obsid,project,name,chan. """

    obsid, project, name, chan = [], [], [], []
    for arg in args:
        with open(arg, "r") as f:
            lines = f.readlines()
            for line in lines:
                l = line.split(",")
                obsid.append(int(l[0]))
                project.append(l[1])
                name.append(l[2])
                chan.append(l[3])
                # print(l[0], l[1], l[2], l[3])

    arr = np.array([np.asarray(obsid), np.asarray(project), 
                    np.asarray(name), np.asarray(chan)]).T

    return arr[arr[:, 0].argsort()]




class DataBase():
    """Class for database handling."""

    def __init__(self, name="calibrators.db"): 

        self.filename = name
        self.con = sqlite3.connect(name)
        self.cur = self.con.cursor()

        # Status hierarchy. Lower-ranked statuses are overwritten.
        self.STATUS_RANKS = {"incomplete": 0,
                             "queued": 2,
                             "cottering": 2,
                             "preprocessed": 2,
                             "staged": 4,
                             "skycal": 10,
                             "preimage": 10,
                             "selfcal": 10,
                             "imaging": 10,
                             "correct": 10,
                             "stored": 10,
                             "fail-4": -1,
                             "fail-5": -1,
                             "fail-6r0": -1,
                             "fail-7r0": -1,
                             "fail-7r1": -1,
                             "fail-6r1": -1
                             }

    def initialise(self):
        """Initialise the main table."""

        self.cur.execute("DROP TABLE IF EXISTS calibrators")
        self.cur.execute("CREATE TABLE calibrators (obsid INT, project TEXT, name TEXT, chan INT)")

    def add_calibrators(self, obsids, projects, names, chans):
        """Add calibrators to the initialised table."""

        for i in range(len(obsids)):
            obsid = obsids[i]
            proj = projects[i]
            name = names[i]
            chan = chans[i]

            self.cur.execute("INSERT into calibrators VALUES (?, ?, ?, ?)", 
                             (int(obsid), proj, name, int(chan)))



    def get_calibrators(self, calibrator=None, near_obs=None, p=False):
        """Print the calibrator list."""

        def line_formatter(obsid, proj, name, chan):
            line = "{:10} | {:5} | {:10} | {:3}".format(
                obsid, proj, name, chan)
            return line

        if calibrator is None and near_obs is None:
            self.cur.execute("SELECT * FROM calibrators")
        elif calibrator is not None:
            self.cur.execute("SELECT * FROM calibrators WHERE name IS ?", (calibrator,))
        elif near_obs is not None:
            self.cur.execute("SELECT * FROM calibrators WHERE obsid BETWEEN {} and {}".format(
                             near_obs-28800, near_obs+28800))  # within 8 hours

        dat = self.cur.fetchall()

        if p:
            for i in range(len(dat)):
                if i == 0:
                    tgps = Time(float(dat[i][0]), format="gps")
                    tiso = Time(tgps, format="iso")
                    print("DATE: {0} ----- #".format(tiso))
                elif abs(float(dat[i-1][0]) - float(dat[i][0])) > 28800:  # 8 hours
                    tgps = Time(float(dat[i][0]), format="gps")
                    tiso = Time(tgps, format="iso")
                    print("DATE: {0} ----- #".format(tiso))     

                if near_obs is not None:
                    print(line_formatter(dat[i][0], dat[i][1], dat[i][2], dat[i][3])," | {:.2f} hours".format(
                        abs(near_obs-dat[i][0])/3600.))
                else:
                    print(line_formatter(dat[i][0], dat[i][1], dat[i][2], dat[i][3]))


    def close(self):
        self.con.commit()
        self.con.close()



def main():
    """
    """

    ps = argparse.ArgumentParser()
    ps.add_argument("-d", "--database", default="calibrators.db")
    ps.add_argument("-c", "--calibrator", default=None)
    ps.add_argument("-o", "--obs", default=None, type=int)
    ps.add_argument("-i", "--initialise", action="store_true")
    ps.add_argument("-O", "--obslist", default=None)

    args = ps.parse_args()

    try:
        if not os.path.exists(args.database) and args.initialise:
            db = DataBase(args.database)
            db.initialise()
        else:
            db = DataBase(args.database)

        if args.initialise and args.obslist is not None:
            
            arr = read_obslist(args.obslist)
            db.add_calibrators(arr[:, 0], arr[:, 1], arr[:, 2], arr[:, 3])

        if args.calibrator is not None:
            db.get_calibrators(calibrator=args.calibrator)
        elif args.obs is not None:
            db.get_calibrators(near_obs=args.obs)
        else:
            db.get_calibrators()


    except:
        raise
    finally:
        db.close()

if __name__ == "__main__":
    main()
#!/bin/bash
#SBATCH --account=mwasci
#SBATCH --partition=gpuq
#SBATCH --time=12:00:00
#SBATCH --nodes=1
#SBATCH --cpus-per-gpu=18
#SBATCH --gres=gpu:1 
#SBATCH --job-name=IDGpair
#SBATCH --mem=160GB
#SBATCH --tmp=300GB
#SBATCH --output=/astro/mwasci/duchesst/G0045/processing/logs/%x.o%A_%a
#SBATCH --error=/astro/mwasci/duchesst/G0045/processing/logs/%x.e%A_%a

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

WEIGHT1="r0.5"
WEIGHT2="uniform"
ROBUST="uniform"
KERNEL=55  # arbitrary selection that seems to work?
DIAGONALS=false 
USE_OLD=false
WINDOW="rectangular"
SAVE_ATERMS=false

# TODO merge these two containers
CONTAINER1="singularity run --bind /nvmetmp/ /astro/mwasci/duchesst/data_processing3.img"
CONTAINER2="singularity run --nv --bind /nvmetmp/ /astro/mwasci/duchesst/gpu_stuff.img"

start_time=$(date)

opts=$(getopt \
    -o :hR:O:d:i:k:r:w:W:DUSf: \
    --long help,reference:,other:,processing_dir:,indir:,kernel:,robust:,robust1:,robust2:,diagonals,use_old,save_aterms,window: \
    --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piipimage
    exit 1
}


for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

set -x

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipIDGpair ;;
        -R|--reference) ref=$2 ; shift 2 ;;
        -O|--other) other=$2 ; shift 2 ;;
        -d|--processing_dir) outdir=$2 ; shift 2 ;;
        -i|--indir) indir=$2 ; shift 2 ;;
        -k|--kernel) KERNEL=$2 ; shift 2 ;;
        -r|--robust) ROBUST=$2 ; shift 2 ;;
        -w|--weight1) WEIGHT1=$2 ; shift 2 ;;
        -W|--weight2) WEIGHT2=$2 ; shift 2 ;;
        -D|--diagonals) DIAGONALS=true ; shift ;;
        -U|--use_old) USE_OLD=true ; shift ;;
        -S|--save_aterms) SAVE_ATERMS=true ; shift ;;
        -f|--window) WINDOW=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR: " ; usage_piipimage ;;
    esac
done

check_required "--reference" "-R" $ref
check_required "--other" "-O" $other
check_required "--processing_dir" "-d" $outdir
check_required "--indir" "-i" $indir

if [ "${ROBUST}" == "uniform" ]; then
    weight="uniform"
    weight_dir="uniform"
elif [ "${ROBUST}" == "natural" ]; then
    weight="natural"
    weight_dir="natural"
else
    weight="briggs ${ROBUST}"
    weight_dir="r${ROBUST}"
fi

if $SAVE_ATERMS; then
    save_aterms="-save-aterms"
else
    save_aterms=""
fi

if [ ! -e ${outdir} ]; then
    mkdir ${outdir}
fi

cd ${outdir}
if [ ! -e ${ref}.ms ] || [ ! $USE_OLD ]; then
    cp -r ${indir}/${ref}/${ref}.m* .
    cp -r ${indir}/${ref}/image_${WEIGHT1}/*${WEIGHT1}_comp.vot ${ref}.vot
    cp -r ${indir}/${ref}/image_${WEIGHT1}/*_cf.fits ${ref}_cf.fits
fi
if [ ! -e ${other}.ms ] || [ ! $USE_OLD ]; then
    cp -r ${indir}/${other}/${other}.m* .
    cp -r ${indir}/${other}/image_${WEIGHT2}/*${WEIGHT2}_comp.vot ${other}.vot
    cp -r ${indir}/${other}/image_${WEIGHT2}/*_cf.fits ${other}_cf.fits
fi
ra=$($CONTAINER1 ${PIIPPATH}/get_header_key.py ${ref}.metafits RA)
dec=$($CONTAINER1 ${PIIPPATH}/get_header_key.py ${ref}.metafits DEC)
chan=$($CONTAINER1 ${PIIPPATH}/get_header_key.py ${ref}.metafits CENTCHAN)
radec=$($CONTAINER1 ${PIIPPATH}/dd_hms_dms.py ${ra} ${dec} -d "hms")

dldmscreens=""
diagonalscreens=""

reference=$ref
mses="${other}.ms ${ref}.ms"

for m in ${mses[@]}; do

    obsid=$(echo ${m} | sed "s/.ms//")

    if [ ! -e ${obsid}_dldm.fits ] || ! $USE_OLD; then

        # phase rotate MSes to common centre:
        ${CONTAINER1} chgcentre ${m} ${radec}
        
        xmcat=${obsid}_${reference}.fits
        ${CONTAINER1} match_catalogues ${obsid}.vot ${reference}.vot \
            --separation .04733727810650887570 \
            --threshold 0.0 \
            --nmax 5000 \
            --outname ${xmcat}  \
            --ra1 ra --dec1 dec \
            --ra2 ra --dec2 dec \
            --flux_key int_flux \
            --eflux_key err_int_flux \
            -f1 int_flux peak_flux \
            -f2 int_flux peak_flux \
            --localrms local_rms \
            --ratio 1.2 
        
        ${CONTAINER1} /astro/mwasci/duchesst/fits_warp2.py \
            --infits ${obsid}_cf.fits \
            --suffix warp2 \
            --ra1 old_ra \
            --dec1 old_dec \
            --ra2 ra \
            --dec2 dec \
            --smooth 0 \
            --plot \
            --testimage \
            --interpolat=nearest \
            --xm ${xmcat}

        if [ "${obsid}" != "${reference}" ]; then

            ${CONTAINER1} ${PIIPPATH}/make_aterm_files.py \
                -x ${obsid}_cf_delx.fits \
                -y ${obsid}_cf_dely.fits \
                -d ${obsid}_cf.fits \
                --dfl 512 \
                --dfd 256 \
                --pad 0.2 \
                -o ${obsid} \
                -m ${obsid}.metafits
        
        else

            # make dl,dm with all zeroes:
            ${CONTAINER1} ${PIIPPATH}/make_aterm_files.py \
                -x ${obsid}_cf_delx.fits \
                -y ${obsid}_cf_dely.fits \
                -d ${obsid}_cf.fits \
                --dfl 512 \
                --dfd 256 \
                --pad 0.2 \
                -o ${obsid} \
                -m ${obsid}.metafits \
                --zeroes \
                --ones

        fi
    fi

    dldmscreens="${dldmscreens} ${obsid}_dldm.fits"
    diagonalscreens="${diagonalscreens} ${obsid}_diag.fits"

done


if $DIAGONALS; then
    diag="--diag ${diagonalscreens}"
else
    diag=""
fi

${CONTAINER1} ${PIIPPATH}/make_aterm_config.py \
    --beam \
    -o "${ref}_aterm.cfg" \
    --dldm ${dldmscreens} \
    --window $WINDOW \
    $diag
# ${CONTAINER1} ${PIIPPATH}/make_aterm_config.py --beam -o "aterm.cfg"

scale=$(echo "0.5/${chan}" | bc -l)
stokesIbase=${reference}_${other}_${weight_dir}_k${KERNEL}

${CONTAINER2} wsclean \
    -niter 1000000 \
    -nmiter 10 \
    -auto-threshold 1 \
    -auto-mask 3 \
    -name ${stokesIbase} \
    -size 9000 9000 \
    -mgain 0.8 \
    -pol I \
    -weight ${weight} \
    -minuv-l 35 \
    -scale ${scale} \
    -abs-mem 160 \
    -j 18 \
    -log-time \
    -verbose \
    -use-idg \
    -idg-mode hybrid \
    -channels-out 4 \
    -join-channels \
    -no-mf-weighting \
    -mwa-path ${BEAM_PATH} \
    -temp-dir /nvmetmp/ \
    -aterm-config ${ref}_aterm.cfg \
    -aterm-kernel-size ${KERNEL} \
    ${save_aterms} ${mses}

image=${stokesIbase}-MFS-image-pb.fits
catalogue=${stokesIbase}-MFS-image-pb_comp.vot
residual=${stokesIbase}-MFS-residual-pb.fits
model=${stokesIbase}-MFS-model-pb.fits
maxsep=$(echo "${scale}*10" | bc -l)
smooth=$(echo "1.0/${scale}" | bc -l)
bmaj=$($CONTAINER1 ${PIIPPATH}/get_header_key.py $image "BMAJ")
zone=$(echo "${bmaj}*2" | bc -l)

$CONTAINER1 ${PIIPPATH}/piipaegean.sh ${image}

$CONTAINER1 match_catalogues ${catalogue} ${SKYMODEL} \
    --separation $maxsep \
    --exclusion_zone $zone \
    --threshold 0.0 \
    --nmax 1000 \
    --outname ${stokesIbase}_matched.fits \
    --localrms "local_rms" \
    --ratio 1.2

$CONTAINER1 fits_warp.py \
    --xm ${stokesIbase}_matched.fits \
    --infits ${image} \
    --suffix warp \
    --ra1 old_ra \
    --dec1 old_dec \
    --ra2 ra \
    --dec2 dec \
    --smooth ${smooth} \
    --plot

image=${stokesIbase}-MFS-image-pb_warp.fits

freq=$($CONTAINER1 ${PIIPPATH}/get_header_key.py ${ref}.metafits FREQCENT)
freqHz=$(echo "${freq}*1000000" | bc -l)
$CONTAINER1 ${PIIPPATH}/update_header_key.py ${image} "FREQ" ${freqHz}

$CONTAINER1 get_beam_lobes ${ref}.metafits -p 0.05 -Mm > ${ref}_beamlobes.txt
if (( $( wc -l < ${obsid}_beamlobes.txt ) )); then
    while read line; do
        beam_size=$(echo "$line" | awk '{print $4}')
        SMOOTHING=$(echo "$beam_size / 4.0" | bc -l)
    done < ${ref}_beamlobes.txt
else    
    SMOOTHING=5.0
fi

threshold=$($CONTAINER1 ${PIIPPATH}/get_scaled_flux.py ${freq} -f 88.0 -S 0.5 -a -0.77)
$CONTAINER1 flux_warp ${stokesIbase}_matched.fits $image \
    --threshold ${threshold} \
    --nmax 1000 \
    --smooth ${SMOOTHING} \
    --test_subset \
    --test_fraction 0.2 \
    --residual \
    --mode "linear_rbf" \
    --outname ${stokesIbase}-MFS-image-pb_rbf.fits \
    --ignore \
    -A "alpha_c" \
    -a "beta_c" \
    -c "gamma_c" \
    --ra_key old_ra \
    --dec_key old_dec \
    -l "local_rms" \
    --plot \
    --nolatex \
    --additional_images ${model} ${residual} \
    --additional_outname ${stokesIbase}-MFS-model-pb_rbf.fits ${stokesIbase}-MFS-residual-pb_rbf.fits 




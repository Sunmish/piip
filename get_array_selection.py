#! /usr/bin/env python

from argparse import ArgumentParser


def get_args():
    ps = ArgumentParser()
    ps.add_argument("selection", type=str)
    ps.add_argument("--array_max", default=1000, type=int)
    return ps.parse_args()


def get_selection(selection, array_max=1000):

    corrected_selections = []
    selection_bits = selection.split(",")

    # first find maximum array job:
    max_array = 0
    min_array = 1e30
    for bit in selection_bits:
        if "-" in bit:
            hyphen_bits = bit.split("-")
            for hbit in hyphen_bits:
                if int(hbit) > max_array:
                    max_array = int(hbit)
                if int(hbit) < min_array:
                    min_array = int(hbit)
        else:
            if int(bit) > max_array:
                max_array = int(bit)
            if int(bit) < min_array:
                min_array = int(bit)


    if max_array-min_array > array_max:
        raise RuntimeError("Array size limited to array max: {}".format(array_max))

    if max_array > array_max:

        offset = min([min_array, array_max])

        for bit in selection_bits:
            if "-" in bit:
                hyphen_bits = bit.split("-")
                hselect = "{}-{}".format(int(hyphen_bits[0])-offset, 
                                         int(hyphen_bits[1])-offset)
                corrected_selections.append(hselect)    
            else:
                corrected_selections.append("{}".format(int(bit)-offset))

        corrected_selection = ",".join(corrected_selections)

    else:
        corrected_selection = selection
        offset = 0


    print("{} {}".format(offset, corrected_selection))
    

def cli(args):
    get_selection(args.selection, args.array_max)


if __name__ == "__main__":
    cli(get_args())

#! /usr/bin/env python

from astropy.io import fits
import numpy as np

from argparse import ArgumentParser


def get_args():

    ps = ArgumentParser()
    ps.add_argument("image")
    ps.add_argument("beam")
    ps.add_argument(
        "-c", "--clip",
        default=0.1,
        type=float
    )

    return ps.parse_args()


def clip(image, beam_image, clip_level=0.2):

    image = fits.open(image, mode="update")
    beam_data = np.squeeze(fits.getdata(beam_image))

    idx =  np.where(beam_data < clip_level)
    if image[0].header["NAXIS"] > 2:
        data = np.squeeze(image[0].data)
        data[idx] = np.nan
        image[0].data[0, 0, :, :] = data  # this is so clunky good grief
    else:
        image[0].data[idx] = np.nan
        
    image.flush()
    image.close()


def cli(args):

    clip(
        image=args.image,
        beam_image=args.beam,
        clip_level=args.clip
    )


if __name__ == "__main__":
    cli(get_args())

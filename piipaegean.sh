#!/bin/bash

# source ${PIIPPATH}piipconfig.cfg
# source ${PIIPPATH}piipfunctions.sh
# source ${PIIPPATH}piiphelp.sh

OVERWRITE=true

filename=$1
ra=$2
dec=$3
radius=$4

if [ -z ${filename} ]; then
    echo "ERROR: no image provided - exiting."
    exit 1
fi

if [ ! -z $5 ]; then
    source $5
fi

root=$(echo ${filename} | sed "s/.fits//")

# TODO create temp masked fits file for running aegean
# FOV based on beam FOV at 0.05 per cent. 
# OR mask based on beam with 0.05 per cent clip?



# if [ ! -z ${ra} ]; then

#     MIMAS -o ${root}.mim +c ${ra} ${dec} ${radius}
#     mim="--region=${root}.mim"

# else

#     mim=""

# fi
# regions are broken in aegean and are not being fixed
mim=""

if [ ! -e ${root}_masked_comp.vot ] || $OVERWRITE; then

    BANE --cores 1 ${filename}
    MIMAS +c $ra $dec $radius -o ${root}_fov.mim
    MIMAS --maskimage ${root}_fov.mim ${filename} ${root}_masked.fits
    # zero_background.py ${background}

    aegean --seedclip=10 \
        --floodclip=4 \
        --autoload \
        --nocov \
        --table=${root}_masked.vot,${root}_masked.reg \
        --noise=${root}_rms.fits \
        --background=${root}_bkg.fits \
        ${root}_masked.fits

    rm ${root}_masked.fits

fi


exit 0

#! /usr/bin/env python

from argparse import ArgumentParser
from casacore.tables import table

def get_args():
    ps = ArgumentParser()
    ps.add_argument("ms", type=str)
    ps.add_argument("-a", "--antennas", required=True, nargs="*", type=int)
    return ps.parse_args()

def drop_ants(ms, antennas):

    MeasurementSet = table(ms, readonly=False, ack=False)
    AntennaSet = table(ms + "/ANTENNA", readonly=False, ack=False)

    print(antennas)
    print(AntennaSet.nrows())

    for i in range(AntennaSet.nrows()-1, -1, -1):
        if i in antennas:
            AntennaSet.removerows([i])
 
    ant1 = MeasurementSet.getcol("ANTENNA1")
    ant2 = MeasurementSet.getcol("ANTENNA2")

    print("MS rows before: {}".format(MeasurementSet.nrows()))
    for i in range(MeasurementSet.nrows()-1, -1, -1):
        if ant1[i] in antennas or ant2[i] in antennas:
            MeasurementSet.removerows([i])
    print("MS rows after: {}".format(MeasurementSet.nrows()))

    MeasurementSet.flush()
    AntennaSet.flush()
    MeasurementSet.close()
    AntennaSet.close()


def cli(args):
    drop_ants(args.ms, args.antennas)


if __name__ == "__main__":
    cli(get_args())


#!/bin/bash
#SBATCH --account=pawsey0272
#SBATCH --partition=work
#SBATCH --time=24:00:00
#SBATCH --exclusive
#SBATCH --nodes=1

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi
echo "INFO: setting PIIPPATH=${PIIPPATH}"

shopt -s expand_aliases

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

USE_OLD=false
IMSIZE=9000
SCALE=0.5
WEIGHT1="r0.5"
WEIGHT2="uniform"
KERNEL=256
DIAGONALS=false
WINDOW="rectangular"
SUBTRACT=false
INTERP="rbf"
CLEAN_REGION=
LINK=false
MS=false
TIME_AVG="8s"
CHAN_AVG=4
REWEIGHT_DATA=false
if [ -e "/nvmetmp/" ]; then
    BINDPATH="--bind /nvmetmp/"
fi

# TODO merge these two containers
# CONTAINER1="singularity run --bind /nvmetmp/ /astro/mwasci/duchesst/piip.img"
#CONTAINER2 not used here - should relabel CONTAINER1 here and elsewhere to CONTAINER to match other scripts
# CONTAINER2="singularity run --nv --bind /nvmetmp/ /astro/mwasci/duchesst/gpu_stuff.img"


set -x

opt=$1
if [[ "${opt}" == *".cfg"* ]]; then
    config=$(echo "${opt}" | sed "s/'//g")
    echo "INFO: using config file ${config}"
    source ${config}
fi

opts=$(getopt \
    -o :ho:M:m:c:d:i:k:R:w:W:DUf:SI:F:t:u:E:z:a:lV \
    --long help,outbase:,mwa2:,mwa1:,chan:,processing_dir:,indir:,kernel:,reference:,weight1:,weight2:,diagonals,use_old,window:,subtract,interp:,fov:,ra:,dec:,exclude:,region:,aterms:,link,ms \
    --name "$(basename "$0")" \
    -- "$@"
)

# [ $? -eq 0 ] || {
#     echo "ERROR: unknown option provided - please check inputs!"
#     usage_piipimage
#     exit 1
# }

# for opt in ${opts[@]}; do
#     if [[ "${opt}" == *".cfg"* ]]; then
#         config=$(echo "${opt}" | sed "s/'//g")
#         echo "INFO: using config file ${config}"
#         source ${config}
#         break
#     fi
# done


eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipIDGpair ;;
        -o|--outbase) outbase=$2 ; shift 2 ;;
        -M|--mwa2) MWA2=$2 ; shift 2 ;;
        -m|--mwa1) MWA1=$2 ; shift 2 ;;
        -c|--chan) chan=$2 ; shift 2 ;;
        -d|--processing_dir) outdir=$2 ; shift 2 ;;
        -i|--indir) indir=$2 ; shift 2 ;;
        -k|--kernel) KERNEL=$2 ; shift 2 ;;
        -R|--reference) ref=$2 ; shift 2 ;;
        -w|--weight1) WEIGHT1=$2 ; shift 2 ;;
        -W|--weight2) WEIGHT2=$2 ; shift 2 ;;
        -D|--diagonals) DIAGONALS=true ; shift ;;
        -U|--use_old) USE_OLD=true ; shift ;;
        -f|--window) WINDOW=$2 ; shift 2 ;;
        -S|--subtract) SUBTRACT=true ; shift ;;
        -I|--interp) INTERP=$2 ; shift 2 ;;
        -F|--fov) fov=$2 ; shift 2 ;;
        -t|--ra) ra=$2 ; shift 2 ;;
        -u|--dec) dec=$2 ; shift 2 ;;
        -E|--exclude) extraMask=$2 ; shift 2 ;;
        -z|--region) CLEAN_REGION=$2 ; shift 2 ;; 
        -a|--aterms) ATERM_FILE=$2 ; shift 2 ;;
        -l|--link) LINK=$2; shift ;;
        -C|--chan_avg) CHAN_AVG=$2 ; shift 2 ;;
        -T|--time_avg) TIME_AVG=$2 ; shift 2 ;;
        -V|--ms) MS=true ; shift ;;
        --) shift ; break ;;
        *) echo "ERROR: $1 not a valid option" ; usage_piipimage ;;
    esac
done

check_required "--outbase" "-o" $outbase
check_required "--chan" "-c" $chan
check_required "--processing_dir" "-d" $outdir
check_required "--indir" "-i" $indir

if [ -z ${MWA1} ] && [ -z ${MWA2} ]; then
    echo "An MWA1 and/or MWA2 OBSID list must be supplied with -m/-M"
    exit 1
fi

if [ -z ${CONTAINER} ]; then
    container=""
else
    container="singularity exec ${BINDPATH} ${CONTAINER}"
fi  

if [ -z ${CASA_LOCATION} ]; then
    CASA_LOCATION="${container} casa"
fi

if [ ! -e ${outdir} ]; then
    mkdir $outdir
fi

cd ${outdir} || exit 1

phase1obs=""
phase2obs=""
PHASE1OBS=()
PHASE2OBS=()
allmetafits=""
METAFITS=()
mses=()
cfgs=()
ants=()

freq=$(echo "1.28*${chan}" | bc -l)
freq=${freq%.*}

if [ ! -z ${MWA1} ]; then
    while read line; do
        if [ ! -z "${line}" ]; then
            obs=$(echo "$line" | awk '{print $1}')
            ant=$(echo "$line" | awk '{print $2}')
            
            if [ -e ${indir}/${obs}.tar ]; then
                tar -xvf ${indir}/${obs}.tar
            fi

            if [ -e ${indir}/${obs}/image_${WEIGHT1}/${obs}.sub.ms ]; then


                phase1obs="${phase1obs} ${obs}.sub.ms"
                allmetafits="${allmetafits} ${obs}.metafits"
                allobs="${allobs} ${obs}.sub.ms"
                mses+=("${obs}.sub.ms")
                PHASE1OBS+=("${obs}.sub.ms")
                METAFITS+=("${obs}.metafits")

                if [ -z ${reference} ]; then
                    reference=${obs}
                fi

                if [ ! -z ${ant} ]; then
                    allants="${allants} ${ant}"
                    ants+=("${ant}")
                else
                    allants="${allants} NONE"
                    ants+=("NONE")
                fi
    
            else
                echo "${obs}" >> ${outbase}_missing.txt

            fi
            ant=
        fi
    done < ${MWA1}
fi

if [ ! -z ${MWA2} ]; then
    reference=
    while read line; do
        if [ ! -z "${line}" ]; then
            obs=$(echo "$line" | awk '{print $1}')
            ant=$(echo "$line" | awk '{print $2}')
            
            if [ -e ${indir}/${obs}.tar ]; then
                tar -xvf ${indir}/${obs}.tar
            fi

            if [ -e ${indir}/${obs}/image_${WEIGHT2}/${obs}.sub.ms ]; then

                phase2obs="${phase2obs} ${obs}.sub.ms"
                allmetafits="${allmetafits} ${obs}.metafits"
                mses+=("${obs}.sub.ms")
                PHASE2OBS+=("${obs}.sub.ms")
                METAFITS+=("${obs}.metafits")

                if [ -z ${reference} ]; then
                    reference=${obs}
                fi 

                if [ ! -z ${ant} ]; then
                    allants="${allants} ${ant}"
                    ants+=("${ant}")
                else
                    allants="${allants} NONE"
                    ants+=("NONE")
                fi
            else
                echo "${obs}" >> ${outbase}_missing.txt
            fi
            ant=
        fi
    done < ${MWA2}
fi

if [ ! -z ${ref} ];then
    reference=$ref
fi

for obs in ${PHASE1OBS[@]}; do
    obsid=$(echo ${obs} | sed "s/.sub.ms//")
    if [ ! -e ${obsid}.sub.ms ] || [ ! $USE_OLD ]; then
        if [ ! -e ${obsid}.sub.ms ] && [ ! $USE_OLD ]; then
            rm -r ${obsid}.sub.ms
        fi
        if ${LINK}; then
            ln -s ${indir}/${obsid}/image_${WEIGHT1}/${obsid}.sub.ms ${obsid}.sub.ms
            ln -s ${indir}/${obsid}/${obsid}.metafits .
            ln -s ${indir}/${obsid}/image_${WEIGHT1}/${obsid}_deep-MFS-image.fits ${obsid}_deep-MFS-image.fits

        else 
            cp -r ${indir}/${obsid}/image_${WEIGHT1}/${obsid}.sub.ms . || exit
            cp ${indir}/${obsid}/${obsid}.metafits .
            cp -r ${indir}/${obsid}/image_${WEIGHT1}/${obsid}_deep-MFS-image.fits .
        fi

        cp -r ${indir}/${obsid}/image_${WEIGHT1}/${obsid}_deep-MFS-image_comp.vot .
        
        if [ ! -e ${obsid}.metafits ]; then
            wget http://ws.mwatelescope.org/metadata/fits/?obs_id=${obsid} -O ${obsid}.metafits
        fi
    fi
done

for obs in ${PHASE2OBS[@]}; do
    obsid=$(echo ${obs} | sed "s/.sub.ms//")
    if [ ! -e ${obsid}.sub.ms ] || [ ! $USE_OLD ]; then
        if [ ! -e ${obsid}.sub.ms ] && [ ! $USE_OLD ]; then
            rm -r ${obsid}.sub.ms
        fi
        if ${LINK}; then
            ln -s ${indir}/${obsid}/image_${WEIGHT2}/${obsid}.sub.ms ${obsid}.sub.ms
            ln -s ${indir}/${obsid}/${obsid}.metafits .
            ln -s ${indir}/${obsid}/image_${WEIGHT2}/${obsid}_deep-MFS-image.fits ${obsid}_deep-MFS-image.fits

        else 
            cp -r ${indir}/${obsid}/image_${WEIGHT2}/${obsid}.sub.ms . || exit
            cp ${indir}/${obsid}/${obsid}.metafits .
            cp -r ${indir}/${obsid}/image_${WEIGHT2}/${obsid}_deep-MFS-image.fits .
        fi

        cp -r ${indir}/${obsid}/image_${WEIGHT2}/${obsid}_deep-MFS-image_comp.vot .
        
        if [ ! -e ${obsid}.metafits ]; then
            wget http://ws.mwatelescope.org/metadata/fits/?obs_id=${obsid} -O ${obsid}.metafits
        fi
    fi
    if [ -z ${reference} ]; then
        reference=${obsid}
    fi
done

radec="${ra} ${dec}"
radec_hms=$($container ${PIIPPATH}/dd_hms_dms.py ${radec} -d "hms")

dldmscreens=""

freq=$($container ${PIIPPATH}/get_header_key.py ${reference}.metafits FREQCENT)
freqMHz=$(echo "${freq}*1000000" | bc -l)
scale=$(echo "${SCALE}/${chan}" | bc -l)
nants=$(${container} ${PIIPPATH}/ms_get_nants.py ${PHASE1OBS[@]} ${PHASE2OBS[@]})

if [ -z ${ATERM_FILE} ]; then
    ATERM_FILE=${outbase}_aterm.cfg
fi

$container get_beam_lobes ${reference}.metafits -p 0.05 -Mm > ${reference}_beamlobes.txt
if (( $( wc -l < ${reference}_beamlobes.txt ) )); then
    while read line; do
        beam_size=$(echo "$line" | awk '{print $4}')
        SMOOTHING=$(echo "$beam_size / 4.0" | bc -l)
    done < ${reference}_beamlobes.txt
else    
    # roughly size of the 216-MHz beam:
    SMOOTHING=22
fi

if [ -z ${fov} ]; then
    fov=$(echo "0.95*${scale}*${IMSIZE}/2" | bc -l)
else
    fov=$(echo "${fov}/2" | bc -l)
fi


# for ms in ${mses[@]}; do

#     ${CASA_LOCATION} --nogui --nologger --agg -c \
#         "mstransform(vis='${ms}', outputvis='${ms/.ms/.avg.ms}', chanaverage=True, chanbin=${CHAN_AVG}, timeaverage=True, timebin='${TIME_AVG}', datacolumn='data')"
    
#     rm -r ${ms}
#     mv ${ms/.ms/.avg.ms} ${ms}
# done

if [ -z ${CLEAN_REGION} ]; then
    if [ ! -z ${extraMask} ]; then
        circles=""
        while read line; do
            extraRA=$(echo "$line" | awk '{print $1}')
            extraDEC=$(echo "$line" | awk '{print $2}')
            extraFOV=$(echo "$line" | awk '{print $3}')
            if [ ! -z ${extraRA} ] && [ ! -z ${extraDEC} ] && [ ! -z ${extraFOV} ]; then
                circles="${circles} -c ${extraRA} ${extraDEC} ${extraFOV}"
            fi
        done < $extraMask
    fi
    ${container} MIMAS -depth 16 +c $radec $fov $circles -o ${outbase}_fov.mim
fi

if [ ! -e ${reference}_template-dirty.fits ]; then
# get template image for a-term screens:
    ${container} wsclean \
        -niter 0 \
        -name ${reference}_template \
        -size ${IMSIZE} ${IMSIZE} \
        -pol I \
        -scale ${scale} \
        -abs-mem ${ABSMEM} \
        -j ${NCPUS} \
        -log-time \
        -verbose \
        -use-wgridder \
        -temp-dir $TMP_DIR \
        ${reference}.sub.ms
fi

template_image=${reference}_template-dirty
if ${SUBTRACT}; then

    if [ -z ${CLEAN_REGION} ]; then
        $container "${PIIPPATH}"/fits_set_all_ones.py "${template_image}.fits"
        $container MIMAS --maskimage "${outbase}_fov.mim" "${template_image}.fits" "${outbase}_mask.fits"
        ${container} ${PIIPPATH}/fits_set_nan_zero.py "${outbase}_mask.fits"
    else
        ${container} ${PIIPPATH}/mask_region.py ${CLEAN_MASK} ${template_image}.fits \
            -o ${outbase}_mask.fits -bMn -m 1 -f 0
    fi
    
fi
pad=0.2
smallkernel=$(echo "(4*${KERNEL}-4*${pad}*${KERNEL})" | bc -l)
smallkernel=${smallkernel%.*}
abspad=$(echo "4*${KERNEL}-${smallkernel}" | bc -l)

bmajref=$($container ${PIIPPATH}/get_header_key.py ${reference}_deep-MFS-image.fits BMAJ)

for m in ${mses[@]}; do

    obsid="${m/.sub.ms/}"
    
    if [ ! -e ${obsid}_dldm.fits ] || [ ! $USE_OLD ]; then


        if $REWEIGHT_DATA; then

            ${container} ${PIIPPATH}/ms_weight_from_image.py ${obsid}.sub.ms ${obsid}_deep-MFS-image.fits.fits

        fi

        if [ "${obsid}" != "${reference}" ]; then

            # bmaj from non-reference is better
            bmaj=$($container ${PIIPPATH}/get_header_key.py ${obsid}_deep-MFS-image.fits BMAJ)

            # filter to remove bad fits - if not non-compact sources might pretend to be compact.
            ${container} $PIIPPATH/get_filtered_aegean_catalogue.py ${obsid}_deep-MFS-image_comp.vot ${obsid}_deep-MFS-image_comp.vot
            # separation=$(echo "${bmaj}*3" | bc -l)

            # we need unfortunately a very strict match but also as many sources as possible...
            # MWA-2 to MWA-2 matches produce up to ~2000 sources so need to limit this for memory reasons
            xmcat=${obsid}_${outbase}.fits
            ${container} match_catalogues ${obsid}_deep-MFS-image_comp.vot ${reference}_deep-MFS-image_comp.vot \
                --separation ${bmajref} \
                --exclusion_zone ${bmaj} \
                --threshold 0.0 \
                --nmax 1000 \
                --outname ${xmcat}  \
                --ra1 ra --dec1 dec \
                --ra2 ra --dec2 dec \
                --flux_key int_flux \
                --eflux_key err_int_flux \
                -f1 int_flux peak_flux \
                -f2 int_flux peak_flux \
                --beam_key1 psf_a psf_b \
                --beam_key2 psf_a psf_b \
                --size1 a b \
                --size2 a b \
                --ratio 1.2 \
                --beam_ratio 1.2 \
                --full
            
            # need to add nearest to normal fits_warp
            smooth=$(echo "0.2/${scale}" | bc -l)
            # ${container} /astro/mwasci/duchesst/fits_warp2.py \
            ${container} ${PIIPPATH}/fits_warp2.py \
                --infits ${template_image}.fits \
                --suffix warp \
                --ra1 ra_1 \
                --dec1 dec_1 \
                --ra2 ra_2 \
                --dec2 dec_2 \
                --smooth ${smooth} \
                --plot \
                --testimage \
                --xm ${xmcat} \
                --interpolat $INTERP \

            mv ${template_image}_delx.fits ${obsid}_delx.fits
            mv ${template_image}_dely.fits ${obsid}_dely.fits


            # files should be curated beforehand!
            if [ ! -e ${obsid}_delx.fits ] || [ ! -e ${obsid}_dely.fits ]; then
                echo "Missing files - exiting."
                exit 1
            fi

    

        fi

        nants=$(${container} ${PIIPPATH}/ms_get_nants.py ${obsid}.sub.ms)

        if ${MS}; then
            MS_OPT="--ms ${outbase}.ms"
        else
            MS_OPT="-a ${nants}"
        fi

        if [ "${obsid}" != "${reference}" ]; then
            
            # nants=$(${container} ${PIIPPATH}/ms_get_nants.py ${obsid}.ms)
            if [ -e ${obsid}_delx.fits ]; then

                

                ${container} ${PIIPPATH}/make_aterm_files.py \
                    -x ${obsid}_delx.fits \
                    -y ${obsid}_dely.fits \
                    --dfl ${smallkernel} \
                    --dfd ${smallkernel} \
                    --pad ${pad} \
                    --abs-pad ${abspad} \
                    -o ${obsid} \
                    -m ${obsid}.metafits \
                    ${MS_OPT}
            fi
        
        else
            
            # make dl,dm with all zeroes:
            ${container} ${PIIPPATH}/make_aterm_files.py \
                -x ${template_image}.fits \
                -y ${template_image}.fits \
                --dfl ${smallkernel} \
                --dfd ${smallkernel} \
                --pad ${pad} \
                --abs-pad ${abspad} \
                -o ${obsid} \
                -m ${obsid}.metafits \
                --zeroes \
                --ones \
                ${MS_OPT}

        fi

    fi

    if [ -e ${obsid}_dldm.fits ]; then
        dldmscreens="${dldmscreens} ${obsid}_dldm.fits"
    fi

done


for screen in ${dldmscreens[@]}; do
    screen_mses="${screen_mses} ${screen/_dldm.fits/.sub.ms}"
done

echo ${screen_mses}


# recreate aterm config in case wanting to try different params?
${container} ${PIIPPATH}/make_aterm_config.py \
    --beam \
    -o ${ATERM_FILE} \
    --dldm ${dldmscreens} \
    --window $WINDOW


#!/bin/bash

#SBATCH --account=mwasci
#SBATCH --partition=workq
#SBATCH --time=06:00:00
#SBATCH --nodes=1


if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

echo "INFO: PIIPPATH=${PIIPPATH}"

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh


FLAG=true
DOPEEL=false

DELETE_OLD=false
USE_OLD=false
PEEL_THRESHOLD=25.0
SUBTRACT_THRESHOLD=1.0
PERCENT=0.2
CALIBRATOR_IS_TARGET=false
CALIBRATION_COLUMN="DATA"
CYGA_MODEL=
SUBTRACT_MODELS=
CAL_MINUV=100
CAL_MAXUV=1900  # roughly MWA-1 resolution
CAL_NSRC_MAX=1000 
CAL_NFREQS=10   # number of frequencies to predict with create_skymodel
CAL_NLOBES=    # number of primary beam lobes to look for sources within
CAL_NLOBES_PERC=0.2  # percent attenation for lobes
CAL_RADIUS=     # radius for sources is based on FOV
FLAG_AFTER_CAL=false
FLAG_BEFORE_CAL=true
FLAG_BEFORE_PEEL=false
POTATO_PEEL=false
REMOVE_MODEL_DATA=true
TMP_DIR=
USE_JOB_FOR_TMP=true
CAL_SOLUTIONS_DIR=
ARRAY_OFFSET=0
TEST_IMAGE=false
IMSIZE=4096
BIND_PATH=
RA_DIR=
DEC_DIR=
SQUASH=false
REMOTE_PROCESSED=
REMOTE_STAGED=

start_time=$(date +%s)

opts=$(getopt \
     -o :hi:d:O:zm:M:C:N:S:f:on:a:pUF:T:A:\
     --long help,indir:,processing_dir:,obsid_list:,noflag,minuv:,maxuv:,calibrator:,calname:,solutions:,field:,overwrite,nsrc_max:,obsnum:,peel,useold,flagfile:,flagtiles:,array_offset: \
     --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piipcal
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipcal ;;
        -d|--processing_dir) OUTDIR=$2 ; shift 2 ;;
        -i|--indir) INDIR=$2 ; shift 2 ;;
        -O|--obsid_list) obsid_list=$2 ; shift 2 ;;
        -z|--noflag) FLAG=false ; shift ;;
        -m|--minuv) CAL_MINUV=$2 ; shift 2 ;;
        -M|--maxuv) CAL_MAXUV=$2 ; shift 2 ;;
        -C|--calibrator) calibrator=$2 ; shift 2 ;;
        -N|--calname) calname=$2 ; shift 2 ;;
        -S|--solutions) solution_file=$2 ; shift 2 ;;
        -f|--field) field=$2 ; shift 2 ;;
        -o|--overwrite) DELETE_OLD=true ; shift ;;
        -n|--nsrc_max) CAL_NSRC_MAX=$2 ; shift 2 ;;
        -a|--obsnum) obsnum=$2; shift 2 ;;
        -p|--peel) DOPEEL=true; shift ;;
        -U|--useold) USE_OLD=true; shift ;;
        -F|--flagfile) flagfile=$2 ; shift 2 ;;
        -T|--flagtiles) flagtiles=$2 ; shift 2 ;;
        -A|--array_offset) ARRAY_OFFSET=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR:" ; usage_piipcal ;;
    esac
done


# after configuration file loading:
set -x

check_required "--processing_dir" "-d" $OUTDIR
check_required "--indir" "-i" $INDIR

if [ -z ${TMP_DIR} ] || [ "${TMP_DIR}" == "./" ]; then
    TMP_DIR="./"
    USING_TMP=false
else
    TMP_DIR="${TMP_DIR}/"
    if $USE_JOB_FOR_TMP; then
        TMP_DIR="${TMP_DIR}/${SLURM_JOB_ID}/"
    fi 
    if [ ! -e ${TMP_DIR} ]; then
        mkdir -p ${TMP_DIR} || exit 1
    fi
    USING_TMP=true
fi

if ${USING_TMP}; then
    BIND_PATH="${TMP_DIR},${BIND_PATH}"
fi

if [ -z ${CONTAINER} ]; then
    container=""
else
    if [ ! -z ${BIND_PATH} ]; then
        BIND_PATH="-B ${BIND_PATH}"
    fi
    container="singularity exec ${BIND_PATH} ${CONTAINER}"
fi 


if [ "${OUTDIR}" = "${INDIR}" ]; then
    echo "Setting DELETE_OLD to false to ensure we do not delete the working directory."
    DELETE_OLD=false
fi

# Set obsid from array:
if [ -z ${SLURM_ARRAY_TASK_ID} ]; then
    check_required "--obsnum" "-a" ${obsnum}
else

    obsnum=$((${SLURM_ARRAY_TASK_ID} + ${ARRAY_OFFSET}))
    
fi

if [ ${#obsnum} = 10 ]; then
    obsid=${obsnum}
else
    check_required "--obsid_list" "-o" $obsid_list
    obsid=$(sed "${obsnum}q;d" ${obsid_list} | awk '{print $1}')
fi

check_obsid ${obsid}

if [ ! -e ${OUTDIR} ]; then
    mkdir ${OUTDIR}
fi

# Create individual directories for each snapshot:
if [ -e ${OUTDIR}/${obsid} ] && $DELETE_OLD; then
    echo "INFO: Old run detected - removing."
    rm -r ${OUTDIR}/${obsid}
    mkdir ${OUTDIR}/${obsid}
elif [ -e ${OUTDIR}/${obsid} ] && $USE_OLD; then
    echo "INFO: Old run detected - using."
elif [ -e ${OUTDIR}/${obsid} ]; then
    if [ -e ${OUTDIR}/${obsid}/${obsid}.ms ]; then
        echo "INFO: Old run detected - removing MS."
        rm -r ${OUTDIR}/${obsid}/${obsid}.m*    
    fi
else
    mkdir ${OUTDIR}/${obsid}
fi

if [ ! -z ${config} ]; then
    cp ${config} ${OUTDIR}/${obsid}/
fi
cd ${OUTDIR}/${obsid} || exit 1

# stolen directly from GLEAM-X pipeline: https://github.com/GLEAM-X/GLEAM-X-pipeline/blob/master/templates/autocal.tmpl
if [[ ${obsid} -gt 1300000000 ]] && [[ ${obsid} -lt 1342950000 ]]
then
    refant=0
elif [[ ${obsid} -gt 1342950000 ]]
then
    refant=8
else
    refant=127
fi

# Check for ${obsid}_ms.zip file or just *.ms and *.metafits files:
if [ ! -e ${OUTDIR}/${obsid}/${obsid}.ms ]; then


    if [ ! -z ${REMOTE_STAGED} ]; then
        rclone copy ${REMOTE_STAGED}${obsid}_ms.tar ${TMP_DIR}
        tar -xvf ${TMP_DIR}${obsid}_ms.tar -C ${TMP_DIR} && rm ${TMP_DIR}${obsid}_ms.tar
        wget http://ws.mwatelescope.org/metadata/fits/?obs_id=${obsid} -O ${TMP_DIR}${obsid}.metafits
    
    # Original download format with no archives:
    elif [ -e ${INDIR}/${obsid}/${obsid}.ms ] && \
       [ -e ${INDIR}/${obsid}/${obsid}.metafits ]; then

        cp -R ${INDIR}/${obsid}/${obsid}.ms ${INDIR}/${obsid}/${obsid}.metafits ${TMP_DIR}

    # Old MWA ASVO format with zip archives:
    elif [ -e ${INDIR}/${obsid}/${obsid}_ms.zip ]; then
       
        cp ${INDIR}/${obsid}/${obsid}_ms.zip ${TMP_DIR}
        unzip ${TMP_DIR}${obsid}_ms.zip -d ${TMP_DIR} && rm ${TMP_DIR}${obsid}_ms.zip

    # Current download format with tar archives:
    elif [ -e ${INDIR}/${obsid}/${obsid}_ms.tar ]; then
       
        cp ${INDIR}/${obsid}/${obsid}_ms.tar ${TMP_DIR}
        tar -xvf ${TMP_DIR}${obsid}_ms.tar -C ${TMP_DIR} && rm ${TMP_DIR}${obsid}_ms.tar

    # Only MS and recreate the metafits:
    elif [ -e ${INDIR}/${obsid}/${obsid}.ms ]; then
       
        cp -R ${INDIR}/${obsid}/${obsid}.ms  ${TMP_DIR}
        wget http://ws.mwatelescope.org/metadata/fits/?obs_id=${obsid} -O ${TMP_DIR}${obsid}.metafits

    else
       
        echo "ERROR: input ${obsid}.ms or archive missing - exiting."
        exit 1

    fi

fi

# Always ensure a metafits file exists in the main directory:
if [ ! -e ${obsid}.metafits ]; then
    if [ -e ${TMP_DIR}${obsid}.metafits ]; then
        cp ${TMP_DIR}${obsid}.metafits .
    else
        wget http://ws.mwatelescope.org/metadata/fits/?obs_id=${obsid} -O ${obsid}.metafits
    fi
fi

# Now get the channel number - previously this would have been specified 
# manually:
chan=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "CENTCHAN")
freq=$(echo "${chan}*1.28" | bc -l)

if [ ! -z "${field}" ] && [ ! -z ${DBFILE} ]; then
    d_format=$(date +%F:%H:%M:%S)

    if [ ${#obsnum} -lt 10 ]; then
        extraOptions="--selection=${obsnum}"
    else
        extraOptions=""
    fi

    $container ${PIIPPATH}/field_manager.py \
        -m "update_snapshot_status" \
        -d $DBFILE \
        --obsid=${obsid} \
        --field=$field \
        --chan=$chan \
        --status="start-skycal-4-[${d_format}]" \
        --overwrite ${extraOptions} 

    if [ ! -z ${calname} ] && [ -z ${calibrator} ]; then
        calparams=$($container ${PIIPPATH}/field_manager.py -m get_calibrator \
            -d ${DBFILE} \
            --obsid=${obsid} \
            --field=${field} \
            --chan=${chan})
        calibrator=$(echo "${calparams}" | awk '{print $2}')
    fi

    if [ -z ${CAL_SOLUTIONS_DIR} ]; then
        CAL_SOLUTIONS_DIR=${OUTDIR}/calibration_solutions
    fi


    if [ -e ${CAL_SOLUTIONS_DIR} ]; then
        if [ -e ${CAL_SOLUTIONS_DIR}/${calibrator}_solutions1.bin ]; then
            solution_file=${CAL_SOLUTIONS_DIR}/${calibrator}_solutions1.bin
            calibrator=
        fi
    fi



fi

# Allow overwriting of DATA column:
if [ ${CALIBRATION_COLUMN} = "DATA" ]; then
    calcopy="-nocopy"
    datacolumn="data"
else
    calcopy="-copy"
    datacolumn="corrected"
fi

# ---------------------------------------------------------------------------- #

# Generate the approximate primary beam of the observation:
if [ ! -e ${obsid}_stokesI_sky.fits ]; then
    $container get_beam_image ${obsid}.metafits -o ${obsid}_stokesI_sky.fits
fi

# ensure phase centre is at pointing centre:
ra=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "RA")
dec=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "DEC")

if [ ! -z ${RA_DIR} ] || [ ! -z ${DEC_DIR} ]; then
    FIND_NEW_LOBE=true
fi

if [ -z ${RA_DIR} ]; then
    RA_DIR=$ra
fi
if [ -z ${DEC_DIR} ]; then
    DEC_DIR=$dec
fi

if ${FIND_NEW_LOBE}; then
    params=$(${container} get_beam_lobes ${obsid}_stokesI_sky.fits -M --pnt "${RA_DIR}" "${DEC_DIR}")
    ra=$(echo "${params}" | awk '{print $2}')
    dec=$(echo "${params}" | awk '{print $3}')
fi


radec_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${ra} ${dec} -d "hms")

# Ensure we are at the right phase centre - this may have been changed from 
# a previous run!
$container chgcentre ${TMP_DIR}${obsid}.ms ${radec_hmsdms}

if ${CALIBRATOR_IS_TARGET}; then
    calibrator=${obsid}
    USE_OLD=false
    calim=${obsid}.metafits
fi


flagExtras=""

if [ ! -z ${calibrator} ] || [ ! -z ${calname} ]; then
    caldir=${OUTDIR}/${calibrator}/
fi

if [ ! -z ${calibrator} ] && [ -z ${solution_file} ] && [ ! -e ${caldir}${calibrator}_solutions1.bin ]; then

    # TODO - 
    check_obsid ${calibrator}
    # caldir=${OUTDIR}/${calibrator}/

    if ! ${CALIBRATOR_IS_TARGET}; then
        # Calibrator gets its own directory so that we can apply solutions
        # from it to other obsids in the same set of observations without
        # having to recalibrate it every time.

        

        if [ ! -e ${OUTDIR}/${calibrator} ]; then
            mkdir ${OUTDIR}/${calibrator}
        fi

        if [ ! -e ${OUTDIR}/${calibrator}/${calibrator}.ms ]; then

            if [ -e "${INDIR}/${calibrator}/${calibrator}_ms.zip" ]; then

                cp ${INDIR}/${calibrator}/${calibrator}_ms.zip ${caldir}
                cd ${caldir}
                unzip ${calibrator}_ms.zip
                rm ${calibrator}_ms.zip
                cd ${OUTDIR}/${obsid}
            
            elif [ -e "${INDIR}/${calibrator}/${calibrator}_ms.tar" ]; then
                cp ${INDIR}/${calibrator}/${calibrator}_ms.tar ${caldir}
                cd ${caldir}
                tar -xvf ${calibrator}_ms.tar && rm ${calibrator}_ms.tar
                cd ${OUTDIR}/${obsid}

            else

                cp -r ${INDIR}/${calibrator}/${calibrator}.ms ${caldir}
                cp ${INDIR}/${calibrator}/${calibrator}.metafits ${caldir}
            
            fi

           
        fi

        if [ ! -e ${caldir}/${calibrator}.metafits ]; then
            wget http://ws.mwatelescope.org/metadata/fits/?obs_id=${calibrator} -O ${caldir}/${calibrator}.metafits
        fi

        calib=${caldir}/${calibrator}.ms
        calim=${caldir}/${calibrator}.metafits
        flagExtras="--calms ${calib}"

    fi
fi


if $FLAG && $FLAG_BEFORE_CAL; then

    if [ ! -z ${flagtiles} ]; then
        flagExtras="${flagExtras} --tiles=${flagtiles}"
    fi 

    ${PIIPPATH}/piipflag.sh ${config} --ms=${TMP_DIR}${obsid}.ms \
        --datacolumn="DATA" \
        --obsid_list="${obsid_list}" \
        --nobaseline ${flagExtras} \
        --obsid=${obsid}

fi

minuv=$($container ${PIIPPATH}/get_baselines_from_lambdas.py ${freq} ${CAL_MINUV})
maxuv=$($container ${PIIPPATH}/get_baselines_from_lambdas.py ${freq} ${CAL_MAXUV})

# ---------------------------------------------------------------------------- #


excludeCygA=""

if [ -z ${CAL_RADIUS} ]; then
    
    # Work out the beamsize and use that to determine radius to search 
    # for calibrator sources:

    $container get_beam_lobes ${obsid}.metafits -p 0.1 -Mm > ${obsid}_beamlobes.txt
    
    if (( $( wc -l < ${obsid}_beamlobes.txt ) )); then

        while read line; do
            beam_size=$(echo "$line" | awk '{print $4}')
            CAL_RADIUS=$(echo "$beam_size * 1.5" | bc -l)
        done < ${obsid}_beamlobes.txt

    fi

fi

cut1=$($container ${PIIPPATH}/get_scaled_flux.py ${freq} -a -0.77 -f 215.0 -S 5.0)
cut2=$($container ${PIIPPATH}/get_scaled_flux.py ${freq} -a -0.77 -f 215.0 -S 0.05)

if [ -z ${CAL_NLOBES} ]; then
    CAL_NLOBES=$($container get_beam_lobes -m ${obsid}.metafits --perc ${CAL_NLOBES_PERC} --nlobes)
fi

# First pass collects bright source models within the main (or bright side) lobe
$container prep_model -d ${MODELS} \
           -m ${obsid}.metafits \
           -t ${cut2} \
           -o A_team_model1.txt \
           -p "model" \
           -r ${CAL_RADIUS} \
           --nlobes ${CAL_NLOBES} \
           --export_prefix model \
           ${excludeCygA}


if [ ! -z ${SUBTRACT_MODELS} ]; then
    
    # Do not read in additional models that have already been added in the first pass
    # Second pass to get bright sources outside of sidelobes
    excludeSources=$(echo "${excludeCygA}" | awk '{print $2}')
    excludeSources="-x ${excludeSources} model*.txt"
    $container prep_model -d ${SUBTRACT_MODELS} \
            -m ${obsid}.metafits \
            -t ${cut1} \
            -o A_team_model2.txt \
            -p "model" \
            -r 360.0 \
            ${excludeSources} 

    tail -n +2 A_team_model2.txt > tmp.txt
    cat A_team_model1.txt tmp.txt > ${caldir}A_team_model.txt

else

    cp A_team_model1.txt A_team_model.txt 

fi

# cal_model="A_team_model.txt"
cal_model="A_team_model.txt"

if [ -z "${SKYMODEL_OPTIONS}" ]; then
    # Allow other sky model catalogues - must still have some relevant columns:
    SKYMODEL_OPTIONS="--amplitude=alpha_c --index=beta_c --curvature=gamma_c --ra_key=RA --dec_key=DEC"
fi

if [ ! -z ${calibrator} ] && [ -e ${caldir}${calibrator}_solutions1.bin ]; then

    # Calibrator solutions already exists, so just apply them.

    # $container hyperdrive solutions-apply \
    #     -d ${TMP_DIR}${obsid}.ms \
    #     -s ${caldir}${calibrator}_solutions1.bin \
    #     --ms-data-column-name DATA \
    #     --outputs ${TMP_DIR}${obsid}_calibrated.ms

    # rm -r ${TMP_DIR}${obsid}.ms
    # mv ${TMP_DIR}${obsid}_calibrated.ms ${TMP_DIR}${obsid}.ms

    $container applysolutions ${calcopy} ${TMP_DIR}${obsid}.ms ${caldir}${calibrator}_solutions1.bin

    solutions=${caldir}${calibrator}_solutions1.bin


elif [ -e "${obsid}_solutions1.bin" ] && $USE_OLD; then

    $container applysolutions ${calcopy} ${TMP_DIR}${obsid}.ms ${obsid}_solutions1.bin

    # removes MWA_TILE_POINTING table which are needed
    # $container hyperdrive solutions-apply \
    #     -d ${TMP_DIR}${obsid}.ms \
    #     -s ${obsid}_solutions1.bin \
    #     --ms-data-column-name DATA \
    #     --outputs ${TMP_DIR}${obsid}_calibrated.ms

    # rm -r ${TMP_DIR}${obsid}.ms
    # mv ${TMP_DIR}${obsid}_calibrated.ms ${TMP_DIR}${obsid}.ms

    solutions=${obsid}_solutions1.bin


elif [ -z ${solution_file} ]; then

    # In-field calibration on a sky model.
    # Run twice - once then flag then run again

    solutions=${obsid}_solutions1.bin

    if [ -z ${CAL_NSRC_MAX} ]; then
        echo "INFO: only bright-source model to be used."
        mv ${cal_model} ${obsid}_skymodel.txt
        cal_model=${obsid}_skymodel.txt
    else

        $container create_skymodel ${SKYMODEL} ${obsid}.metafits \
            --outname ${obsid}_skymodel_GLEAM.txt \
            --threshold ${cut2} \
            --radius ${CAL_RADIUS} \
            --exclude A_team_model.txt ${CYGA_MODEL} \
            --nmax ${CAL_NSRC_MAX} \
            --nlobes ${CAL_NLOBES} \
            --nfreqs ${CAL_NFREQS} \
            ${SKYMODEL_OPTIONS}

            
        # Add the specific models of bright sources to the GLEAM model:
        tail -n +2 ${obsid}_skymodel_GLEAM.txt > tmp.txt
        cat ${cal_model} tmp.txt > ${obsid}_skymodel.txt
        cal_model=${obsid}_skymodel.txt

    fi

    if [ -z ${CAL_CHAN_AVG} ]; then
        CAL_CHAN_AVG=1
    fi
    if [ -z ${CAL_TSTEPS} ]; then
        # TODO: autoset based on model flux like with selfcal
        # get_model flux --> skymodel
        modelflux=$($container get_model_flux -a ${cal_model} -A -m ${obsid}.metafits)
        threshold=$($container ${PIIPPATH}/get_scaled_flux.py 216.0 -f ${freq} -a -0.77 -S ${modelflux} -i)
        echo "INFO: total apparent brightness of model: ${modelflux}"
        # these settings are for now trial-and-error:
        if [ ${threshold} -lt 100 ]; then
            CAL_TSTEPS=999
        elif [ ${threshold} -lt 300 ]; then
            CAL_TSTEPS=15
        elif [ ${threshold} -lt 800 ]; then
            CAL_TSTEPS=10
        elif [ ${threshold} -lt 1500 ]; then
            # e.g. CenA fields
            CAL_TSTEPS=6
        else
            # the SUN/CygA
            CAL_TSTEPS=1
        fi
        echo "INFO: setting calibration TSTEPs to ${CAL_TSTEPS}"
    
    fi
        
    # convert AO-style calibration models to hyperdrive format:
    $container hyperdrive srclist-convert \
        ${cal_model} ${cal_model/.txt/.yaml} \
        -i ao -o hyperdrive


    $container hyperdrive di-calibrate \
        -d ${TMP_DIR}${obsid}.ms ${obsid}.metafits \
        --ms-data-column-name DATA \
        -s ${cal_model/.txt/.yaml} \
        -b fee \
        --beam-file ${BEAM_PATH}//mwa_full_embedded_element_pattern.h5 \
        -o ${solutions} \
        --uvw-max "${maxuv}m" \
        --uvw-min "${minuv}m" \
        --source-dist-cutoff 180 \
        -t ${CAL_TSTEPS}

    ${container} ${PIIPPATH}/aocal_phaseref.py ${solutions} ${solutions/.bin/_ref.bin} "${refant}" \
        --xy -2.806338586067941065e+01 \
        --dxy -4.426533296449057023e-07 \
        --ms ${TMP_DIR}${obsid}.ms

    mv ${solutions/.bin/_ref.bin} ${solutions}

    $container applysolutions ${calcopy} ${TMP_DIR}${obsid}.ms ${obsid}_solutions1.bin

    # $container hyperdrive solutions-apply \
    #     -d ${TMP_DIR}${obsid}.ms \
    #     -s ${solutions} \
    #     --ms-data-column-name DATA \
    #     --outputs ${TMP_DIR}${obsid}_calibrated.ms

    # rm -r ${TMP_DIR}${obsid}.ms
    # mv ${TMP_DIR}${obsid}_calibrated.ms ${TMP_DIR}${obsid}.ms


elif [ ! -z ${solution_file} ]; then

    echo "INFO: applying existing solutions file: ${solution_file}"

    # $container hyperdrive solutions-apply \
    #     -d ${TMP_DIR}${obsid}.ms \
    #     -s ${solution_file} \
    #     --ms-data-column-name DATA \
    #     --outputs ${TMP_DIR}${obsid}_calibrated.ms

    # rm -r ${TMP_DIR}${obsid}.ms
    # mv ${TMP_DIR}${obsid}_calibrated.ms ${TMP_DIR}${obsid}.ms

    $container applysolutions ${calcopy} ${TMP_DIR}${obsid}.ms ${solution_file}

    solutions=${solution_file}
    cp ${solution_file} .

else

    echo "ERROR: what are we doing here?"
    exit 1

fi


# ${PIIPPATH}ms_plot_chan_amp.py ${obsid}.ms initial_cal CORRECTED_DATA


# Do some flagging now before doing peeling/subtraction
if $FLAG && $FLAG_BASELINES; then

    $container ${PIIPPATH}/ms_flag_by_uvdist.py ${TMP_DIR}${obsid}.ms ${CALIBRATION_COLUMN} -a 

fi


if ${FLAG} && ${FLAG_BEFORE_PEEL}; then

        ${PIIPPATH}/piipflag.sh ${config} --ms=${TMP_DIR}${obsid}.ms \
            --datacolumn=${CALIBRATION_COLUMN} \
            --obsid_list="${obsid_list}" \
            --nobaseline \
            --obsid=${obsid}

fi


if $DOPEEL; then


    # peeling/subtraction has to be done again if starting from existing 
    # solutions.
    peel_threshold=$($container ${PIIPPATH}/get_scaled_flux.py ${freq} -a -0.77 -f 215.0 -S ${PEEL_THRESHOLD})
    subtract_threshold=$($container ${PIIPPATH}/get_scaled_flux.py ${freq} -a -0.77 -f 215.0 -S ${SUBTRACT_THRESHOLD})

    if [ -z ${SUBTRACT_MODELS} ]; then
        SUBTRACT_MODELS=${MODELS}
    fi
    $container prep_model -d ${SUBTRACT_MODELS} \
            -m ${obsid}.metafits \
            -t ${subtract_threshold} \
            -o A_team_models3.txt \
            -p "model" \
            -r 360.0 \
            --pnt "${ra}" "${dec}" \
            ${excludeCygA}

    if ${POTATO_PEEL}; then
        ${PIIPPATH}/potatopeel.sh ${config} \
                -O ${obsid} \
                -t ${peel_threshold} \
                -T ${subtract_threshold} \
                -m ${CAL_MINUV} \
                -M ${CAL_MAXUV} \
                -f A_team_models3.txt \
                -c ${cut1} \
                -D ${CALIBRATION_COLUMN} \
                -N \
                --ra $ra \
                --dec $dec 
    else
        ${PIIPPATH}/piippeel.sh ${config} \
                    -O ${obsid} \
                    -t ${peel_threshold} \
                    -T ${subtract_threshold} \
                    -m ${CAL_MINUV} \
                    -M ${CAL_MAXUV} \
                    -f A_team_models3.txt \
                    -c ${cut1} \
                    -D ${CALIBRATION_COLUMN} \
                    -N
    fi

fi

if $PEEL_LOBES; then

    
    ${PIIPPATH}/potatopeel.sh ${config} \
            -O ${obsid} \
            -m ${minuv} \
            -t 0.0 \
            -D ${CALIBRATION_COLUMN} \
            -P ${PERCENT} \
            --ra ${ra} \
            --dec ${dec} \
            -L

fi


if ${TEST_IMAGE}; then

    scale=$(echo "${SCALE}/${chan}" | bc -l)

    ${container} wsclean -name ${TMP_DIR}${obsid}_test1 \
        -size ${IMSIZE} ${IMSIZE} \
        -niter 5000 \
        -pol I \
        -weight briggs 0.0 \
        -scale ${scale:0:8} \
        -auto-threshold 1 \
        -auto-mask 10 \
        -abs-mem ${ABSMEM} \
        -padding 1.5 \
        -no-update-model-required \
        -data-column ${CALIBRATION_COLUMN} \
        -channels-out 4 \
        -j ${NCPUS} \
        -join-channels \
        -no-mf-weighting \
        -nmiter 1 \
        -minuv-l 0 \
        -use-wgridder \
        -temp-dir ${TMP_DIR} \
        ${TMP_DIR}${obsid}.ms

    if ${USING_TMP}; then
        cp ${TMP_DIR}${obsid}_test1*.fits .
    fi

    files=$(find . -type f -name "${obsid}_test1*.fits" -not \
        \( -name "*MFS*-image*.fits" \))
    for file in ${files[@]}; do
        if [ -e ${file} ]; then
            rm ${file}
        fi
    done

    ${container} ${PIIPPATH}/quick_look.py "${obsid}_test1-MFS-image.fits" \
        --cmap cubehelix \
        --inset \
        --rms

fi

if [ ! -e "aocal_plots" ]; then
    mkdir aocal_plots
fi
for amp in 2 5 50; do
    
    $container ${PIIPPATH}/plot_solutions.py ${solutions} \
        --outname="./aocal_plots/${obsid}_solutions_${amp}" \
        --amp_max=${amp} \
        --metafits ${obsid}.metafits

    $container ${PIIPPATH}/plot_solutions.py ${solutions} \
        --refant=127 \
        --outname="./aocal_plots/${obsid}_solutions_ref127_${amp}" \
        --amp_max=${amp} \
        --metafits ${obsid}.metafits
done

# Copy MS from the temp processing directory at the end.
if ${USING_TMP}; then
    
    if [ ! -z ${REMOTE_PROCESSED} ]; then
        tar -cf ${TMP_DIR}${obsid}_ms.tar -C ${TMP_DIR} ${obsid}.ms
        rclone copy ${TMP_DIR}${obsid}_ms.tar ${REMOTE_PROCESSED}
    else

        rsync -av ${TMP_DIR}${obsid}.ms .

    fi

    rm -r ${TMP_DIR}${obsid}.ms

elif [ ! -z ${REMOTE_PROCESSED} ]; then
    tar -cf ${TMP_DIR}${obsid}_ms.tar -C ${TMP_DIR} ${obsid}.ms
    rclone copy ${TMP_DIR}${obsid}_ms.tar ${REMOTE_PROCESSED}
fi



# ---------------------------------------------------------------------------- #

end_time=$(date +%s)
duration=$(echo "${end_time}-${start_time}" | bc -l)
echo "INFO: Total time taken: ${duration}"

if [ ! -z ${SLURM_ARRAY_TASK_ID} ]; then
    mv ${LOG_LOCATION}/${SLURM_JOB_NAME}.o${SLURM_ARRAY_JOB_ID}_${obsnum} \
        ${LOG_LOCATION}/${SLURM_JOB_NAME}.e${SLURM_ARRAY_JOB_ID}_${obsnum} .
fi

if [ ! -z "${field}" ] && [ ! -z ${DBFILE} ]; then
    
    d_format=$(date +%F:%H:%M:%S)
    $container ${PIIPPATH}/field_manager.py -m "update_snapshot_status" \
        -d ${DBFILE} \
        --field=${field} \
        --chan=${chan} \
        --status="skycal-4-[${d_format}]" \
        --overwrite \
        --obsid=${obsid} ${extraOptions}

fi

exit 0
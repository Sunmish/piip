#! /bin/bash

#SBATCH --account=mwasci
#SBATCH --partition=workq
#SBATCH --time=06:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=12
#SBATCH --mem=120GB
#SBATCH --output=/astro/mwasci/duchesst/G0045/processing/logs/%x.o%A_%a
#SBATCH --error=/astro/mwasci/duchesst/G0045/processing/logs/%x.e%A_%a
#SBATCH --mail-type=ALL
#SBATCH --mail-user=stefanduchesne@gmail.com


source ~/.profile

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

set -x


max_rms=0.05

# blank sky
ra=68.7680775
dec=-61.0976

# cluster center
ra=67.8380963
dec=-61.4316792
peak=2.5  # total flux
size=500  # radius in kpc
bmaj=0.05  # major axis of beam
redshift=0.0594
med=2.6  
alpha="-1.4"

# imaging parameters should in this first fiel
source $1
# halo parameters should be in this second file
# overwrite values above
source $2

name=${peak}Jy${size}kpc

workdir=$3
field=$4
outdir=$5
outname=$6
field2=$7
if [ -z ${field2} ]; then
    field2=$field
fi

CONTAINER="singularity run $CONTAINER"

INDIR="/astro/mwasci/duchesst/G0045/processing/FIELD3266/"

cd ${INDIR} || exit 1

dirties=$(ls */${outdir}/*kpc_dirty.fits)

if [ -e ${INDIR}/${workdir} ]; then
    rm -r ${INDIR}/${workdir}
fi
mkdir ${INDIR}/${workdir}

cd ${INDIR}/${workdir} || exit 1

for file in ${dirties[@]}; do


    echo ${INDIR}/${file} >> ${name}_dirty.list
    # echo ${INDIR}/${file:0:10}/${outdir}/${file:0:10}_mock-MFS-image-pb.fits >> ${name}_image.list
    echo ${INDIR}/${file:0:10}/${outdir}/${file:0:10}_data_halo${name}_image.fits >> ${name}_image.list
    echo ${file:0:10} >> ${name}_obsid.list
done

ra=67.8380963
dec=-61.4316792

$CONTAINER /astro/mwasci/duchesst/piip/stackshots2.py \
    -o ${outname}_${name} \
    -c ${ra} ${dec} \
    -s 5.0 5.0 \
    --max_rms=${max_rms} \
    -UbnC -P 2 -O ./${name}_obsid.list ./${name}_image.list

$CONTAINER /astro/mwasci/duchesst/piip/stackshots2.py \
    -o ${outname}_${name}_dirty \
    -c ${ra} ${dec} \
    -s 5.0 5.0 \
    --max_rms=${max_rms} \
    -Ubn -P 2 -O ./${name}_obsid.list ./${name}_dirty.list


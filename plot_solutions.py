#! /usr/bin/env python

# Stripped down version of aocal_plot.py/aocal.py.

from __future__ import print_function, division

import os
import math
import struct
from collections import namedtuple

from astropy.io import fits
from astropy.table import Table

from argparse import ArgumentParser

import numpy as np
import matplotlib
matplotlib.use("Agg")
from matplotlib import gridspec
from matplotlib import pyplot as plt

from astropy.io import fits

HEADER_FORMAT = "8s6I2d"
HEADER_SIZE = struct.calcsize(HEADER_FORMAT)
HEADER_INTRO = "MWAOCAL\0"
Header = namedtuple("header", "intro fileType structureType intervalCount antennaCount channelCount polarizationCount timeStart timeEnd")
Header.__new__.__defaults__ = (HEADER_INTRO, 0, 0, 0, 0, 0, 0, 0.0, 0.0)

POLS = {0: "XX", 1: "XY", 2: "YX", 3: "YY"}
# POL_COLOR = {"XX": "#0000FF", "XY": "#AAAAFF", "YX": "#FFAAAA", "YY": "#FF0000"}
POL_COLOR = {"XX": "crimson", "XY": "black", "YX": "grey", "YY": "dodgerblue"}
POL_ZORDER = {"XX": 3, "XY": 1, "YX": 2, "YY": 4}



class AOCal():
    def __init__(self, cal_filename):

        with open(cal_filename, "rb") as cal_file:
            header_string = cal_file.read(struct.calcsize(HEADER_FORMAT))
            self.header = Header._make(struct.unpack(HEADER_FORMAT, header_string))
            self.count = self.header.intervalCount * \
                         self.header.antennaCount * \
                         self.header.channelCount * \
                         self.header.polarizationCount
            cal_file.seek(HEADER_SIZE, os.SEEK_SET)
            data = np.fromfile(cal_file, dtype=np.complex128, count=self.count)

        shape = [self.header.intervalCount, 
                 self.header.antennaCount, 
                 self.header.channelCount, 
                 self.header.polarizationCount]


        self.data = data.reshape(shape)
        
         
def nanaverage(a, axis=None, weights=None):
    """
    weighted average treating NaN as zero
    """
    if weights is None:
        weights = np.ones(a.shape)
    return np.nansum(a*weights, axis=axis)/np.nansum(weights, axis=axis)

def get_tile_names(metafits, n_ants):

    tile_names = []

    with fits.open(metafits) as f:
        table = Table(f[1].data)
        for i in range(n_ants):
            idx = np.where(table["Antenna"] == i)[0][0]
            tile_names.append(table[idx]["TileName"])
    
    return tile_names



def plot(ao, plot_filename, refant=None, n_rows=8, plot_title="",
         marker=",", markersize=2, fileformat="png",
         amp_max=None,
         metafits=None):
    """
    """

    n_cols = math.ceil(ao.header.antennaCount / n_rows)

    gs = gridspec.GridSpec(n_rows, n_cols)
    gs.update(hspace=0.0, wspace=0.0)

    ao_amp = np.abs(ao.data)


    if refant is not None:
        if refant < 0:
            ant_avg = nanaverage(ao.data, axis=1, weights=ao_amp**-2)#correct phase
            ant_avg /= np.abs(ant_avg) # normalised
            ant_avg *= nanaverage(ao_amp, axis=1, weights=ao_amp**-2) #standard scalar avg for amp
            ao.data = ao.data / ant_avg[:, np.newaxis, :, :]
            plot_title += " average refant"
        else:
            ao.data = ao.data / ao.data[:, refant, :, :][:, np.newaxis, :, :]
            plot_title += " refant = {:d}".format(refant)
    else:
        plot_title += " no refant"
    

    if amp_max is None:
        amp_max = 2 * np.median(np.nan_to_num(np.abs(ao.data[..., [0,-1]])))

    if metafits is not None:
        tile_names = get_tile_names(
            metafits=metafits,
            n_ants=ao.header.antennaCount
        )
    else:
        tile_names = None

    for timestep in range(ao.header.intervalCount):

        plt.close("all")

        phsfig = plt.figure(figsize=(24., 13.5))
        ampfig = plt.figure(figsize=(24., 13.5))

        for i in range(ao.header.antennaCount):

            vertical_index = i // n_cols
            horizontal_index = i % n_cols

            ax1 = phsfig.add_subplot(gs[vertical_index, horizontal_index])
            ax2 = ampfig.add_subplot(gs[vertical_index, horizontal_index])

            for ax in [ax1, ax2]:
                ax.text(0.05, 0.05, i, 
                        horizontalalignment="left",
                        verticalalignment="bottom",
                        transform=ax.transAxes
                    )
                if tile_names is not None:
                    ax.text(0.05, 0.95, tile_names[i], 
                        horizontalalignment="left",
                        verticalalignment="top",
                        transform=ax.transAxes
                    )
  
            
            for pol in range(ao.header.polarizationCount):

                polstr = POLS[pol]
                amps = np.abs(ao.data[timestep, i, :, pol])
                angles = np.angle(ao.data[timestep, i, :, pol], deg=True)

                ax1.plot(angles, 
                         color=POL_COLOR[polstr],
                         zorder=POL_ZORDER[polstr],
                         linestyle="none",
                         marker=marker,
                         markersize=markersize, 
                         alpha=0.7)

                ax2.plot(amps, 
                         color=POL_COLOR[polstr],
                         zorder=POL_ZORDER[polstr],
                         linestyle="none",
                         marker=marker,
                         markersize=markersize,
                         alpha=0.7)


            for ax in [ax1, ax2]:
                ax.set_xticks([])
                ax.set_yticks([])
                ax.set_xlim([-1, ao.header.channelCount])
            ax1.set_ylim([-180., 180.])
            ax2.set_ylim([0., amp_max])

        phsfig.suptitle("phase:" + plot_title, fontsize=16., y=0.9)
        ampfig.suptitle("amplitude:" + plot_title + " (amp range 0 - {:.1f})".format(amp_max), 
                        fontsize=16., y=0.9)

        if ao.header.intervalCount > 1:
            int_str = "_t{:4>0d}".format(timestep)
        else:
            int_str = ""

        ampfig.savefig("{}{}_amp.{}".format(plot_filename, int_str, fileformat), bbox_inches="tight")
        phsfig.savefig("{}{}_phase.{}".format(plot_filename, int_str, fileformat), bbox_inches="tight")


def main():

    ps = ArgumentParser()
    ps.add_argument("cal_filename", type=str)
    ps.add_argument("-r", "--refant", default=None, type=int)
    ps.add_argument("-t", "--title", default="", type=str)
    ps.add_argument("-o", "--outname", default=None, type=str)
    ps.add_argument("-a", "--amp_max", "--amp-max", dest="amp_max",
                    default=None, type=float)
    ps.add_argument("-O", "--outdir", default="./", type=str)
    ps.add_argument("-m", "--size", default=2, type=int)
    ps.add_argument("-M", "--marker", default=",", type=str)
    ps.add_argument("--metafits", default=None)

    args = ps.parse_args()
    
    if args.outname is None:
        args.outname = args.cal_filename.replace(".bin", "")
    if not args.outdir.endswith("/"):
        args.outdir += "/"

    ao = AOCal(args.cal_filename)

    plot(ao, args.outdir+args.outname, 
         refant=args.refant, 
         n_rows=8, 
         plot_title=args.title,
         marker=args.marker, 
         markersize=args.size, 
         fileformat="png",
         amp_max=args.amp_max,
         metafits=args.metafits
        )


if __name__ == "__main__":
    main()
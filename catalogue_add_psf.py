#! /usr/bin/env python

from argparse import ArgumentParser

from astropy.io import fits
from astropy.table import Table
from astropy.wcs import WCS
import numpy as np
from astropy import units as u


def get_args():
    ps = ArgumentParser(description="Add PSF information to catalogue.")
    ps.add_argument("catalogue")
    ps.add_argument("psf_image")
    return ps.parse_args()


def add_psf(catalogue : str, psf_image : str):
    table = Table.read(catalogue)
    psf_header = fits.getheader(psf_image)
    psf_data = fits.getdata(psf_image)

    wcs = WCS(psf_header).celestial
    y, x = wcs.all_world2pix(table["ra"], table["dec"], 0)

    y = y.astype(int)
    x = x.astype(int)

    bmaj = np.full((len(x),), psf_header["BMAJ"])
    bmin = np.full((len(x),), psf_header["BMIN"])
    bpa = np.full((len(x),), psf_header["BPA"])

    max_x = psf_data[0, ...].shape[0]
    max_y = psf_data[0, ...].shape[1]

    for i in range(len(bmaj)):
        if x[i] >= max_x:
            x_i = max_x-1
        else:
            x_i = x[i]
        if y[i] >= max_y:
            y_i = max_y-1
        else:
            y_i = y[i]

        bmaj[i] = psf_data[0, x_i, y_i]
        bmin[i] = psf_data[1, x_i, y_i]
        bpa[i] = psf_data[2, x_i, y_i]

    # bmaj = np.full((len(table),), psf_header["BMAJ"]*3600.*u.arcsec)
    # bmin = np.full((len(table),), psf_header["BMIN"]*3600.*u.arcsec)
    # bpa = np.full((len(table),), psf_header["BPA"]*u.deg)
    table.add_column(bmaj, name="BMAJ")
    table.add_column(bmin, name="BMIN")
    table.add_column(bpa, name="BPA")
    table.write(catalogue, overwrite=True)


def cli(args):
    add_psf(args.catalogue, args.psf_image)


if __name__ == "__main__":
    cli(get_args())

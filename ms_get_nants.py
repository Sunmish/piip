#! /usr/bin/env python

from argparse import ArgumentParser
from casacore.tables import table


def get_args():
    ps = ArgumentParser()
    ps.add_argument("mses", nargs="*", help="MS(es) to obtain (max) N antennas.")
    return ps.parse_args()


def nants(mses):

    max_nant = 0
    for ms in mses:
        ms = table(ms+"/ANTENNA", ack=False)
        if ms.nrows() > max_nant:
            max_nant = ms.nrows()
    
    return max_nant


def cli(args):
    print(nants(args.mses))


if __name__ == "__main__":
    cli(get_args())

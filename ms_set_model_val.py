#! /usr/bin/env python

import sys
from casacore import tables

def set_model_val(ms, value, col):
    """If the MODEL_DATA column exists, set it to `value`."""

    MeasurementSet = tables.table(ms, readonly=False)

    for i in range(MeasurementSet.nrows()):

        row = MeasurementSet.row().get(i)

        if col in row.keys():

            row[col] = value
            

    MeasurementSet.flush()
    MeasurementSet.close()


def main():
    """
    """

    ms = sys.argv[1]
    value = sys.argv[2]
    try:
        col = sys.argv[3]
    except IndexError:
        col = "MODEL_DATA"

    set_model_val(ms, value, col)


if __name__ == "__main__":
    main()
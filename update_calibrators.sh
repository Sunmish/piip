#!/bin/bash -l

# Get directory of piip script to get piipconfig.cfg location:
if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi
PIIPPATH="${PIIPPATH}/"

echo "INFO: setting PIIPPATH=${PIIPPATH}"

source ${PIIPPATH}piipconfig.cfg
source ${PIIPPATH}piipfunctions.sh
source ${PIIPPATH}piiphelp.sh


AMP_CUT=40
PHASE_CUT=2
PERC=0.3
USE_OLD=false
CAL_SOLUTIONS_DIR=
INDIR1=
INDIR2=
MIN_RMS=10
OUTDIR=
IMAGE_DIR=
IMAGE_STUB="_test1-MFS-image.fits"

opts=$(getopt \
     -o :ho:f:c:O:A:P:N:Ud:D:X:S: \
     --long help,obsids:,field:,chan:,outname:,amp_cut:,phase_cut:,perc:,use_old,dir1:,dir2:,image_dir:,stub: \
     --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piip
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        datename=$(date +%s)
        cp ${config} "${config}.${datename}"
        config="${config}.${datename}"
        break
    fi
done

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) exit 1 ;;
        -o|--obsids) obsids="$2" ; shift 2 ;;
        -d|--dir1) INDIR1=$2 ; shift 2 ;;
        -D|--dir2) INDIR2=$2 ; shift 2 ;;
        -f|--field) field=$2 ; shift 2 ;;
        -c|--channel) chan=$2 ; shift 2 ;;
        -O|--outname) outname=$2 ; shift 2 ;;
        -A|--amp_cut) AMP_CUT=$2 ; shift 2 ;;
        -P|--phase_cut) PHASE_CUT=$2 ; shift 2 ;;
        -N|--perc) PERC=$2 ; shift 2 ;;
        -U|--use_old) USE_OLD=true ; shift ;; 
        -X|--image_dir) IMAGE_DIR=$2 ; shift 2 ;; 
        -S|--stub) IMAGE_STUB=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR: check inputs." ;;
    esac
done   

set -x

if [ -z ${CONTAINER} ]; then
    container=""
else
    container="singularity exec ${CONTAINER}"
fi  

if [ -z ${outname} ]; then
    outname="FIELD${field}_c${chan}.txt"
fi

if [ -z ${CAL_SOLUTIONS_DIR} ]; then
    CAL_SOLUTIONS_DIR=./calibration_solutions
fi

if [ ! -e ${CAL_SOLUTIONS_DIR} ]; then
    mkdir ${CAL_SOLUTIONS_DIR}
fi



for obsid in ${obsids[@]}; do
    if [ -e ${CAL_SOLUTIONS_DIR}/${obsid}_solutions1.bin ] && $USE_OLD; then
        echo "${CAL_SOLUTIONS_DIR}/${obsid}_solutions1.bin"
    else
        echo "${obsid}/${obsid}_solutions1.bin"
        cp ${obsid}/${obsid}_solutions1.bin ${CAL_SOLUTIONS_DIR}/
    fi
done

if [ -z ${INDIR1} ] || [ -z ${INDIR2} ]; then

    ${container} ${PIIPPATH}get_calibration_solutions.py ${obsids[@]} \
        -o ${outname} \
        -A ${AMP_CUT} \
        -P ${PHASE_CUT} \
        -N ${PERC} \
        -C "${CAL_SOLUTIONS_DIR}/" 
else

    # compare images:
    echo "Comparing images" 
    for OBSID in ${obsids[@]}; do

        GOOD_IMAGE=
        IMAGE=${OBSID}${IMAGE_STUB}
        if [ -z $IMAGE_DIR ]; then
            IMAGE1=${INDIR1}/${OBSID}/${IMAGE}
            IMAGE2=${INDIR2}/${OBSID}/${IMAGE}
            SOLUTIONS1=${INDIR1}/${OBSID}/*_solutions1.bin
            SOLUTIONS2=${INDIR2}/${OBSID}/*_solutions1.bin

            PARAMS=$(${CONTAINER} ${PIIPPATH}/compare_images_for_selection.py ${IMAGE1} ${IMAGE2} --min_rms ${MIN_RMS} --solutions ${SOLUTIONS1} ${SOLUTIONS2})
        else
            IMAGE1=${INDIR1}/${IMAGE_DIR}/${OBSID}${IMAGE_STUB}
            IMAGE2=${INDIR2}/${IMAGE_DIR}/${OBSID}${IMAGE_STUB}
            PARAMS=$(${CONTAINER} ${PIIPPATH}/compare_images_for_selection.py ${IMAGE1} ${IMAGE2} --min_rms ${MIN_RMS})
            OBSID=$(echo "$PARAMS" | awk '{print $2}')
            GOOD_IMAGE=$(echo "$PARAMS" | awk '{print $1}')
            if [ "${GOOD_IMAGE}" == "${IMAGE2}" ]; then
                echo "${OBSID}" >> ${outname/.txt/_bandpass.txt}
            fi
        fi

        OBSID=$(echo "$PARAMS" | awk '{print $2}')
        GOOD_IMAGE=$(echo "$PARAMS" | awk '{print $1}')
        GOOD_DIR=$(echo "$PARAMS" | awk '{print $3}')

        if [ ! -z ${GOOD_IMAGE} ]; then 
            echo "${OBSID} ${GOOD_IMAGE} NaN" >> ${outname}
            echo "${OBSID} ${GOOD_DIR}" >> ${outname/.txt/_images.txt}
        fi

    done

fi

if [ ! -z ${field} ] && [ ! -z ${chan} ]; then

    while read line; do

        obsid=$(echo "$line" | awk '{print $1}')
        calid=$(echo "$line" | awk '{print $2}')
        timed=$(echo "$line" | awk '{print $3}')

        echo "Updating ${obsid} with CALID: ${calid} (${timed} hours)"

        ${container} ${PIIPPATH}field_manager.py -m "update_calibrator" \
            -d ${DBFILE} \
            --field=${field} \
            --chan=${chan} \
            --obsid=${obsid} \
            --calid=${calid} \
            --calname=${calid}
    
    done < ${outname}

fi

exit 0

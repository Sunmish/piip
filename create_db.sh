#!/bin/bash -l

PIIPPATH=""
ignore_list="RFI_test"
dbfile="fields.db"
chans=(69 93 121 145 169)

field=$1
if [ -z ${field} ]; then
    echo "usage: create_db.sh FIELD"
    exit 1
fi

# Initialise main database
${PIIPAPTH}field_manager.py -m "initiate_database" \
    --db ${dbfile}

for c in 69 93 121 145 169; do

    # Extract calibrator details from ASVO xml files:
    ${PIIPPATH}asvo_parse.py calib*.xml \
        -c ${c} \
        -i ${ignore_list} \
        -o FIELD${field}_cal_${c}.csv

    # Extract obsids from ASVO xml files:
    ${PIIPATH}asvo_parse.py results*.xml \
        -c ${c} \
        -i ${ignore_list} \
        -o FIELD${field}_${c}.txt

    # Initialise and add to calibrator database "calibrators.db"
    ${PIIPPATH}calbase.py -i -O FIELD${field}_cal_${c}.csv

    # Add obsids for field to database:
    ${PIIPPATH}field_manager.py -m "add_field" \
        --db ${dbfile} \
        --field ${field} \
        --chan ${c} \
        --sdir FIELD${field}_${c}.txt \
        --calbase ./calibrators.db

    # Find appropriate calibrator for all obsids:
    ${PIIPPATH}field_manager.py -m "find_calibrators" \
        --db ${dbfile} \
        --field ${field} \
        --chan ${c} \
        --sdir FIELD${field}_${c}.txt \
        --calbase ./calibrators.db

    # Print out the field details:
    ${PIIPPATH}field_manager.py -m "print_field" \
        --db ${dbfile} \
        --chan=${c} \
        --field=${field} \
        --show_calibrators

done





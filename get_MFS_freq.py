#! /usr/bin/env python

import numpy as np
import sys
from astropy.io import fits



def main():
    """
    """

    images = sys.argv[1:]

    freqs = []
    for image in images:
        hdr = fits.getheader(image)
        freq, found_freq = None, False
        if "FREQ" in hdr.keys():
            found_freq = True
            freq = hdr["FREQ"]
        if not found_freq and "CRVAL3" in hdr.keys():
            if "FREQ" in hdr["CTYPE3"]:
                found_freq = True
                freq = hdr["CRVAL3"]
        if not found_freq and "CRVAL4" in hdr.keys():
            if "FREQ" in hdr["CTYPE4"]:
                found_freq = True
                freq = hdr["CRVAL4"]
        if not found_freq:
            raise RuntimeError("no frequency information for {}".format(image))

        if hdr["BMAJ"] != 0.:
            freqs.append(freq)

    mfs_freq = np.mean(freqs)

    print(mfs_freq)



if __name__ == "__main__":
    main()

#! /bin/bash

BS="\e[1m"
BE="\e[0m"
US="\e[4m"
UE="\e[0m"

usage_piip() {

echo -e "usage: ${BS}piip${BE} [config.cfg] " \
"${BS}-i${BE} ${US}INDIR${UE} " \
"${BS}-d${BE} ${US}PROCESSING_DIR${UE} " \
"${BS}-O${BE} ${US}OBSID_LIST${UE} " \
"${BS}-c${BE} ${US}CHANNEL${UE} " \
"${BS}-r${BE} ${US}ROBUST1${UE}[${US}ROBUST2${UE},${US}...${UE}] " \
"${BS}-s|-a${BE} ${US}SELECTION${UE}|${US}OBSNUM${UE}"

echo ""
echo "required" 
echo -e "    ${BS}-i|--indir${BE} ${US}INDIR${UE} "
echo "        Input directory where MeasurementSets/.zips are located. Required." 
echo -e "    ${BS}-d|--processing_dir${BE} ${US}PROCESSING_DIR${UE} " 
echo "        Output/working directory for processing. Requried."
echo -e "    ${BS}-O|--obsid_list${BE} ${US}OBSID_LIST${UE} " 
echo "        Text file list of observation IDs. Required."
echo -e "    ${BS}-c|--channel${BE} ${US}CHANNEL${UE} " 
echo "        Channel number. Required."
echo -e "    ${BS}-s|--selection${BE} ${US}SELECTION${UE} "
echo "        Selection of observation IDs within the text file list. Acceptable formats are either e.g. '1' or '1-5'. Required if using a supercomputer."
echo -e "    ${BS}-a|--obsnum${BE} ${US}OBSNUM${UE} "
echo -e "        Instead of ${BS}-s|--selection${BE}. Used for a single observation ID when NOT using a supercomputer."
echo -e "    ${BS}-r|--robust${BE} ${US}ROBUST1${UE}[${US}ROBUST2${UE},${US}...${UE}] "
echo "        Briggs robustness parameter for image weighting. Alternatively 'uniform' and 'natural' are valid options. Required if imaging."
echo ""
echo "switches (can be set in the configuration file)"
echo -e "    ${BS}-z|--nocal${BE} (DOCAL=false) "
echo -e "    ${BS}-x|--noselfcal${BE} (DOSELFCAL=false) "
echo -e "    ${BS}-W|--noimage${BE} (DOIMAGE=false) "
echo -e "    ${BS}-v|--nocorrect${BE} (DOCORRECT=false) "
echo -e "    ${BS}-u|--nopeel${BE} (DOPEEL=false) "
echo -e "    ${BS}-w|--nominw${BE} (MINW=false) "
echo -e "    ${BS}-U|--useold${BE} (DELETE_OLD=false) "
echo ""
echo "super computer options (can be set in the configuration file)"
echo -e "    ${BS}-A|--account${BE} ${US}ACCOUNT${UE} "
echo -e "    ${BS}-M|--cluster${BE} ${US}CLUSTER${UE} "
echo -e "    ${BS}-P|--patition${BE} ${US}PARTITION${UE} "

1>&2
exit 1

}


usage_piipcal() {

echo -e "usage: ${BS}piipcal.sh${BE} [config.cfg] " \
"${BS}-i${BE} ${US}INDIR${UE} " \
"${BS}-d${BE} ${US}PROCESSING_DIR${UE} " \
"${BS}-O${BE} ${US}OBSID_LIST${UE} " \
"[${BS}-a${BE} ${US}OBSNUM${UE}] "

echo ""
echo "required" 
echo -e "    ${BS}-i|--indir${BE} ${US}INDIR${UE} "
echo "        Input directory where MeasurementSets/.zips are located. Required." 
echo -e "    ${BS}-d|--processing_dir${BE} ${US}PROCESSING_DIR${UE} " 
echo "        Output/working directory for processing. Requried."
echo -e "    ${BS}-O|--obsid_list${BE} ${US}OBSID_LIST${UE} " 
echo "        Text file list of observation IDs. Required."
echo -e "    ${BS}-a|--obsnum${BE} ${US}OBSNUM${UE} "
echo "        If not using super computer array jobs, the number in the observation ID list to process. "


echo ""
echo "optional (most can be set in the configuration file)"
echo -e "    ${BS}-m|--minuv${BE} ${US}MINUV${UE} "
echo "        Minimum uv (in m) to use for calibration. Default read from configuration file."
echo -e "    ${BS}-M|--maxuv${BE} ${US}MAXUV${UE} "
echo "        Maximum uv (in m) to use for calibration. Default read from configuration file."
echo -e "    ${BS}-n|--nsrc_max${BE} ${US}NSRC_MAX${UE} "
echo "        Maximum number of sources to include in the sky model for in-field calibration. Default read from configuration file."
echo -e "    ${BS}-C|--calibrator${BE} ${US}CALIBRATOR${UE} "
echo "        Calibrator observation ID if wanting to do single-source calibration."
echo -e "    ${BS}-N|--calname${BE} ${US}CALNAME${UE} "
echo "        Name of calibrator."
echo -e "    ${BS}-S|--solutions${BE} ${US}SOLUTION_FILE${UE} "
echo "        Name of existing solutions file to be applied. No calibrator will be done."
echo -e "    ${BS}-F|--flagfile${BE} ${US}FLAGFILE${UE} "
echo "        File (i.e. bash script) with additional CASA flagging/AOFlagging commands."

echo ""
echo "switches (can be set in the configuration file)"
echo -e "    ${BS}-z|--noflag${BE} (FLAG=false) "
echo -e "    ${BS}-p|--peel${BE}   (DOPEEL=true) "
echo -e "    ${BS}-U|--useold${BE} (DELETE_OLD=true) "

1>&2
exit 1

}


usage_piippeel() {

echo -e "usage: ${BS}piippeel.sh${BE} [config.cfg] " \
"${BE}-O${BE} ${US}OBSID${UE} " \
"${BE}-t${BE} ${US}THRESHOLD1${UE} "

echo ""
echo "required"
echo -e "    ${BS}-O|--obsid${BE} ${US}OBSID${UE} "
echo "        Observation ID. Note that the observation ID directory should in be in working directory."
echo -e "    ${BS}-t|--threshold1${BE} ${US}THRESHOLD1${UE} "
echo "        Apparent brightness threshold for source peeling (or image-based subtraction)."

echo ""
echo "optional (some of which can be set from the configuration file)"
echo -e "    ${BS}-T|--threshold2${BE} ${US}THRESHOLD2${UE} "
echo  "        Secondary direct subtraction threshold. Default 0.25*THRESHOLD1 "
echo -e "    ${BS}-m|--minuv${BE} ${US}MINUV${UE} "
echo "        Minimum uv (in m) to use for calibration. Default read from configuration file."
echo -e "    ${BS}-M|--maxuv${BE} ${US}MAXUV${UE} "
echo "        Maximum uv (in m) to use for calibration. Default read from configuration file."
echo -e "    ${BS}-d|--modeldir${BE} ${US}MODELDIR${UE} " 
echo "        Directory containing AO-style model files for bright sources. Default read from configuration file."
echo -e "    ${BS}-f|--modelfile${BE} ${US}MODELFILE${UE} "
echo "        Specifc AO-style model file to use. Used in place of MODELDIR."
echo -e "    ${BS}-D|--data_column${BE} ${US}DATA_COLUMN${UE} "
echo "        DATA or CORRECTED_DATA."
echo -e "    ${BS}-P|--percent${BE} ${US}PERCENT${UE} "
echo "        If subtracting primary beam lobes, this is the percentage threshold. Default 0.2"

echo ""
echo "switches"
echo -e "    ${BS}-L|--lobes${BE} (LOBES=true) "
echo "        Do not do single source peeling/subtraction. Instead image the sidelobes above PERCENT then subtract."
echo -e "    ${BS}-N|--nolobes${BE} (LOBES=false) "

1>&2
exit 1

}


usage_piipselfcal() {

echo -e "usage: ${BS}piipselfcal.sh${BE} [config.cfg] " \
"${BS}-d${BE} ${US}PROCESSING_DIR${UE} " \
"${BS}-O${BE} ${US}OBSID_LIST${UE} " \
"[${BS}-a${BE} ${US}OBSNUM${UE}] "

echo ""
echo "required"
echo -e "    ${BS}-d|--processing_dir${BE} ${US}PROCESSING_DIR${UE} " 
echo "        Output/working directory for processing. Requried."
echo -e "    ${BS}-O|--obsid_list${BE} ${US}OBSID_LIST${UE} " 
echo "        Text file list of observation IDs. Required."
echo -e "    ${BS}-a|--obsnum${BE} ${US}OBSNUM${UE} "
echo "        If not using super computer array jobs, the number in the observation ID list to process. "

echo ""
echo "optional (can be set in the configuration file)"
echo -e "    ${BS}-s|--scale${BE} ${US}SCALE${UE} "
echo "        Scale for pixel size. Note this is pixel size * channel number. Default read from configuration file."
echo -e "    ${BS}-m|--minuv${BE} ${US}MINUV${UE} "
echo "        Minimum uv (in m) to use for calibration and imaging. Default read from configuration file."

echo ""
echo "switches (can be set in the configuration file)"
echo -e "    ${BS}-w|--nominw${BE} (MINW=false)"
echo -e "    ${BS}-U|--useold${BE} Use old self calibration solutions if they exist (as in piipcal.sh) (USE_OLD=true) "

1>&2
exit 1

}


usage_piipimage() {

echo -e "usage: ${BS}piipselfcal.sh${BE} [config.cfg] " \
"${BS}-d${BE} ${US}PROCESSING_DIR${UE} " \
"${BS}-O${BE} ${US}OBSID_LIST${UE} " \
"${BS}-r${BE} ${US}ROBUST${UE} " \
"[${BS}-a${BE} ${US}OBSNUM${UE}] "

echo ""
echo "required"
echo -e "    ${BS}-d|--processing_dir${BE} ${US}PROCESSING_DIR${UE} " 
echo "        Output/working directory for processing. Requried."
echo -e "    ${BS}-O|--obsid_list${BE} ${US}OBSID_LIST${UE} " 
echo "        Text file list of observation IDs. Required."
echo -e "    ${BS}-r|--robust${BE} ${US}ROBSUT${UE} "
echo "        Briggs robust parameter for image weighting. Alternatively, 'uniform' or 'natural' can be specified."
echo -e "    ${BS}-a|--obsnum${BE} ${US}OBSNUM${UE} "
echo "        If not using super computer array jobs, the number in the observation ID list to process. "

echo ""
echo "optional (many options can be specified in the configuration file)"
echo -e "    ${BS}-s|--scale${BE} ${US}SCALE${UE} "
echo "        Scale for pixel size. Note this is pixel size * channel number. Default read from configuration file."
echo -e "    ${BS}-m|--minuv${BE} ${US}MINUV${UE} "
echo "        Minimum uv (in m) to use for calibration and imaging. Default read from configuration file."
echo -e "    ${BS}-n|--niter${BE} ${US}NITER${UE} "
echo "        Maximum number of CLEAN minor cycles. Default read from configuration file."
echo -e "    ${BS}-F|--flagfile${BE} ${US}FLAGFILE${UE} "
echo "        File (i.e. bash script) with additional CASA flagging/AOFlagging commands."

echo ""
echo "switches (can be set in configuration file)"
echo -e "    ${BS}-w|--nominw${BE} (MINW=false) "
echo -e "    ${BS}-y|--noimage${BE} (DOIMAGE=false) "
echo -e "    ${BS}-z|--nocorrect${BE} (DOCORRECT=false) "
echo -e "    ${BS}-M|--multiscale${BE} (MULTISCALE=true) "
echo -e "    ${BS}-S|--subbands${BE} "
echo "        Subbands are always created as part of imaging, but switching this on allows subbands to also be post-processed (SUBBANDS=true)."

1>&2
exit 1

}


usage_dirty_bias() {
    
echo -e "usage: ${BS}dirty_bias.sh${BE} [config.cfg] "\
"${BS}-d${BE} ${US}PROCESSING_DIR${UE} " \
"${BS}-o${BE} ${US}OBSID${UE} " \
"${BS}-r${BE} ${US}ROBUST${UE} " \

echo ""
echo "required"
echo -e "    ${BS}-d|--processing_dir${BE} ${US}PROCESSING_DIR${UE} " 
echo "        Output/working directory for processing. Requried."
echo -e "    ${BS}-o|--obsid${BE} ${US}OBSID${UE} " 
echo "        Observation ID number. Required."
echo -e "    ${BS}-r|--robust${BE} ${US}ROBSUT${UE} "
echo "        Briggs robust parameter for image weighting. Alternatively, 'uniform' or 'natural' can be specified."

1>&2
exit 1
}

usage_piipdownload() {
    
echo -e "usage: ${BS}piipdownload.sh${BE} [config.cfg] "\
"${BS}-d${BE} ${US}DOWNLOADS${UE} " \
"${BS}-O${BE} ${US}OBSID${UE} " \
"${BS}-f${BE} ${US}FIELD${UE} " \
"${BS}-c${BE} ${US}CHAN${UE} " \

echo ""
echo "required"
echo -e "    ${BS}-d|--downloads${BE} ${US}DOWNLOADS${UE} " 
echo "        Output download directory. Requried."
echo -e "    ${BS}-O|--obsid${BE} ${US}OBSID${UE} " 
echo "        Observation ID number. Required."

echo ""
echo "optional"
echo -e "    ${BS}-f|--field${BE} ${US}FIELD${UE} " 
echo "        FIELD ID, only used for database updating."
echo -e "    ${BS}-c|--chan${BE} ${US}CHAN${UE} " 
echo "        Channel number, only used for database updating."

1>&2
exit 1
}

usage_piipflag() {
    
echo -e "usage: ${BS}piipflag.sh${BE} [config.cfg] "\
"${BS}-m${BE} ${US}MS${UE} " \
"${BS}-d${BE} ${US}DATACOLUMN${UE} " \
"[${BS}-O${BE} ${US}OBSID_LIST${UE}] " \


echo ""
echo -e "    ${BS}-m|--ms${BE} ${US}MS${UE} " 
echo "        MeasurementSet name. Must be GPS-time name as is usual with MWA data. Requried."
echo -e "    ${BS}-d|--datacolumn${BE} ${US}DATACOLUMN${UE} " 
echo "        Column name in MS to operate on. Requried."
echo -e "    ${BS}-O|--obsid_list${BE} ${US}OBSID_LIST${UE} " 
echo "        Observation ID list. Required for channel and tile flagging."

echo ""
echo "switches"
echo -e "    ${BS}-A/-z|--aoflag/--noaoflag${BE} " 
echo "        Run/do not run AOFlagger."
echo -e "    ${BS}-T/-y|--tileflag/--notile${BE} " 
echo "        Run/do not run tile flagging. Tile specified in OBSID_LIST."
echo -e "    ${BS}-c/-x|--channelflag/--nochannel${BE} " 
echo "        Run/do not run channel selection flagging. Channels specified in OBSID_LIST."
echo -e "    ${BS}-C/-w|--autochannelflag/--noautochannel${BE} " 
echo "        Run/do not run automatic channel flagger."
echo -e "    ${BS}-q/-Z|--quackflag/--noquack${BE} " 
echo "        Run/do not run 4 sec quack flagging."
echo -e "    ${BS}-b/-Y|--baselineflag/--nobaseline${BE} " 
echo "        Run/do not run automatic baseline flagger."
echo -e "    ${BS}-r/-X|--rflag/--norflag${BE} " 
echo "        Run/do not run CASA rflag task."
echo -e "    ${BS}-t/-W|--tfcrop/--notfcrop${BE} " 
echo "        Run/do not run CASA tfcrop task."

1>&2
exit 1
}
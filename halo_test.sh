#! /bin/bash

#SBATCH --account=mwasci
#SBATCH --partition=workq
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=7
#SBATCH --mem=64GB
#SBATCH --tmp=178GB
#SBATCH --output=/astro/mwasci/duchesst/G0045/processing/logs/%x.o%A_%a
#SBATCH --error=/astro/mwasci/duchesst/G0045/processing/logs/%x.e%A_%a
#SBATCH --mail-type=ALL
#SBATCH --mail-user=stefanduchesne@gmail.com

source ~/.profile

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

module load singularity

IMAGE_MINUV=20

set -x

# blank sky
ra=68.7680775
dec=-61.0976

# cluster center
ra=67.8380963
dec=-61.4316792
peak=2.5  # total flux
size=500  # radius in kpc
bmaj=0.05  # major axis of beam
redshift=0.0594
med=2.6  
alpha="-1.4"

# imaging parameters should in this first fiel
source $1
# halo parameters should be in this second file
# overwrite values above
source $2

obsid=$3
field=$4
robust=$5
outdir=$6
field2=$7
if [ -z ${field2} ]; then
    field2=$field
fi

container=$CONTAINER
CONTAINER="singularity run --bind /nvmetmp/ $CONTAINER"


if [ "${robust}" == "uniform" ]; then
    weight="uniform"
    weight_dir="uniform"
elif [ "${robust}" == "natural" ]; then
    weight="natural"
    weight_dir="natural"
else
    weight="briggs ${robust}"
    weight_dir="r${robust}"
fi

# if [ -e /astro/mwasci/duchesst/G0045/processing/FIELD${field2}/${obsid}/image_${weight_dir} ]; then
#     rm -r /astro/mwasci/duchesst/G0045/processing/FIELD${field2}/${obsid}/image_${weight_dir}
# fi

if [ -e /astro/mwasci/duchesst/G0045/processing/FIELD${field2}/${obsid}/${outdir} ]; then
    rm -r /astro/mwasci/duchesst/G0045/processing/FIELD${field2}/${obsid}/${outdir}
fi


mkdir /astro/mwasci/duchesst/G0045/processing/FIELD${field2}/${obsid}/${outdir}

cd /astro/mwasci/duchesst/G0045/processing/FIELD${field2}/${obsid}/${outdir}/ || exit 1
rm -r *.ms
cp -r ../*.m* .

name="${obsid}_deep"

chan=$($CONTAINER ${PIIPPATH}/get_header_key.py ${obsid}.metafits "CENTCHAN")
freq=$(echo "${chan}*1.28" | bc -l)
scale=$(echo "${SCALE}/${chan}" | bc -l)

if $MULTISCALE; then
    mscale="-multiscale -multiscale-scale-bias 0.8 -multiscale-max-scales $MULTISCALE_MAX_SCALES"
else
    mscale=""
fi


mgain=0.8

bmaj2=$(echo "${bmaj}*3600" | bc -l)

# $CONTAINER wsclean -name ${obsid}_mock \
#     -use-wgridder -pol I \
#     -scale ${scale:0:8} \
#     -size ${IMSIZE} ${IMSIZE} \
#     -weight ${weight} \
#     -data-column ${CALIBRATION_COLUMN} \
#     -abs-mem 64 \
#     -j 7 \
#     -temp-dir /nvmetmp/ \
#     -log-time \
#     -channels-out 4 \
#     -join-channels \
#     -no-mf-weighting \
#     ${obsid}.ms

$CONTAINER wsclean -name ${obsid}_mock \
    -size ${IMSIZE} ${IMSIZE} \
    -niter 300000 \
    -nmiter 7 \
    -auto-threshold 1 \
    -auto-mask 3 \
    -pol I \
    -padding 1.5 \
    -weight  $weight \
    -scale ${scale:0:8} \
    -abs-mem 64 \
    -j 7 \
    -temp-dir /nvmetmp/ \
    -mgain ${mgain} \
    -log-time \
    -channels-out 4 \
    -join-channels \
    -no-mf-weighting \
    -apply-primary-beam \
    -mwa-path ${BEAM_PATH} \
    -datacolumn ${CALIBRATION_COLUMN} \
    -use-wgridder  \
    -save-weights \
    -minuv-l ${IMAGE_MINUV} \
    $mscale ${obsid}.ms

rms=$(${CONTAINER} $PIIPPATH/get_std_of_image.py ${obsid}_mock-MFS-residual.fits -p)
clip=$(echo "${rms}*3" | bc -l)

# $CONTAINER ${PIIPPATH}/mock_halo.py ${peak} ${redshift} -i ${obsid}_mock-image.fits -a 0.0 -x0 $ra -y0 $dec -R ${size} -m $med -o ${obsid}_mock-model.fits -w -B ${bmaj} -c 2.5
$CONTAINER ${PIIPPATH}/mock_halo.py ${peak} ${redshift} \
    -i ${obsid}_mock-*-image.fits \
    -a ${alpha} \
    -x0 $ra \
    -y0 $dec \
    -R ${size} \
    -m $med \
    -w \
    -f ${freq} \
    -c 2.0

$CONTAINER ${PIIPPATH}/ms_set_column_zero.py ${obsid}.ms MODEL_DATA


$CONTAINER wsclean -name ${obsid}_mock \
    -use-wgridder -pol I \
    -scale ${scale:0:8} \
    -size ${IMSIZE} ${IMSIZE} \
    -weight natural \
    -data-column ${CALIBRATION_COLUMN} \
    -predict \
    -abs-mem 64 \
    -j 7 \
    -channels-out 4 \
    ${obsid}.ms

$CONTAINER wsclean -name ${obsid}_mock_MODEL \
    -size ${IMSIZE} ${IMSIZE} \
    -niter 1 \
    -threshold $clip \
    -pol I \
    -padding 1.5 \
    -weight $weight \
    -scale ${scale:0:8} \
    -abs-mem 64 \
    -j 7 \
    -temp-dir /nvmetmp/ \
    -log-time \
    -channels-out 4 \
    -no-mf-weighting \
    -join-channels \
    -apply-primary-beam \
    -mwa-path ${BEAM_PATH} \
    -datacolumn MODEL_DATA \
    -use-wgridder  \
    -save-weights \
    -minuv-l ${IMAGE_MINUV} \
    $mscale ${obsid}.ms


# $CONTAINER subtrmodel -datacolumn $CALIBRATION_COLUMN -r $modelfile ${obsid}.ms

# $CONTAINER ${PIIPPATH}/ms_add_columns.py ${obsid}.ms ${CALIBRATION_COLUMN} MODEL_DATA
# $CONTAINER ${PIIPPATH}/ms_add_column.py ${obsid}.ms ${CALIBRATION_COLUMN} ${obsid}.ms MODEL_DATA

cp ${obsid}_mock-MFS-model.fits ${obsid}_halo${peak}Jy${size}kpc.fits
cp ${obsid}_mock_MODEL-MFS-image-pb.fits ${obsid}_halo${peak}Jy${size}kpc_image-pb.fits 
cp ${obsid}_mock_MODEL-MFS-dirty.fits ${obsid}_halo${peak}Jy${size}kpc_dirty.fits
cp ${obsid}_mock_MODEL-MFS-image.fits ${obsid}_halo${peak}Jy${size}kpc_image.fits
cp ${obsid}_mock_MODEL-MFS-residual.fits ${obsid}_halo${peak}Jy${size}kpc_residual.fits
cp ${obsid}_mock_MODEL-MFS-residual-pb.fits ${obsid}_halo${peak}Jy${size}kpc_residual-pb.fits

$CONTAINER $PIIPPATH/fits_add_images.py ${obsid}_mock-MFS-image-pb.fits ${obsid}_halo${peak}Jy${size}kpc_image.fits \
    -o ${obsid}_data_halo${peak}Jy${size}kpc_image.fits

# harsh cleanup
rm *mock*-000?-*.fits
rm -r *.ms
rm *mock*model*.fits
rm *mock*psf*.fits
rm *mock*residual*.fits
rm *mock*dirty*.fits
rm *mock*image.fits


#! /bin/bash

#SBATCH --account=mwasci
#SBATCH --partition=workq
#SBATCH --time=24:00:00
#SBATCH --export=NONE

source ${PIIPPATH}piipconfig.cfg
source ${PIIPPATH}piipfunctions.sh
source ${PIIPPATH}piiphelp.sh

USEOLD=false
NOEXTRA=false
SOURCE_FINDER="aegean"

usage () {
    echo "usage: $(basename "$0") -i INDIR " \
                                 "-o OUTDIR " \
                                 "-O OBSID_LIST " \
                                 "-r ROBUST " \
                                 "-I IMSIZE " \
                                 "-c CHANNEL " \
                                 "-s SELECTION " \
                                 "-P PREFIX_NAME " \
                                 "-S SUFFIX_NAME " \
                                 "-n IMAGE_NAME " \
                                 "[-R RA] " \
                                 "[-D DEC] " \
                                 "[-U/--useold] " \
                                 "[-f/--freqMHz FREQMHZ] "
    1>&2
    exit 1
}

opts=$(getopt \
       -o :hi:o:O:r:I:c:s:P:S:n:R:D:UNf: \
       --long help,indir:,outdir:,obsid_list:,robust:,imsize:,channel:,selection:,prefix_name:,suffix_name:,image_name:,ra:,dec:,useold,noextra,freqMHz: \
       --name "$(basename "$0")" \
       -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage ;;
        -i|--indir) INDIR="$2" ; shift 2 ;;
        -o|--outdir) OUTDIR=$2 ; shift 2 ;;
        -O|--obsid_list) OBSID_LIST="$2" ; shift 2 ;;
        -r|--robust) ROBUST=$2 ; shift 2 ;;
        -s|--selection) SELECTION="$2" ; shift 2 ;;
        -I|--imsize) IMSIZE=$2 ; shift 2 ;;
        -c|--channel) CHANNEL=$2 ; shift 2 ;;
        -P|--prefix_name) PREFIX_NAME=$2 ; shift 2 ;;
        -S|--suffix_name) SUFFIX_NAME=$2 ; shift 2 ;;
        -n|--image_name) IMAGE_NAME=$2 ; shift 2 ;;
        -R|--ra) RA=$2 ; shift 2 ;;
        -D|--dec) DEC=$2 ; shift 2 ;;
        -U|--useold) USEOLD=true ; shift ;;
        -N|--noextra) NOEXTRA=true ; shift ;;
        -f|--freqMHz) FREQ=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR: "; usage ;;
    esac
done

check_required "--indir" "-i" $INDIR
check_required "--obsid_list" "-O" $OBSID_LIST
check_required "--robust" "-r" $ROBUST
check_required "--selection" "-s" $SELECTION
check_required "--imsize" "-I" $IMSIZE
check_required "--channel" "-c" $CHANNEL
check_required "--prefix_name" "-P" $PREFIX_NAME
check_required "--suffix_name" "-S" $SUFFIX_NAME

if [ -z ${FREQ} ]; then
    get_frequency_from_channel $CHANNEL
fi

if [ ! -e ${OUTDIR} ]; then
    echo "ERROR: missing output directory."
    exit 1
fi

basename=${PREFIX_NAME}_C${CHANNEL}_r${ROBUST}${SUFFIX_NAME}

cd ${OUTDIR}

if [ -e ${basename}.fits ] && $USEOLD; then

    echo "INFO: mosaic already made!"
    exit 0

else

    if $USEOLD; then
        useold="-U"
    else
        useold=""
    fi

    if $NOEXTRA; then
        extra_dir=""
    else
        extra_dir="--extra_dir /image_r${ROBUST}/"
    fi

    ${PIIPPATH}stackshots.py -o ${OBSID_LIST} \
                  -i ${INDIR} \
                  -b _${FREQ}MHz_r${ROBUST}${IMAGE_NAME} \
                  --outbase ${basename} \
                  -c ${RA} ${DEC} \
                  -I ${IMSIZE} \
                  -s ${SELECTION} \
                  -p SIN \
                  -CBW ${extra_dir} \
                  ${useold}

    BANE ${basename}.fits

    ${PIIPPATH}mosaic_plot.py ${basename}.fits \
                   ${basename}_psfmap.fits \
                   ${basename}_rms.fits \
                   ${basename}_sumBeamI.fits    


    ${PIIPPATH}scale_mosaics.sh ${basename}.fits

fi

exit 0
#! /usr/bin/env python

import numpy as np
import sys

from datetime import datetime
from casacore import tables

from argparse import ArgumentParser

import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


from matplotlib import rc
# Without this the default font will have issues with MNRAS (being type 3)
rc('font', **{'family':'serif', 'serif':['Times'], 'weight':'medium'})
rc('text', usetex=True)
params = {"text.latex.preamble": [r"\usepackage{siunitx}", \
          r"\sisetup{detect-family = true}"]}
plt.rcParams.update(params)

mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['ytick.direction'] = 'in'


def get_amplitudes(ms):
    """
    """

    MeasurementSet = tables.table(ms, readonly=True)
    nrows = MeasurementSet.nrows()

    data = MeasurementSet.col("DATA")
    flag = MeasurementSet.col("FLAG")

    ant1 = MeasurementSet.col("ANTENNA1")
    ant2 = MeasurementSet.col("ANTENNA2")

    cross_baselines = []
    unique_baselines = []
    for i in range(nrows):
        if ant1[i] != ant2[i]:
            cross_baselines.append(i)
        if "{}-{}".format(ant1[i], ant2[i]) not in unique_baselines:
            unique_baselines.append("{}-{}".format(ant1[i], ant2[i]))

    nbases = len(unique_baselines)

    data_xxr = np.array([data[i].real[:, 0] for i in cross_baselines])
    data_yyr = np.array([data[i].real[:, 3] for i in cross_baselines])

    flag_xxr = np.array([flag[i][:, 0] for i in cross_baselines])
    flag_yyr = np.array([flag[i][:, 3] for i in cross_baselines])

    for i in range(len(data_xxr)):

        data_xxr[i][np.where(flag_xxr[i] == True)] = np.nan
        data_yyr[i][np.where(flag_yyr[i] == True)] = np.nan

    del flag_xxr, flag_yyr

    return data_xxr, data_yyr, nbases


def plot_amplitudes(xx, yy, outname, nbases, xx_color="red", yy_color="blue",
                    chans=768, text="", tsteps=3):
    """
    """

    size = (10, 8)
    axes = [0.15, 0.1, 0.83, 0.88]

    

    ntsteps = len(xx) / nbases
    print nbases
    print ntsteps

    j = 0

    for t in range(0, ntsteps):

        fig = plt.figure(figsize=size)
        ax1 = plt.axes(axes)

        ax1.set_xlim([1, len(xx[0])+1])
        ax1.set_ylim([0, 1650])

        j += t*nbases

        for i in range(j, j+nbases):

            chans = range(len(xx[i]))
            ax1.plot(chans, xx[i], c=xx_color, mec="none", ms=0.7, marker="s", 
                     alpha=0.7, ls="", mfc=xx_color, fillstyle="full")
            ax1.plot(chans, yy[i], c=yy_color, mec="none", ms=0.7, marker="s", 
                     alpha=0.7, ls="", mfc=yy_color, fillstyle="full")

        legend_elements = \
            [Line2D([0], [0], marker="s", color="red", label="XX", 
                    markerfacecolor="red", markersize="8.", markeredgecolor="none", 
                    ls=""),
             Line2D([0], [0], marker="s", color="blue", label="YY", 
                    markerfacecolor="blue", markersize="8.", markeredgecolor="none", 
                    ls="")]

        legend = ax1.legend(handles=legend_elements, loc="upper right", 
                            shadow=False, fancybox=False, frameon=True)
        legend.get_frame().set_edgecolor("black")

        ax1.text(0.1, 0.95, text, horizontalalignment="left", 
                 verticalalignment="center", transform=ax1.transAxes)

        ax1.set_xlabel("Channel", fontsize=24.)
        ax1.set_ylabel("\"Amplitude\"", fontsize=24.)
        ax1.tick_params(axis="both", which="both", labelsize=20, \
                        pad=7.)

        plt.savefig(outname+"_{}.png".format(t), dpi=720, bbox_inches="tight")
        plt.close("all")


def main():
    """
    """


    ps = ArgumentParser()
    ps.add_argument("ms")
    ps.add_argument("-o", "--outname", default=None)
    ps.add_argument("-c", "--channels", default=768, type=int)
    ps.add_argument("-x", "--xx_color", default="red", type=str)
    ps.add_argument("-y", "--yy_color", default="blue", type=str)

    args = ps.parse_args()

    xx, yy, nbases = get_amplitudes(args.ms)

    if args.outname is None:
        args.outname = args.ms.replace(".ms", "_chan_amp.png")

    plot_amplitudes(xx, yy, args.outname, nbases, args.xx_color, args.yy_color)


if __name__ == "__main__":
    main()







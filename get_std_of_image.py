#! /usr/bin/env python

from argparse import ArgumentParser

from astropy.io import fits
import numpy as np

def get_value(image, func=np.nanstd):
    """Get STD (or other value) from image."""

    with fits.open(image) as f:
        value = func(np.squeeze(f[0].data))

    return value


def main():
    """
    """

    ps = ArgumentParser()
    ps.add_argument("image", help="Filepath to image.")
    ps.add_argument("-p", "--print_out", action="store_true")
    ps.add_argument("-M", "--max", action="store_true")

    args = ps.parse_args()

    if args.max:
        f = np.nanmax
    else:
        f = np.nanstd
    value = get_value(args.image, func=f)
    if args.print_out:
        print(value)

if __name__ == "__main__":
    main()





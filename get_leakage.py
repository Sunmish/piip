#! /usr/bin/env python


from argparse import ArgumentParser
import os

import numpy as np

from astropy.io import fits
from astropy.table import Table, vstack
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.wcs import WCS
from astropy.stats import sigma_clip

from tqdm import trange

class StokesData():
    def __init__(self, catalogue_i, image_p,
        # noise_p,
        zone_bmaj_factor=2.,
        ra_key="ra",
        dec_key="dec",
        multiplier=1.):
        """
        """

        if catalogue_i.lower().endswith(".fits"):
            table_format = "fits"
        elif catalogue_i.lower().endswith(".vot") or catalogue_i.lower().endswith(".xml"):
            table_format = "votable"
        else:
            table_format = None

        self.catalogue_i = Table.read(catalogue_i, format=table_format)
        self.image_p = fits.open(image_p)
        self.header = self.image_p[0].header
        self.isolation_zone = self.image_p[0].header["BMAJ"]*zone_bmaj_factor
        self.wcs = WCS(self.header).celestial
        self.data = np.squeeze(self.image_p[0].data)
        self.multiplier = multiplier
        # self.noise = np.squeeze(fits.getdata(noise_p))
        self.noise = np.nanstd(sigma_clip(np.abs(self.data), sigma=3, maxiters=5))
        print("Stokes V median map value: {}".format(
            self.noise
        ))
        try:
            self.coords = SkyCoord(
                ra=self.catalogue_i[ra_key]*u.deg,
                dec=self.catalogue_i[dec_key]*u.deg
            )
        except Exception:
            self.coords = SkyCoord(
                ra=self.catalogue_i[ra_key],
                dec=self.catalogue_i[dec_key]
            )


    def get_isolated(self):
        idx, seps, _ = self.coords.match_to_catalog_sky(self.coords, 
            nthneighbor=2
        )
        isolated = np.where(seps.value > self.isolation_zone)[0]
        print("Isolated fraction: {}".format(
            len(isolated) / len(self.catalogue_i) 
        ))
        self.catalogue_i = self.catalogue_i[isolated]
        self.coords = self.coords[isolated]


    def get_compact(self, 
        ratio=(0.83, 1.2), 
        int_flux="int_flux",
        peak_flux="peak_flux"
        ):
        self.peak_flux = peak_flux
        self.int_flux = int_flux
        over_cond = self.catalogue_i[int_flux] / self.catalogue_i[peak_flux] > ratio[0]
        under_cond = self.catalogue_i[int_flux] / self.catalogue_i[peak_flux] < ratio[1]
        compact = np.where(over_cond & under_cond)[0]
        self.catalogue_i = self.catalogue_i[compact]
        self.coords = self.coords[compact]

    def get_xycoords(self):
        self.x, self.y = self.wcs.all_world2pix(self.coords.ra, self.coords.dec, 0)
        self.x = self.x.astype(int)
        self.y = self.y.astype(int)


    def get_leakage(self, local_rms="local_rms", sigma=10,
        near_edge_box=20,
        nolog=False):
        self.local_rms = local_rms
        self.sigma = sigma

        p_over_i = []
        pols = []
        rmses = []


        if not nolog:
            pbar = trange(len(self.coords))
        else:
            pbar = range(len(self.coords))
        for i in pbar:

            try:
                # Check if near a beam edge:
                if np.isnan(
                    self.data[
                        self.y[i]-near_edge_box:self.y[i]+near_edge_box, 
                        self.x[i]-near_edge_box:self.x[i]+near_edge_box
                    ].flatten()).any():
                    raise ValueError
            
                cutout = self.data[self.y[i]-1:self.y[i]+1, self.x[i]-1:self.x[i]+1].flatten()
                # noise_cutout = self.noise[self.y[i]-1:self.y[i]+1, self.x[i]-1:self.x[i]+1].flatten()
                pol = cutout[np.nanargmax(np.abs(cutout))]
                # rms = np.nanmedian(noise_cutout)
            except (IndexError, ValueError):
                pol = np.nan 
                rms = np.nan

            if self.catalogue_i[self.peak_flux][i] / self.catalogue_i[local_rms][i] > sigma:
                pi = pol / (self.catalogue_i[self.peak_flux][i] * self.multiplier)
            else:
                pi = np.nan

            p_over_i.append(pi)
            pols.append(pol)
            rmses.append(self.noise)

        self.pols = np.asarray(pols)
        self.p_over_i = np.asarray(p_over_i)
        self.rms = np.asarray(rmses)

    def get_table_leakage(self, outname):
        self.catalogue_i.add_columns(
            cols=[self.x, self.y, self.p_over_i, self.pols, self.rms],
            names=["p_x", "p_y", "p_over_i", "pol", "p_rms"],
        )
        self.catalogue_i = self.catalogue_i[np.where(np.isfinite(self.catalogue_i["p_over_i"]))[0]]
        self.catalogue_i.write(outname, overwrite=True)


def stokes_leakage(catalogue, pol_image, outname, 
    sigma=10,
    ra_key="ra",
    dec_key="dec",
    local_rms="local_rms",
    int_flux="int_flux",
    peak_flux="peak_flux",
    zone_bmaj_factor=2.,
    multiplier=1.,
    nolog=False
    ):

    stokes = StokesData(
        catalogue_i=catalogue,
        image_p=pol_image,
        # noise_p=pol_noise,
        ra_key=ra_key,
        dec_key=dec_key,
        zone_bmaj_factor=zone_bmaj_factor,
        multiplier=multiplier
        )
    stokes.get_isolated()
    stokes.get_compact(int_flux=int_flux, peak_flux=peak_flux)
    stokes.get_xycoords()
    stokes.get_leakage(local_rms=local_rms, sigma=sigma, nolog=nolog)
    stokes.get_table_leakage(outname)

def get_args():
    ps = ArgumentParser()
    ps.add_argument("catalogue")
    ps.add_argument("pol_image")
    # ps.add_argument("noise")
    ps.add_argument("-o", "--outname", default=None)
    ps.add_argument("-s", "--sigma", default=10, type=int)
    ps.add_argument("--local_rms", default="local_rms")
    ps.add_argument("-r", "--ra_key", default="ra")
    ps.add_argument("-d", "--dec_key", default="dec")
    ps.add_argument("-F", "--int_flux", default="int_flux")
    ps.add_argument("-P", "--peak_flux", default="peak_flux")
    ps.add_argument("-z", "--zone", default=2., type=float)
    ps.add_argument("-m", "--multiplier", default=1., type=float)
    ps.add_argument("--nologger", action="store_true")
    args = ps.parse_args()
    if args.outname is None:
        args.outname = ".".join(args.catalogue.split()[:-1]) + "_leakage.fits"
    return args

def cli(args):
    stokes_leakage(
        catalogue=args.catalogue, 
        pol_image=args.pol_image, 
        # pol_noise=args.noise,
        outname=args.outname, 
        sigma=args.sigma,
        ra_key=args.ra_key,
        dec_key=args.dec_key,
        local_rms=args.local_rms,
        int_flux=args.int_flux,
        peak_flux=args.peak_flux,
        zone_bmaj_factor=args.zone,
        multiplier=args.multiplier,
        nolog=args.nologger
    )

if __name__ == "__main__":
    cli(get_args())


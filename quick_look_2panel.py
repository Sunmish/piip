#! /usr/bin/env python

import os

import numpy as np

from argparse import ArgumentParser
from astropy.io import fits
from astropy.wcs import WCS
from astropy.stats import sigma_clip

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt


def main():

    ps = ArgumentParser(description="Get a quick look at a simple FITS image. "
                                    " This is NOT in world coordinates.")
    ps.add_argument("image", nargs=2)
    ps.add_argument("outname")
    ps.add_argument("-m", "--vmin", type=float, default=-0.1)
    ps.add_argument("-M", "--vmax", type=float, default=1.)
    ps.add_argument("-c", "--cmap", type=str, default="viridis")
    ps.add_argument("-i", "--inset", action="store_true")
    ps.add_argument("-C", "--coords", nargs=2, type=float, default=None)
    ps.add_argument("-s", "--size", type=int, default=500)
    ps.add_argument("-r", "--rms", action="store_true")

    args = ps.parse_args()

    data = np.squeeze(fits.getdata(args.image[0]))
    data2 = np.squeeze(fits.getdata(args.image[1]))
    wcs = WCS(fits.getheader(args.image[0])).celestial
    wcs2 = WCS(fits.getheader(args.image[1])).celestial

    if args.coords is not None:
        
        y, x = wcs.all_world2pix(args.coords[0], args.coords[1], 0)
        irange_x = np.linspace(x-args.size//2, x+args.size//2, args.size)
        irange_y = np.linspace(y-args.size//2, y+args.size//2, args.size)
        data = data[int(irange_x[0]):int(irange_x[-1]),
                    int(irange_y[0]):int(irange_y[-1])]

    
    fig = plt.figure(figsize=(20, 10))
    ax1 = plt.axes([0, 0, 0.5, 1])
    ax2 = plt.axes([0.5, 0., 0.5, 1.0])
    ax1.imshow(data, vmin=args.vmin, vmax=args.vmax, cmap=args.cmap, origin="lower")
    ax2.imshow(data2, vmin=args.vmin, vmax=args.vmax, cmap=args.cmap, origin="lower")
    for ax in [ax1, ax2]:
        ax.set_xlabel("")
        ax.set_ylabel("")
        ax.set_xticks([])
        ax.set_yticks([])

    datas = [data, data2]

    if args.inset:

        
        axes = [
            plt.axes([0.99-(0.2/2.)-0.5, 0.01, 0.2/2., 0.2]),
            plt.axes([0.99-(0.2/2.), 0.01, 0.2/2., 0.2])
        ]
        
        for n in range(2):
            # Plots a small inset of around the brightest part of image.
            indices = np.where(datas[n] == np.nanmax(datas[n][
                int(0.2*datas[n].shape[0]):int(0.8*datas[n].shape[0]),
                int(0.2*datas[n].shape[1]):int(0.8*datas[n].shape[1])
            ]))
            i, j = indices[0][0], indices[1][0]
            irange = np.array(range(int(max(0, i-0.02*datas[n].shape[-2])),
                        int(min(i+0.02*datas[n].shape[-2], datas[n].shape[-2]))))
            jrange = np.array(range(int(max(0, j-0.02*datas[n].shape[-1])),
                        int(min(j+0.02*datas[n].shape[-1], datas[n].shape[-1]))))
            
            axes[n].imshow(datas[n][int(irange[0]):int(irange[-1])-1, int(jrange[0]):int(jrange[-1])-1], 
                    vmin=args.vmin, vmax=args.vmax, cmap=args.cmap,
                    origin="lower")
        
            axes[n].set_xlabel("")
            axes[n].set_ylabel("")
            axes[n].set_xticks([])
            axes[n].set_yticks([])

            for spine in axes[n].spines.values():
                spine.set_edgecolor("white")
        


    ax1.text(0.02, 0.98, os.path.basename(args.image[0]), 
             horizontalalignment="left",
             verticalalignment="top", 
             transform=ax1.transAxes,
             color="white", 
             backgroundcolor="black",
             fontsize=20.)
    ax2.text(0.02, 0.98, os.path.basename(args.image[1]), 
             horizontalalignment="left",
             verticalalignment="top", 
             transform=ax2.transAxes,
             color="white", 
             backgroundcolor="black",
             fontsize=20.)


    if args.rms:

        axes = [ax1, ax2]

        for n in range(2):
            rms = np.nanstd(sigma_clip(datas[n][
                int(0.2*datas[n].shape[0]):int(0.8*datas[n].shape[0]),
                int(0.2*datas[n].shape[1]):int(0.8*datas[n].shape[1])],
                sigma=3, maxiters=5)
            )*1000.

            axes[n].text(0.02, 0.02, "rms = {:.2f} mJy/beam".format(round(rms, 2)), 
                horizontalalignment="left",
                verticalalignment="bottom", 
                transform=axes[n].transAxes,
                color="white",
                backgroundcolor="black", 
                fontsize=20.)


    fig.savefig(args.outname, dpi=144, bbox_inches="tight")


if __name__ == "__main__":
    main()
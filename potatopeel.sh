#!/bin/bash

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

SCALE=0.55
CUT=1.0
PERC=0.9
PERC_SUBTRACT=0.9
DATA_COLUMN="DATA"
PEEL_USE_AO=false
PEEL_BRIGGS=0.0
PEEL_SOLINT="120 20"
PEEL_CALMODE="ap ap"
PEEL_AUTOMASK="10 10"
CAL_MINUV=100
CAL_MAXUV=1900
IMAGE_MINUV=0
TMP_DIR=
HEAVY_CLEANUP=false
USE_JOB_FOR_TMP=true
ra=
dec=

PEEL_LOBES=false
PEEL_LOBES_PERCENT=0.2
POTATO_CONTAINER=

start_time=$(date +%s)

opts=$(getopt \
     -o :ht:T:O:m:M:d:f:c:D:r:LNP:z:y: \
     --long help,threshold1:,threshold2:,obsid:,minuv:,maxuv:,modeldir:,modelfile:,cut:,data_column:,reduce:,lobes,nolobes,percent:,ra:,dec: \
     --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piippeel
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done


# after configuration file loading:
set -x

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piippeel ;;
        -t|--threshold1) threshold1=$2 ; shift 2 ;;
        -T|--threshold2) threshold2=$2 ; shift 2 ;;
        -O|--obsid) obsid=$2 ; shift 2 ;;
        -m|--minuv) CAL_MINUV=$2 ; shift 2 ;;
        -M|--maxuv) CAL_MAXUV=$2 ; shift 2 ;;
        -d|--modeldir) MODELS=$2 ; shift 2 ;;
        -f|--modelfile) modelfile=$2 ; shift 2 ;;
        -c|--cut) CUT=$2 ; shift 2 ;;
        -D|--data_column) DATA_COLUMN=$2 ; shift 2 ;;
        -r|--reduce) PERC=$2 ; shift 2 ;;
        -L|--lobes) PEEL_LOBES=true; shift ;;
        -N|--nolobes) PEEL_LOBES=false ; shift ;;
        -P|--percent) PERCENT=$2 ; shift 2 ;;
        -z|--ra) ra=$2; shift 2 ;;
        -y|--dec) dec=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR:" ; usage_piippeel ;;
    esac
done


check_required "--obsid" "-O" $obsid
check_required "--threshold1" "-t" $threshold1
check_obsid ${obsid}

if [ -z ${TMP_DIR} ]; then
    TMP_DIR="./"
    USING_TMP=false
else
    TMP_DIR="${TMP_DIR}/"
    if $USE_JOB_FOR_TMP; then
        TMP_DIR="${TMP_DIR}/${SLURM_JOB_ID}/"
    fi 
    USING_TMP=true
fi

if [ -z ${threshold2} ]; then
    threshold2=$(echo "0.25*${threshold1}" | bc -l)
fi

if [ -z ${POTATO_CONTAINER} ]; then
    # annoying numba requirements make things annoying so easier to have separate containers
    POTATO_CONTAINER=${CONTAINER}
fi
if [ -z ${POTATO_CONTAINER} ]; then
    pcontainer=""
else
    # /home required because CASA 
    # CASA inside container? 
    if ${USING_TMP}; then
        pcontainer="singularity exec -B /home/,${TMP_DIR} ${POTATO_CONTAINER}"
    else
        pcontainer="singularity exec -B /home/ ${POTATO_CONTAINER}"
    fi
fi

if [ -z ${CONTAINER} ]; then
    container=""
else
    # /home required because CASA 
    # CASA inside container? 
    if ${USING_TMP}; then
        container="singularity exec -B /home/,${TMP_DIR} ${CONTAINER}"
    else
        container="singularity exec -B /home/ ${CONTAINER}"
    fi
fi  

chan=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "CENTCHAN")
freq=$(echo "${chan}*1.28" | bc -l)
scale=$(echo "${SCALE}/${chan}" | bc -l)
fov=$(echo "${scale}*${IMSIZE}/2" | bc -l)

minuvm=$($container ${PIIPPATH}/get_baselines_from_lambdas.py ${freq} ${CAL_MINUV})
maxuvm=$($container ${PIIPPATH}/get_baselines_from_lambdas.py ${freq} ${CAL_MAXUV})

PERC=1.0
peel_rad=$(echo "${PERC}*${IMSIZE}*${scale}/2.0" | bc -l)

subtract_rad=$(echo "${PERC_SUBTRACT}*${IMSIZE}*${scale}/2.0" | bc -l)

if [ -z ${ra} ]; then
    ra=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits RA)
fi
if [ -z ${dec} ]; then
    dec=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits DEC)
fi
radec_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${ra} ${dec} -d "hms")

if ! $PEEL_LOBES; then
    if [ -z ${modelfile} ]; then
        models=$(ls ${MODELS}/model-*.txt)
        $container peel_suggester -f ${models[@]} \
                       -m ${obsid}.metafits \
                       -t ${threshold1} \
                       -r ${subtract_rad} \
                       -T ${threshold2} \
                       -R ${subtract_rad} \
                       --pnt "${ra}" "${dec}" \
                       > peel.log

    else
        $container peel_suggester -f ${modelfile} \
                       -m ${obsid}.metafits \
                       -t ${threshold1} \
                       -r ${subtract_rad} \
                       -T ${threshold2} \
                       -R ${subtract_rad} \
                       --pnt "${ra}" "${dec}" \
                       > peel.log

    fi

    peeled_sources=""


    if (( $( wc -l < peel.log ) )); then 

        while read line; do

            # Sources are peeled from brightest to faintest
            # No consideration is taken for bright runner-up sources

            # Peel out source:
            source_model=$(echo "$line" | awk '{print $2}')
            source_name=$(echo "$line" | awk '{print $1}')
            source_flux=$(echo "$line" | awk '{print $3}')
            source_ra=$(echo "$line" | awk '{print $4}')
            source_dec=$(echo "$line" | awk '{print $5}')
            source_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${source_ra} ${source_dec} -d "hms")

            method=$(echo "$line" | awk '{print $6}')

            source_flux=$($container ${PIIPPATH}/get_scaled_flux.py 88 -a "-1.0" -f ${freq} -S ${source_flux} -i)
            maxIntervals=$($container ${PIIPPATH}/ms_get_max_intervals.py ${TMP_DIR}${obsid}.ms)

            if [ ${source_flux%.*} -gt 500 ]; then
                timeSteps=1
                timesOut=${maxIntervals}
                chansOut=32
            elif [ ${source_flux%.*} -gt 250 ]; then
                chansOut=24
                timesOut=15
                timeSteps=6
            elif [ ${source_flux%.*} -gt 100 ]; then
                chansOut=16
                timesOut=10
                timeSteps=10
            elif [ ${source_flux%.*} -gt 50 ]; then
                chansOut=12
                timesOut=6
                timeSteps=15
            else
                chansOut=8
                timesOut=1
                timeSteps=999
            fi

            if [[ "${source_model}" == *"CygA"* ]] && [ "${method}" = "peel" ]; then
                echo "Peeling Cygnus A..."
                
                # image 2 degrees around Cygnus A
                image_size=$(echo "0.5/${scale}" | bc -l)
                image_size=${image_size%.*}

                # phase rotate to Cygnus A:
                ${pcontainer} ${PIIPPATH}/ms_rotate.py ${TMP_DIR}${obsid}.ms ${source_ra} ${source_dec} -c ${DATA_COLUMN}

                ${container} wsclean -name ${TMP_DIR}${obsid}_CygA_initial \
                    -size ${image_size} ${image_size} \
                    -pol xx,yy \
                    -niter 100000 \
                    -weight briggs ${PEEL_BRIGGS} \
                    -scale ${scale:0:8} \
                    -auto-mask 5 \
                    -auto-threshold 1 \
                    -padding 3 \
                    -use-wgridder \
                    -data-column ${DATA_COLUMN} \
                    -mgain 0.8 \
                    -nmiter 5 \
                    -channels-out ${chansOut} \
                    -join-channels \
                    -fit-spectral-pol 3 \
                    -j ${NCPUS} \
                    -minuv-l ${IMAGE_MINUV} \
                    -intervals-out ${timesOut} \
                    -temp-dir ${TMP_DIR} \
                    ${extraWSClean} ${TMP_DIR}${obsid}.ms

                minuv=$(${container} ${PIIPPATH}/get_baselines_from_lambdas.py ${freq} ${CAL_MINUV})
                
                ${container} calibrate -j ${NCPUS} \
                    -absmem ${ABSMEM} \
                    -minuv ${minuv} \
                    -t ${timeSteps} \
                    -ch 2 \
                    -datacolumn ${DATA_COLUMN} \
                    ${TMP_DIR}${obsid}.ms \
                    ${TMP_DIR}${obsid}_CygA.bin > ${obsid}_calibrate_CygA.log 2>&1

                check_solutions=
                check_solutions=$(${container} ${PIIPPATH}/get_calibration_solutions.py ${obsid} -N 0.4 --solution_file ${TMP_DIR}${obsid}_CygA.bin)

                if [ ! -z ${check_solutions} ]; then

                    ${pcontainer} ${PIIPPATH}/invert_solutions.py ${TMP_DIR}${obsid}_CygA.bin -o ${TMP_DIR}${obsid}_CygA_inv.bin

                    ${container} applysolutions -datacolumn "MODEL_DATA" -nocopy \
                        ${TMP_DIR}${obsid}.ms \
                        ${TMP_DIR}${obsid}_CygA_inv.bin
                else
                    echo "Bad solutions for CygA! Not peeling only subtracting."
                fi


                ${container} subtrmodel -usemodelcol \
                       -datacolumn ${DATA_COLUMN} \
                       "model" \
                       ${TMP_DIR}${obsid}.ms 


                ${container} ${PIIPPATH}/ms_set_column_zero.py ${TMP_DIR}${obsid}.ms MODEL_DATA

                ${pcontainer} ${PIIPPATH}/ms_rotate.py ${TMP_DIR}${obsid}.ms ${ra} ${dec}

                #TODO remove
                cp ${TMP_DIR}${obsid}_CygA_initial*.fits .

            else

                # until better source size calculations, set large size for CenA 
                if [[ "${source_name}" == *"Cen"* ]]; then
                    peel_fov=1.0
                else
                    peel_fov=0.2
                fi

                configFile="${source_name}_peel.cfg"
                $pcontainer peel_configuration.py "${configFile}" \
                    --image_size=${IMSIZE} \
                    --image_scale=${scale} \
                    --image_briggs=${PEEL_BRIGGS} \
                    --image_channels=4 \
                    --image_minuvl=0 \
                    --peel_size=1000 \
                    --peel_scale=${scale} \
                    --peel_channels=${chansOut} \
                    --peel_nmiter=7 \
                    --peel_minuvl=0 \
                    --peel_multiscale

                $pcontainer potato ${TMP_DIR}${obsid}.ms ${source_ra} ${source_dec} ${peel_fov} ${subtract_rad} \
                    --config ${configFile} \
                    --peel_solint ${PEEL_SOLINT} \
                    --peel_calmode ${PEEL_CALMODE}  \
                    --peel_automask ${PEEL_AUTOMASK} \
                    -minpeelflux ${threshold1} \
                    --name ${source_name} \
                    --refant 1 \
                    --direct_subtract \
                    --no_time \
                    --intermediate_peels \
                    --tmp ${TMP_DIR}

                # TODO uncomment
                # rm *afterpeel*
                # rm *forpeel*
                # rm *aftersubtract*
                # rm *forsubtract*
                # rm *0.0.initial*
                if ${USING_TMP}; then
                    cp ${TMP_DIR}*-MFS-image.fits ./
                    cp ${TMP_DIR}*-MFS-residual.fits ./
                fi 


            fi

            echo "INFO: ${source_name} ${source_model} ${method}" >> ${obsid}_peeled_sources.log
            peeled_sources+="${source_name}"

        done < peel.log
    fi

else

    # Find the brightest sidelobe(s) and remove it/them.

    # ra=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits RA)
    # dec=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits DEC)
    # radec_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${ra} ${dec} -d "hms")

    # $container get_beam_lobes ${obsid}.metafits -p ${PERCENT} -mS > ${obsid}_beamlobes.txt
    if [ ! -e ${obsid}_stokesI_sky.fits ]; then
        $container get_beam_image ${obsid}.metafits -o ${obsid}_stokesI_sky.fits
    fi

    ${container} get_beam_lobes ${obsid}_stokesI_sky.fits -S --pnt "${ra}" "${dec}" -p ${PEEL_LOBES_PERCENT} > ${obsid}_beamlobes.txt

    if (( $( wc -l < ${obsid}_beamlobes.txt ) )); then 

        i=0

        while read line; do

            ((i++))

            ra_beam=$(echo "$line" | awk '{print $2}')
            dec_beam=$(echo "$line" | awk '{print $3}')
            fov_beam=$(echo "$line" | awk '{print $4}')

            radec_hmsdms_beam=$($container ${PIIPPATH}/dd_hms_dms.py ${ra_beam} ${dec_beam} -d "hms")

            scale=$(echo "${SCALE}/${chan}" | bc -l)

            lobe_image_size=$(echo "1.2*${fov_beam}/${scale}" | bc -l)
            lobe_image_size=${lobe_image_size%.*}

            if [[ "$lobe_image_size" -gt "${IMSIZE}" ]]; then
                lobe_image_size=${IMSIZE}
            fi

            $container chgcentre ${TMP_DIR}${obsid}.ms ${radec_hmsdms_beam}


            if $MINW; then
                $container chgcentre -minw -shiftback ${obsid}.ms
                extraWSClean="-nwlayers-for-size 14000 14000"
            else
                extraWSClean="-use-wgridder"
            fi

            $container wsclean -name ${TMP_DIR}${obsid}_lobe${i} \
                    -size ${lobe_image_size} ${lobe_image_size} \
                    -niter 250000 \
                    -pol xx,yy \
                    -weight briggs ${PEEL_BRIGGS} \
                    -scale ${scale:0:8} \
                    -auto-mask 5 \
                    -auto-threshold 1 \
                    -padding 1.5 \
                    -abs-mem ${ABSMEM} \
                    -mgain 0.8 \
                    -data-column ${DATA_COLUMN} \
                    -channels-out 4 \
                    -join-channels \
                    -nmiter 4 \
                    -no-mf-weighting \
                    -j ${NCPUS} \
                    -minuv-l ${IMAGE_MINUV} \
                    -temp-dir ${TMP_DIR} \
                    ${extraWSClean} ${TMP_DIR}${obsid}.ms

            if [ ! -e ${obsid}_lobe${i}-MFS-XX-image${i}.fits ]; then
                cp ${TMP_DIR}${obsid}_lobe${i}*.fits .
            fi

            files=$(find . -type f -name "${obsid}_lobe${i}*.fits" -not \
                    \( -name "*MFS*-image.fits" \))
            for file in ${files[@]}; do
                if [ -e ${file} ]; then
                    rm ${file}
                fi
            done


            $container subtrmodel -usemodelcol \
                       -datacolumn ${DATA_COLUMN} \
                       "model" \
                       ${TMP_DIR}${obsid}.ms

            $container ${PIIPPATH}/ms_set_column_zero.py ${TMP_DIR}${obsid}.ms MODEL_DATA



        done < ${obsid}_beamlobes.txt

        
        $container chgcentre ${TMP_DIR}${obsid}.ms ${radec_hmsdms}

    fi

fi

if ${HEAVY_CLEANUP}; then
    rm ${TMP_DIR}*.fits
fi


end_time=$(date +%s)
duration=$(echo "${end_time}-${start_time}" | bc -l)
echo "INFO: Total time taken: ${duration}"

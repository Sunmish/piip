#! /usr/bin/env python

import numpy as np
import os
from subprocess import Popen
from astropy.io import fits
from argparse import ArgumentParser

def get_args():

    ps = ArgumentParser()
    ps.add_argument("image")
    ps.add_argument("-o", "--outname", default=None)
    ps.add_argument("-b", "--beam_clip", default=None, type=float)

    return ps.parse_args()


def make_weight_map(image, beam_clip=0.3, outname=None):

    if outname is None:
        outname = image.replace(".fits", "") + ".weight.fits"

    if not os.path.exists(image.replace(".fits", "_rms.fits")):

        Popen("BANE --cores 1 {}".format(image), shell=True).wait()
    
    if os.path.exists(image.replace(".fits", "_bkg.fits")):

        os.remove(image.replace(".fits", "_bkg.fits"))

    with fits.open(image.replace(".fits", "_rms.fits")) as f:

        if beam_clip is not None:
            
            beam_map = image.replace(".fits", "_beam.fits")
            if not os.path.exists(beam_map):
                Popen(
                    "get_beam_image {0}.metafits -o {1} -i {2}".format(
                        image[0:10], beam_map, image
                    ),
                    shell=True
                )
            with fits.open(beam_map) as g:

                g[0].data[np.where(g[0].data < beam_clip)] = np.nan
                
                weights = g[0].data**2 / f[0].data**2
            
        else:

            weights = 1./f[0].data**2

        fits.writeto(outname, weights, f[0].header, overwrite=True)

    
def cli(args):

    make_weight_map(
        image=args.image,
        outname=args.outname,
        beam_clip=args.beam_clip
    )


if __name__ == "__main__":
    cli(get_args())
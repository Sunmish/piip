#! /usr/bin/env python

from argparse import ArgumentParser
from casacore.tables import *

def multiply(msname, value, column="MODEL_DATA"):
    ms = table(msname, readonly=False, ack=False)
    col = ms.getcol(column)
    col *= value
    ms.putcol(column, col)
    ms.close()

def main():
    ps = ArgumentParser()
    ps.add_argument("msname")
    ps.add_argument("value", type=float)
    ps.add_argument("-c", "--column", default="MODEL_DATA")
    args = ps.parse_args()
    multiply(args.msname, args.value, args.column)

if __name__ == "__main__":
    main()


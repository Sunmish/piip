#! /usr/bin/env python

from argparse import ArgumentParser

import os

import numpy as np
from astropy.io import fits
from astropy.stats import sigma_clip


import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def get_args():
    ps = ArgumentParser()
    ps.add_argument("image1")
    ps.add_argument("image2")
    ps.add_argument("--min_rms", default=10)
    ps.add_argument("--solutions", type=str, nargs=2, default=None)
    return ps.parse_args()



def compare(image1, image2, min_rms=10, solutions=None):

    if not os.path.exists(image1) and os.path.exists(image2):
        image = image2
        index = 1
    elif os.path.exists(image1) and not os.path.exists(image2):
        image = image1
        index = 0
    elif not os.path.exists(image1) and not os.path.exists(image2):
        return "", "", ""
    else:
    

        data1 = np.squeeze(fits.getdata(image1))
        data2 = np.squeeze(fits.getdata(image2))

        try:
            rms1 = np.nanstd(sigma_clip(data1[
                int(0.2*data1.shape[0]):int(0.8*data1.shape[0]),
                int(0.2*data1.shape[1]):int(0.8*data1.shape[1])],
                sigma=3, maxiters=5)
            )*1000.
        except ValueError:
            rms1 = 1.e30

        try:
            rms2 = np.nanstd(sigma_clip(data2[
                int(0.2*data2.shape[0]):int(0.8*data2.shape[0]),
                int(0.2*data2.shape[1]):int(0.8*data2.shape[1])],
                sigma=3, maxiters=5)
            )*1000.
        except ValueError:
            rms2 = 1.e30


        logger.info("Original rmses: {}, {}".format(rms1, rms2))


        if rms1 < min_rms:
            rms1 = 1e30
        if rms2 < min_rms:
            rms2 = 1e30

        if rms1 > rms2:
            image = image2
            index = 1
        else:
            image = image1
            index = 0


        logger.info("{} rms: {:.2f} mJy/beam".format(image1, rms1))
        logger.info("{} rms: {:.2f} mJy/beam".format(image2, rms2))
        logger.info("Selection: {}".format(image))
    if solutions is not None:
        solution = solutions[index]
        obsid = os.path.basename(solution)[0:10]
        logger.info("Solutions OBSID: {}".format(obsid))
        # return obsid
    else:
        # return image
        obsid = os.path.basename(image)[0:10]
    dirname = os.path.dirname(image)
    
    return image, obsid, dirname



def cli(args):
    image, obsid, dirname = compare(args.image1, args.image2, solutions=args.solutions)
    print("{} {} {}".format(image, obsid, dirname))


if __name__ == "__main__":
    cli(get_args())


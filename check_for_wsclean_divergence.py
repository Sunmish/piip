#! /usr/bin/env python

import numpy as np
from argparse import ArgumentParser

def get_args():
    ps = ArgumentParser()
    ps.add_argument("logfile")
    ps.add_argument("-f", "--factor", default=0.1, type=float)
    return ps.parse_args()

def check_log1(logfile):
    noise_estimates = []
    major_iterations = 0
    with open(logfile) as f:
        lines = f.readlines()
        for line in lines:
            if "Estimated standard deviation of background noise:" in line:
                bits = line.split()
                pval = float(bits[-2])
                punit = bits[-1]
                if punit == "mJy":
                    pval /= 1000.
                elif punit == "KJy":
                    pval *= 1000.
                elif punit == "uJy":
                    pval /= 1.e6
                noise_estimates.append(pval)


    diverge_iteration = None
    for i in range(1, len(noise_estimates)):
        major_iterations += 1
        if noise_estimates[i] > noise_estimates[i-1]:
            diverge_iteration = i
        
    if diverge_iteration is not None:
        print(f"Diverged {diverge_iteration} (out of {major_iterations} major iteraitons)")
        return True
    else:
        return False

def check_log2(logfile, factor=0.1):
    peak_values = []
    minor_iterations = []
    major_iteration = 0
    major_iterations = []
    with open(logfile) as f:
        lines = f.readlines()
        for line in lines:
            if "Estimated standard deviation of background noise:" in line:
                major_iteration += 1
            if "Iteration" in line and "scale" in line:
                bits = line.split()
                pval = float(bits[-4])
                punit = bits[-3]
                if punit == "mJy":
                    pval /= 1000.
                elif punit == "KJy":
                    pval *= 1000.
                elif punit == "uJy":
                    pval /= 1.e6
                peak_values.append(abs(pval))
                minor_iterations.append(int(bits[-9].replace(",", "")))
                major_iterations.append(major_iteration)
            
    
    peak_values = np.asarray(peak_values)
    diverge_iteration = None
    current_max_peak = 1.e30
    for i in range(1, len(peak_values)):
        if peak_values[i] < current_max_peak:
            current_max_peak = peak_values[i]
        elif (peak_values[i] > (np.asarray(peak_values[0:i-1])+factor*np.asarray(peak_values[0:i-1]))).any():
            diverge_iteration = major_iterations[i]

    if diverge_iteration is not None:
        print(f"Diverged {diverge_iteration} (out of {major_iteration} major iteraitons)")
        return True
    else:
        return False    

def cli(args):
    check = check_log1(args.logfile)
    if not check:
        check = check_log2(args.logfile, args.factor)


if __name__ == "__main__":
    cli(get_args())
    


    
    



    

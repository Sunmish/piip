#! /bin/bash

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi
echo "INFO: setting PIIPPATH=${PIIPPATH}"

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh


BIND_PATH=

opts=$(getopt \
    -o :hd:O:f:c: \
    --long help,downloads:,obsid:,field:,chan: \
    --name "$(basename "$0")" \
    -- "$@" 
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piipdownload
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipdownload ;;
        -d|--downloads) download_directory=$2 ; shift 2 ;;
        -O|--obsid) obsid=$2 ; shift 2 ;;
        -f|--field) field=$2 ; shift 2 ;;
        -c|--chan) chan=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR:" ; usage_piipdownload ;;
    esac
done

check_required "--downloads" "-d" $download_directory
check_required "--obsid" "-O" $obsid

if [ -z ${CONTAINER} ]; then
    container=""
else
    if [ ! -z ${BIND_PATH} ]; then
        BIND_PATH="-B ${BIND_PATH}"
    fi
    container="singularity exec ${BIND_PATH} ${CONTAINER}"
fi  

$container mwa_client -l > ${obsid}_jobs.list
while read line; do
	case "$line" in
		"Ready for Download"*"${obsid}"*)
			jobid=$(echo "$line" | awk '{print $6}')
		;;
	esac
done < ${obsid}_jobs.list


if [ -z $jobid ]; then
	echo "$obsid not ready for download."
	exit 1 
fi	

if [ -e ${obsid} ]; then
    rm -rf ${obsid}
fi

mkdir ${obsid} && \
    $container mwa_client -d $obsid -w $jobid && \
    mv ${obsid}/${obsid}_${jobid}_ms.tar ${obsid}/${obsid}_ms.tar

rm ${obsid}_jobs.list

if [ ! -z "${field}" ] && [ ! -z "${chan}" ] && [ ! -z ${DBFILE} ]; then
    d_format=`date +%F:%H:%M:%S`
    $container ${PIIPPATH}/field_manager.py -m "update_snapshot_status" \
    -d ${DBFILE} \
    --selection=None \
    --field=$field \
    --chan=$chan \
    --status="staged-1-[${d_format}]" \
    --overwrite \
    --obsid=$obsid
fi

exit 0


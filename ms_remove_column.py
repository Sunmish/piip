#! /usr/bin/env python

import sys
from casacore import tables

def remove_column(ms, col):
    """Remove column from MS."""

    MeasurementSet = tables.table(ms, readonly=False)
    if col in MeasurementSet.colnames():
        MeasurementSet.removecols(col)
    else:
        print("WARNING: no {} column in {}".format(col, ms))
    MeasurementSet.flush()
    MeasurementSet.close()


def main():
    """
    """

    ms = sys.argv[1]
    col = sys.argv[2]

    remove_column(ms, col)


if __name__ == "__main__":
    main()





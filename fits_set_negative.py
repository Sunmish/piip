#! /usr/bin/env python

from argparse import ArgumentParser

from astropy.io import fits


def get_args():
    ps = ArgumentParser(description="Multiply image by -1.")
    ps.add_argument("image")
    ps.add_argument("outname")
    return ps.parse_args()


def negate(image, outname):
    with fits.open(image) as f:
        f[0].data *= -1
        fits.writeto(outname, f[0].data, f[0].header, overwrite=True)
    

def cli(args):
    negate(args.image, args.outname)


if __name__ == "__main__":
    cli(get_args())


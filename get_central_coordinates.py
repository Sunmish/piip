#! /usr/bin/env python

from argparse import ArgumentParser
import numpy as np
from astropy.io import fits


def mean_coordinates(ra, dec):
    """
    """

    # TODO: replace with https://docs.astropy.org/en/stable/stats/circ.html
    mean_dec = np.mean(dec)
    mean_ra = np.mean(
        np.degrees(
                np.arctan2(
                    (np.sum(np.sin(np.radians(ra)))/len(ra)), 
                    (np.sum(np.cos(np.radians(ra)))/len(ra))
                )
            )
        )

    return mean_ra, mean_dec

def extract_coordinates(metafits):
    """
    """

    ra = np.array([fits.getheader(m)["RA"] for m in metafits])
    dec = np.array([fits.getheader(m)["DEC"] for m in metafits])

    return ra, dec


def main():
    ps = ArgumentParser()
    ps.add_argument("metafits", nargs="*")
    args = ps.parse_args()

    ra, dec = extract_coordinates(args.metafits)
    mean_ra, mean_dec = mean_coordinates(ra, dec)
    print("{} {}".format(mean_ra, mean_dec))

if __name__ == "__main__":
    main()



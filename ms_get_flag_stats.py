#! /usr/bin/env python

from __future__ import print_function, division
from datetime import datetime
from casacore.tables import taql
from argparse import ArgumentParser

def cli():
    ps = ArgumentParser()
    ps.add_argument("msname")
    return ps.parse_args()

def get_flags(msname):
    t1 = datetime.now()
    unflagged = taql("CALC sum([select nfalse(FLAG) from $msname])")
    flagged = taql("CALC sum([select ntrue(FLAG) from $msname])")
    flag_perc = flagged[0] / (unflagged[0] + flagged[0])
    print("{} % flagged: {:.3f}%".format(t1, flag_perc * 100))

if __name__ == "__main__":
    get_flags(cli().msname)




#! /usr/bin/env python

from argparse import ArgumentParser

from casacore.tables import table

def get_args():

    ps = ArgumentParser()
    ps.add_argument("ms")
    ps.add_argument("column_in")
    ps.add_argument("column_out")
    ps.add_argument("-M", "--other_ms", default=None)

    return ps.parse_args()



def replace_columns(ms1, col1, col2, ms2=None):
    """Replace col1 from ms1 with col2 from ms2."""

    if ms2 is None:
        ms2 = ms1
    t1 = table(ms1, readonly=False, ack=False)
    t2 = table(ms2, readonly=True, ack=False)
    if col2 not in t2.colnames():
        t1.close()
        t2.close()
        print("WARNING: {} not in {}".format(col2, ms2))
    elif col1 not in t1.colnames():
        t1.close()
        t2.close()
        print("WARNING: {} not in {}".format(col1, ms1))
    else:
        c2 = t2.getcol(col2)
        t1.putcol(col1, c2)
        t1.close()
        t2.close()


def cli(args):

    replace_columns(
        ms1=args.ms,
        ms2=args.other_ms,
        col2=args.column_in,
        col1=args.column_out
    )


if __name__ == "__main__":
    cli(get_args())
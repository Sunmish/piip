#! /bin/bash -l

check_required() {
    if [ -z $3 ]; then
        echo "ERROR: $1 is a required option [$2]."
        exit 1
    fi
}

check_image_complete() {
    bmajTemp=$(${PIIPPATH}get_header_key.py $1 BMAJ)
    if python -c "exit(0 if ${bmajTemp} == 0.0 else 1)"; then
        imageBad=true
    else
        imageBad=false
    fi
}

check_obsid() {
    obsid_temp=$1
    if [[ ${#obsid_temp} -ne 10 ]]; then
        echo "ERROR: ${obsid_temp} not correctly specified. "
        echo "It is probably empty or commented out."
        exit 1
    else
        echo "INFO: Processing ${obsid_temp}."
    fi
}


minAngular() {
    # Calculate uv-distance corresponding to an angular scale.
    wl=$(python -c "from astropy.constants import c; wl = c.value/(${1}*1.e6); print('{:.2f}'.format(wl))")
    uv=$(python -c "import numpy as np; uv = ${wl} / np.radians(${2}); print('{:.1f}'.format(uv))")
}


get_frequency_from_channel() {
  chan=$1
  declare -A CHAN2FREQ

  CHAN2FREQ["69"]="088"
  CHAN2FREQ["93"]="118"
  CHAN2FREQ["121"]="154"
  CHAN2FREQ["145"]="185"
  CHAN2FREQ["169"]="216"
  # subbands:
  CHAN2FREQ["69-0"]="076"
  CHAN2FREQ["69-1"]="084"
  CHAN2FREQ["69-2"]="092"
  CHAN2FREQ["69-3"]="099"
  CHAN2FREQ["93-0"]="107"
  CHAN2FREQ["93-1"]="115"
  CHAN2FREQ["93-2"]="122"
  CHAN2FREQ["93-3"]="130"
  CHAN2FREQ["121-0"]="143"
  CHAN2FREQ["121-1"]="150"
  CHAN2FREQ["121-2"]="158"
  CHAN2FREQ["121-3"]="166"
  CHAN2FREQ["145-0"]="173"
  CHAN2FREQ["145-1"]="181"
  CHAN2FREQ["145-2"]="189"
  CHAN2FREQ["145-3"]="196"
  CHAN2FREQ["169-0"]="204"
  CHAN2FREQ["169-1"]="212"
  CHAN2FREQ["169-2"]="220"
  CHAN2FREQ["169-3"]="227"

  FREQ=${CHAN2FREQ[$chan]}
  
}

get_69_subbands() {
  subband=$1
  declare -A SUBBAND2FREQ69

  SUBBAND2FREQ69["MFS"]="088"
  SUBBAND2FREQ69["0000"]="076"
  SUBBAND2FREQ69["0001"]="084"
  SUBBAND2FREQ69["0002"]="092"
  SUBBAND2FREQ69["0003"]="099"

  subfreq=${SUBBAND2FREQ69[${subband}]}
}

get_93_subbands() {
    subband=$1
    declare -A SUBBAND2FREQ93

    SUBBAND2FREQ93["MFS"]="118"
    SUBBAND2FREQ93["0000"]="107"
    SUBBAND2FREQ93["0001"]="115"
    SUBBAND2FREQ93["0002"]="122"
    SUBBAND2FREQ93["0003"]="130"

    subfreq=${SUBBAND2FREQ93[${subband}]}
}

get_121_subbands() {
    subband=$1
    declare -A SUBBAND2FREQ121

    SUBBAND2FREQ121["MFS"]="154"
    SUBBAND2FREQ121["0000"]="143"
    SUBBAND2FREQ121["0001"]="150"
    SUBBAND2FREQ121["0002"]="158"
    SUBBAND2FREQ121["0003"]="166"

    subfreq=${SUBBAND2FREQ121[${subband}]}

}

get_145_subbands() {
    subband=$1
    declare -A SUBBAND2FREQ145

    SUBBAND2FREQ145["MFS"]="185"
    SUBBAND2FREQ145["0000"]="173"
    SUBBAND2FREQ145["0001"]="181"
    SUBBAND2FREQ145["0002"]="189"
    SUBBAND2FREQ145["0003"]="196"

    subfreq=${SUBBAND2FREQ145[${subband}]}

}

get_169_subbands() {
    subband=$1
    declare -A SUBBAND2FREQ169

    SUBBAND2FREQ169["MFS"]="216"
    SUBBAND2FREQ169["0000"]="204"
    SUBBAND2FREQ169["0001"]="212"
    SUBBAND2FREQ169["0002"]="220"
    SUBBAND2FREQ169["0003"]="227"

    subfreq=${SUBBAND2FREQ169[${subband}]}

}

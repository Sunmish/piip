#! /usr/bin/env python

# Use sqlite3 to manage fields for diffuse cluster emission project.

from __future__ import print_function, division


from subprocess import Popen

import sqlite3
import os
import time
import re
try:
    from astropy.time import Time
except ImportError:
    raise

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s", \
                    level=logging.DEBUG)

try:
    import numpy as np
except ImportError:
    pass

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\x1b[1m'
   UNDERLINE = '\033[4m'
   END = '\x1b[0m'
   ESCAPE = '\u001b[7m '
   NEND = '\u001b[0m '


def _join(*values):
    return ";".join(str(v) for v in values)
def color_text(s, c, base=30):
    template = "\x1b[{0}m{1}\x1b[0m"
    t = _join(base+8, 2, _join(*c))
    return template.format(t, s)
def len_color_text(s, ansi="x1b"):
    """
    """
    return len(s.replace(ansi, ""))


class Colors:

    # Greys
    White = (255, 255, 255)
    Black = (0, 0, 0)
    DimGrey = (105, 105, 105)
    DarkGrey = (169, 169, 169)
    LightGrey = (211, 211, 211)

    # Reds
    Magenta = (255,0,255)
    Pink = (255, 192, 203)
    LightCoral = (240, 128, 128)
    Red = (255, 0, 0)
    FireBrick = (178, 34, 34)

    # Yellows
    LemonChiffon = (255, 250, 205)
    Yellow = (255, 255, 0)
    Orange = (255, 165, 0)
    OrangeRed = (255, 69, 0)

    # Purples 
    RebeccaPurple = (102, 51, 153)
    MediumPurple = (147, 112, 219)
    Violet = (238, 130, 238)
    Plum = (221, 160, 221)

    # Blues
    Blue = (0, 0, 255)
    DogderBlue = (30, 144, 255)
    Cyan = (0, 255, 255)
    LightCyan = (224, 255, 255)

    # Greens
    Green = (0, 128, 0)
    SpringGreen = (0, 255, 127)
    MediumSpringGreen = (0, 250, 154)
    Lime = (0, 255, 0)

    # Incomplete = Black
    # Queued: 





STATUS_COLORS = {
    "incomplete": Colors.Black,
    "queued": Colors.DimGrey,
    "cottering": Colors.DarkGrey,
    "preprocessed": Colors.LightGrey, 
    "staged": Colors.White,
    "start-skycal": Colors.FireBrick,
    "skycal": Colors.LightCoral,
    "start-preimage": Colors.OrangeRed,
    "preimage": Colors.Orange,
    "selfcal": Colors.Yellow,
    "noselfcal": Colors.OrangeRed,
    "start-image-6": Colors.MediumPurple,
    "imaging-6": Colors.Magenta,
    "imaging-7": Colors.Magenta,
    "start-prep-8": Colors.Green,
    "prep-8": Colors.Lime,
    "stored": Colors.DogderBlue,
    "downloaded": Colors.Cyan,
    "nodownload": Colors.DarkGrey,
    "archive": Colors.MediumSpringGreen,
    "bad": Colors.Black,
    "corrected": Colors.Lime,
    "correcting": Colors.Green,
    "start-peel": Colors.FireBrick,
    "peel": Colors.LightCoral
}


debug=True

class DataBase():
    """Class for database handling."""

    def __init__(self, name="fields.db"):

        self.filename = name
        self.con = sqlite3.connect(name, timeout=120.0)
        self.cur = self.con.cursor()


        # Status hierarchy. Lower-ranked statuses are overwritten.
        self.STATUS_RANKS = {"incomplete": 0,
                             "queued": 1.1,
                             "cottering": 1.2,
                             "preprocessed": 1.3,
                             "staged": 1.4,
                             "skycal": 10,
                             "peel": 10,
                             "preimage": 10,
                             "selfcal": 10,
                             "imaging": 10,
                             "image": 10,
                             "correcting": 10,
                             "stored": 10,
                             "prep": 10,
                             "downloaded": 10,
                             "nodownload": 10,
                             "archive": 10,
                             "fail-4": -1,
                             "fail-5": -1,
                             "fail-6r0": -1,
                             "fail-7r0": -1,
                             "fail-7r1": -1,
                             "fail-6r1": -1,
                             "bad": -2,
                             "corrected": 10}

        self.CAL_PREF = {"none": -1,
                         "CenA": 0,
                         "PKS2356-61": 1,
                         "HerA": 8,
                         "PicA": 10,
                         "3C444": 11,
                         "HydA": 9,
                         }



    def initiate(self):
        """Create a new database."""

        self.cur.execute('''CREATE TABLE IF NOT EXISTS fields
                         (field INT, status TEXT)''')

        self.con.commit()


    def upate_field_status(self, field, status):
        """Update status of a field.

        E.g., from 'incomplete' to 'in-progress' to 'complete'.
        """

        self.cur.execute("UPDATE fields SET status = ? WHERE field = ?", (status, field))
        self.con.commit()


    def add_field(self, field, chan, obslist):
        """Add table describing the specific field (per frequency setting).

        This is a list of OBS IDs and flagged/to-flag tiles.
        """


        self.cur.execute("DROP TABLE IF EXISTS 'field{field}_{chan}'".format(field=field, chan=chan))
        self.cur.execute("CREATE TABLE 'field{field}_{chan}' (id INT, snapshot TEXT, flags " \
                         "TEXT, status TEXT, comments TEXT, calname TEXT, calid INT, 'r0.0' TEXT, 'r1.0' TEXT)".format(field=field, chan=chan))

        obs_ids, flag_tiles = read_obslist(obslist)

        for i in range(len(obs_ids)):
            self.cur.execute("INSERT OR REPLACE INTO 'field{field}_{chan}' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)".format(\
                             field=field, chan=chan), (i+1, obs_ids[i], flag_tiles[i], \
                             "incomplete", "", "", "", "", ""))

        self.con.commit()

    def update_flags(self, field, chan, selection, tiles, mode="update"):
        """Update array tile flags based on selection."""

        if not isinstance(tiles, list):
            tiles = [tiles]

        for i in range(len(selection)):

            if mode == "update":
                self.cur.execute("SELECT * FROM 'field{0}_{1}' WHERE id = ?".format( \
                                 field, chan), (selection[i],))
                dat = self.cur.fetchone()[2]
                if dat == "":
                    flags = tiles[i]
                else:
                    flags = dat+","+tiles[i]
            else:
                flags = tiles[i]

            self.cur.execute("UPDATE 'field{0}_{1}' SET flags = ? WHERE id = ?".format( \
                             field, chan), (flags, selection[i]))

        self.con.commit()



    def add_comments(self, field, chan, selection, comments, overwrite=True, obsid=None):
        """Add comment to snapshot."""


        if obsid is None:


            if not isinstance(selection, list):
                selection = [selection]
            if not isinstance(comments, list) and isinstance(comments, str):
                comments = [comments]
            if len(comments) < len(selection) and len(comments) == 1:
                comments = comments*len(selection)

            for i in range(len(selection)):

                self.cur.execute("UPDATE 'field{field}_{chan}' SET comments = ? WHERE id = ?".format( \
                                 field=field, chan=chan), (comments[i], selection[i]))

        else:

            self.cur.execute("UPDATE 'field{field}_{chan}' SET comments = ? WHERE snapshot = ?".format( \
                                     field=field, chan=chan), (comments, obsid))



    def add_calibrators(self, field, chan, obsids, calbase):
        """Add calibrators to the comments column."""

        con = sqlite3.connect(calbase)

        used_calibrators = []
        all_obsids, all_calibrators = [], []

        try:
            self.cur.execute("ALTER TABLE 'field{}_{}' ADD calname TEXT".format(field, chan))
        except sqlite3.OperationalError:
            pass
        try:
            self.cur.execute("ALTER TABLE 'field{}_{}' ADD calid INT".format(field, chan))
        except sqlite3.OperationalError:
            pass

        try:
            
            cur = con.cursor()

            for i in range(len(obsids)):
                if "#" not in obsids[i]:
                    obsid = int(obsids[i])
                    cur.execute("SELECT * FROM calibrators WHERE obsid BETWEEN "
                                "{} and {} AND chan = ?".format(obsid-43200, obsid+43200), (chan,))
                    dat = cur.fetchall()

                    calname = "none"
                    calid = ""
                    for j in range(len(dat)):
                        full_calname = dat[j][2]
                        if full_calname not in self.CAL_PREF.keys():
                            for key in self.CAL_PREF.keys():
                                if key in full_calname:
                                    full_calname = key


                        if self.CAL_PREF[full_calname] >= self.CAL_PREF[calname]:
                            calname = full_calname
                            calid = dat[j][0]

                    # index = (np.abs(np.asarray([dat[j][0] for j in range(len(dat))]) - int(obsid))).argmin()
                    # calid = dat[index][0]
                    # calname = dat[index][2]

                    self.cur.execute("UPDATE 'field{}_{}' SET calname = ? WHERE snapshot = ?".format(field, chan), 
                                     (calname, obsid))
                    self.cur.execute("UPDATE 'field{}_{}' SET calid = ? WHERE snapshot = ?".format(field, chan), 
                                     (calid, obsid))
                    
                    if calid not in used_calibrators:
                        used_calibrators.append(calid)
                    all_calibrators.append(calid)
                    all_obsids.append(obsid)

        except:
            raise
        finally:
            con.close() 
    
        return used_calibrators, all_calibrators, all_obsids

    def update_calibrator(self, field, chan, obsid, calid, calname):
        """Update a calibrator entry for a given OBSID."""

        self.cur.execute(
            "UPDATE 'field{}_{}' SET calname = ? WHERE snapshot = ?".format(
                field, chan), 
            (calname, obsid)
        )

        self.cur.execute(
            "UPDATE 'field{}_{}' SET calid = ? WHERE snapshot = ?".format(
                field, chan), 
            (calid, obsid)
        )



    def get_calibrator(self, field, chan, obsid):
        """
        """
        self.cur.execute("SELECT * FROM 'field{field}_{chan}' WHERE snapshot = {obsid}".format(
            field=field, chan=chan, obsid=int(obsid)
        ))
        dat = self.cur.fetchall()[0]
        print("{calname} {calid}".format(calname=dat[5], calid=dat[6]))


    def add_imaging(self, field, chan, obsids):

        """
        """

        try:
            self.cur.execute("ALTER TABLE 'field{}_{}' ADD 'r0.0' TEXT".format(field, chan))
        except sqlite3.OperationalError:
            raise
        try:
            self.cur.execute("ALTER TABLE 'field{}_{}' ADD 'r1.0' TEXT".format(field, chan))
        except sqlite3.OperationalError:
            raise






    def update_status(self, field, chan, selection, status="complete", overwrite=False, 
                      obsid=None):
        """Update the status of a field."""

        self.cur.execute("SELECT * FROM 'field{field}_{chan}'".format(field=field, \
                         chan=chan))

        dat = self.cur.fetchall()
        ids = [v[0] for v in dat[:]]
        obsids = [v[1] for v in dat[:]]
        statuses = [v[3] for v in dat[:]]

        robust=None

        if obsid is None:

            for i in selection:

                full_status = statuses[ids.index(i)]

                for s in self.STATUS_RANKS.keys():
                    
                    if s in full_status:
                        status_old = s
                    if s in status:
                        status_new = s
                    if (s not in full_status) and (s not in status):
                        status_old = "incomplete"

                if status_old in self.STATUS_RANKS.keys():
                    rank_old = self.STATUS_RANKS[status_old]
                else:
                    rank_old = 0
                rank_new = self.STATUS_RANKS[status_new]

                if not overwrite:

                    if rank_new >= rank_old:
                        overwrite = True
                    else:
                        overwrite = False

                if ("imag" in status or "correct" in status) and "pre" not in status:
                    if "r1.0" in status:
                        robust = "'r1.0'"
                    elif "r0.0" in status:
                        robust = "'r0.0'"

                    if robust is not None:

                        try:
                            self.cur.execute("ALTER TABLE 'field{}_{}' ADD {} TEXT".format(field, chan, robust))
                        except sqlite3.OperationalError:
                            pass

                        # for i in range(72):
                            # try:
                        self.cur.execute("UPDATE 'field{field}_{chan}' SET {robust} = ? WHERE id = ?".format( \
                                         field=field, chan=chan, robust=robust), (status, i))
                        #     except sqlite3.OperationalError:
                        #         time.sleep(5)
                        #     else:
                        #         break
                        # else:
                        #     raise sqlite3.OperationalError

                    
                if overwrite:
                    # for i in range(72):
                        # try:
                    self.cur.execute("UPDATE 'field{field}_{chan}' SET status = ? WHERE id = ?".format( \
                                     field=field, chan=chan), (status, i))
                        # except sqlite3.OperationalError:
                            # time.sleep(5)
                        # else:
                            # break
                    # else:
                        # raise sqlite3.OperationalError
                else:
                    # for i in range(72):
                        # try:
                    self.cur.execute("UPDATE 'field{field}_{chan}' SET status = ? WHERE id = ? and status = ?".format( \
                                 field=field, chan=chan), (status, i, "incomplete"))
                    #     except sqlite3.OperationalError:
                    #         time.sleep(5)
                    #     else:
                    #         break
                    # else:
                    #     raise sqlite3.OperationalError

        else:

            full_status = statuses[obsids.index(obsid)]
            
            for s in self.STATUS_RANKS.keys():
                if s in full_status:
                    status_old = s
                if s in status:
                    status_new = s
                if (s not in full_status) and (s not in status):
                    status_old = "incomplete"

            if status_old in self.STATUS_RANKS.keys():
                rank_old = self.STATUS_RANKS[status_old]
            else:
                rank_old = 0
            rank_new = self.STATUS_RANKS[status_new]

            if not overwrite:

                if rank_new >= rank_old:
                    overwrite = True
                else:
                    overwrite = False

            if ("imag" in status or "correct" in status) and "pre" not in status and \
                (("r1.0" in status) or ("r0.0" in status)):
                if "r1.0" in status:
                    robust = "'r1.0'"
                elif "r0.0" in status:
                    robust = "'r0.0'"


                if robust is not None:


                    try:
                        self.cur.execute("ALTER TABLE 'field{}_{}' ADD {} TEXT".format(field, chan, robust))
                    except sqlite3.OperationalError:
                        pass


                    # for i in range(72):
                        # try:
                    self.cur.execute("UPDATE 'field{field}_{chan}' SET {robust} = ? WHERE snapshot = ?".format( \
                                     field=field, chan=chan, robust=robust), (status, obsid))
                    # self.cur.execute("UPDATE field{field}_{chan} SET {robust} = ? WHERE id = ?".format( \
                    #                      field=field, chan=chan, robust=robust), (status, i))
                    #     except sqlite3.OperationalError:
                    #         time.sleep(5)
                    #     else:
                    #         break
                    # else:
                    #     raise sqlite3.OperationalError

            if overwrite:
                # for i in range(72):
                    # try:
                self.cur.execute("UPDATE 'field{field}_{chan}' SET status = ? WHERE snapshot = ?".format( \
                                 field=field, chan=chan), (status, obsid))
                #     except sqlite3.OperationalError:
                #         time.sleep(5)
                #     else:
                #         break
                # else:
                #     raise sqlite3.OperationalError


            else:
                # for i in range(72):
                    # try:
                self.cur.execute("UPDATE 'field{field}_{chan}' SET status = ? WHERE snapshot = ? and status = ?".format( \
                                 field=field, chan=chan), (status, obsid, "incomplete"))
                #     except sqlite3.OperationalError:
                #         time.sleep(5)
                #     else:
                #         break
                # else:
                #     raise sqlite3.OperationalError


        self.con.commit()


    def write_obslist(self, field, chan, outname=None, outdir="./"):
        """Write out OBS ID list with flag tiles.

        """

        if outname is None:
            outname = outdir+"FIELD{field}_{chan}.txt".format(field=field, chan=chan)

        with open(outname, "w+") as f:

            self.cur.execute("SELECT * FROM field{field}_{chan}".format( \
                             field=field, chan=chan))
            dat = self.cur.fetchall()

            for i in range(len(dat)):

                f.write("{obsid} {flag}\n".format(obsid=dat[i][1], \
                        flag=dat[i][2]))





    def get_field(self, field, chan, verbose=True, show_imaging=False):
        """Get field table."""

        def line_formatter(id_, obsid, status, comment=""):
            
            line = "ID: {:3} | OBS: {:10} | {:39} | {:12}".format(
                id_, obsid, status, comment)
            return line

        def simple_formatter(id_, obsid, status):
            id_str = "{: >3}".format(id_)
            for s in STATUS_COLORS.keys():
                if status.startswith(s):
                    # id_str = color_text(id_str, STATUS_COLORS[s])
                    # obsid_str = color_text(str(obsid), STATUS_COLORS[s])
                    status_str = color_text(status, STATUS_COLORS[s])
                # else:
                    # status_str = status
            line = color.BOLD + "ID:" + color.END + " {} |".format(id_str) \
                + color.BOLD + " OBS:" + color.END + " {:10} | {}".format(
                obsid, status_str
            )
            return line

        def cal_line_formatter(id_, obsid, status, calname, calid):
            line = "ID: {:3} | OBS: {:10} | {:.>39} | {:8} | {:10}".format(
                id_, obsid, status, calname, calid)
            return line

        def image_line_formatter(id_, obsid, status, r0, r1):

            original_length = len(status)
            original_max_length = 39


            if r0 is None or isinstance(r0, int):
                r0 = ""
            if r1 is None or isinstance(r1, int):
                r1 = ""

            if "[" in r0:
                r0 = r0.split("[")[0] + color.BOLD + "[" + r0.split("[")[-1] + color.END
            if "[" in r1:
                r1= r1.split("[")[0] + color.BOLD + "[" + r1.split("[")[-1] + color.END

            if "[" in status:
                status = status.split("[")[0] + color.BOLD + "[" + status.split("[")[-1] + color.END

            width = 39
            for s in STATUS_COLORS.keys():
                if status.startswith(s):
                    len_status = len(re.sub('\x1b\\[(K|.*?m)', '', status))
                    status = color_text(status, STATUS_COLORS[s])
                    # if len(status) > width:
                    width += (len(status) - len_status)
                        


            width1 = 39
            for s in STATUS_COLORS.keys():
                if r0.startswith(s):
                    len_status = len(re.sub('\x1b\\[(K|.*?m)', '', r0))
                    r0 = color_text(r0, STATUS_COLORS[s])
                    width1 += (len(r0) - len_status)


            width2 = 39
            for s in STATUS_COLORS.keys():
                if r1.startswith(s):
                    len_status = len(re.sub('\x1b\\[(K|.*?m)', '', r1))
                    r1 = color_text(r1, STATUS_COLORS[s])
                    width2 += (len(r1) - len_status)
            

            line = u"{:3} | {:10} | {:.>{width}} | {:.>{width1}} | {:.>{width2}}".format(
                id_, obsid, status, r0, r1, width=width, width1=width1, width2=width2)
            return line



        self.cur.execute("SELECT * FROM 'field{field}_{chan}'".format(field=field, \
                         chan=chan))

        dat = self.cur.fetchall()


        if verbose:
            for i in range(len(dat)):

                if i == 0:
                    tgps = Time(float(dat[i][1]), format="gps")
                    tiso = Time(tgps, format="iso")
                    print(color.BOLD + "DATE: {} ".format(tiso, "") + color.END)
                elif abs(float(dat[i-1][1])-float(dat[i][1])) > 21600.0:  # 6 hours
                    tgps = Time(float(dat[i][1]), format="gps")
                    tiso = Time(tgps, format="iso")
                    print(color.BOLD + "DATE: {} ".format(tiso, "") + color.END)

                if show_imaging:
                    print(
                        simple_formatter(
                            id_=dat[i][0], 
                            obsid=dat[i][1], 
                            status=dat[i][3]
                        )
                    )

                # if show_imaging and len(dat[0]) == 7:
                #     print(image_line_formatter(dat[i][0], dat[i][1], dat[i][3], dat[i][5], dat[i][6]))
                # elif show_imaging and len(dat[0]) == 8:
                #     print(image_line_formatter(dat[i][0], dat[i][1], dat[i][3], dat[i][7], ""))
                # elif show_imaging and len(dat[0]) > 8:
                #     print(image_line_formatter(dat[i][0], dat[i][1], dat[i][3], dat[i][7], dat[i][8]))
                # # elif len(dat[0]) == 5:
                # #     print(line_formatter(dat[i][0], dat[i][1], dat[i][3], dat[i][4]))
                else:
                    try:
                        print(cal_line_formatter(dat[i][0], dat[i][1], dat[i][3], dat[i][5], dat[i][6]))
                    except Exception:
                        raise
                        try:
                            print(image_line_formatter(dat[i][0], dat[i][1], dat[i][3], dat[i][5], dat[i][6]))
                        except Exception:
                            print(image_line_formatter(dat[i][0], dat[i][1], dat[i][3], dat[i][5], ""))
        else:
            return dat



    def close(self):
        """Commit then close database."""

        self.con.commit()
        self.con.close()




def read_obslist(obslist):
    """Read in OBS ID list.

    This may be simply a list of OBS IDs, and optionally a list of 
    tiles to flag per snapshot.
    """

    obs_ids, obs_flags = [], []

    with open(obslist, "r") as obs:

        lines = obs.readlines()
        for line in lines:
            bits = line.replace("\n", "").rstrip().split(" ")
            if len(bits) == 1:  # No flag tiles on this line.
                flags = ""
            elif len(bits) == 2:
                flags = bits[1]
            else:
                raise ValueError("OBS ID file line has too many items: \n" \
                                 "{0}".format(line))

            obs_ids.append(bits[0])
            obs_flags.append(flags)

    return obs_ids, obs_flags

def select_all(obs_ids):
    """Get selection of all obs ids."""

    selection = []
    for i in range(len(obs_ids)):
        selection.append(i+1)
    return selection


def backup(name="fields.db"):
    """Backup the databse.

    Read: make a copy with the date attached to it.
    """


    import shutil
    import datetime

    bkup = name[:-3]+"-"+str(datetime.date.today())+".db"

    shutil.copy2(name, bkup)


def convert_selection(selection):
    """Convert selection `1-10` to python list."""


    if "-" in selection:
        s1, s2 = selection.split("-")[0], selection.split("-")[1]
        return range(int(s1), int(s2)+1)
    else:
        return [int(selection)]


def check_required(*args):

    for arg in args:
        if arg is None:
            raise ValueError("An argument hasn't been set!")



if __name__ == "__main__":


    import optparse
    ps = optparse.OptionParser(description="Modify FIELD database entries.")

    ps.add_option("-m", "--mode", dest="mode", type=str, help="Mode of operation: [update_field_status, " \
                    "update_snapshot_flags, update_snapshot_status, add_snapshot_comments, " \
                    "update_flags_writeout, initiate_database, print_field, backup, merge_flags]")
    ps.add_option("-d", "--db", dest="db", type=str, help="Database name.", default="fields.db")    
    ps.add_option("--selection", dest="selection", type=str, help="Snapshot selection: e.g. '1-10'.", default=None)
    ps.add_option("-f", "--field", dest="field", type=str, help="Field selection.", default=None)
    ps.add_option("-c", "--chan", dest="chan", type=str, help="Channel number.", default=None)
    ps.add_option("--status", dest="status", type=str, help="Status of either field or snapshot, depending on mode. Will be applied to all in `selection`.", default=None)
    ps.add_option("--comments", dest="comments", type=str, help="Comment to add to snapshots. Will be applied to all in `selection`.", default=None)
    ps.add_option("--tiles", dest="tiles", type=str, help="Tiles to flag. E.g. '1,55,66,22'.", default=None)
    ps.add_option("--obslist", "--sdir", dest="sdir", type=str, help="Directory for OBS ID list files.", default=None)
    ps.add_option("--obsid", dest="obsid", type=str, help="Obsid.")
    ps.add_option("--overwrite", dest="overwrite", action="store_true", help="Switch on if wanting to overwrite flags.")
    ps.add_option("--calbase", dest="calbase", default=None, help="Calibrator database prepared with calbase.py")
    ps.add_option("--show_calibrators", action="store_false", dest="show_imaging", help="Print out calibrators instead of imaging progess.",
                  default=True)
    ps.add_option("--output_calibrators", action="store_true")
    ps.add_option("--calid", type=int, default=None,
        help="Update calibrator entry with this calibrator OBSID.")
    ps.add_option("--calname", type=str, default=None,
        help="Update calibrator entry with this calibrator name.")

    chans = [69, 93, 121, 145, 169]  # frequency = 1.28 * chan

    (args, leftover) = ps.parse_args()

    if os.path.exists(args.db) and args.mode == "initiate_database":
        os.remove(args.db)
    database = DataBase(args.db)

    try:

        if args.mode == "update_field_status":
            check_required(args.field, args.status)
            database.update_field_status(args.field, args.status)

        elif args.mode == "add_field":
            check_required(args.field, args.chan, args.sdir)
            # if not args.sdir.endswith("/"): args.sdir += "/"
            # obslist = args.sdir+"FIELD"+args.field+"_"+args.chan+".txt"
            obslist = args.sdir
            database.add_field(args.field, args.chan, obslist)

        elif args.mode == "find_calibrators":
            check_required(args.sdir, args.field, args.chan, args.calbase)
            obs_ids, flag_tiles = read_obslist(args.sdir)
            calibrators, all_calibrators, all_obsids = database.add_calibrators(
                args.field, args.chan, obs_ids, args.calbase
            )
            if args.output_calibrators:
                with open(args.sdir.replace(".txt", "_calibrators.txt"), "w+") as f:
                    for i in range(len(calibrators)):
                        f.write("{}\n".format(calibrators[i]))
                with open(args.sdir.replace(".txt", "_obscal.txt"), "w+") as f:
                    f.write("OBSID,CALID\n")
                    for i in range(len(all_calibrators)):
                        f.write("{},{}\n".format(all_obsids[i], all_calibrators[i]))

        elif args.mode == "get_calibrator":
            database.get_calibrator(
                field=args.field,
                chan=args.chan,
                obsid=args.obsid
            )
            
        elif args.mode == "add_imaging":
            check_required(args.sdir, args.field, args.chan)
            obs_ids, _ = read_obslist(args.sdir)
            database.add_imaging(args.field, args.chan, obs_ids)


        elif args.mode == "update_snapshot_status":
            check_required(args.field, args.chan, args.status)
            if args.obsid is None:
                selection = convert_selection(args.selection)
            else:
                selection = None
            database.update_status(args.field, args.chan, selection, args.status,
                                   overwrite=args.overwrite, obsid=args.obsid)

        elif args.mode == "update_snapshot_flags":
            check_required(args.field, args.chan, args.selection, args.tiles)
            selection = convert_selection(args.selection)
            database.update_flags(args.field, args.chan, selection, args.tiles, mode=args.overwrite)

        elif args.mode == "update_flags_writeout":
            check_required(args.field, args.chan, args.selection, args.tiles, args.sdir)
            selection = convert_selection(args.selection)
            if not args.sdir.endswith("/"): args.sdir += "/"
            database.update_flags(args.field, args.chan, selection, args.tiles, mode=args.overwrite)
            database.write_obslist(args.field, args.chan, outdir=args.sdir)

        elif args.mode == "add_snapshot_comments":
            check_required(args.field, args.chan, args.selection, args.comments)
            if args.obsid is None:
                selection = convert_selection(args.selection)
            else:
                selection = None
            database.add_comments(args.field, args.chan, selection, args.comments, 
                                  overwrite=args.overwrite, obsid=args.obsid)

        elif args.mode == "print_field":
            check_required(args.field, args.chan)
            database.get_field(args.field, args.chan, verbose=True, show_imaging=args.show_imaging)

        elif args.mode == "merge_flags":
            # Merger flags from snapshot file to database.
            check_required(args.field, args.chan, args.sdir)
            obslist = args.sdir+"FIELD"+args.field+"_"+args.chan+".txt"
            obs_ids, obs_flags = read_obslist(obslist)
            selection = select_all(obs_ids)
            database.update_flags(args.field, args.chan, selection, obs_flags, mode=args.overwrite)

        elif args.mode == "update_calibrator":
            check_required(
                args.field, args.chan, args.obsid, args.calid, args.calname
            )
            database.update_calibrator(
                field=args.field,
                chan=args.chan,
                obsid=args.obsid,
                calid=args.calid,
                calname=args.calname
            )

        elif args.mode == "backup":
            backup(args.name)

        elif args.mode == "initiate_database":
            database.initiate()

        else:
            raise ValueError("No mode of type '{mode}'. \n".format(mode=args.mode) + \
                             "`mode` must be one of [update_field_status, " \
                             "update_snapshot_flags, update_snapshot_status, add_snapshot_comments " \
                             "initiate_database]")


    except:
        raise
    finally:
        # Make sure we ALWAYS close the database. 
        database.close()

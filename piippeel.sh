#! /bin/bash -l

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

CUT=1.0
PERC=0.9
DATA_COLUMN="CORRECTED_DATA"
PEEL_USE_AO=false
PEEL_BRIGGS=2.0
CAL_MINUV=100
CAL_MAXUV=1900
IMAGE_MINUV=0

PEEL_LOBES=false
PERCENT=0.2

start_time=$(date +%s)

opts=$(getopt \
     -o :ht:T:O:m:M:d:f:c:D:r:LNP: \
     --long help,threshold1:,threshold2:,obsid:,minuv:,maxuv:,modeldir:,modelfile:,cut:,data_column:,reduce:,lobes,nolobes,percent: \
     --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piippeel
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done


# after configuration file loading:
set -x

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piippeel ;;
        -t|--threshold1) threshold1=$2 ; shift 2 ;;
        -T|--threshold2) threshold2=$2 ; shift 2 ;;
        -O|--obsid) obsid=$2 ; shift 2 ;;
        -m|--minuv) CAL_MINUV=$2 ; shift 2 ;;
        -M|--maxuv) CAL_MAXUV=$2 ; shift 2 ;;
        -d|--modeldir) MODELS=$2 ; shift 2 ;;
        -f|--modelfile) modelfile=$2 ; shift 2 ;;
        -c|--cut) CUT=$2 ; shift 2 ;;
        -D|--data_column) DATA_COLUMN=$2 ; shift 2 ;;
        -r|--reduce) PERC=$2 ; shift 2 ;;
        -L|--lobes) PEEL_LOBES=true; shift ;;
        -N|--nolobes) PEEL_LOBES=false ; shift ;;
        -P|--percent) PERCENT=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR:" ; usage_piippeel ;;
    esac
done

check_required "--obsid" "-O" $obsid
check_required "--threshold1" "-t" $threshold1
check_obsid ${obsid}

if [ -z ${CONTAINER} ]; then
    container=""
else
    container="singularity run ${CONTAINER}"
fi  

if [ -z ${threshold2} ]; then
    threshold2=$(echo "0.25*${threshold1}" | bc -l)
fi

chan=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "CENTCHAN")
freq=$(echo "${chan}*1.28*1000000" | bc -l)
scale=$(echo "${SCALE}/${chan}" | bc -l)

minuvm=$($container ${PIIPPATH}/get_baselines_from_lambdas.py ${freq} ${CAL_MINUV})
maxuvm=$($container ${PIIPPATH}/get_baselines_from_lambdas.py ${freq} ${CAL_MAXUV})

PERC=1.0
peel_rad=$(echo "${PERC}*${IMSIZE}*${scale}/2.0" | bc -l)
PERC_SUBTRACT=0.9
subtract_rad=$(echo "${PERC_SUBTRACT}*${IMSIZE}*${scale}/2.0" | bc -l)

ra=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits RA)
dec=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits DEC)
radec_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${ra} ${dec} -d "hms")

if ! $PEEL_LOBES; then
    if [ -z ${modelfile} ]; then
        models=$(ls ${MODELS}/model-*.txt)
        $container peel_suggester -f ${models[@]} \
                       -m ${obsid}.metafits \
                       -t ${threshold1} \
                       -r ${peel_rad} \
                       -T ${threshold2} \
                       -R ${subtract_rad} \
                       > peel.log

    else
        $container peel_suggester -f ${modelfile} \
                       -m ${obsid}.metafits \
                       -t ${threshold1} \
                       -r ${peel_rad} \
                       -T ${threshold2} \
                       -R ${subtract_rad} \
                       > peel.log

    fi




    peeled_sources=""


    if (( $( wc -l < peel.log ) )); then 

        while read line; do

            # Sources are peeled from brightest to faintest
            # No consideration is taken for bright runner-up sources

            # Peel out source:
            source_model=$(echo "$line" | awk '{print $2}')
            source_name=$(echo "$line" | awk '{print $1}')
            source_flux=$(echo "$line" | awk '{print $3}')
            source_ra=$(echo "$line" | awk '{print $4}')
            source_dec=$(echo "$line" | awk '{print $5}')
            source_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${source_ra} ${source_dec} -d "hms")

            method=$(echo "$line" | awk '{print $6}')

            $container chgcentre ${obsid}.ms ${source_hmsdms}

            if [ "${method}" = "peel" ]; then

                echo "INFO: peeling ${source_name} using model ${source_model}"

                if ! $PEEL_USE_AO; then


                    if [ ${source_flux%.*} -gt 500 ]; then
                        chansOut=32
                    elif [ ${source_flux%.*} -gt 200 ]; then
                        chansOut=24
                    elif [ ${source_flux%.*} -gt 100 ]; then
                        chansOut=16
                    elif [ ${source_flux%.*} -gt 50 ]; then
                        chansOut=12
                    else
                        chansOut=8
                    fi


                    $container wsclean -name ${source_name}_initial \
                            -size 500 500 \
                            -niter 100000 \
                            -pol I \
                            -weight briggs ${PEEL_BRIGGS} \
                            -scale ${scale:0:8} \
                            -auto-threshold 3 \
                            -auto-mask 5 \
                            -abs-mem ${ABSMEM} \
                            -mgain 0.8 \
                            -channels-out ${chansOut} \
                            -join-channels -no-mf-weighting \
                            -j ${NCPUS} \
                            -data-column ${DATA_COLUMN} \
                            -minuv-l ${IMAGE_MINUV} \
                            ${obsid}.ms

                    if [ ! -e ${source_name}_initial-MFS-image.fits ]; then
                        echo "Peel images not made."
                        exit 1
                    fi

                    files=$(find . -type f -name "*.fits" -not \
                        \( -name "*MFS-image.fits" -or -name "*MFS-model.fits" -or -name "*MFS-residual.fits" \))
                    for file in ${files[@]}; do
                        if [ -e ${file} ]; then
                            rm ${file}
                        fi
                    done

                    $container subtrmodel -usemodelcol \
                               -datacolumn ${DATA_COLUMN} \
                               "model" \
                               ${obsid}.ms

                    $container ${PIIPPATH}/ms_set_column_zero.py ${obsid}.ms MODEL_DATA

                else
                
                    if [ ${source_flux%.*} -gt 500 ]; then
                        tsteps=1
                    elif [ ${source_flux%.*} -gt 250 ]; then
                        tsteps=1
                    elif [ ${source_flux%.*} -gt 100 ]; then
                        tsteps=5
                    else
                        tsteps=999
                    fi

                    $container peel -datacolumn ${DATA_COLUMN} \
                         -minuv ${minuvm} \
                         -a 0.001 0.000001 \
                         -i 200 \
                         -t ${tsteps} \
                         ${obsid}.ms \
                         &> peel_${source_name}.log

                    


                fi

            elif [ "${method}" = "subtract" ]; then

                echo "INFO: subtracting ${source_name} using CLEAN model"

                if [ ${source_flux%.*} -gt 500 ]; then
                    chansOut=32
                elif [ ${source_flux%.*} -gt 200 ]; then
                    chansOut=24
                elif [ ${source_flux%.*} -gt 100 ]; then
                    chansOut=16
                elif [ ${source_flux%.*} -gt 50 ]; then
                    chansOut=12
                else
                    chansOut=8
                fi

                $container wsclean -name ${source_name}_initial \
                        -size 500 500 \
                        -niter 100000 \
                        -pol I \
                        -weight briggs ${PEEL_BRIGGS} \
                        -scale ${scale:0:8} \
                        -auto-threshold 3 \
                        -auto-mask 10 \
                        -abs-mem ${ABSMEM} \
                        -mgain 0.8 \
                        -channels-out 12 \
                        -join-channels -no-mf-weighting \
                        -j ${NCPUS} \
                        -data-column ${DATA_COLUMN} \
                        -minuv-l ${IMAGE_MINUV} \
                        ${obsid}.ms
                
                if [ ! -e ${source_name}_initial-MFS-image.fits ]; then
                    echo "Peel images not made."
                    exit 1
                fi

                files=$(find . -type f -name "*.fits" -not \
                    \( -name "*MFS*-image.fits" -or -name "*MFS*-model.fits" -or -name "*MFS*-residual.fits" \))
                for file in ${files[@]}; do
                    if [ -e ${file} ]; then
                        rm ${file}
                    fi
                done

                $container subtrmodel -usemodelcol \
                           -datacolumn ${DATA_COLUMN} \
                           "model" \
                           ${obsid}.ms

                $container ${PIIPPATH}/ms_set_column_zero.py ${obsid}.ms MODEL_DATA

            fi 

            echo "INFO: ${source_name} ${source_model} ${method}" >> ${obsid}_peeled_sources.log

            peeled_sources+="${source_name} "

        done < peel.log

        # Ensure we are still pointing in the right direction afterwards:
        $container chgcentre ${obsid}.ms ${radec_hmsdms}  

    fi

else

    # Find the brightest sidelobe and remove it.

    ra=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits RA)
    dec=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits DEC)

    $container get_beam_lobes ${obsid}.metafits -p ${PERCENT} -mS > ${obsid}_beamlobes.txt

    if (( $( wc -l < ${obsid}_beamlobes.txt ) )); then 

        i=0

        while read line; do

            ((i++))

            ra_beam=$(echo "$line" | awk '{print $2}')
            dec_beam=$(echo "$line" | awk '{print $3}')
            fov_beam=$(echo "$line" | awk '{print $4}')

            radec_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${ra_beam} ${dec_beam} -d "hms")
            $container chgcentre ${obsid}.ms ${radec_hmsdms}

            if $MINW; then
                $container chgcentre -minw -shiftback ${obsid}.ms
                extraWSClean="-nwlayers-for-size 14000 14000"
            else
                extraWSClean="-use-wgridder"
            fi

            imsize_beam=$(echo "${fov_beam}/${scale}" | bc -l)

            $container wsclean -name ${obsid}_lobe${i} \
                    -size ${imsize_beam%.*} ${imsize_beam%.*} \
                    -niter 250000 \
                    -pol I \
                    -weight briggs ${PEEL_BRIGGS} \
                    -scale ${scale:0:8} \
                    -auto-mask 5 \
                    -auto-threshold 3 \
                    -padding 1.5 \
                    -abs-mem ${ABSMEM} \
                    -mgain 0.6 \
                    -data-column ${DATA_COLUMN} \
                    -channels-out 4 \
                    -join-channels \
                    -nmiter 4 \
                    -no-mf-weighting \
                    -j ${NCPUS} \
                    -minuv-l ${IMAGE_MINUV} \
                    ${extraWSClean} ${obsid}.ms

            if [ ! -e ${obsid}_lobe${i}-MFS-image.fits ]; then
                echo "Lobe images not made."
                exit 1
            fi

            files=$(find . -type f -name "*.fits" -not \
                    \( -name "*MFS*-image.fits" -or -name "*MFS*-model.fits" -or -name "*MFS*-residual.fits" \))
            for file in ${files[@]}; do
                if [ -e ${file} ]; then
                    rm ${file}
                fi
            done


            $container subtrmodel -usemodelcol \
                       -datacolumn ${DATA_COLUMN} \
                       "model" \
                       ${obsid}.ms

            $container ${PIIPPATH}/ms_set_column_zero.py ${obsid}.ms MODEL_DATA



        done < ${obsid}_beamlobes.txt

        radec_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${ra} ${dec} -d "hms")

        $container chgcentre ${obsid}.ms ${radec_hmsdms}

    fi

fi


end_time=$(date +%s)
duration=$(echo "${end_time}-${start_time}" | bc -l)
echo "INFO: Total time taken: ${duration}"

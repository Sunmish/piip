#! /usr/bin/env python

import sys
from astropy.io import fits, votable
from astropy.table import Table
import numpy as np

def writeout(catalogue, outname):
    """
    """
    catalogue.write(outname, overwrite=True, format="votable")

def filter(catalogue):
    """
    """
    fcat = catalogue[catalogue["flags"].data == 0]
    print("{:.1f} per cent of catalogue selected.".format(100.*len(fcat)/len(catalogue)))
    return fcat

def main():
    catalogue = sys.argv[1]
    outname = sys.argv[2]    
    writeout(filter(Table.read(catalogue)), outname)



if __name__ == "__main__":
    main()
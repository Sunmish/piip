skymodel fileformat 1.1
source {
    name "test_gaussian"
    component {
        type gaussian
        shape YY YY 0
        position ZZ
        measurement {
            frequency XX MHz
            fluxdensity Jy 1.0 0.0 0.0 0.0
        }
    }
}
#! /usr/bin/env python

import numpy as np
from argparse import ArgumentParser

from astropy.io import fits
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from astropy import units as u
from regions import Regions, PixCoord


def get_args():
    ps = ArgumentParser()
    ps.add_argument("region", type=str)
    ps.add_argument("image", type=str)
    ps.add_argument("-o", "--outname", default=None)
    ps.add_argument("-b", "--blank_image", action="store_true")
    ps.add_argument("-B", "--blank_below", default=None, type=float)
    ps.add_argument("-M", "--make_mask", action="store_true")
    ps.add_argument("-m", "--mask_value", default=1, type=int)
    ps.add_argument("-f", "--fill_value", default=0., type=float)
    
    ps.add_argument("-n", "--negate", action="store_true")
    args = ps.parse_args()
    return args

def mask(regionfile, fitsfile, 
    outname=None, 
    blank_image=False, 
    fill_value=0., 
    negate=False, 
    make_mask=False,
    mask_value=1,
    blank_below=None):
    """
    """

    image = fits.open(fitsfile)
    wcs = WCS(image[0].header).celestial
    indices = np.indices((image[0].header["NAXIS1"], image[0].header["NAXIS2"]))
    indices_x = indices[0]
    indices_y = indices[1]
    ra, dec = wcs.all_pix2world(indices_x, indices_y, 0)
    skycoords = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)


    mask_image = np.full((image[0].header["NAXIS1"], image[0].header["NAXIS2"]), 0.)
    regions = Regions.read(regionfile, format="ds9")
    for region in regions:

        mask = region.contains(skycoords, wcs)
        mask_image[indices_y[mask], indices_x[mask]] = 1.

    if image[0].header["NAXIS"] > 2:
        mask_data = image[0].data.copy()
        mask_data[..., :, :] = mask_image
    else:
        mask_data = mask_image

    if outname is None:
        outname = fitsfile.replace(".fits", "-mask.fits")

    if blank_image:
        if blank_below is not None:
            image[0].data[np.where(np.abs(image[0].data < blank_below))] = fill_value
        if negate:
            if make_mask:
                image[0].data[np.where(mask_data != 0)] = mask_value
            image[0].data[np.where(mask_data == 0)] = fill_value
        else:
            if make_mask:
                image[0].data[np.where(mask_data != 1)] = mask_value
            image[0].data[np.where(mask_data == 1)] = fill_value
        image.writeto(outname, overwrite=True)
    else:
        fits.writeto(outname, mask_image, image[0].header, overwrite=True)


def cli(args):
    mask(
        regionfile=args.region,
        fitsfile=args.image,
        outname=args.outname,
        blank_image=args.blank_image,
        fill_value=args.fill_value,
        negate=args.negate,
        make_mask=args.make_mask,
        mask_value=args.mask_value,
        blank_below=args.blank_below
    )

if  __name__ == "__main__":
    cli(get_args())







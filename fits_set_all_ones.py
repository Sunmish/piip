#! /usr/bin/env python

import sys
from astropy.io import fits

image = sys.argv[1]

with fits.open(image, mode="update") as f:
    f[0].data[:] = 1.

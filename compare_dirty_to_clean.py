#! /usr/bin/env python

import math
import os
import sys

import numpy as np
from argparse import ArgumentParser
# from regions import read_ds9
from subprocess import Popen


from astropy.io import fits, votable             # For handling FITS files.
from astropy.wcs import WCS               # For computing LAS/positions.
from astropy.wcs.utils import proj_plane_pixel_scales
from astropy.coordinates import SkyCoord  # For cross-referencing.
# SkyCoord or coordinates are returning import errors?
# This is needed for `measure_tree` - finding a source in a catalogue.
from astropy import units as u

from scipy.special import erf

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s", \
                    level=logging.INFO)


from matplotlib import pyplot as plt

units = {"arcsec": u.arcsec, \
         "arcmin": u.arcmin, \
         "deg"   : u.degree, \
         "degree": u.degree, \
         "as"    : u.arcsec, \
         "am"    : u.arcmin, \
         "radian": u.radian, \
         "rad"   : u.radian
         }

# from AstroTools import fluxtools
# measure_aperture = fluxtools.measure_aperture

# from astroplot2 import splot
# from flux_tools import flux_measure


# https://stackoverflow.com/a/61629292/3293881 @Ehsan
def norm_method(arr, point):
    point = np.asarray(point)
    return np.linalg.norm(np.indices(arr.shape, sparse=True)-point)


def bane_wrapper(image):
    if not os.path.exists(image.replace(".fits", "_rms.fits")):
        Popen("BANE {}".format(image), shell=True).wait()


def read_fits(fitsimage):
    """Read a FITS file with astropy.io.fits and return relevant data.

    Because the FITS standard is in fact not completely standard there are
    a few little things that need to be checked to look for the necessary
    header data.
    """

    opened = False
    if isinstance(fitsimage, str):
        # if not fitsimage.endswith(".fits"): fitsimage += ".fits"
        hdulist = fits.open(fitsimage)
        opened = True
    elif isinstance(fitsimage, fits.HDUList):
        hdulist = fitsimage
    else:
        raise IOError(">>> Specified FITS image must an astropy.io.fits.HDUList" \
                      "object or a filepath.")

    harray = hdulist[0].header
    farray = np.squeeze(hdulist[0].data)


    naxis = harray["NAXIS"]

    if ("NAXIS3" in harray.keys()) and naxis == 2:
        for k in ["NAXIS", "CDELT", "CRVAL", "CTYPE", "CRPIX"]:
            del harray[k+"3"]
    if ("NAXIS4" in harray.keys()) and naxis < 4:
        for k in ["NAXIS", "CDELT", "CRVAL", "CTYPE", "CRPIX"]:
            del harray[k+"4"]

    warray = WCS(harray).celestial

    if opened: hdulist.close()

    # Read some things from the FITS header and check some things:

    # try: farray.shape = (harray["NAXIS2"], harray["NAXIS1"])
    # except ValueError:  raise ValueError(">>> FITS files must be flat.")

    # Try to find pixel sizes:
    try:
        cd1, cd2 = harray["CDELT1"], harray["CDELT2"]
    except KeyError:
        try:
            cd1, cd2 = harray["CD1_1"], harray["CD2_2"]
        except KeyError:
            raise KeyError(">>> Cannot find key for pixel sizes.")

    # Beam parameters:
    semi_a, semi_b, beam_area, beam_not_found = None, None, None, True
    if ("BMAJ" in harray.keys()) and ("BMIN" in harray.keys()):
        semi_a = 0.5 * harray["BMAJ"]  # Normal keys.
        semi_b = 0.5 * harray["BMIN"]
        beam_not_found = False
    elif ("CLEANBMJ" in harray.keys()) and ("CLEANBMN" in harray.keys()):
        semi_a = 0.5 * harray["CLEANBMJ"]  # Abnormal keys, e.g. VLSSr.
        semi_b = 0.5 * harray["CLEANBMN"]
        beam_not_found = False
    else:
        try:  # Check for NVSS:
            for i in range(len(harray["HISTORY"])):
                if "'NRAO VLA SKY SURVEY" in harray["HISTORY"][i]:
                    semi_a = 0.5 * 1.2500e-2
                    semi_b = 0.5 * 1.2500e-2
                    beam_not_found = False
            if beam_not_found: raise KeyError
        except KeyError:
            try:  # AIPS has a tell-tale mark:
                for i in range(len(harray["HISTORY"])):
                    if "BMAJ" in harray["HISTORY"][i]:
                        l = []
                        for t in harray["HISTORY"][i].split():
                            try: l.append(float(t))
                            except ValueError: pass
                        semi_a = 0.5 * l[0]
                        semi_b = 0.5 * l[1]
                        beam_not_found = False
                if beam_not_found: raise KeyError
            except (KeyError, IndexError):
                try:  # Check for SUMSS, which does NOT have the beam information.
                      # We will use the declination-dependent beam size based on
                      # the reference pixel of the FITS file.
                    for i in range(len(harray["HISTORY"])):
                        if "sumss" in harray["HISTORY"][i].lower():
                            decl = math.radians(abs(harray["CRVAL2"]))
                            # beam_area = 0.25 * np.pi * (45.0**2 / 3600.0**2) * \
                            #             (1.0 / math.sin(decl))
                            semi_a = 0.5 * (45./3600.)
                            semi_b = 0.5 * (45./(3600. * math.sin(decl)))
                            beam_not_found = False
                    if beam_not_found: raise KeyError
                except KeyError:
                    raise KeyError(">>> Beam parameters not found. {}".format(fitsimage))

    if beam_area is None:
        beam_area = np.pi * semi_a * semi_b
    bpp = beam_area / (abs(cd1*cd2) * np.log(2.0))

    beam = (semi_a*2., semi_b*2.)
    beam_major = semi_a*2.
    beam_minor = semi_b*2.


    # include abs() for beam params as sometimes these can be negative...
    return np.squeeze(farray), warray, abs(bpp), cd1, cd2, naxis, abs(beam_major), abs(beam_minor)



def get_header(fitsimage):
    """
    """

    opened = False
    if isinstance(fitsimage, str):
        # if not fitsimage.endswith(".fits"): fitsimage += ".fits"
        hdulist = fits.open(fitsimage)
        opened = True
    elif isinstance(fitsimage, fits.HDUList):
        hdulist = fitsimage
    else:
        raise IOError(">>> Specified FITS image must an astropy.io.fits.HDUList" \
                      "object or a filepath.")

    harray = hdulist[0].header

    return harray


def get_bpp(bmaj, bmin, cd1, cd2):
    return (np.pi*bmaj*bmin) / (4.*abs(cd1*cd2)*np.log(2.))


def rms_array(rms, farray=None):
    """Load or generate rms array.

    """

    if farray is not None:
        farray = np.squeeze(farray)

    try:
        rms = float(rms)
        rarray = None
    except (TypeError, ValueError):
        if isinstance(rms, str): rarray = fits.getdata(rms)
        elif isinstance(rms, fits.HDUList): rarray = rms[0].data
        else: raise ValueError(">>> RMS must be specified as either a single " \
                               "value or as an array/filepath.")


    if rarray is None:
        if farray is None:
            raise ValueError(">>> If RMS array is to be made a template array" \
                             " must be specified")
        else:
            rarray = np.full_like(farray, rms)

    rarray = np.squeeze(rarray)

    if rarray.shape != farray.shape:
        print(farray.shape, rarray.shape)
        raise ValueError(">>> RMS array and image array must be the same size.")

    return np.squeeze(rarray)


def psf_array(psf, farray=None, index=0):
    """
    """

    try:
        psf = float(psf)
        parray = None
    except (TypeError, ValueError):
        if isinstance(psf, str):
            parray = fits.getdata(psf)[index, ...]
        elif isinstance(psf, fits.HDUList):
            parray = psf[0].data[index, ...]
        else:
            raise ValueError(">>> PSF must be specified as either a single " 
                             "value or a as an array/filepath.")

    if parray is None:
        if farray is None:
            raise ValueError(">>> If PSF array is to be made a template array "
                             "must be specified.")
        else:
            parray = np.full_like(farray, psf)

    parray = np.squeeze(parray)

    if parray.shape != farray.shape:
        raise ValueError(">>> PSF array and image array must be the same size.")

    return parray


def measure_aperture(fitsimage, coords, radius, rms=None, sigma=3, LAS=False, \
                     verbose=True, radius_units="deg", z=0, psf=None,
                     offset_correct=True, absolute_flux=False,
                     writeout_image=False):
    """Measure flux within a circular aperture.

    Parameters
    ----------
    fitsfimage   : string or astropy.io.fits.HDUList
                 If string this should be the filename and path to a FITS file.
    RA           : float
                 Central RA in decimal degrees.
    DEC          : float
                 Central DEC in decimal degrees.
    radius       : float
                 Aperture within which to calculate flux in degree.
     rms         : float or str
                 Either a single rms value for the image, or a rms image mirroring
                 the input FITS file. Minimum detection threshold is `rms` * `cutoff`
    cutoff       : int, optional
                 Multiple of the RMS required for detection. Default is 3.
    LAS          : bool, optional
                 If True an LAS is calculated.
    verbose      : bool, optional
                 If True results are printed to the terminal.
    radius_units : {'deg', 'arcmin', 'arcsec'}, optional
                 Specifies the unit for `radius`.

    """

    print(">>> running measure_aperture on {} with rms {}".format(fitsimage, rms))

    farray, warray, bpp, cd1, cd2, naxis, beam_major, beam_minor = read_fits(fitsimage)
    if rms is not None:
        rarray = rms_array(rms, farray)
        rarray = np.squeeze(rarray)

    if psf is None:
        logging.info(">>> using PSF {:.2f}\" times {:.2f}\"".format(beam_major*3600.,
                                                                    beam_minor*3600.))
        bmaj = psf_array(beam_major, farray)
        bmin = psf_array(beam_minor, farray)
    else:
        logging.info(">>> using PSF map from {}".format(psf))
        bmaj = psf_array(psf, farray, 0)
        bmin = psf_array(psf, farray, 1)
        # set pixel scaling so we can strip projection-related effects?
        

    bpp_array = get_bpp(bmaj, bmin, cd1, cd2)

    farray = np.squeeze(farray)

    r = (radius * units[radius_units]).to(u.degree).value

    scale = int((r / abs(cd1)) + 10)
    yr, xr = warray.celestial.all_world2pix(coords[0], coords[1], 0)
    yr = int(yr)
    xr = int(xr)

    if absolute_flux:
        farray = np.absolute(farray)

    source_flux, source_rms, source_xpixel, source_ypixel, source_coords = \
     [], [], [], [], []
    source_bpp  = []
    source_peak = 0


    x_range = [xr-scale, xr+scale]
    y_range = [yr-scale, yr+scale]
    scale_pix = scale - 10

    dist = norm_method(farray, (xr, yr))
    cond1 = dist <= scale_pix
    if rms is not None:
        cond2 = farray >= sigma*rarray
    else:
        cond2 = True
    indices = np.where(cond1 & cond2)
    indices2 = np.where(~cond1 | ~cond2)

    if writeout_image:
        farray[indices2] = np.nan
        fits.writeto(fitsimage.replace(".fits", "_ww.fits"),
            farray,
            fits.getheader(fitsimage),
            overwrite=True)

    source_flux = farray[indices].flatten()
    source_bpp = bpp_array[indices].flatten()
    if rms is not None:
        source_rms = rarray[indices].flatten()
    source_xpixel = indices[0].flatten()
    source_ypixel = indices[1].flatten()
    source_peak = np.nanmax(source_flux)

    # for i in range(xr-scale, xr+scale):
    #     for j in range(yr-scale, yr+scale):

    #         try:
    #             if rms is not None:
    #                 cond = farray[i, j] >= sigma*rarray[i, j]
    #             else:
    #                 cond = True

    #             if cond:

    #                 c = warray.celestial.all_pix2world(j, i, 0)
    #                 diff = angular_distance(coords, c)

    #                 if diff <= r:

    #                     source_flux.append(farray[i, j])
    #                     source_bpp.append(bpp_array[i, j])
    #                     if rms is not None:
    #                         source_rms.append(rarray[i, j])
    #                     source_xpixel.append(i)
    #                     source_ypixel.append(j)
    #                     source_coords.append(c)
    #                     if farray[i, j] > source_peak:
    #                         source_peak = farray[i, j]
    #         except IndexError:
    #             pass

    # source_bpp = np.asarray(source_bpp)
    # source_flux = np.asarray(source_flux)
    # source_rms = np.asarray(source_rms)
    # source_peak = np.nanmax(source_flux)
    int_flux = sum(source_flux / source_bpp)
    if rms is not None:
        unc_flux = (sum(source_rms / source_bpp) * 
            np.sqrt(np.nanmean(source_bpp) / float(len(source_rms))))
    else:
        unc_flux = 0.0
    area     = len(source_flux) * abs(cd1*cd2)
    npix     = len(source_flux)
    on_source_rms      = np.sqrt((np.mean(np.asarray(source_flux)**2)))
    rms      = np.mean(source_rms)


    if offset_correct:
        hdr = get_header(fitsimage)
        if "BIASA" in hdr.keys() and "BIASB" in hdr.keys():
            peak_bias = (source_peak/rms)*hdr["BIASA"] + hdr["BIASB"]
            # peak_bias = hdr["BIASB"]
            logging.info("peak bias: {} Jy/beam".format(peak_bias))
            int_bias = (peak_bias / source_peak)*int_flux
            logging.info("integrated flux density bias: {} Jy".format(int_bias))
            int_flux += int_bias

    LAS = False  # none of this crap
    if LAS:
        las = 0
        for i in range(len(source_coords)):
            for j in range(len(source_coords)):
                if angular_distance(source_coords[i], source_coords[j]) > las:
                    las = angular_distance(source_coords[i], source_coords[j])
    else:
        las = "NA"

    if verbose:
        print(">>> The following parameters have been found:\n" \
              "............... Int. flux = {0} [Jy]\n" \
              "......... Error int. flux = {1} [Jy]\n" \
              "............... Peak flux = {2} [Jy/beam]\n" \
              ".............. No. pixels = {3}\n" \
              "..................... LAS = {4} [deg]\n" \
              "............. Source area = {5} [deg^2]\n" \
              "........... On source rms = {7} [Jy/beam]\n" \
              "..................... rms = {6} [Jy/beam]".format(\
              int_flux, unc_flux, source_peak, npix, las, area, rms, on_source_rms))

    if np.isnan(unc_flux):
        unc_flux = 0.

    return int_flux, unc_flux, source_peak, npix, las, area, rms



def plot(dirty_flux, clean_flux, radii, dirty_peak, clean_peak, title, freq,
         limits=None, bmaj=None, bmin=None, name=None, clean_aegean=None):
    """
    """

    # plt.title(title)
    # plt.plot(radii, dirty_flux, label="dirty", marker="+")
    # plt.plot(radii, clean_flux, label="clean", marker="x")

    # plt.legend()

    # plt.savefig("{}.png".format(title))

    


    peak =  np.asarray(dirty_peak) / np.asarray(clean_peak)
    flux = np.asarray(dirty_flux) / np.asarray(clean_flux)


    p = splot.Splot(x=radii,
                    y=flux,
                    ylabel=r"Ratio",
                    xlabel=r"Gaussian FWHM in image [arcmin]",
                    logscale=False,
                    # text=text,
                    figure_size="test",
                    cbar=True,
                    FONT_TICKS=20,
                    FONT_LABELS=22.,
                    marker="",
                    linestyle="-",
                    color="crimson",
                    label=r"$S_\mathrm{int,dirty} / S_\mathrm{int,clean}$",
                    )



    p.add_data(x=radii,
               y=peak,
               marker="",
               label=r"$S_\mathrm{peak,dirty} / S_\mathrm{peak,clean}$",
               linestyle="--",
               color="dodgerblue",
               )
    p.add_data(x=radii,
               y=clean_flux,
               marker="",
               label=r"$S_\mathrm{int,clean}$",
               linestyle="-.",
               color="black")

    if clean_aegean is not None:
        p.add_data(x=radii,
                   y=clean_aegean,
                   marker="",
                   label=r"$S_\mathrm{int,clean,\texttt{aegean}}$",
                   linestyle=":",
                   color="grey")


    if name is None:
        text = "{}~MHz".format(freq)
    else:
        text = name
    p.add_data_label(0.98, 0.95, text, relative=True,
        buff=0., size=22., ha="right")

    if limits is not None:
        p.set_limits(ylimits=limits)
    p.set_limits(xlimits=[0., max(radii)])

    if bmaj is not None:
        # p.ax1.axvline(bmaj, c="black", ls="--")
        p.add_shading(x1=0, x2=bmaj, color="grey",)
    if bmin is not None:
        p.ax1.axvline(bmin, c="grey", ls=":")

    p.add_legend(loc="lower left", fontsize=20.)

    p.save(title+".pdf", tight=True)
    p.close()



def aegean_wrapper(image, rms=None):
    """
    """

    if rms is None:
         s = "aegean --find --autoload --table={} {} --seedclip=50 --maxsummits=1 --floodclip=4".format(
        image.replace(".fits", ".vot"), image)
    else:
        s = "aegean --find --forcerms={} --forcebkg=0.0 --table={} {} --seedclip=20 --maxsummits=1 --floodclip=4".format(
            rms, image.replace(".fits", ".vot"), image)
    Popen(s, shell=True).wait()
    if os.path.exists(image.replace(".fits", "_comp.vot")):
        t = votable.parse_single_table(image.replace(".fits", "_comp.vot")).array
        s = t["int_flux"][0]
    else:
        s = 0.

    return s


def clip(val, clip, do_apply=False):
    """
    """
    if (val > clip) and do_apply:
        val = clip

    return val






def main():
    """
    """

    ps = ArgumentParser()
    ps.add_argument("freq", type=str)
    ps.add_argument("basename", type=str)
    ps.add_argument("sources", type=str, nargs="*")
    ps.add_argument("--rms", "--noise", dest="noise", default=None, type=float)
    ps.add_argument("--radius-unit", dest="unit", default=1., type=float)
    ps.add_argument("-p", "--plot", action="store_true")
    ps.add_argument("--name", default=None)
    ps.add_argument("-a", "--aegean", action="store_true")
    ps.add_argument("-c", "--clip", action="store_true")
    ps.add_argument("-A", "--aperture", default=None, type=float)
    ps.add_argument("-r", "--ra", default=None, type=float)
    ps.add_argument("-d", "--dec", default=None, type=float)
    ps.add_argument("--snr", default=10., type=float)
    ps.add_argument("--yrange", default=None, nargs=2, type=float)
    ps.add_argument("--sigma", default=1., type=float)

    args = ps.parse_args()

    dirty_flux = []
    clean_flux = []
    dirty_peak = []
    clean_peak = []
    labels = []
    radii = []
    true_radii = []
    if args.aegean:
        clean_aegean = []
    else:
        clean_aegean = None

    title = "{}_{}MHz_dirty_clean_comparison".format(args.basename, args.freq)

    header_clean = fits.getheader("{}_{}-MFS-image.fits".format(args.basename, args.sources[0]))
    header_dirty = fits.getheader("{}_{}-MFS-dirty.fits".format(args.basename, args.sources[0]))

    print("clean beam: {:.2f} arcsec times {:.2f} arcsec, {:.2f} deg".format(
        header_clean["BMAJ"]*3600., header_clean["BMIN"]*3600., header_clean["BPA"]))
    print("dirty beam: {:.2f} arcsec times {:.2f} arcsec, {:.2f} deg".format(
        header_dirty["BMAJ"]*3600., header_dirty["BMIN"]*3600., header_dirty["BPA"]))

 

    for source in args.sources:

        print(source)

        dirty = "{}_{}-MFS-dirty.fits".format(args.basename, source)
        clean = "{}_{}-MFS-image.fits".format(args.basename, source)

        bane_wrapper(dirty)
        bane_wrapper(clean)

        header = fits.getheader(dirty)
        maxval = np.nanmax(np.squeeze(fits.getdata(dirty)))

        if args.ra is None:
            ra = header["CRVAL1"]
        else:
            ra = args.ra
        if args.dec is None:
            dec = header["CRVAL2"]
        else:
            dec = args.dec


        print("using {}, {}".format(ra, dec))


        if args.noise is None:
            rms_dirty = np.squeeze(fits.getdata(dirty.replace(".fits", "_rms.fits")))
            rms_clean = np.squeeze(fits.getdata(clean.replace(".fits", "_rms.fits")))
            max_rms = np.nanmax(rms_clean)
            min_rms = np.nanmin(rms_clean)
        else:
            rms_dirty = args.noise
            rms_clean = args.noise
            max_rms = args.noise

        print(maxval*0.5/max_rms)

        size = np.sqrt((float(source)/3600.)**2 + header_clean["BMAJ"]**2)
        print(size)


        if args.aperture is None:
            paramsd = flux_measure.measure_source(dirty,
                ra=ra,
                dec=dec,
                rms=rms_dirty,
                initial_cutoff=maxval*0.5,
                final_cutoff=2.,
                plot=args.plot)


            paramsc = flux_measure.measure_source(clean,
                ra=ra,
                dec=dec,
                rms=rms_clean,
                initial_cutoff=maxval*0.5,
                final_cutoff=2.,
                plot=args.plot)

        else:

            paramsc = measure_aperture(clean,
                coords=(ra,
                        dec),
                radius=args.aperture*size,
                rms=np.nanmax(fits.getdata(clean))/args.snr,
                sigma=args.sigma,
                offset_correct=False,
                writeout_image=True)

            paramsd = measure_aperture(dirty,
                coords=(ra,
                        dec),
                radius=args.aperture*size,
                rms=np.nanmax(fits.getdata(dirty))/args.snr,
                sigma=args.sigma,
                offset_correct=False,
                writeout_image=True)


        dirty_flux.append(paramsd[0])
        clean_flux.append(paramsc[0])
        dirty_peak.append(paramsd[2])
        clean_peak.append(paramsc[2])
        
        fwhm = np.sqrt((float(source)/3600.)**2 + header_clean["BMAJ"]**2)

        radii.append(fwhm*60.)
        true_radii.append(float(source)/60.)

        if args.aegean:

            # if not args.noise:
                # args.noise = np.nanmedian(np.squeeze(fits.getdata(clean.replace(".fits", "_rms.fits"))))
                # print("using {} Jy/beam for noise".format(args.noise))
            clean_aegean.append(aegean_wrapper(clean, rms=None))



    # plot(dirty_flux, clean_flux, radii, dirty_peak, clean_peak, title, args.freq, 
    #     # limits=(0.35, 1.05),
    #     bmaj=header_clean["BMAJ"]*60.,
    #     name=args.name,
    #     clean_aegean=clean_aegean,
    #     limits=args.yrange)

    if args.aegean:
        with open(title+".txt", "w+") as f:
            f.write("#arcmin,dirty_flux,clean_flux,aegean_flux,dirty_peak,clean_peak,true_arcmin\n")
            for i in range(len(radii)):
                f.write("{},{},{},{},{},{},{}\n".format(radii[i], dirty_flux[i], clean_flux[i], clean_aegean[i], dirty_peak[i], clean_peak[i], true_radii[i]))
    else:
        with open(title+".txt", "w+") as f:
            for i in range(len(radii)):
                f.write("{},{},{},{},{}\n".format(radii[i], 
                    dirty_flux[i], 
                    clean_flux[i],
                    dirty_peak[i],
                    clean_peak[i]))


if __name__ == "__main__":
    main()

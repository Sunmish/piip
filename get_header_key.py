#! /usr/bin/env python

from __future__ import print_function

import argparse
from astropy.io.fits import getval


def main():
    
    ps = argparse.ArgumentParser(prog="get_header_key",
                                 description="Return value for a specified key "
                                             "in a FITS header.",
                                 epilog="A small wrapper for astropy's "
                                        "convenience function: getval.")

    ps.add_argument("fitsfile", help="A FITS file.")
    ps.add_argument("header_key", help="A FITS header key.")

    args = ps.parse_args()

    val = getval(args.fitsfile, args.header_key, 0)

    print(val)


if __name__ == "__main__":
    main()


#! /bin/bash

#!/bin/bash
#SBATCH --account=mwasci
#SBATCH --time=24:00:00

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

set -x


IMSIZE=9000
SCALE=0.5
WEIGHT1="r0.5"
WEIGHT2="uniform"
ROBUST="uniform"
KERNEL=55  # arbitrary selection that seems to work?
DIAGONALS=false 
USE_OLD=false
WINDOW="rectangular"
SAVE_ATERMS=false
SUBTRACT=false
INTERP="rbf"
NOCORRECT=false
ONLYCORRECT=false
SELFCAL=false
SELFCAL_MINUV=0
SELFCAL_TSTEPS=999
SELFCAL_CHAN_AVG=2
CALIBRATION_COLUMN="CORRECTED_DATA"
MWA1=
MWA2=
MULTISCALE_MAX_SCALES=5
MULTISCALE_BIAS=0.7
IMAGE_MINUV=35
SUBBANDS_OUT=4
IDG_MGAIN=0.9
IDG_NMITER=5
IDG_LOCAL_RMS=false

if [ -e "/nvmetmp" ]; then
    BINDPATH="--bind /nvmetmp/"
fi

# TODO merge these two containers
# use /nvmetmp/:/tmp/ to allow general /tmp usage
# CONTAINER="singularity run --bind /nvmetmp/ /astro/mwasci/duchesst/piip.img"
# GPU_CONTAINER="singularity run --nv --bind /nvmetmp/ /astro/mwasci/duchesst/gpu_stuff.img"

start_time=$(date)



opt=$1
if [[ "${opt}" == *".cfg"* ]]; then
    config=$(echo "${opt}" | sed "s/'//g")
    echo "INFO: using config file ${config}"
    source ${config}
fi

opts=$(getopt \
    -o :ho:M:m:c:d:i:k:r:ANCSa: \
    --long help,outbase:,mwa2:,mwa1:,chan:,processing_dir:,indir:,kernel:,robust:,save_aterms,nocorrect,onlycorrect,selfcal,aterms: \
    --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piipimage
    exit 1
}


# for opt in ${opts[@]}; do
#     if [[ "${opt}" == *".cfg"* ]]; then
#         config=$(echo "${opt}" | sed "s/'//g")
#         echo "INFO: using config file ${config}"
#         source ${config}
#         break
#     fi
# done




eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipIDGpair ;;
        -o|--outbase) outbase=$2 ; shift 2 ;;
        -M|--mwa2) MWA2=$2 ; shift 2 ;;
        -m|--mwa1) MWA1=$2 ; shift 2 ;;
        -c|--chan) chan=$2 ; shift 2 ;;
        -d|--processing_dir) outdir=$2 ; shift 2 ;;
        -k|--kernel) KERNEL=$2 ; shift 2 ;;
        -r|--robust) ROBUST=$2 ; shift 2 ;;
        -A|--save_aterms) SAVE_ATERMS=true ; shift ;;
        -N|--nocorrect) NOCORRECT=true ; shift ;;
        -C|--onlycorrect) ONLYCORRECT=true ; shift ;;
        -S|--selfcal) SELFCAL=true; shift ;;
        -a|--aterms) ATERM_FILE=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR: " ; usage_piipimage ;;
    esac
done



NCPUS=$SLURM_CPUS_PER_GPU
ABSMEM=$(echo "$SLURM_MEM_PER_NODE/1024" | bc -l)

check_required "--outbase" "-o" $outbase
check_required "--chan" "-c" $chan
check_required "--processing_dir" "-d" $outdir

if [ -z ${MWA1} ] && [ -z ${MWA2} ]; then
    echo "An MWA1 and/or MWA2 OBSID list must be supplied with -m/-M"
    exit 1
fi

if [ -z ${CONTAINER} ]; then
    container=""
else
    container="singularity exec ${BINDPATH} ${CONTAINER}"
fi  

if [ -z ${GPU_CONTAINER} ]; then
    gpu_container=""
else
    gpu_container="singularity exec --nv ${BINDPATH} ${GPU_CONTAINER}"
fi  

cd ${outdir} || exit 1

if [ "${ROBUST}" == "uniform" ]; then
    weight="uniform"
    weight_dir="uniform"
elif [ "${ROBUST}" == "natural" ]; then
    weight="natural"
    weight_dir="natural"
else
    weight="briggs ${ROBUST}"
    weight_dir="r${ROBUST}"
fi

if $SAVE_ATERMS; then
    saveAterms="-save-aterms"
else
    saveAterms=""
fi

# Allow overwriting of DATA column:
if [ ${CALIBRATION_COLUMN} = "DATA" ]; then
    calcopy="-nocopy"
else
    calcopy="-copy"
fi

phase1obs=""
phase2obs=""

if [ ! -z ${MWA1} ]; then
    while read line; do
        if [ ! -z "${line}" ]; then
            obs=$(echo "$line" | awk '{print $1}')
            ant=$(echo "$line" | awk '{print $2}')
            
            if [ -e ${obs}.ms ]; then

                phase1obs="${phase1obs} ${obs}.ms"
                allmetafits="${allmetafits} ${obs}.metafits"
                allobs="${allobs} ${obs}.ms"
                mses+=("${obs}.ms")

                if [ -z ${reference} ]; then
                    reference=${obs}
                fi

                if [ ! -z ${ant} ]; then
                    allants="${allants} ${ant}"
                    ants+=("${ant}")
                else
                    allants="${allants} NONE"
                    ants+=("NONE")
                fi
            fi
            ant=
        fi
    done < ${MWA1}
fi

if [ ! -z ${MWA2} ]; then
    # only use MWA1 reference when no MWA2 available
    reference=
    while read line; do
        if [ ! -z "${line}" ]; then
            obs=$(echo "$line" | awk '{print $1}')
            ant=$(echo "$line" | awk '{print $2}')
            
            if [ -e ${obs}.ms ]; then

                phase2obs="${phase2obs} ${obs}.ms"
                allmetafits="${allmetafits} ${obs}.metafits"
                mses+=("${obs}.ms")

                if [ -z ${reference} ]; then
                    reference=${obs}
                fi 

                if [ ! -z ${ant} ]; then
                    allants="${allants} ${ant}"
                    ants+=("${ant}")
                else
                    allants="${allants} NONE"
                    ants+=("NONE")
                fi

            fi
            ant=
        fi
    done < ${MWA2}
fi

freq=$($container ${PIIPPATH}/get_header_key.py ${reference}.metafits FREQCENT)
scale=$(echo "${SCALE}/${chan}" | bc -l)
stokesIbase=${outbase}_${weight_dir}_k${KERNEL}

if [ ! -z ${TAPER} ]; then
    taper="-taper-gaussian ${TAPER}"
else
    taper=""
fi

if [ -e ${outbase}_mask.fits ]; then
    fitsmask="-fits-mask ${outbase}_mask.fits"
else
    fitsmask=""
fi

if [ -z ${ATERM_FILE} ]; then
    ATERM_FILE=${outbase}_aterm.cfg
fi

if ! ${ONLYCORRECT}; then

    SELFCAL=false  # TODO
    if $SELFCAL; then

        NOCORRECT=true

        ${gpu_container} wsclean \
            -niter 500000 \
            -data-column ${CALIBRATION_COLUMN} \
            -nmiter 5 \
            -auto-threshold 3 \
            -auto-mask 20 \
            -name ${stokesIbase} \
            -size ${IMSIZE} ${IMSIZE} \
            -mgain 0.8 \
            -pol I \
            -weight uniform \
            -minuv-l 35 \
            -scale ${scale} \
            -abs-mem ${ABSMEM} \
            -j ${NCPUS} \
            -log-time \
            -verbose \
            -use-idg \
            -idg-mode hybrid \
            -channels-out ${SUBBANDS_OUT} \
            -join-channels \
            -no-mf-weighting \
            -mwa-path ${BEAM_PATH} \
            -temp-dir /nvmetmp/ \
            -clean-border 2 \
            -aterm-config ${outbase}_aterm.cfg \
            -aterm-kernel-size ${KERNEL} \
            ${fitsmask} ${saveAterms} ${phase1obs} ${phase2obs}

        minuv=$($container ${PIIPPATH}/get_baselines_from_lambdas.py ${freq} ${SELFCAL_MINUV})

        for obsid in ${allobs[@]}; do
            $container calibrate -j ${NCPUS} \
                -absmem ${ABSMEM} \
                -minuv ${minuv} \
                -t ${SELFCAL_TSTEPS} \
                -ch ${SELFCAL_CHAN_AVG} \
                -datacolumn ${CALIBRATION_COLUMN} \
                ${obsid}.ms \
                ${obsid}_jselfcal.bin > ${obsid}_jcalibrate.log 2>&1
            
            # TDOO add checks before apply?
            $container applysolutions -datacolumn ${CALIBRATION_COLUMN} ${calcopy} \
                ${obsid}.ms \
                ${obsid}_jselfcal.bin


            mv ${obsid}_dldm.fits ${obsid}_dldm_initial.fits
        
        done



    else


    if ${IDG_LOCAL_RMS}; then
        local_rms="-local-rms"
    else
        local_rms=""
    fi


    ${gpu_container} wsclean \
        -niter 1000000 \
        -data-column ${CALIBRATION_COLUMN} \
        -nmiter ${IDG_NMITER} \
        -auto-threshold 1 \
        -auto-mask 3 \
        -name ${stokesIbase} \
        -size ${IMSIZE} ${IMSIZE} \
        -mgain ${IDG_MGAIN} \
        -pol I \
        -weight ${weight} \
        -minuv-l ${IMAGE_MINUV} \
        -scale ${scale} \
        -abs-mem ${ABSMEM} \
        -j ${NCPUS} \
        -log-time \
        -verbose \
        -use-idg \
        -idg-mode hybrid \
        -channels-out ${SUBBANDS_OUT} \
        -join-channels \
        -mwa-path ${BEAM_PATH} \
        -temp-dir /nvmetmp/ \
        -clean-border 1.5 \
        -multiscale \
        -multiscale-scale-bias ${MULTISCALE_BIAS} \
        -multiscale-max-scales ${MULTISCALE_MAX_SCALES} \
        -aterm-config ${ATERM_FILE} \
        -aterm-kernel-size ${KERNEL} \
        ${local_rms} ${taper} ${fitsmask} ${saveAterms} ${phase1obs} ${phase2obs}

    fi

fi

if ${NOCORRECT} && ${ONLYCORRECT}; then
    echo "What do you want from me? I'm going to correct anyway."
fi

if ! ${NOCORRECT}; then
    image=${stokesIbase}-MFS-image-pb.fits
    catalogue=${stokesIbase}-MFS-image-pb_masked_comp.vot
    residual=${stokesIbase}-MFS-residual-pb.fits
    model=${stokesIbase}-MFS-model-pb.fits
    maxsep=$(echo "${scale}*10" | bc -l)
    smooth=$(echo "1.0/${scale}" | bc -l)
    bmaj=$($container ${PIIPPATH}/get_header_key.py $image "BMAJ")
    zone=$(echo "${bmaj}*2" | bc -l)

    ra=$($container $PIIPPATH/get_header_key.py ${image} CRVAL1)
    dec=$($container $PIIPPATH/get_header_key.py ${image} CRVAL2)
    radius=$(echo "${scale}*${IMSIZE}/2.0 - 0.5" | bc -l)

    $container ${PIIPPATH}/piipaegean.sh ${image} ${ra} ${dec} ${radius}

    if [ -z ${MATCH_CATALOGUES_OPTIONS} ]; then
        MATCH_CATALOGUES_OPTIONS="--ra2 ra --dec2 dec"
    fi

    if [ -z ${FLUX_WARP_OPTIONS} ]; then
        FLUX_WARP_OPTIONS="-A alpha_c -a beta_c -c gamma_c"
    fi

    $container match_catalogues ${catalogue} ${SKYMODEL} \
        --separation $maxsep \
        --exclusion_zone $zone \
        --threshold 0.0 \
        --nmax 1000 \
        --outname ${stokesIbase}_matched.fits \
        --localrms "local_rms" \
        --ratio 1.2 \
        ${MATCH_CATALOGUES_OPTIONS}

    $container fits_warp.py \
        --xm ${stokesIbase}_matched.fits \
        --infits ${image} \
        --suffix warp \
        --ra1 old_ra \
        --dec1 old_dec \
        --smooth ${smooth} \
        --plot \
        ${MATCH_CATALOGUES_OPTIONS}

    image=${stokesIbase}-MFS-image-pb_warp.fits

    
    freqHz=$(echo "${freq}*1000000" | bc -l)
    $container ${PIIPPATH}/update_header_key.py "${image}" "FREQ" "${freqHz}"

    if [ -z ${SMOOTHING} ]; then

        $container get_beam_lobes "${reference}.metafits" -p 0.05 -Mm > "${reference}_beamlobes.txt"
        
        if (( $( wc -l < ${reference}_beamlobes.txt ) )); then

            while read line; do
                beam_size=$(echo "$line" | awk '{print $4}')
                SMOOTHING=$(echo "$beam_size / 4.0" | bc -l)
            done < ${reference}_beamlobes.txt

        fi

        if [ -z $SMOOTHING ]; then
            SMOOTHING=22
        fi

    fi

    threshold=$($container ${PIIPPATH}/get_scaled_flux.py ${freq} -f 88.0 -S 0.5 -a -0.77)
    $container flux_warp ${stokesIbase}_matched.fits $image \
        --threshold ${threshold} \
        --nmax 1000 \
        --smooth ${SMOOTHING} \
        --test_subset \
        --test_fraction 0.2 \
        --residual \
        --mode "linear_rbf" \
        --outname ${stokesIbase}-MFS-image-pb_rbf.fits \
        --ignore \
        --ra_key old_ra \
        --dec_key old_dec \
        -l "local_rms" \
        --plot \
        --nolatex \
        --additional_images ${model} ${residual} \
        --additional_outname ${stokesIbase}-MFS-model-pb_rbf.fits ${stokesIbase}-MFS-residual-pb_rbf.fits \
        ${FLUX_WARP_OPTIONS}
fi


rm ${stokesIbase}*-000?-*.fits
#! /usr/bin/env python

import sys
import os
import numpy as np
from astropy.io import fits
from astropy.stats.circstats import circmean


bmaj = 0.
bmin = 0.
bpa = []

for i in range(1, len(sys.argv)):
    if os.path.exists(sys.argv[i]):
        hdr = fits.getheader(sys.argv[i])
        if hdr["BMAJ"] > bmaj:
            bmaj = hdr["BMAJ"]
        if hdr["BMIN"] > bmin:
            bmin = hdr["BMIN"]

        bpa.append(hdr["BPA"])

bpa = circmean(np.asarray(bpa))

print("{:.4f} {:4f} {:.2f}".format(
    bmaj*3600., bmin*3600., bpa
))
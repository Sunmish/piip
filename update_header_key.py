#! /usr/bin/env python

import sys

from astropy.io import fits

image = sys.argv[1]
key  = sys.argv[2]
value = sys.argv[3]

with fits.open(image, mode="update") as f:

    f[0].header[key] = float(value)
    f.flush()

    
#! /usr/bin/env python

from __future__ import print_function, division

import numpy as np
from argparse import ArgumentParser
from astropy.io.votable import parse_single_table
from astropy.table import Table, vstack


FOV = {69: 60,
       93: 50,
       121: 40,
       145: 30,
       169: 20,
       235: 15}

CALIBRATORS = ["PKS2356-61", "CenA", "PicA", "HydA", "HerA", "3C444"]

def open_xmls(xmls):
    """Open .xml file from ASVO."""

    tables = []
    for xml in xmls:
        t = Table.read(xml)
        tables.append(t)
    table = vstack(tables)
    return table


def get_obs(table, source, ignore, chan, print_names=False):
    """Get a list of observation IDs for `source` at `chan`."""

    obsids, fov, ra, dec = [], [], [], []
    names = []
    unique_names = []
    used_names = []
    for i in range(len(table["obsname"])):

        # print(table["obs_title"][i])
        try:
            entry = table["obsname"][i].decode("utf8")
        except AttributeError:
            entry = table["obsname"][i]
        # name = "_".join(table["obs_title"][i].split("_")[:-1])
        name = "_".join(entry.split("_")[:-1])
        channel = table["center_channel_number"][i]+1  # off by one >:

        # print([name, source, chan, channel])

        if source is None:
            for c in CALIBRATORS:
                if c in name:
                    name = c

        if name not in ignore:

            if source is None and channel == int(chan):
                obsids.append(table["obs_id"][i])
                fov.append(FOV[channel])
                ra.append(table["ra"][i])
                dec.append(table["dec"][i])
                names.append(name)

            elif channel == int(chan) and \
                (source in name):
                obsids.append(table["obs_id"][i])
                fov.append(FOV[channel])
                ra.append(table["ra"][i])
                dec.append(table["dec"][i])
                names.append(name)
        if name not in unique_names:
            unique_names.append(name)

    if print_names:
        print("Used names:")
        print(list(set(names)))

    return obsids, fov, ra, dec, names, unique_names



def region_formatter(name, fov, ra, dec, color="green"):
    """Create a region file."""

    with open(name, "w+") as f:
        f.write("# Region file format: DS9 version 4.1\n")
        f.write("global color={} dashlist=8 3 width=1 font=\"helvetica 10 "
                "normal roman\" select=1 highlite=1 dash=0 fixed=0 edit=1 "
                "move=1 delete=1 include=1 source=1\n".format(color))
        f.write("fk5\n")

        for i in range(len(fov)):
            f.write("circle({}d,{}d,{}d)\n".format(ra[i], dec[i], float(fov[i])/2.))


def table_formatter(name, ra, dec, obsids):
    """Create simple csv table."""

    with open(name, "w+") as f:
        f.write("OBSID,RA,DEC\n")
        for i in range(len(ra)):
            f.write("{},{:.2f},{:.2f}\n".format(obsids[i], ra[i], dec[i]))




def main():
    """The main function."""

    ps = ArgumentParser(description="Output list of observation IDs given "
                                    "an input VOTable output from the MWA "
                                    "ASVO online archive.")

    ps.add_argument("table", type=str, nargs="*",
                    help="VOTable (.xml) output from the MWA ASVO archive.")
    ps.add_argument("-s", "--source", type=str, default=[None], nargs="*",
                    help="Source/target name, without channel appended. "
                         "Multiple sources can be specified.")
    ps.add_argument("-i", "--ignore", type=str, nargs="*", default=[None, ""])
    ps.add_argument("-c", "--chan", type=str, default=None, nargs="*",
                    help="Central channel of observation, appended to target "
                         " name. Multiple channels can be specified.")
    ps.add_argument("-o", "--outname", type=str, default=None, 
                    help="Output file name. By default, the output file name "
                         "is determined from the source name and channel.")
    ps.add_argument("-f", "--footprint", action="store_true", 
                    help="Switch to create a DS9 region file showing the "
                         "footprint of each observation.")
    ps.add_argument("-C", "--color", "--colour", dest="color", default="green",
                    help="Color for the footprint region file.")
    ps.add_argument("--project", default="D0006", type=str,
                    help="Project name if making a calibrator list. Defaults for calibrator project.")
    ps.add_argument("--print_names", action="store_true")
    ps.add_argument(
        "--output_csv", 
        default=None,
        help="If specified, a CSV file will be output with OBSIDs and coordinates."
    )


    args = ps.parse_args()
    table = open_xmls(args.table)

    args.ignore.append(None)
    args.ignore.append("")

    for source in args.source:
        for chan in args.chan:

            if args.outname is None:
                outname = "{}_{}.txt".format(source, chan)
            else:
                outname = args.outname

            obsids, fov, ra, dec, names, unique_names = get_obs(
                table=table,
                source=source,
                ignore=args.ignore,
                chan=chan,
                print_names=args.print_names
            )

            with open(outname, "w+") as f:
                for i, obs in enumerate(obsids):
                    
                    if outname.endswith(".csv"):
                        f.write("{obsid},{project},{calibrator},{chan}\n".format(
                            obsid=obs,
                            project=args.project,
                            calibrator=names[i],
                            chan=chan
                        ))
                    else:
                        f.write("{}\n".format(obs))

            if args.footprint:
                r_outname = outname[0:-4] + ".reg"
                region_formatter(r_outname, fov, ra, dec, args.color)

            if args.output_csv is not None:
                table_formatter(
                    name=args.output_csv,
                    ra=ra,
                    dec=dec,
                    obsids=obsids
                )

if __name__ == "__main__":
    main()

#! /usr/bin/env python

from __future__ import print_function, division

import numpy as np

from argparse import ArgumentParser

c = 299792458.

def main():
    """
    """

    ps = ArgumentParser()

    ps.add_argument("freq", type=float)
    ps.add_argument("scale", type=float)
    ps.add_argument("maxangular", type=float)

    args = ps.parse_args()

    beamsize = np.degrees((c/(args.freq*1.e6))/5.5e3)
    mscales = "0,"

    for i in [1, 3, 7, 13, 25, 33, 77, 144, 201, 288]:
        if i*beamsize < 0.9*args.maxangular:
            mscales += "{},".format(int(i*beamsize / args.scale))

    print(mscales[:-1])


if __name__ == "__main__":
    main()
#! /usr/bin/env python

from __future__ import print_function, division

import numpy as np
import os
import sys
import shutil

from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.wcs import WCS

from argparse import ArgumentParser

try:
    import subprocess32 as subprocess
except ImportError:
    import subprocess

from datetime import datetime

from astropy.io import fits

import sinfactors


import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def strip_naxis(image):
    with fits.open(image, mode="update") as f:
        print(image)
        f[0].data = np.squeeze(f[0].data)
        f[0].header = sinfactors.strip_wcsaxes(f[0].header)
        f.flush()
        f.close()


def strip_wscaxes(hdr):
    """
    """

    remove_keys = [key+i for key in ["CRVAL", "CDELT", "CRPIX", "CTYPE", "CUNIT", "NAXIS"]
                         for i in ["3", "4"]]

    for key in remove_keys:
        if key in hdr.keys():
            del hdr[key]

    return hdr


def blank(fitsimage, stride=225000):
    """
    """

    with fits.open(fitsimage, mode="update") as f:

        hdr = f[0].header
        arr = np.squeeze(f[0].data)
        wcs = WCS(hdr).celestial

        indices = np.indices(arr.shape)
        y = indices[0].flatten()
        x = indices[1].flatten()
        n = len(x)

        for i in range(0, n, stride):

            r, d = wcs.all_pix2world(x[i:i+stride], y[i:i+stride], 0)
            b = np.where(~np.isfinite(r))[0]

            f[0].data[..., y[i:i+stride][b], x[i:i+stride][b]] = np.nan

        f.flush()
        f.close()

  
def stack(images, rms_weights=None, beam_weights=None):
    """
    """

    for image in images:
        strip_naxis(image)
    if beam_weights is not None:
        for image in beam_weights:
            strip_naxis(image)

    print("Initialising stack...")

    arr1 = fits.getdata(images[0])
    if len(arr1.shape) > 2:
        arr1 = arr1[0, :, :]

    stacked = np.full_like(arr1, np.nan, dtype=np.float32)
    weights = np.full_like(arr1, np.nan, dtype=np.float32)

    for i in range(0, len(images)):

        print("Adding {} {}/{}".format(images[0], i, len(images)))

        arr1 = fits.getdata(images[i])
        if len(arr1.shape) > 2:
            arr1 = arr1[0, :, :]
        arr1[np.where(~np.isfinite(arr1))] = 0.

        arr2 = np.full_like(arr1, 1e-30, dtype=np.float32)
        if rms_weights is not None:
            arr2_0 = fits.getdata(rms_weights[i])
            arr2_0[~np.isfinite(arr2_0)] = 1.e30 
            arr2 *= (1./(arr2_0)**2)
        if beam_weights is not None:
            arr2_0 = fits.getdata(beam_weights[i])
            arr2_0[~np.isfinite(arr2_0)] = 1.e-30
            arr2 *= (arr2_0)**2

        stacked += (arr1*arr2)
        weights += arr2

    print(np.nanmax(stacked))
    print(np.nanmax(weights))

    return (stacked / weights)






def stack_one_by_one(images, rms, beam, weight_type="rms"):


    for image in images:
        strip_naxis(image)

    print("Initialising stack")

    arr1 = fits.getdata(images[0])

    if len(arr1.shape) > 2:
        arr1 = arr1[0, :, :]


    arr1[np.where(~np.isfinite(arr1))] = 0.

    arr2 = fits.getdata(rms[0])
    # if weight_type == "rms":
    arr2[np.where(~np.isfinite(arr2))] = 1.e30
    weights2 = 1./(arr2**2)
    # else:
    arr3 = fits.getdata(beam[0])
    arr3[np.where(~np.isfinite(arr3))] = 1.e-30
    weights3 = arr3**2

    weights = weights2*weights3

    value_weights = arr1*weights2*weights3


    for i in range(1, len(images)):

        print("Adding image {} to stack".format(i))
        print(images[i],rms[i])

        arr = fits.getdata(images[i])
        if len(arr.shape) > 2:
            arr = arr[0, :, :]
        arr[np.where(~np.isfinite(arr))] = 0.
        arr_rms = fits.getdata(rms[i])
        arr_beam = fits.getdata(beam[i])

        # if weight_type == "rms":
        arr_rms[np.where(~np.isfinite(arr_rms))] = 1.e30
        rms_weights = 1./(arr_rms**2)
        # else:
        arr_beam[np.where(~np.isfinite(arr_rms))] = 1.e-30
        beam_weights = arr_beam**2

        
        arr_value_weights = arr*rms_weights*beam_weights

        weights += (rms_weights*beam_weights)
        value_weights += arr_value_weights


    return value_weights / weights


def check_convol_results(log):
    """Check the results of convol to ensure convolution is actually done.
    """

    with open(log) as f:
        lines = f.readlines()
        for line in lines:
            if "### Warning:  Bmaj or bmin missing ... no scaling" in line:
                return False

    return True





def get_stack(images, fill_nan=0.0):
      open_images = [fits.getdata(f) for f in images]
      for image in open_images:
          image[np.where(~np.isfinite(image))] = fill_nan
      return np.stack(open_images)


def convert_selection(selection):
    """Convert selection `1-10` to python list."""

    if selection == "all":
        return selection
    elif "-" in selection:
        s1, s2 = selection.split("-")[0], selection.split("-")[1]
        return range(int(s1), int(s2)+1)
    else:
        return [selection]


def read_obslist(obslist, selection="all"):
    """Read in OBS ID list.

    This may be simply a list of OBS IDs, and optionally a list of 
    tiles to flag per snapshot.
    """

    obs_ids, obs_flags = [], []

    with open(obslist, "r") as obs:

        lines = obs.readlines()
        for l in range(len(lines)):
            if (selection == "all" or l+1 in selection) and "#" not in lines[l]:

                line = lines[l]
                bits = line.replace("\n", "").split(" ")
                if len(bits) == 1:  # No flag tiles on this line.
                    flags = ""
                elif len(bits) == 2:
                    flags = bits[1]
                elif len(bits) == 3:
                    flags = bits[1]
                    chans = bits[2]
                else:
                    logger.warning("OBS ID file line has too many items? \n" 
                                    "{0}".format(line))
                    flags = bits[1]
                    chans = bits[2]

                obs_ids.append(bits[0])
                obs_flags.append(flags)

    return obs_ids, obs_flags


def bpp(major, minor, cd):
    """Integration factor for an elliptical beam on square pixels."""

    return np.pi*(major*minor) / (cd**2 * 4.*np.log(2.))



def make_config_file(ra, dec, basename="mosaic", projection="SIN", imsize=8000,
                     combine_type="WEIGHTED", weight_type="MAP_RMS", mem=8127,
                     nthreads=4, resample_dir="./resamp", resample_only=False,
                     combine_only=False, weight_ext="_rms.fits", pix="MIN"):
    """Make a default SWarp configuration file for co-addition."""

    name = basename + "_cfg"

    if resample_only:
        resample_only = "Y"
        combine_only = "N"
        name += "_resamp"
        # combine_type = "AVERAGE"
        # weight_ext = ""
    elif combine_only:
        resample_only = "N"
        combine_only = "Y"
        name += "_coadd"
    # weight_ext = "_rms.fits"

    if pix == "MIN":
        pixscale = 0.0
    else:
        pixscale = pix
        pix = "MANUAL"

    with open("{0}.swarp".format(name), "w+") as f:

        f.write("""
#----------------------------------- Output -----------------------------------#
IMAGEOUT_NAME     {basename}.fits      # Output filename
WEIGHTOUT_NAME  {basename}.weight.fits # Output weight-map filename
 
HEADER_ONLY            N               # Only a header as an output file (Y/N)?
HEADER_SUFFIX          .head           # Filename extension for additional headers
VERBOSE_TYPE         FULL
 
#------------------------------- Input Weights --------------------------------#
 

WEIGHT_TYPE            NONE   # BACKGROUND,MAP_RMS,MAP_VARIANCE
                                       # or MAP_WEIGHT
RESCALE_WEIGHTS        Y               # Rescale input weights/variances (Y/N)?
# WEIGHT_IMAGE    @{basename}_rms.list # Weightmap filename if suffix not used
# # 
WEIGHT_SUFFIX          {weight_ext}    # Suffix to use for weight-maps
                                       # (all or for each weight-map)
WEIGHT_THRESH                          # Bad pixel weight-threshold
 
#------------------------------- Co-addition ----------------------------------#
 
COMBINE                {combine_only}  # Combine resampled images (Y/N)?
COMBINE_TYPE           AVERAGE  # MEDIAN,AVERAGE,MIN,MAX,WEIGHTED,CLIPPED
                                       # CHI-OLD,CHI-MODE,CHI-MEAN,SUM,
                                       # WEIGHTED_WEIGHT,MEDIAN_WEIGHT,
                                       # AND,NAND,OR or NOR
CLIP_AMPFRAC           0.3             # Fraction of flux variation allowed
                                       # with clipping
CLIP_SIGMA             4.0             # RMS error multiple variation allowed
                                       # with clipping
CLIP_WRITELOG          N               # Write output file with coordinates of
                                       # clipped pixels (Y/N) 
CLIP_LOGNAME           clipped.log     # Name of output file with coordinates
                                       # of clipped pixels
BLANK_BADPIXELS        N               # Set to 0 pixels having a weight of 0
 
#-------------------------------- Astrometry ----------------------------------#
 
CELESTIAL_TYPE         NATIVE          # NATIVE, PIXEL, EQUATORIAL,
                                       # GALACTIC,ECLIPTIC, or SUPERGALACTIC
PROJECTION_TYPE        {projection}    # Any WCS projection code or NONE
PROJECTION_ERR         0.001           # Maximum projection error (in output
                                       # pixels), or 0 for no approximation
CENTER_TYPE            MANUAL          # MANUAL, ALL or MOST
CENTER                 {ra}, {dec}     # Coordinates of the image center
PIXELSCALE_TYPE        {pix}             # MANUAL,FIT,MIN,MAX or MEDIAN
PIXEL_SCALE            {pixscale}             # Pixel scale
IMAGE_SIZE           {imsize},{imsize} # Image size (0 = AUTOMATIC)
 
#-------------------------------- Resampling ----------------------------------#
 
RESAMPLE               {resample_only} # Resample input images (Y/N)?
RESAMPLE_DIR           {resample_dir}  # Directory path for resampled images
RESAMPLE_SUFFIX        .resamp.fits    # filename extension for resampled images
 
RESAMPLING_TYPE        LANCZOS3        # NEAREST,BILINEAR,LANCZOS2,LANCZOS3
                                       # LANCZOS4 (1 per axis) or FLAGS
OVERSAMPLING           4               # Oversampling in each dimension
                                       # (0 = automatic)
INTERPOLATE            N               # Interpolate bad input pixels (Y/N)?
                                       # (all or for each image)
 
FSCALASTRO_TYPE        FIXED           # NONE,FIXED, or VARIABLE
FSCALE_KEYWORD         FLXSCALE        # FITS keyword for the multiplicative
                                       # factor applied to each input image
FSCALE_DEFAULT         1.0             # Default FSCALE value if not in header
 
GAIN_KEYWORD           GAIN            # FITS keyword for effect. gain (e-/ADU)
GAIN_DEFAULT           0.0             # Default gain if no FITS keyword found
                                       # 0 = infinity (all or for each image)
SATLEV_KEYWORD         SATURATE        # FITS keyword for saturation level (ADU)
SATLEV_DEFAULT         50000.0         # Default saturation if no FITS keyword
 
#--------------------------- Background subtraction ---------------------------#
 
SUBTRACT_BACK          N               # Subtraction sky background (Y/N)?
                                       # (all or for each image)
 
#------------------------------ Memory management -----------------------------#
 
VMEM_DIR               .               # Directory path for swap files
VMEM_MAX               {mem}           # Maximum amount of virtual memory (MB)
MEM_MAX                {mem}           # Maximum amount of usable RAM (MB)
COMBINE_BUFSIZE        {mem}           # RAM dedicated to co-addition(MB)
 
#------------------------------ Miscellaneous ---------------------------------#
 
DELETE_TMPFILES        N               # Delete temporary resampled FITS files
                                       # (Y/N)?
WRITE_FILEINFO         N               # Write information about each input
                                       # file in the output image header?
WRITE_XML              N               # Write XML file (Y/N)?
NNODES                 1               # Number of nodes (for clusters)
NODE_INDEX             0               # Node index (for clusters)
 
NTHREADS               {nthreads}      # Number of simultaneous threads for
                                       # the SMP version of SWarp
                                       # 0 = automatic
NOPENFILES_MAX         512            # Maximum number of files opened by SWarp
#------------------------------------------------------------------------------#

""".format(basename=basename, projection=projection, ra=ra, dec=dec, 
           imsize=imsize, combine_type=combine_type, weight_type=weight_type, 
           mem=int(mem), nthreads=nthreads, resample_dir=resample_dir,
           combine_only=combine_only,
           resample_only=resample_only,
           weight_ext=weight_ext,
           pix=pix,
           pixscale=pixscale*3600.))
    
    return "{0}.swarp".format(name)


def main():
    """
    """

    description = """
    Make a stacked mosaic of a set of MWA snapshot images.
    """

    epilog = """
    The directory/file structure should be as follows: 
    indir/obsid/extra_dir/obsid_basename.fits
    """

    help_ = {"o": "The list of observation IDs.",
             "i": "The input directory.",
             "s": "The selection of OBS IDs from the list. 1-indexed." 
                  "[Default all]",
             "b": "The base input file name.",
             "O": "The base output file name.",
             "c": "The central coordinates of the mosaic.",
             "p": "The projection. [Default SIN]",
             "U": "Switch to use old files if they exist.",
             "I": "Image size in degrees. [Default ~ 80% of input images]",
             "N": "Number of cores to use for SWarp. [Default all available]",
             "M": "Memory to use for SWarp. [Default all available]",
             "x": "Extra directory between input directory and the obsid." 
                  "[Default '']",
             "r": "Directory for resampled images - created if it does not "
                  "exist a runtime. [Default './resamp']"
             }

    ps = ArgumentParser(description=description, epilog=epilog)

    req_ps = ps.add_argument_group("required parameters")
    # Inputs
    req_ps.add_argument("-o", "--obslist", type=str, nargs="*", required=True,
                        help=help_["o"])
    req_ps.add_argument("-i", "--indir", type=str, nargs="*", required=True,
                        help=help_["i"])
    req_ps.add_argument("-b", "--basename", type=str, required=True,
                        help=help_["b"], nargs="*")

    # Outputs
    req_ps.add_argument("-O", "--outbase", type=str, required=True,
                        help=help_["O"])
    req_ps.add_argument("-c", "--coordinates", dest="coords", type=float,
                        metavar=("RA", "DEC"), nargs=2, required=True,
                        help=help_["c"])



    opt_ps = ps.add_argument_group("optional parameters")
    opt_ps.add_argument("-s", "--selection", type=str, nargs="*",
                        default=["all"], help=help_["s"])
    opt_ps.add_argument("-p", "--projection", type=str, default="SIN", 
                        choices=["SIN", "ZEA", "TAN"], help=help_["p"])
    opt_ps.add_argument("-U", "--useold", action="store_true", help=help_["U"])
    opt_ps.add_argument("-I", "--imsize", default=None, type=float,
                        help=help_["I"], nargs="*")
    opt_ps.add_argument("-N", "--ncpus", default=None, type=int, 
                        help=help_["N"])
    opt_ps.add_argument("-M", "--absmem", default=None, type=float,
                        help=help_["M"])
    opt_ps.add_argument("-x", "--extra_dir", default="", type=str,
                        help=help_["x"])
    opt_ps.add_argument("-r", "--resample_directory", dest="resamp", type=str,
                        default="./resamp/", help=help_["r"])
    opt_ps.add_argument("-C", "--convol", action="store_true")
    opt_ps.add_argument("-B", "--beam_mosaic", action="store_true")
    opt_ps.add_argument("-W", "--weight_by_beam", action="store_true")
    opt_ps.add_argument("--noobsid", action="store_true")

    args = ps.parse_args()

    t1 = datetime.now()

    # Validate some inputs;
    try:
        assert len(args.obslist) == len(args.indir)
    except AssertionError:
        raise IOError("An indir must be specified uniquely for each obslist.")

    if len(args.selection) < len(args.obslist) and args.selection[0] == "all":
        args.selection = ["all"]*len(args.obslist)
    elif len(args.selection) < len(args.obslist):
        raise IOError("A selection should be specified for each obslist.")

    print(args.selection)

    if not os.path.exists(args.resamp):
        os.mkdir(args.resamp)

    # # Look at memory and cores:
    # ncores_avail = psutil.cpu_count()
    # nmem_avail = psutil.virtual_memory().available*0.75 / 1.e6  # in MB

    # if args.ncpus is None:
    #     args.ncpus = ncores_avail
    # elif args.ncpus > ncores_avail:
    #     args.ncpus = ncores_avail

    # if args.absmem is None:
    #     args.absmem = nmem_avail
    # elif args.absmem > nmem_avail:
    #     args.absmem = nmem_avail


    # Read in snapshots to use:
    snapshots = []
    rmses = []
    metafitsfiles = [] 

    if len(args.basename) < len(args.obslist):
        args.basename = args.basename*len(args.obslist)

    for n in range(len(args.obslist)):

        print(args.basename[n])
        
        selection = convert_selection(args.selection[n])
        
        obs_ids, _ = read_obslist(args.obslist[n], selection)

        if not args.indir[n].endswith("/"):
            args.indir[n] += "/"
        if not args.extra_dir.endswith("/"):
            args.extra_dir += "/"   


        for i in range(len(obs_ids)):

            if not args.noobsid:

                rootname = "{0}{1}{2}{1}{3}".format(args.indir[n], 
                                                    obs_ids[i],
                                                    args.extra_dir,
                                                    args.basename[n])

                metafitsname = "{0}{1}{2}{1}.metafits".format(args.indir[n], 
                                                    obs_ids[i],
                                                    args.extra_dir)

            else:

                rootname = "{0}{2}{1}{3}".format(args.indir[n], 
                                                obs_ids[i],
                                                args.extra_dir,
                                                args.basename[n])

                matafitsname = "{0}{1}{2}.metafits".format(args.indir[n], 
                                                obs_ids[i],
                                                args.extra_dir)

            if os.path.exists(rootname+".fits"):

                logger.info("Found {}.fits".format(rootname))
                snapshots.append(rootname)
                metafitsfiles.append(metafitsname)

            else:

                logger.warn("No {}.fits found".format(rootname))


    snapshots = np.asarray(sorted(snapshots))

    print("Number of snapshots: {}".format(len(snapshots)))
 
    # ------------------------------------------------------------------------ #
    # Find frequency information:
    ref_hdr = fits.getheader(snapshots[0]+".fits")
    if "FREQ" in ref_hdr.keys():
        frequency = ref_hdr["FREQ"]
    elif "CRVAL3" in ref_hdr.keys():
        frequency = ref_hdr["CRVAL3"]
    elif "CENTCHAN" in ref_hdr.keys():
        frequency = int(ref_hdr["CENTCHAN"])*1.28*1.e6
    else:
        raise ValueError("No frequency information found in {}.fits".format(snapshots[0]))
    if "CDELT1" in ref_hdr.keys():
        cd = abs(ref_hdr["CDELT1"])
    elif "CD1_1" in ref_hdr.keys():
        cd = abs(ref_hdr["CD1_1"]) 


    logger.info("Found frequency of {:.3f} MHz".format(round(frequency/1.e6, 3)))

    # Get beam information:
    # TODO assert all beams ~~similar?

    bmaj = 0.
    bmin = 0.
    bpa = 0.
    cd = 1.e9
    for snapshot in snapshots:

        with fits.open(snapshot+".fits") as f:

            if f[0].header["BMAJ"] > bmaj:
                bmaj = f[0].header["BMAJ"]
            if f[0].header["BMIN"] > bmin:
                bmin = f[0].header["BMIN"]

            logger.debug("{} has {}\" times {}\"".format(snapshot, 
                                                          f[0].header["BMAJ"]*3600.,
                                                          f[0].header["BMIN"]*3600.))

            # pixel sizes should already be equal!
            # pixel size should be passed to SWarp to ensure each resampled image
            # is the same size. If not, then SWarp will create resampled images
            # at different dimensions, but with equal pixel sizes.
            if "CDELT1" in f[0].header.keys():
                if abs(f[0].header["CDELT1"]) < cd:
                    cd = abs(f[0].header["CDELT1"])
            elif "CD1_1" in f[0].header.keys():
                if abs(f[0].header["CD1_1"]) < cd:
                    cd = abs(f[0].header["CD1_1"])

    logger.info("Found beam information: {}\" times {}\", {} deg".format(bmaj*3600.,
                                                                          bmin*3600.,
                                                                          bpa))




    # Work out image size in pixels:
    if args.imsize is None:
        imsize = max(int(ref_hdr["NAXIS1"]), int(ref_hdr["NAXIS2"]))
        imcut = (imsize, imsize)
    else:
        if "CDELT1" in ref_hdr.keys():
            cd1 = abs(ref_hdr["CDELT1"])
            cd_key = "CDELT1"
        elif "CD1_1" in ref_hdr.keys():
            cd1 = abs(ref_hdr["CD1_1"])
            cd_key = "CD1_1"
        else:
            raise ValueError("No pixel size information found in {}.fits".format(snapshots[0]))

        if len(args.imsize) == 1:
            args.imsize = args.imsize[0]
            imsize = int(float(args.imsize) / cd1)
            if imsize > max(int(ref_hdr["NAXIS1"]), int(ref_hdr["NAXIS2"])):
                # No point in making LARGER images than the input images...
                imcut =  max(int(ref_hdr["NAXIS1"]), int(ref_hdr["NAXIS2"]))
            else:
                imcut = imsize
            imcut = (imcut, imcut)
        else:
            imcut = (args.imsize[0]/cd1, args.imsize[1]/cd1)


    logger.info("Setting image size to {0}*{1} pixels".format(imcut[0], imcut[1]))

    sub_snapshots = []
    bpps = []


    logger.info("Ensuring the same beam...")

    convol_snapshots = snapshots

    # Now lets makes some resampled images:
    input_images = " ".join([snapshot+".fits" for snapshot in convol_snapshots])

    resampled_images = []
    rms_images = []

    if args.beam_mosaic:
        beam_images = []

    psf_images = []
    sin_images = []
    beams = []

    for j, snapshot in enumerate(convol_snapshots):

        print(("Starting on {}/{}...".format(j, len(convol_snapshots))))

        shutil.copy2(snapshot+".fits", args.resamp+"/"+os.path.split(snapshot+".fits")[-1])
        snapshot = os.path.split(snapshot)[-1]

        hdr_i = fits.getheader(args.resamp+"/"+snapshot+".fits")

        if os.path.exists("{1}/{0}.regrid.fits".format(snapshot, args.resamp)) and args.useold:

            print("{1}/{0}.regrid.fits already done - passing".format(snapshot, args.resamp))
            pass

        else:

            print("making {1}/{0}.regrid.fits".format(snapshot, args.resamp))

            image_i = snapshot+".fits"

            blank("{1}/{0}.fits".format(snapshot, args.resamp))

            if os.path.exists("{1}/{0}.mir".format(snapshot, args.resamp)) and args.useold:
                pass
            else:

                if os.path.exists("{1}/{0}.mir".format(snapshot, args.resamp)):
                    shutil.rmtree("{1}/{0}.mir".format(snapshot, args.resamp))

                s = "fits in={1}/{0}.fits out={1}/{0}.mir op=xyin".format(snapshot, args.resamp)
                print(s)

                subprocess.Popen(s, shell=True).wait()

            if os.path.exists("{1}/{0}.cvl".format(snapshot, args.resamp)) and args.useold:

                pass
            else:

                if os.path.exists("{1}/{0}.cvl".format(snapshot, args.resamp)):
                    shutil.rmtree("{1}/{0}.cvl".format(snapshot, args.resamp))

                bmaj_asec, bmin_asec = bmaj*3600., bmin*3600.

                s = "convol map={1}/{0}.mir fwhm={2},{3} pa=0.0 out={1}/{0}.cvl options=final".format(
                    snapshot, args.resamp, bmaj_asec, bmin_asec)
                print(s)

                with open("{1}/{0}.convol.log".format(snapshot, args.resamp), "w+") as log:
                    subprocess.Popen(s, shell=True, stdout=log, stderr=log).wait()

                if not check_convol_results("{1}/{0}.convol.log".format(snapshot, args.resamp)):

                    # assume convolution couldn't happen because the beam is already
                    # at the required resolution:

                    try:
                        shutil.copytree("{1}/{0}.mir".format(snapshot, args.resamp),
                                        "{1}/{0}.cvl".format(snapshot, args.resamp),
                                        )
                    except OSError:
                        shutil.rmtree("{1}/{0}.cvl".format(snapshot, args.resamp))
                        shutil.copytree("{1}/{0}.mir".format(snapshot, args.resamp),
                                        "{1}/{0}.cvl".format(snapshot, args.resamp),
                                        )



                if not os.path.exists("{1}/{0}.cvl".format(snapshot, args.resamp)):

                    logger.warning("{} did not convol - assuming it is already at the required resolution.".format(snapshot))

                    shutil.copytree("{1}/{0}.mir".format(snapshot, args.resamp), 
                                    "{1}/{0}.cvl".format(snapshot, args.resamp))

            

            if os.path.exists("{1}/{0}.regrid".format(snapshot, args.resamp)) and args.useold:
                pass

            else:

                if os.path.exists("{1}/{0}.regrid".format(snapshot, args.resamp)):
                    shutil.rmtree("{1}/{0}.regrid".format(snapshot, args.resamp))

                s = "regrid in={7}/{0}.cvl out={7}/{0}.regrid tol=0 axes=1,2 desc={1},{2},{3},{4},{5},{9},{8},{10} project={6}".format(
                    snapshot, args.coords[0], int(imcut[0]/2.), -cd, imcut[0], args.coords[1], args.projection, args.resamp, cd,
                    int(imcut[1]/2.), imcut[1])

                print(s)

                subprocess.Popen(s, shell=True).wait()



            if os.path.exists("{1}/{0}.regrid.fits".format(snapshot, args.resamp)) and args.useold:

                pass

            else:

                if os.path.exists("{1}/{0}.regrid.fits".format(snapshot, args.resamp)):
                    os.remove("{1}/{0}.regrid.fits".format(snapshot, args.resamp))

                s = "fits in={1}/{0}.regrid out={1}/{0}.regrid.fits op=xyout".format(snapshot, args.resamp)

                subprocess.Popen(s, shell=True).wait()
        


        resampled_images.append(args.resamp+"/"+snapshot+".regrid.fits")

        if os.path.exists("{1}/{0}.regrid_rms.fits".format(snapshot, args.resamp)) and args.useold:

            pass

        else:

            s = "BANE {1}/{0}.regrid.fits".format(snapshot, args.resamp)

            subprocess.Popen(s, shell=True).wait()

            os.remove(args.resamp+"/"+snapshot+".regrid_bkg.fits")
            os.remove(args.resamp+"/"+snapshot+".fits")

        rms_images.append(args.resamp+"/"+snapshot+".regrid_rms.fits")


        if args.beam_mosaic or args.weight_by_beam:

            if os.path.exists("{1}/{0}.beamI.fits".format(snapshot, args.resamp)) and args.useold:
                pass
            else:

                # Create a set of stokes I beam images to mosaic:
                if os.path.exists(metafitsfiles[j]):
                    shutil.copy2(metafitsfiles[j], "{}.metafits".format(snapshot[:10]))
                else:
                    make_metafits = "make_metafits.py -g {}".format(snapshot[:10])
                    subprocess.Popen(make_metafits, shell=True).wait()


                make_beam_image = "get_beam_image {}.metafits -i {}/{}.regrid.fits -o {}/{}.beamI.fits".format(snapshot[:10], args.resamp, snapshot,
                    args.resamp, snapshot)
                subprocess.Popen(make_beam_image, shell=True).wait()

            beam_images.append("{}/{}.beamI.fits".format(args.resamp, snapshot))


        for image_dir in [".regrid", ".mir", ".cvl"]:
            if os.path.exists("{1}/{0}{2}".format(snapshot, args.resamp, image_dir)):
                shutil.rmtree("{1}/{0}{2}".format(snapshot, args.resamp, image_dir))


        if args.projection != "SIN":
            with fits.open("{1}/{0}.regrid.fits") as f:
                f[0].data[:] = 1.
                f.writeto("{1}/{0}.regrid.dOmega.fits", overwrite=True)
                

        if os.path.exists("{1}/{0}.regrid.dOmega.fits".format(snapshot, args.resamp)) and args.useold:
            pass
        else:

            snapshot_r = "{1}/{0}.regrid".format(snapshot, args.resamp)

            sinfactors.make_ratio_map(fitsimage="{0}.fits".format(snapshot_r),
                                      outname="{0}.dOmega.fits".format(snapshot_r),
                                      ra0=hdr_i["CRVAL1"],
                                      dec0=hdr_i["CRVAL2"])          

        sin_images.append("{1}/{0}.regrid.dOmega.fits".format(snapshot, args.resamp))


    dOmega_stack = stack_one_by_one(sin_images, rms_images, beam_images)
    fits.writeto(args.outbase+"_dOmega.fits", dOmega_stack,
                 fits.getheader(sin_images[0]), overwrite=True)

    sinfactors.make_effective_psf(args.outbase+"_dOmega.fits",
                                  bmaj=bmaj, bmin=bmin, bpa=bpa,
                                  outname=args.outbase+"_psfmap.fits")


    avg_stack = stack_one_by_one(resampled_images, rms_images, beam_images)

    hdr = fits.getheader(resampled_images[0])
    hdr["BMAJ"] = bmaj
    hdr["BMIN"] = bmin
    hdr["BPA"] = bpa

    fits.writeto(args.outbase+".fits", avg_stack, hdr, clobber=True)

    if args.beam_mosaic:

        beam_stack = stack_one_by_one(beam_images, rms_images, beam_images)
        fits.writeto(args.outbase+"_beamI.fits", beam_stack, fits.getheader(beam_images[0]), overwrite=True)

        arr1 = np.squeeze(fits.getdata(beam_images[0]))
        for i in range(1, len(beam_images)):
            arr1 += np.squeeze(fits.getdata(beam_images[i]))

        fits.writeto(args.outbase+"_sumBeamI.fits", arr1, fits.getheader(beam_images[0]), overwrite=True)
    
    print(beams)

    # Add some information back in:
    for im in [""]:
        with fits.open("{}{}.fits".format(args.outbase, im), mode="update") as f:
            f[0].header["BMAJ"] = bmaj
            f[0].header["BMIN"] = bmin
            f[0].header["BPA"] = bpa
            f[0].header["BUNIT"] = "JY/BEAM"
            f[0].header["BTYPE"] = "INTENSITY"
            f[0].header["FREQ"] = frequency
            f.flush()

    time_taken = datetime.now()-t1

    # Record some stats:
    with open(args.outbase+"_stats.log", "w+") as f:
        f.write("# Total snapshots used: {}\n".format(len(snapshots)))
        f.write("# Time taken..........: {}\n".format(time_taken))

    logger.info("Time taken: {}".format(time_taken))

    sys.exit(0)


if __name__ == "__main__":
    main()




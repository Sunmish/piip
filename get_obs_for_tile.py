#! /usr/bin/env python

from argparse import ArgumentParser
import numpy as np
import os
from astropy.table import Table
from astropy.coordinates import SkyCoord
from astropy import units as u


def get_args():
    ps = ArgumentParser()
    ps.add_argument("-T", "--sky_tiling", default=None)
    ps.add_argument("-t", "--tile", default=None)
    ps.add_argument("--tile_ra", type=float, default=None)
    ps.add_argument("--tile_dec", type=float, default=None)
    ps.add_argument("--size_ra", type=float, default=None)
    ps.add_argument("--size_dec", type=float, default=None)
    ps.add_argument("obslist", type=str)
    ps.add_argument("-i", "--indir", default="./")
    ps.add_argument("-s", "--stub", default="_???MHz_r0.0_warp_rbf.fits")
    ps.add_argument("-S", "--sep", default=9.0, type=float)
    ps.add_argument("-o", "--outbase", default="tile")
    return ps.parse_args()


def cli(args):

    if args.tile is not None and args.sky_tiling is not None:
        args.tile_ra, args.tile_dec, args.size_ra, args.size_dec = \
            get_tile(args.sky_tiling, args.tile)

    find_obs(
        tile_coord=(args.tile_ra, args.tile_dec),
        tile_size=(args.size_ra/2, args.size_dec/2),
        obslist=args.obslist,
        indir=args.indir,
        stub=args.stub,
        sep=args.sep,
        outbase=args.outbase
    )


def get_tile(sky_tiling, tile):
    table = Table.read(sky_tiling)
    idx = np.where(table["TILE"] == tile)[0]
    tile_ra = np.asarray(table["RA"][idx])[0]
    tile_dec = np.asarray(table["DEC"][idx])[0]
    size_ra = np.asarray(table["RA_SIZE"][idx])[0]
    size_dec = np.asarray(table["DEC_SIZE"][idx])[0]
    return tile_ra, tile_dec, size_ra, size_dec


def find_obs(tile_coord, tile_size, obslist, 
    indir="./", 
    stub="_300MHz_r0.0_warp_rbf.fits", 
    sep=10.,
    outbase="tile"):

    obsids = Table.read(obslist)
    coords = SkyCoord(
        ra=obsids["RA"]*u.deg,
        dec=obsids["DEC"]*u.deg
    )
    tile_coord = SkyCoord(
        ra=tile_coord[0]*u.deg,
        dec=tile_coord[1]*u.deg
    )

    obsids_tile = []
    images_tile = []
    ra, dec = [], []

    boundaries = [
        (tile_coord.ra.value-tile_size[0], tile_coord.dec.value),
        (tile_coord.ra.value-tile_size[0], tile_coord.dec.value+tile_size[1]),
        (tile_coord.ra.value-tile_size[0], tile_coord.dec.value+tile_size[1]),
        (tile_coord.ra.value+tile_size[0], tile_coord.dec.value),
        (tile_coord.ra.value+tile_size[0], tile_coord.dec.value+tile_size[1]),
        (tile_coord.ra.value+tile_size[0], tile_coord.dec.value+tile_size[1]),
        (tile_coord.ra.value, tile_coord.dec.value),
        (tile_coord.ra.value, tile_coord.dec.value+tile_size[1]),
        (tile_coord.ra.value, tile_coord.dec.value+tile_size[1]),
    ]

    for boundary in boundaries:
        boundary_coords = SkyCoord(
            ra=boundary[0]*u.deg,
            dec=boundary[1]*u.deg
        )
        seps = boundary_coords.separation(coords)
        idx = np.where(seps.value < sep)[0]
        for i in idx:
            obsid = str(int(obsids["OBSID"][i]))
            if obsid not in obsids_tile:
                obsids_tile.append(obsid)
                image = "{}/{}{}".format(indir, obsids["OBSID"][i], stub)
                images_tile.append(image)
                ra.append(obsids["RA"][i])
                dec.append(obsids["DEC"][i])

    t = Table()
    t.add_column(obsids_tile, name="OBSID")
    t.add_column(images_tile, name="IMAGE")
    t.add_column(ra, name="RA")
    t.add_column(dec, name="DEC")


    filename = "J{:0>4}{}".format(
        # tile_coord.ra.to_string(u.hour, sep="")[0:4], 
        # tile_coord.dec.to_string(sep="", alwayssign=True, pad=True)[0:3]
    tile_coord.ra.to_string(u.hour, sep="", fields=2), 
        tile_coord.dec.to_string(sep="", alwayssign=True, pad=True, fields=1)
    )


    
    t.write("{}_{}_list.fits".format(outbase, filename
    ), overwrite=True)

    filename = "{}_{}_list.txt".format(outbase, filename
    )
    with open(filename, "w+") as f:
        for i in range(len(images_tile)):
            f.write("{}\n".format(images_tile[i]))
    
    with open(filename.replace(".txt", ".reg"), "w+") as f:
        for i in range(len(images_tile)):
            f.write("fk5;circle({} {} {}) # color=green ".format(
                ra[i], 
                dec[i], 
                sep/2) + r" text={" + "{}".format(obsids_tile[i]) + r"}" + "\n"
            )
    
    print("{} {} {} {} {}".format(
        filename, tile_coord.ra.value, tile_coord.dec.value, tile_size[0]*2, tile_size[1]*2)
    )


if __name__ == "__main__":
    cli(get_args())
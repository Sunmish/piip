#! /usr/bin/env python

import sys
import os
from astropy.io import fits

image = sys.argv[1]

if os.path.exists(image):
    with fits.open(image, mode="update") as f:
        f[0].data[:] = 0.  
        f.close()


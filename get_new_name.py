#! /usr/bin/env python

import sys
import os

from astropy.io import fits

import argparse


def main():
    """The main function."""

    ps = argparse.ArgumentParser(description="Rename images.")
    ps.add_argument("filename")
    ps.add_argument("-r", "--robust", default="", type=str)
    ps.add_argument("-v", "--stokesv", action="store_true")

    args = ps.parse_args()

    hdr = fits.getheader(args.filename)

    obsid = args.filename.split("_")[0]

    if args.stokesv:
        newname = "{}_{:0>3}MHz_{}_V.fits".format(
            obsid, int(round(hdr["FREQ"]/1.e6)), args.robust)
    else:
        newname = "{}_{:0>3}MHz_{}.fits".format(
            obsid, int(round(hdr["FREQ"]/1.e6)), args.robust)


    print(newname)


if __name__ == "__main__":
    main()



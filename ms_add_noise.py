#! /usr/bin/env python

import sys
import numpy as np
from casacore import tables

def set_noise(ms, sigma, col):
    """
    """

    MeasurementSet = tables.table(ms, readonly=False)
    data = MeasurementSet.getcol(col)
    noiser = np.random.normal(0., sigma, data.shape)
    noisei = np.random.normal(0., sigma, data.shape)
    data.real += noiser
    data.imag += noisei
    MeasurementSet.putcol(col, data)
    MeasurementSet.flush()
    MeasurementSet.close()


def main():
    """
    """

    ms = sys.argv[1]
    value = float(sys.argv[2])
    try:
        col = sys.argv[3]
    except IndexError:
        col = "DATA"

    set_noise(ms, value, col)


if __name__ == "__main__":
    main()




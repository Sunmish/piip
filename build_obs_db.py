#! /usr/bin/env python

import os
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.io import fits
from astropy.table import Table
from tqdm import trange

import numpy as np


from argparse import ArgumentParser


def get_args():
    ps = ArgumentParser()
    ps.add_argument("images", nargs="*")
    ps.add_argument("-o", "--outname", required=True)
    return ps.parse_args()


def build_db(images, outname):

    ra = np.full((len(images),), np.nan, dtype=float)
    dec = np.full((len(images),), np.nan, dtype=float)
    obsids = []

    pbar = trange(len(images))
    for i in pbar:
        image = images[i]
        pbar.set_description(image)
        header = fits.getheader(image)
        coords = SkyCoord(
            ra=header["CRVAL1"]*u.deg,
            dec=header["CRVAL2"]*u.deg
        )
        ra[i] = coords.ra.value
        dec[i] = coords.dec.value
        obsid = os.path.basename(image)[0:10]
        obsids.append(obsid)


    t = Table()
    t.add_column(obsids, name="OBSID")
    t.add_column(ra, name="RA")
    t.add_column(dec, name="DEC")

    t.write(outname, overwrite=True)


def cli(args):
    build_db(args.images, args.outname)

if __name__ == "__main__":
    cli(get_args())
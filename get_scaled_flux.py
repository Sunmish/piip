#! /usr/bin/env python

from argparse import ArgumentParser

def from_index(freq, flux0, freq0, alpha):
    return flux0*(freq/freq0)**alpha

def main():
    ps = ArgumentParser("Get flux density at a different frequency, "
                        "scaled by the spectral index.")
    ps.add_argument("freq", type=float, 
                    help="Frequency to evaluate flux density at.")
    ps.add_argument("-a", "--alpha", type=float, default=-0.77,
                    help="Spectral index. Default -0.77.")
    ps.add_argument("-f", "--freq0", type=float, default=88.0,
                    help="Freqeucny to extrapolate from (same unit as `freq`). Default 88 MHz.")
    ps.add_argument("-S", "--flux0", type=float, default=1.0,
                    help="Reference flux density at `freq0`. Default 1 Jy.")    
    ps.add_argument("-i", "--integer", action="store_true",
                    help="Print out as an integer using simple rounding.")
    args = ps.parse_args()

    flux1 = from_index(freq=args.freq,
                       flux0=args.flux0,
                       freq0=args.freq0,
                       alpha=args.alpha)

    if args.integer:
        print("{:.0f}".format(flux1))
    else:
        print(flux1)

if __name__ == "__main__":
    main()
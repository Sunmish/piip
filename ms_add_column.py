#! /usr/bin/env python

from argparse import ArgumentParser
from casacore.tables import *

def add_cols(ms1, ms2, col1, col2, replace=False):
    t1 = table(ms1, readonly=False)
    t2 = table(ms2, readonly=True)
    c1 = t1.getcol(col1)
    c2 = t2.getcol(col2)
    if replace:
        c1 = c2
    else:
        c1 += c2
    t1.putcol(col1, c1)
    t1.close()
    t2.close()


def main():

    ps = ArgumentParser()
    ps.add_argument("ms1")
    ps.add_argument("col1")
    ps.add_argument("ms2")
    ps.add_argument("col2")
    ps.add_argument("-r", "--replace", action="store_true",
        help="Replace col2 with col1.")
    args = ps.parse_args()

    add_cols(args.ms1, args.ms2, args.col1, args.col2,
        replace=args.replace)

if __name__ == "__main__":
    main()


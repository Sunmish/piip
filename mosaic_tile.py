#! /usr/bin/env python

from argparse import ArgumentParser
import numpy as np
from astropy.table import Table
from astropy.coordinates import SkyCoord
from astropy import units as u


def get_args():
    ps = ArgumentParser()
    ps.add_argument("-c", "-r", "--ref", default=[0., -26.7], nargs=2)
    ps.add_argument("-d", "--decs", 
        # default=[-72.0, -63.1, -55.0, -47.5, -40.4, -33.5, -26.7, -19.9, -13.0, -5.9, 1.6, 9.7, 18.6],
        default=None,
        nargs="*",
        type=float
    )
    ps.add_argument("-rs", "--ra_sep",
        default=20.0,
        type=float
    )
    ps.add_argument("-ds", "--dec_sep",
        default=10.0,
        type=float
    )
    ps.add_argument("-f", "--factor",
        default=1.1,
        type=float,
    )
    ps.add_argument("-o", "--outbase",
        default="output.csv",
        type=str
    )
    ps.add_argument("--max_dec",
        default=None,
        type=float
    )
    return ps.parse_args()


def tile_with_ref(ra_sep, dec_sep, factor, outbase, ref_dec=0., dec_range=(-80, 30), max_dec=None):
        
    cra, cdec = [], []
    rseps = []
    dseps = []
    tiles = []

    dec_abs_range = abs(dec_range[0]-dec_range[1])
    nd = dec_abs_range//dec_sep
    dec_sep = dec_abs_range/nd
    decs = np.concatenate([
        np.arange(ref_dec, dec_range[0], -dec_sep), 
        np.arange(ref_dec+dec_sep, dec_range[1], dec_sep)
    ])
    print(decs)
    # decs = np.arange(dec_range[0], dec_range[1], dec_sep)

    for dec in decs:
        
        if max_dec is not None:
            if dec > max_dec:
                continue

        rsep = ra_sep/np.cos(np.radians(dec))
        # rsep = ra_sep
        nr = 360.//rsep
        rsep = 360./nr
        ras = np.arange(0., 359, rsep)
        rfactor = factor*np.cos(np.radians(dec+dec_sep*0.5))
        for ra in ras:
            cra.append(ra)
            cdec.append(dec)
            rseps.append(rsep*rfactor)
            dseps.append(dec_sep*factor)

            tile_coord = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)
            filename = "J{:0>4}{}".format(
                # tile_coord.ra.to_string(u.hour, sep="")[0:4], 
                # tile_coord.dec.to_string(sep="", alwayssign=True, pad=True)[0:3]
            tile_coord.ra.to_string(u.hour, sep="", fields=2), 
                tile_coord.dec.to_string(sep="", alwayssign=True, pad=True, fields=1)
            )

            tiles.append(filename)

    t = Table()
    t.add_column(tiles, name="TILE")
    t.add_column(cra, name="RA")
    t.add_column(cdec, name="DEC")
    t.add_column(rseps, name="RA_SIZE")
    t.add_column(dseps, name="DEC_SIZE")
    t.write(outbase+".fits", overwrite=True)

    with open(outbase+".csv", "w+") as f:
        f.write("RA,DEC,RA_SIZE,DEC_SIZE\n")
        for i in range(len(cra)):
            f.write("{},{},{},{},{}\n".format(tiles[i],cra[i], cdec[i], rseps[i], dseps[i]))

    with open(outbase+".txt", "w+") as f:
        for i in range(len(cra)):
            f.write("{} {} {} {} {}\n".format(tiles[i],cra[i], cdec[i], rseps[i], dseps[i]))



def tile(decs, sep, factor, outbase):
        
    cra, cdec = [], []
    rseps = []
    dseps = []

    for dec in decs:
        rsep = sep/np.cos(np.radians(dec))
        nr = 360.//rsep
        rsep = 360./nr
        ras = np.arange(0., 359, rsep)
        rfactor = factor/np.cos(np.radians(dec))
        for ra in ras:
            cra.append(ra)
            cdec.append(dec)
            rseps.append(rsep*factor)
            dseps.append(rsep*np.cos(np.radians(dec))*factor)

    t = Table()
    t.add_column(cra, name="RA")
    t.add_column(cdec, name="DEC")
    t.add_column(rseps, name="RA_SIZE")
    t.add_column(dseps, name="DEC_SIZE")
    t.write(outbase+".fits", overwrite=True)


    with open(outbase+".csv", "w+") as f:
        f.write("RA,DEC,RA_SIZE,DEC_SIZE\n")
        for i in range(len(cra)):
            f.write("{},{},{},{}\n".format(cra[i], cdec[i], rseps[i], dseps[i]))


def cli(args):
    
    if args.decs is not None:
        tile(
            decs=args.decs,
            sep=args.ra_sep,
            factor=args.factor,
            outbase=args.outbase
        )
    else:
        tile_with_ref(
            ra_sep=args.ra_sep, 
            dec_sep=args.dec_sep, 
            factor=args.factor, 
            outbase=args.outbase, 
            ref_dec=args.ref[1],
            dec_range=(-80, 30),
            max_dec=args.max_dec
        )


if __name__ == "__main__":
    cli(get_args())
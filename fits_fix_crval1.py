#! /usr/bin/env python

import sys
from astropy.io import fits

with fits.open(sys.argv[1], mode="update") as f:
    if "CRVAL1" in f[0].header.keys():
        crval1 = "CRVAL1"
    else:
        crval1 = "CD1_1"

    if f[0].header[crval1] < 0:
        f[0].header[crval1] += 360. 

    f.flush()
    f.close()
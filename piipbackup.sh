#!/bin/bash -l

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

module load miniocli
source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

CLEANUP=false

start_time=$(date +%s)

opts=$(getopt \
     -o :hd:O:f:a:C: \
     --long help,processing_dir:,obsid_list:,field:,obsnum:,cleanup: \
     --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piipimage
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

set -x

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipimage ;;
        -d|--processing_dir) INDIR=$2 ; shift 2 ;;
        -O|--obsid_list) obsid_list=$2 ; shift 2 ;;
        -f|--field) field=$2 ; shift 2 ;;
        -a|--obsnum) obsnum=$2 ; shift 2 ;;
        -C|--cleanup) CLEANUP=true; shift ;;
        --) shift ; break ;;
        *) echo "ERROR." ;;
    esac
done

# Set obsid from array:
if [ -z ${SLURM_ARRAY_TASK_ID} ]; then
    check_required "--obsnum" "-a" ${obsnum}
else
    obsnum=${SLURM_ARRAY_TASK_ID}  
fi

if [ ${#obsnum} = 10 ]; then
    obsid=${obsnum}
else
    check_required "--obsid_list" "-o" $obsid_list
    obsid=$(sed "${obsnum}q;d" ${obsid_list} | awk '{print $1}')
fi

cd ${INDIR}/ || exit 1

chan=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "CENTCHAN")

if [ ! -z ${field} ] && [ ! -z ${DBFILE} ]; then
    d_format=$(date +%F:%H:%M:%S)
    if [ ${#obsnum} -lt 10 ]; then
        extraOptions="--selection=${obsnum}"
    else
        extraOptions=""
    fi
    $container ${PIIPPATH}/field_manager.py -m "update_snapshot_status" \
        -d ${DBFILE} \
        --field=${field} \
        --chan=${chan} \
        --status="start-backup-9-[${d_format}]" \
        --overwrite \
        --obsid=${obsid} ${extraOptions}
fi

tar -cf ${obsid}.tar ${obsid}
mc cp ${obsid}.tar ${BACKUP_LOCATION}/

if [ ! -z ${field} ] && [ ! -z ${DBFILE} ]; then
    d_format=$(date +%F:%H:%M:%S)
    if [ ${#obsnum} -lt 10 ]; then
        extraOptions="--selection=${obsnum}"
    else
        extraOptions=""
    fi
    $container ${PIIPPATH}/field_manager.py -m "update_snapshot_status" \
        -d ${DBFILE} \
        --field=${field} \
        --chan=${chan} \
        --status="backup-9-[${d_format}]" \
        --overwrite \
        --obsid=${obsid} ${extraOptions}
fi

end_time=$(date +%s)
duration=$(echo "${end_time}-${start_time}" | bc -l)
echo "Total time taken: ${duration}"

exit 0


#! /usr/bin/env python

from argparse import ArgumentParser

import numpy as np
from astropy.io import fits
from astropy.stats import sigma_clip

def get_args():

    ps = ArgumentParser()
    ps.add_argument("images", nargs="*")
    ps.add_argument("-r", "--rms_cut", type=float, default=100., 
        help="RMS cut in mJy/beam.")
    ps.add_argument("-g", "--good", action="store_true")
    ps.add_argument("-b", "--bad", action="store_true")
    ps.add_argument("-p", "--print", action="store_true")
    
    return ps.parse_args()

def cut_images(images, rms_cut=100., print_out=False):
    """Remove images from the input list when rms>rms_cut."""


    good_images = []
    bad_images = []

    for image in images:
        
        data = fits.getdata(image)
        rms = np.nanstd(sigma_clip(data[
            int(0.2*data.shape[0]):int(0.8*data.shape[0]),
            int(0.2*data.shape[1]):int(0.8*data.shape[1])],
            sigma=3, maxiters=5)
        )*1000.
        
        if print_out:
            print("{}: {} mJy/beam".format(image, rms))

        if rms <= rms_cut:

            good_images.append(image)

        else:

            bad_images.append(image)
    
    if print_out:
        print("N good: {}".format(len(good_images)))
        print(" N bad: {}".format(len(bad_images)))

    return good_images, bad_images


def cli(args):

    good_images, bad_images = cut_images(
        images=args.images,
        rms_cut=args.rms_cut,
        print_out=args.print
    )

    if args.good:
        print(
            " ".join(good_images)
        )
    if args.bad:
        print(
            " ".join(bad_images)
        )



if __name__ == "__main__":
    cli(get_args())


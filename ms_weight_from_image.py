#! /usr/bin/env python

from argparse import ArgumentParser

import numpy as np
from casacore.tables import table

from astropy.io import fits
from astropy.stats import sigma_clip


def get_args():
    ps = ArgumentParser()
    ps.add_argument("msname")
    ps.add_argument("image")
    return ps.parse_args()



def weight_by_rms(msname, image):

    ms = table(msname, readonly=False, ack=False)
    data = np.squeeze(fits.getdata(image))
    rms = np.nanstd(sigma_clip(data[
        int(0.2*data.shape[0]):int(0.8*data.shape[0]),
        int(0.2*data.shape[1]):int(0.8*data.shape[1])],
        sigma=3, maxiters=5)
    )*1000.

    weights = ms.getcol("WEIGHT_SPECTRUM")
    weights *= 1./rms**2

    ms.putcol("WEIGHT_SPECTRUM", weights)

    ms.flush()
    ms.close()


def cli(args):
    weight_by_rms(args.msname, args.image)


if __name__ == "__main__":
    cli(get_args())

    

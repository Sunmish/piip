#! /usr/bin/env python

import sys
from casacore.tables import table, maketabdesc, makecoldesc

def set_column_value(ms, value, col):
    """If the MODEL_DATA column exists, set it to `value`."""

    MeasurementSet = table(ms, readonly=False)
    if col not in MeasurementSet.colnames():
        # create column if it does not exist, populate with DATA, which
        # we assume to exist for obvious reasons
        cdesc = MeasurementSet.getcoldesc("DATA")
        dminfo = MeasurementSet.getdminfo("DATA")
        dminfo["NAME"] = col
        cdesc["comment"] = "The {} column".format(col)
        MeasurementSet.addcols(maketabdesc(makecoldesc(col, cdesc)), dminfo)
        data_ = MeasurementSet.getcol(col)
        MeasurementSet.putcol(col, data_)
    data = MeasurementSet.getcol(col)
    data[:] = value
    MeasurementSet.putcol(col, data)
    MeasurementSet.flush()
    MeasurementSet.close()

def main():
    """
    """

    ms = sys.argv[1]
    try:
        col = sys.argv[2]
    except IndexError:
        col = "MODEL_DATA"

    set_column_value(ms, 0.0, col)


if __name__ == "__main__":
    main()
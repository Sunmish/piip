#! /usr/bin/env python

import numpy as np

from argparse import ArgumentParser
from astropy.io import fits


def strip_wcsaxes(hdr):
    """Strip extra axes in a header object."""

    remove_keys = [key+i for key in 
                   ["CRVAL", "CDELT", "CRPIX", "CTYPE", "CUNIT", "NAXIS"]
                   for i in ["3", "4"]]

    for key in remove_keys:
        if key in hdr.keys():
            del hdr[key]

    return hdr


def strip_naxis(image):
    with fits.open(image, mode="update") as f:
        f[0].data = np.squeeze(f[0].data)
        f[0].header = strip_wcsaxes(f[0].header)
        f.flush()
        f.close()


def stack(images, rms_weights=None, beam_weights=None):
    """
    """

    for image in images:
        strip_naxis(image)
    if beam_weights is not None:
        for image in beam_weights:
            strip_naxis(image)

    print("Initialising stack...")

    arr1 = fits.getdata(images[0])
    if len(arr1.shape) > 2:
        arr1 = arr1[0, :, :]

    stacked = np.full_like(arr1, 0., dtype=np.float32)
    weights = np.full_like(arr1, 0., dtype=np.float32)

    for i in range(0, len(images)):

        print("Adding {} {}/{}".format(images[i], i, len(images)))

        arr1 = fits.getdata(images[i])
        if len(arr1.shape) > 2:
            arr1 = arr1[0, :, :]
        arr1[np.where(~np.isfinite(arr1))] = 0.

        arr2 = np.full_like(arr1, 1., dtype=np.float32)
        if rms_weights is not None:
            arr2_0 = fits.getdata(rms_weights[i])
            arr2_0[~np.isfinite(arr2_0)] = 1.e30 
            arr2 *= (1./(arr2_0)**2)
        if beam_weights is not None:
            arr2_0 = fits.getdata(beam_weights[i])
            arr2_0[~np.isfinite(arr2_0)] = 1.e-30
            arr2 *= (arr2_0)**2

        stacked += (arr1*arr2)
        weights += arr2

    return (stacked / weights)


def main():
    """
    """

    ps = ArgumentParser()
    ps.add_argument("outbase", type=str)
    ps.add_argument("-i", "--images", nargs="*", type=str)
    ps.add_argument("-b", "--beam_weights", nargs="*", type=str)
    ps.add_argument("-r", "--rms_weights", nargs="*", type=str)
    ps.add_argument("-p", "--psf_maps", "--omega_maps", nargs="*", type=str, 
        default=None)
    ps.add_argument("-O", "--Omega", action="store_true")
    ps.add_argument("-m", "--multiply", type=float, default=1.)

    args = ps.parse_args()


    hdr = fits.getheader(args.images[0])

    data = stack(args.images, rms_weights=args.rms_weights, 
        beam_weights=args.beam_weights)*args.multiply
    fits.writeto(args.outbase+".fits", data, hdr, overwrite=True)

    if args.psf_maps is not None:
        psf_data = stack(args.psf_maps, beam_weights=args.beam_weights)
        shape = psf_data.shape
        psf_map = np.full((3, shape[0], shape[1]), np.nan)
        psf_map[0, ...] = psf_data
        psf_map[1, ...] = hdr["BMIN"]
        try:
            psf_map[2, ...] = hdr["BPA"]
        except KeyError:
            psf_map[2, ...] = 0.

        fits.writeto(args.outbase+"_psfmap.fits", psf_map, hdr, overwrite=True)



if __name__ == "__main__":
    main()

#! /usr/bin/env python

from __future__ import print_function, division

import numpy as np
import os
import sys
import shutil
from glob import glob

from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.wcs import WCS
from astropy.table import Table
from astropy.stats import sigma_clip

from argparse import ArgumentParser

try:
    import subprocess32 as subprocess
except ImportError:
    import subprocess
Popen = subprocess.Popen

from datetime import datetime

from astropy.io import fits

import sinfactors


import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Colors:
    Yellow = (255, 255, 0)
    MediumPurple = (147, 112, 219)
    DogderBlue = (30, 144, 255)
    MediumSpringGreen = (0, 250, 154)
    Orange = (255, 165, 0)
def _join(*values):
    return ";".join(str(v) for v in values)
def color_text(s, c, base=30):
    template = "\x1b[{0}m{1}\x1b[0m"
    t = _join(base+8, 2, _join(*c))
    return template.format(t, s)


def fitsin(fitsfile, mirfile):
    with open("fits.log", "a+") as log:
        Popen("fits in={} out={} op=xyin".format(fitsfile, mirfile), 
              stderr=log, stdout=log, shell=True).wait()
def fitsout(fitsfile, mirfile):
    with open("fits.log", "a+") as log:
        Popen("fits in={} out={} op=xyout".format(mirfile, fitsfile),
              stderr=log, stdout=log, shell=True).wait()
def convol_wrapper(mirfile, outfile, beam):
    with open("convol.log", "a+") as log:
        Popen("convol map={} out={} fwhm={},{} pa={} options=final".format(
               mirfile, outfile, beam[0], beam[1], beam[2]), 
               stderr=log, stdout=log, shell=True).wait()
def regrid_wrapper(mirfile, outfile, crval, crpix, cdelt, naxis, proj="ZEA"):
    with open("regrid.log", "a+") as log:
        Popen("regrid axes=1,2 in={} out={} desc={},{},{},{},{},{},{},{} project={}".format(
              mirfile, outfile, crval[0], crpix[0], cdelt[0], naxis[0],
              crval[1], crpix[1], cdelt[1], naxis[1], proj), stderr=log, stdout=log, 
              shell=True).wait()
def bane_wrapper(fitsfile, grid, box):
    with open("bane.log", "a+") as log:
        Popen("BANE {0} --cores=1 --grid {1} {1} --box {2} {2}".format(fitsfile, 
              grid, box), stderr=log, stdout=log, shell=True).wait()
def make_metafits(obsid):
    with open("make_metafits.log", "a+") as log:
        Popen("wget http://ws.mwatelescope.org/metadata/fits/?obs_id={0} -O {0}.metafits".format(obsid),
              stdout=log, stderr=log, shell=True).wait()
def make_beam_image(obsid, fitsfile, outfile):
    with open("make_beam_image.log", "a+") as log:
        Popen("get_beam_image {}.metafits -i {} -o {}".format(obsid, fitsfile,
              outfile), stdout=log, stderr=log, shell=True).wait()

def beamcon_2D_wrapper_dryrun(images, resamp, logfile):
    # requires update to beamcon_2D: https://github.com/Sunmish/RACS-tools/tree/textfile-input
    with open(f"{logfile}.images", "w+") as f:
        for image in images:
            f.write(f"{image}\n")
    
    with open("beamcon_2D.log", "a+") as log:
        s = "beamcon_2D -o {0} --dryrun --log {1} {1}.images --list -v".format(
            resamp, logfile
        )
        Popen(s, stdout=log, stderr=log, shell=True).wait()
    bdata = Table.read(logfile, format="ascii.commented_header")
    bmaj = bdata["Target BMAJ"][0]
    bmin = bdata["Target BMIN"][0]
    bpa = bdata["Target BPA"][0]
    return bmaj, bmin, bpa

def beamcon_2D_wrapper_wetrun(image, bmaj, bmin, bpa, suffix):
    
    shutil.copy2(image, image.replace(".fits", ".zero.fits"))
    nan_to_zero(image.replace(".fits", ".zero.fits"))
    
    with open("beamcon_2D.log", "a+") as log:
        s = "beamcon_2D {} -s {} --bmaj {} --bmin {} --bpa {} -v".format(
            image.replace(".fits", ".zero.fits"), 
            suffix, bmaj*3600., bmin*3600., bpa
        )


        logger.debug(s)
        Popen(s, shell=True, stdout=log, stderr=log).wait()

    shutil.copy2(image.replace(".fits", f".zero.{suffix}.fits"), image.replace(".fits", f".{suffix}.fits"))
    os.remove(image.replace(".fits", f".zero.{suffix}.fits"))
    os.remove(image.replace(".fits", ".zero.fits"))

    reference = fits.open(image)
    with fits.open(image.replace(".fits", f".{suffix}.fits"), mode="update") as f:
        f[0].data[np.where(np.isnan(reference[0].data))] = np.nan
        f.flush()
    reference.close()
    
    return image.replace(".fits", ".{}.fits".format(suffix))



def strip_axes(image):
    with fits.open(image, mode="update") as f:
        f[0].data = np.squeeze(f[0].data)
        f[0].header = sinfactors.strip_wcsaxes(f[0].header)
        f.flush()
        f.close()

def blank_beyond_horizon(fitsimage, stride=225000, value=np.nan):
    with fits.open(fitsimage, mode="update") as f:

        hdr = f[0].header
        arr = np.squeeze(f[0].data)
        wcs = WCS(hdr).celestial

        indices = np.indices(arr.shape)
        y = indices[0].flatten()
        x = indices[1].flatten()
        n = len(x)

        for i in range(0, n, stride):

            r, d = wcs.all_pix2world(x[i:i+stride], y[i:i+stride], 0)
            b = np.where(~np.isfinite(r))[0]

            f[0].data[..., y[i:i+stride][b], x[i:i+stride][b]] = value

        f.flush()
        f.close()

def nan_to_zero(fitsimage, abs_value=1e30):

    with fits.open(fitsimage, mode="update") as f:
        f[0].data[np.where(np.abs(f[0].data) > abs_value)] = np.nan
        f.flush()

    with fits.open(fitsimage, mode="update") as f:
        f[0].data[np.where(~np.isfinite(f[0].data))] = 0.
        f.flush()

def pb_clip(fitsimage, beamimage, clip_level=0.05):
    """Clip fitsimage below clip_level*beam_image.
    """

    logger.info("Clipping {}".format(fitsimage))
    beam = fits.getdata(beamimage)
    with fits.open(fitsimage, mode="update") as f:
        f[0].data[np.where(beam < clip_level)] = np.nan
        f.flush()





def stack_one_by_one(images, weights):
    for image in images:
        strip_axes(image)
    for image in weights:
        strip_axes(image)

    logger.info("Initialising stack")

    logger.info("{} 1/{}".format(images[0], len(images)))

    arr1 = fits.getdata(images[0])
    arr2 = fits.getdata(weights[0])

    arr1[np.where(~np.isfinite(arr1))] = 0.
    arr2[np.where(~np.isfinite(arr2))] = 1.e-30

    values = arr1*arr2
    all_weights = arr2

    for i in range(1, len(images)):

        logger.info("{} {}/{}".format(images[i], i+1, len(images)))

        arr1 = fits.getdata(images[i])
        arr2 = fits.getdata(weights[i])
        arr1[np.where(~np.isfinite(arr1))] = 0.
        arr2[np.where(~np.isfinite(arr2))] = 1.e-30

        values += (arr1*arr2)
        all_weights += arr2

    return values / all_weights


def main():
    """
    """

    _help = {"level": "Clip image below Stokes I beam*level if beam maps are being used. [Default 0.05]",
             "max_rms": "Throw away images where median rms is > `max_rms`.",
             "proj": "Projection of output FITS files."}

    ps = ArgumentParser()

    ps.add_argument("imagelist", nargs="*")
    ps.add_argument("-o", "--outbase", type=str, default="mosaic")
    ps.add_argument("-c", "--coordinates", dest="coords",
                    type=float, nargs=2, metavar=("RA", "DEC"),
                    required=True)
    ps.add_argument("-s", "-I", "--imsize", "--size", "--fov", 
                    type=float, default=[1., 1.], nargs=2, dest="imsize")
    ps.add_argument("-U", "--use-old", "--use_old", dest="overwrite",
                    action="store_false")

    ps.add_argument("-r", "--resample_directory", "--resample-directory",
                    dest="resamp", type=str, default="./resamp/")
    ps.add_argument("--use_short_names", action="store_true")
    ps.add_argument("-b", "--beam_weight", "--beam-weight",
                    dest="beam_weight", action="store_true")
    ps.add_argument("-n", "--noise_weight", "--noise-weight",
                    dest="noise_weight", action="store_true")
    ps.add_argument("--single_noise_value_weight", action="store_true")
    ps.add_argument("--rms_box", default=None, type=int)
    ps.add_argument("--rms_grid", default=None, type=int)
    ps.add_argument("-B", "--beam_mosaic", "--beam-mosaic",
                    dest="beam_mosaic", action="store_true")
    ps.add_argument("-C", "--convolve", action="store_true")
    ps.add_argument("--max_beam", default=None, type=float)
    ps.add_argument("-p", "--psf", nargs=3, default=None,
                    type=float)
    ps.add_argument("-P", "--pad", type=float, default=1.)
    ps.add_argument("-l", "--level", "--clip-level", "--clip_level",
                    type=float, default=None, dest="level", help=_help["level"])
    ps.add_argument("--max_rms", "--max-rms", default=1e30, type=float,
                    help=_help["max_rms"])

    ps.add_argument("-O", "--obsids", type=str, default=None)
    ps.add_argument("--obsid_from_name", action="store_true")
    ps.add_argument("-R", "--residuals", type=str, default=None, nargs="*")
    ps.add_argument("-M", "--models", type=str, default=None, nargs="*")

    ps.add_argument("--frequency", type=float, default=None)
    ps.add_argument("--projection", type=str, default="ZEA", help=_help["proj"])

    ps.add_argument("--find_psf", action="store_true")
    ps.add_argument("--find_pixel", action="store_true")
    ps.add_argument("--pixel_factor", default=5, type=float)
    ps.add_argument("--ignore_broken", action="store_true")
    ps.add_argument("--clip_beam_std", default=None, type=float)
    ps.add_argument("--clip_rms_std", default=None, type=float)
    ps.add_argument("--min_rms", default=1e-3, type=float)
    ps.add_argument("--ignore_missing", action="store_true")
    ps.add_argument("--abs_max", default=1e30, type=float,
        help="Maximum absolute value to include - absolute values above this will be set to NaN."
    )
    ps.add_argument("--apply_beam", action="store_true",
        help="Apply Stokes I beam to images."
    )

    args = ps.parse_args()


    

    # ------------------------------------------------------------------------ #

    # Work out what images we've got to work with:
    if len(args.imagelist) == 1:    
        in_snapshots = np.genfromtxt(args.imagelist[0], dtype=str)
        print(in_snapshots)
        snapshots = []
        for i in range(len(in_snapshots)):
            try:
                snapshots.append(glob(in_snapshots[i])[0])
            except IndexError:
                if args.ignore_missing:
                    logger.debug("Missing {}".format(in_snapshots[i]))
                    pass
                else:
                    raise IndexError("Missing snapshot {}".format(in_snapshots[i]))
    else:
        snapshots = args.imagelist



    print(snapshots)

    if args.residuals is not None:
        if len(args.residuals) == 1:
            residualslist = np.genfromtxt(args.residuals, dtype=str)
        else:
            residualslist = args.residuals
        if len(residualslist) != len(snapshots):
            raise RuntimeError("Residual and image list not equal!")
    if args.models is not None:
        if len(args.models) == 1:
            modelslist = np.genfromtxt(args.models, dtype=str)
        else:
            modelslist = args.models
        if len(modelslist) != len(snapshots):
            raise RuntimeError("Model and image list not equal!")
    if args.obsids is not None:
        obsids = np.genfromtxt(args.obsids, dtype=str)        
        if len(obsids) != len(snapshots):
            raise RuntimeError("Obsids and image list not equal!")
    elif (args.beam_mosaic or args.beam_weight) and not args.obsid_from_name:
        raise RuntimeError("Beam mosaic/weights require obsids.")

    if not os.path.exists(args.resamp):
        os.mkdir(args.resamp)



    logger.info("Number of snapshots: {}".format(len(snapshots)))

    # Find frequency  and pixel information information:
    ref_hdr = fits.getheader(snapshots[0])

    if args.frequency is None:
        if "FREQ" in ref_hdr.keys():
            frequency = ref_hdr["FREQ"]
        elif "CRVAL3" in ref_hdr.keys():
            frequency = ref_hdr["CRVAL3"]
        elif "CENTCHAN" in ref_hdr.keys():
            frequency = int(ref_hdr["CENTCHAN"])*1.28*1.e6
        else:
            raise ValueError("No frequency information found in {}".format(snapshots[0]))
    else:
        frequency = args.frequency

    logger.info("Found frequency of {:.3f} MHz".format(round(frequency/1.e6, 3)))

    # Find largest beam and smallest pixel:
    bmaj, bmin, bpa, cd = 0., 0., 0., 1.e9
    bpas = []

    
    temp_snapshots = []
    temp_obsids = []
    rejected_for_beam = 0
    bmajs = []
    bmins = []
    rmses = []
    rmses_no_clip = []
    for i, snapshot in enumerate(snapshots):


        header = fits.getheader(snapshot)

        if args.max_beam is not None:
            if header["BMAJ"]*3600 > args.max_beam or \
                header["BMIN"]*3600 > args.max_beam:
                rejected_for_beam += 1
                continue

        bmajs.append(header["BMAJ"]*3600.)
        bmins.append(header["BMIN"]*3600.)

        if header["BMAJ"] > bmaj:
            bmaj = header["BMAJ"]
        if header["BMIN"] > bmin:
            bmin = header["BMIN"]

        logger.debug("{} has {:.1f}\" times {:.1f}\", {:.1f} deg".format(snapshot, 
            header["BMAJ"]*3600., header["BMIN"]*3600., header["BPA"]))

        if "CDELT1" in header.keys():
            if abs(header["CDELT1"]) < cd:
                cd = abs(header["CDELT1"])
        elif "CD1_1" in header.keys():
            if abs(header["CD1_1"]) < cd:
                cd = abs(header["CD1_1"])

        bpas.append(header["BPA"])

        if args.clip_rms_std is not None:
            try:
                data = np.squeeze(fits.getdata(snapshot))
                rms = np.nanstd(sigma_clip(data[
                    int(0.2*data.shape[0]):int(0.8*data.shape[0]),
                    int(0.2*data.shape[1]):int(0.8*data.shape[1])],
                    sigma=3, maxiters=5)
                )
                big_rms = np.nanstd(data)
                if rms > args.max_rms:
                    raise ValueError("rms > max rms")
                if not np.isfinite(rms) or not np.isfinite(big_rms):
                    raise ValueError("nan rms?")
            except Exception:
                # raise
                rms = np.nan
                big_rms = np.nan
            rmses.append(rms)
            rmses_no_clip.append(big_rms)
            logger.debug("{} has {:.3f}  ({:.3f}) mJy/beam noise".format(snapshot, rms*1000., big_rms*1000.))

        temp_snapshots.append(snapshot)

    clipped_bmaj_values = sigma_clip(bmajs, sigma=3, maxiters=2)
    std_bmaj = np.std(clipped_bmaj_values)
    mean_bmaj = np.median(bmajs)
    
    snapshots_beam = []
    rmses_beam = []
    rmses_no_clip_beam = []
    if args.clip_beam_std is not None:
        new_bmajs, new_bmins, new_bpas = [], [], []
        logger.info("BMAJ stddev: {:.2f}".format(std_bmaj))
        logger.info("BMAJ mean: {:.2f}".format(mean_bmaj))
        logger.info("Clipping BMAJ > {:.2f}".format(args.clip_beam_std*std_bmaj+mean_bmaj))
        for i in range(len(temp_snapshots)):
            if bmajs[i] < (args.clip_beam_std*std_bmaj)+mean_bmaj:
                logger.debug("{} BMAJ = {:.2f}".format(temp_snapshots[i], bmajs[i]))
                snapshots_beam.append(temp_snapshots[i])
                rmses_beam.append(rmses[i])
                rmses_no_clip_beam.append(rmses_no_clip[i])
                new_bmajs.append(bmajs[i])
                new_bmins.append(bmins[i])
                new_bpas.append(bpas[i])
            else:
                logger.debug("Throwing away {} as BMAJ = {:.2f}".format(temp_snapshots[i], bmajs[i]))
                rejected_for_beam += 1
        logger.info("{} snapshots selected based on BMAJ.".format(len(snapshots_beam)))

        rmses_no_clip = rmses_no_clip_beam
        rmses = rmses_beam
        bmajs = new_bmajs
        bmins = new_bmins
        bpas = new_bpas
    else:
        snapshots_beam = temp_snapshots


    snapshots = []
    if args.clip_rms_std is not None:
        std_rms = np.nanstd(rmses)
        median_rms = np.nanmedian(rmses)

        logger.info("RMS stddev: {:.2f}".format(std_rms*1000.))
        logger.info("RMS median: {:.2f}".format(median_rms*1000.))
        logger.info("Clipping RMS > {:.2f}".format(args.clip_rms_std*std_rms+median_rms))
        for i in range(len(snapshots_beam)):
            logger.debug("{}: {:.2f} {:.2f}".format(snapshots_beam[i], rmses[i]*1000., rmses_no_clip[i]*1000.))
            if ((args.min_rms < rmses[i] < args.clip_rms_std*std_rms+median_rms) and (rmses_no_clip[i] < args.max_rms)) and \
                 np.isfinite(rmses[i]):
                snapshots.append(snapshots_beam[i])
            else:
                logger.debug("Throwing away {} because RMS = {:.2f} ({:.2f}) ".format(snapshots_beam[i], rmses[i]*1000., rmses_no_clip[i]*1000.))
        logger.info("{} snapshots selected based on RMS.".format(len(snapshots)))
    else:   
        snapshots = snapshots_beam

    logger.info("{} snapshots rejected for large beam size.".format(rejected_for_beam))

    bmaj = np.max(bmajs)
    bmaj /= 3600.
    bmin = np.max(bmins)
    bmin /= 3600.
    bpa = np.degrees(np.arctan2((np.sum(np.cos(np.radians(bpas)))/len(bpas)), 
                (np.sum(np.sin(np.radians(bpas)))/len(bpas))))
    bpa = np.mean(bpas)

    bmaj += args.pad/3600.
    bmin += args.pad/3600.

    if args.psf is not None:
        bmaj, bmin, bpa = args.psf
    elif args.find_psf:
        bmaj, bmin, bpa = beamcon_2D_wrapper_dryrun(
            images=snapshots, 
            resamp=args.resamp, 
            logfile=args.outbase + ".psf.log"
        )

    logger.info("Found beam information: {:.1f}\" times {:.1f}\", {:.1f} deg".format(
        bmaj*3600., bmin*3600., bpa))


    if args.find_pixel:
        cd = (bmin/args.pixel_factor)
        logger.info("Found pixel size: {:.1f}\"".format(cd*3600.))


    # Work out image size in pixels:
    imsize = (int(args.imsize[0]/cd), int(args.imsize[1]/cd))

    logger.info("Setting image size to {} by {} pixels".format(imsize[0],
                                                               imsize[1]))

    # ------------------------------------------------------------------------ #

    # ------------------------------------------------------------------------ #

    # Move images to a shorter directory (i.e. resample directory) so that 
    # miriad does not complain about long filenames...

    logger.info("Copying images to {} for processing".format(args.resamp))

    images, models, residuals, obsids_from_name = [], [], [], []
    for i in range(len(snapshots)):

        logger.debug("Copying {} to {}".format(snapshots[i], args.resamp))
        image = os.path.basename(snapshots[i])
        
        if args.use_short_names:
            image = image[0:10] + ".fits"
        if not os.path.exists("{}/{}".format(args.resamp, image)) or args.overwrite:
            shutil.copy2(snapshots[i], "{}/{}".format(args.resamp, image))
        
        images.append("{}/{}".format(args.resamp, image))

        if args.models is not None:
            model = os.path.basename(modelslist[i])
            if not os.path.exists("{}/{}".format(args.resamp, model)) or args.overwrite:
                shutil.copy2(modelslist[i], "{}/{}".format(args.resamp, model))
            models.append("{}/{}".format(args.resamp, model))
        else:
            models.append(None)

        if args.residuals is not None:
            residual = os.path.basename(residualslist[i])
            if not os.path.exists("{}/{}".format(args.resamp, residual)) or args.overwrite:
                shutil.copy2(residualslist[i], "{}/{}".format(args.resamp, residual))
            residuals.append("{}/{}".format(args.resamp, residual))
        else:
            residuals.append(None)

        if args.obsid_from_name:
            obsid = os.path.basename(snapshots[i])[0:10]
            obsids_from_name.append(obsid)

    if args.obsid_from_name:
        obsids = obsids_from_name

    # ------------------------------------------------------------------------ #

    # Time for some fun!

    weights, dOmegas, pbeams = [], [], []
    final_images = []

    rms_images = []
    median_rms = []

    for i, image in enumerate(images):



        s = color_text("Working on {}/{}: {}".format(i+1, len(images), image), Colors.MediumSpringGreen)
        logger.info(s)


        try:

            header = fits.getheader(image)

            # keep header for frequency information
            strip_axes(image)

            snapshot = image.replace(".fits", "")
            regrid = "{}.regrid.fits".format(snapshot)
            convol = "{}.convol.fits".format(snapshot)



            if not os.path.exists(regrid) or args.overwrite:

                blank_beyond_horizon(image, value=np.nan)
                # nan_to_zero(image)

                # TODO useold checks
                if args.find_psf:
                    


                    beamcon_2D_wrapper_wetrun(
                        image=image,
                        bmaj=bmaj,
                        bmin=bmin,
                        bpa=bpa,
                        suffix="cvl"
                    )
                    fitsin(image.replace(".fits", ".cvl.fits"), image.replace(".fits", ".mir"))
                else:
                    fitsin(image, image.replace(".fits", ".mir"))
                
                rinfile = image.replace(".fits", ".mir")

                if ((not os.path.exists(convol) or args.overwrite) and args.convolve) and not args.find_psf:

                    logger.info(">>> Convolving {}.fits".format(snapshot))

                    convol_wrapper(mirfile=image.replace(".fits", ".mir"),
                                outfile=image.replace(".fits", ".cvl"),
                                beam=[bmaj*3600., bmin*3600., bpa])
                    rinfile = image.replace(".fits", ".cvl")

                    if not os.path.exists(image.replace(".fits", ".cvl")):
                        s = color_text(">>! {} did not convolve - assuming already at required resolution".format(snapshot), Colors.Orange)
                        logger.warning(s)
                        shutil.copytree(image.replace(".fits", ".mir"),
                                        image.replace(".fits", ".cvl"))

                elif args.convolve and not args.find_psf:
                    rinfile = image.replace(".fits", ".cvl")

                else:
                    s = color_text(">>! {}.fits already convolved".format(snapshot), Colors.Orange)
                    logger.warning(s)


                logger.info(">>> Making {}".format(regrid))

                regrid_wrapper(mirfile=rinfile, 
                            outfile=image.replace(".fits", ".rgd"),
                            crval=args.coords,
                            crpix=(int(imsize[0]/2.), int(imsize[1]/2.)),
                            cdelt=(-cd, cd),
                            naxis=imsize,
                            proj=args.projection)

                fitsout(regrid, image.replace(".fits", ".rgd"))

                for ext in ["mir", "cvl", "rgd"]:
                    if os.path.exists(image.replace(".fits", "."+ext)):
                        shutil.rmtree(image.replace(".fits", "."+ext))

            else:
                s = color_text(">>! {} already made".format(regrid), Colors.Orange)
                logger.warning(s)


            images[i] = regrid

            if models[i] is not None:

                # Regrid the model as well:
                strip_axes(models[i])
                mregrid = models[i].replace(".fits", ".regrid.fits")

                if not os.path.exists(mregrid) or args.overwrite:

                    logger.info(">>> Making {}".format(mregrid))

                    fitsin(models[i], models[i].replace(".fits", ".mir"))
                    regrid_wrapper(mirfile=models[i].replace(".fits", ".mir"), 
                                outfile=models[i].replace(".fits", ".rgd"),
                                crval=args.coords,
                                crpix=(int(imsize[0]/2.), int(imsize[1]/2.)),
                                cdelt=(-cd, cd),
                                naxis=imsize)
                    fitsout(mregrid, models[i].replace(".fits", ".rgd"))

                    for ext in ["mir", "rgd"]:
                        if os.path.exists(models[i].replace(".fits", "."+ext)):
                            shutil.rmtree(models[i].replace(".fits", "."+ext))




                else:
                    s = color_text(">>! {} already made".format(mregrid), Colors.Orange)
                    logger.warning(s)

                models[i] = mregrid


            if residuals[i] is not None:
        

                # Regrid the residuals as well:
                strip_axes(residuals[i])
                rregrid = residuals[i].replace(".fits", ".regrid.fits")

                if not os.path.exists(rregrid) or args.overwrite:

                    logger.info(">>> Making {}".format(rregrid))

                    fitsin(residuals[i], residuals[i].replace(".fits", ".mir"))
                    regrid_wrapper(mirfile=residuals[i].replace(".fits", ".mir"), 
                                outfile=residuals[i].replace(".fits", ".rgd"),
                                crval=args.coords,
                                crpix=(int(imsize[0]/2.), int(imsize[1]/2.)),
                                cdelt=(-cd, cd),
                                naxis=imsize)
                    fitsout(rregrid, residuals[i].replace(".fits", ".rgd"))

                    for ext in ["mir", "rgd"]:
                        if os.path.exists(residuals[i].replace(".fits", "."+ext)):
                            shutil.rmtree(residuals[i].replace(".fits", "."+ext))

                else:
                    s = color_text(">>! {} already made".format(rregrid), Colors.Orange)
                    logger.warning(s)

                residuals[i] = rregrid


            # Make some weight maps:

            if not os.path.exists(images[i].replace(".fits", "_weights.fits")) or args.overwrite:

                logger.info(">>> Making weight map")

                if not os.path.exists(images[i].replace(".fits", "_rms.fits")) or args.overwrite and \
                    (args.noise_weight or args.single_noise_value_weight):

                    logger.info(">>> Making RMS map")

                    if args.single_noise_value_weight:
                        with fits.open(images[i]) as hdu:
                            arr = np.full_like(hdu[0].data, rmses[i])
                            fits.writeto(images[i].replace(".fits", "_rms.fits"), arr, hdu[0].header, overwrite=True)

                    else:
                        if args.rms_grid is None:
                            grid = int(20.*bmaj/cd)
                        else:
                            grid = int(args.rms_grid)
                        if args.rms_box is None:
                            box = int(5.*grid)
                        else:
                            box = int(args.rms_box)
                        bane_wrapper(images[i], grid, box)
                        os.remove(images[i].replace(".fits", "_bkg.fits"))

                else:
                    logger.warning(">>! RMS map already made or not being used")

                if args.beam_mosaic or args.beam_weight:
                    if not os.path.exists(images[i].replace(".fits", "_beamI.fits")) or args.overwrite:

                        logger.info(">>> Making primary beam map")
                        make_metafits(obsids[i])
                        make_beam_image(obsids[i], images[i], images[i].replace(".fits", "_beamI.fits"))

                    else:
                        logger.warning(">>! Primary beam map already made")

                    pbm_i = fits.open(images[i].replace(".fits", "_beamI.fits"))
                    extra_weight = np.squeeze(pbm_i[0].data**2)
                    
                
                else:
                    extra_weight = np.full_like(fits.getdata(images[i]), 1.)

                if args.noise_weight:

                    rms_i = fits.open(images[i].replace(".fits", "_rms.fits"))
                    weight_map =  extra_weight / np.squeeze(rms_i[0].data**2)
                
                elif args.single_noise_value_weight:
                    rms_mean = np.nanmin(fits.getdata(images[i].replace(".fits", "_rms.fits")))
                    rms_i = np.full_like(fits.getdata(images[i].replace(".fits", "_rms.fits")), rms_mean)
                    weight_map = extra_weight / np.squeeze(rms_i**2)
                elif args.beam_weight or args.beam_mosaic:
                    weight_map = extra_weight
                else:
                    weight_map = np.full_like(fits.getdata(images[i]), 1.)

                fits.writeto(images[i].replace(".fits", "_weights.fits"), weight_map, fits.getheader(images[i]), 
                    overwrite=True)

            else:
                s = color_text(">>! Weight map already made", Colors.Orange)
                logger.warning(s)

            weights.append(images[i].replace(".fits", "_weights.fits"))
            if args.beam_mosaic or args.beam_weight:
                pbeams.append(images[i].replace(".fits", "_beamI.fits"))



            # Make the projected PSF maps:

            if not os.path.exists(images[i].replace(".fits", "_dOmega.fits")) or args.overwrite:

                logger.info(">>> Making dOmega map")

                sinfactors.make_ratio_map(fitsimage=images[i],
                                        outname=images[i].replace(".fits", "_dOmega.fits"),
                                        ra0=header["CRVAL1"],
                                        dec0=header["CRVAL2"])

            else:
                s = color_text(">>! dOmega map already made.", Colors.Orange)
                logger.warning(s)

            if models[i] is not None:
                if not os.path.exists(models[i].replace(".regrid.fits", ".dOmega.fits")) or args.overwrite:

                    logger.info(">>> Applying dOmega to model")
                    
                    model_image = fits.open(models[i])
                    dOmega_image = fits.getdata(images[i].replace(".fits", "_dOmega.fits"))
                    model_image[0].data *= dOmega_image
                    fits.writeto(models[i].replace(".regrid.fits", ".dOmega.fits"), 
                                model_image[0].data, model_image[0].header,
                                overwrite=True)
                
                else:
                    s = color_text(">>! dOmega already applied to model", Colors.Orange)
                    logger.warning(s)

                models[i] = models[i].replace(".regrid.fits", ".dOmega.fits")


            dOmegas.append(images[i].replace(".fits", "_dOmega.fits"))

            final_images.append(images[i])

        except TypeError as err:
            # usually a corrupt file?
            if args.ignore_broken:
                s = color_text("{} likely corrupt - skipping.".format(image), Colors.Orange)
                logger.warning(s)
            else:
                raise err
        

    images = final_images
    template_header = sinfactors.strip_wcsaxes(fits.getheader(images[0]))
    
    if (args.beam_weight or args.beam_mosaic) and (args.level is not None):
        # clip image by beam percent
        print(pbeams)
        s = color_text("Clipping images by {}*Stokes I beam".format(args.level),
                       Colors.MediumSpringGreen)
        logger.info(s)
        for i, image in enumerate(images):
            pb_clip(image, pbeams[i], args.level)

    
    if args.max_rms is not None and (args.noise_weight or args.single_noise_value_weight):
        # instead of throwing them images away, make weight maps arbitrarily tiny
        for i, weight in enumerate(weights):
            rms_i = np.nanmedian(np.squeeze(fits.getdata(images[i].replace(".fits", "_rms.fits"))))
            if rms_i > args.max_rms:
                logger.warn(">>! De-weighting {}".format(images[i]))
                with fits.open(weight, mode="update") as f:
                    f[0].data[:] = 1e-30
                # for re-runs it might be best to keep the original resampled image
                # with fits.open(images[i], mode="update") as f:
                #     f[0].data[:] = 0.
        

    # Making PSF stack:
    s = color_text("Making PSF stack", Colors.MediumSpringGreen)
    logger.info(s)
    dOmega_stack = stack_one_by_one(dOmegas, weights)
    fits.writeto(args.outbase+"_dOmega.fits", dOmega_stack, template_header, overwrite=True)
    sinfactors.make_effective_psf(args.outbase+"_dOmega.fits",
                                  bmaj=bmaj, 
                                  bmin=bmin,
                                  bpa=bpa,
                                  outname=args.outbase+"_psfmap.fits")

    # Make stokes I stack:
    s = color_text("Making main image stack", Colors.MediumSpringGreen)
    logger.info(s)
    avg_stack = stack_one_by_one(images, weights)
    bmaj /= dOmega_stack[int(dOmega_stack.shape[0]/2.), int(dOmega_stack.shape[1]/2.)]
    template_header["BMAJ"] = bmaj
    template_header["BMIN"] = bmin
    template_header["BPA"] = bpa
    template_header["BUNIT"] = "JY/BEAM"
    template_header["BTYPE"] = "INTENSITY"
    template_header["FREQ"] = frequency
    fits.writeto(args.outbase+".fits", avg_stack, template_header, overwrite=True)


    if args.beam_mosaic:
        # Make beam stack:
        s = color_text("Making weights image stack", Colors.MediumSpringGreen)
        logger.info(s)
        beam_stack = stack_one_by_one(pbeams, weights)
        fits.writeto(args.outbase+"_beamI.fits", beam_stack, template_header, overwrite=True)
        weight_stack = stack_one_by_one(weights, weights)
        fits.writeto(args.outbase+"_weights.fits", weight_stack, template_header, overwrite=True)
        arr1 = np.squeeze(fits.getdata(pbeams[0]))
        for i in range(1, len(pbeams)):
            arr1 += np.squeeze(fits.getdata(pbeams[i]))
        fits.writeto(args.outbase+"_sumBeamI.fits", arr1, template_header, overwrite=True)


    if args.residuals is not None:
        # Make residual stack
        s = color_text("Making residual image stack", Colors.MediumSpringGreen)
        logger.info(s)
        res_stack = stack_one_by_one(residuals, weights)
        fits.writeto(args.outbase+"_residual.fits", res_stack, template_header, overwrite=True)


    if args.models is not None:
        # Make model stack:
        s = color_text("Making model stack", Colors.MediumSpringGreen)
        logger.info(s)
        model_stack = stack_one_by_one(models, weights)
        del template_header["BMAJ"]
        del template_header["BMIN"]
        del template_header["BPA"]
        template_header["BUNIT"] = "JY/PIXEL"
        fits.writeto(args.outbase+"_model.fits", model_stack, template_header, overwrite=True)

    with open(args.outbase+"_stats.log", "w+") as f:
        f.write("# Total snapshots used: {}\n".format(len(images)))
        




if __name__ == "__main__":
    main()
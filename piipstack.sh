#!/bin/bash

# Create a deep image.

#SBATCH --account=mwasci
#SBATCH --partition=workq
#SBATCH --time=06:00:00
#SBATCH --nodes=1

source ${PIIPPATH}piipconfig.cfg
source ${PIIPPATH}piipfunctions.sh
source ${PIIPPATH}piiphelp.sh

# set -x

SELECTION=1-1000
EXT="_warp_linear_rbf"
USEOLD=false
INDIR="./"

opts=$(getopt \
    -o :hd:f:r:R:D:I:O:s:o:e:U \
    --long help,indir:,freq:,robust:,ra:,dec:,imsize:,obsid_list:,selection:,outbase:,extension:,useold \
    --name "$(basename "$0")" \
    -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage ;;
        -d|--indir) INDIR="$2" ; shift 2 ;;
        -f|--freq) freq=$2 ; shift 2 ;;
        -r|--robust) r=$2 ; shift 2 ;;
        -R|--ra) ra=$2 ; shift 2 ;;
        -D|--dec) dec=$2 ; shift 2 ;;
        -I|--imsize) fov="$2" ; shift 2 ;;
        -O|--obsid_list) obsid_list="$2" ; shift 2 ;;
        -s|--selection) SELECTION="$2" ; shift 2 ;;
        -o|--outbase) outbase=$2 ; shift 2 ;;
        -e|--extension) EXT=$2 ; shift 2 ;;
        -U|--useold) USEOLD=true ; shift ;;
        --) shift ; break ;;
        *) echo "ERROR: "; usage ;;
    esac
done


check_required "--indir" "-i" $INDIR
check_required "--obsid_list" "-O" $obsid_list
check_required "--robust" "-r" $r
check_required "--selection" "-s" $SELECTION
check_required "--imsize" "-I" $fov
check_required "--ra" "-R" $ra
check_required "--dec" "-D" $dec
check_required "--outbase" "-o" $outbase
check_required "--freq" "-f" $freq

rm ${outbase}_*.list

selections=()
for selection in ${SELECTION[@]}; do
    selections+=($selection)
done
indirs=()
for indir in ${INDIR[@]}; do
    indirs+=($indir)
done

j=0; for list in ${obsid_list[@]}; do

    selection=${selections[$j]}
    indir=${indirs[$j]}

    if [ -z  ${selection} ]; then
        start=1
        end=$(wc -l ${list} | awk '{print $1}')
    else
        start=${selection%-*}
        end=${selection#*-}
    fi

    i=0; while read line; do
        (( i++ ))
        if [ $i -gt $((${start} - 1)) ] && [ $i -lt $((${end} + 1)) ]; then

            obsid=$(echo "$line" | awk '{print $1}')

            if [ -e ${indir}/${obsid}/${obsid}_${freq}MHz_${r}${EXT}.fits ]; then

                echo "${indir}/${obsid}/${obsid}_${freq}MHz_${r}${EXT}.fits found"

                echo ${indir}/${obsid}/${obsid}_${freq}MHz_${r}${EXT}.fits >> ${outbase}_image.list
                echo ${indir}/${obsid}/${obsid}_${freq}MHz_${r}_model${EXT}.fits >> ${outbase}_model.list
                echo ${indir}/${obsid}/${obsid}_${freq}MHz_${r}_residual${EXT}.fits >> ${outbase}_residual.list
                echo ${obsid} >> ${outbase}_obsids.list

            else
                echo "${indir}/${obsid}/${obsid}_${freq}MHz_${r}${EXT}.fits missing"
            fi

        fi
    done < $list

    (( j++ ))

done


if $USEOLD; then useold="-U"; else useold=""; fi

${PIIPPATH}/stackshots2.py ${outbase}_image.list \
    --outbase ${outbase} \
    --coord ${ra} ${dec} \
    --imsize ${fov} \
    -bBC \
    -P 2.0 \
    --obsids ${outbase}_obsids.list \
    --models ${outbase}_model.list \
    --residuals ${outbase}_residual.list \
    --level=0.0 \
    --noise_weight \
    ${useold}

${PIIPPATH}/scale_mosaics.sh ${outbase}.fits

BANE ${outbase}_residual_mean.fits
mv ${outbase}_residual_mean_rms.fits ${outbase}_mean_rms.fits
mv ${outbase}_psfmap.fits ${outbase}_mean_psfmap.fits
mv ${outbase}_dOmega.fits ${outbase}_mean_dOmega.fits

exit 0
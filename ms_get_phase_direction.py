#! /usr/bin/env python

import sys
import numpy as np
from casacore.tables import *

def get_phase_direction(msname):
    field = table(msname+"/FIELD", ack=False)
    pd = field.getcol("PHASE_DIR")
    coords = pd[0][0]
    return np.degrees(coords[0]), np.degrees(coords[1])
    

def main():
    msname = sys.argv[1]
    ra, dec = get_phase_direction(msname)
    print("{} {}".format(ra, dec))

if __name__ == "__main__":
    main()
#!/bin/bash

#SBATCH --account=mwasci
#SBATCH --partition=workq
#SBATCH --time=06:00:00
#SBATCH --nodes=1

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

PADDING=2
NMITER=10
NMITERV=2
NITERV=10000
SOURCE_FINDER="aegean"
SUBBANDS=false
DO_POL=false
REMOVE_SUBBANDS=false
FLUXMODE="mean"
FLAG=true
FLAG_IMAGE=true
FLAG_AO=false
FLAG_TILE=true
FLAG_CHANNEL=false
FLAG_QUACK=false
FLAG_BASELINES=false
MGAIN=0.8
IMAGE_MINUV=0
MULTISCALE_BIAS=0.6
MULTISCALE_MAX_SCALES=5
CLIP_PB_LEVEL=
TMP_DIR=
USE_JOB_FOR_TMP=true
REMOTE_PROCESSED=
BIND_PATH=
STOKES="I"
STOKESV_IMAGE=false
RA_DIR=
DEC_DIR=
MAX_MATCHED_SOURCES=400
SMOOTHING=
APPLYBEAM=true
PREPARE_FOR_IDG=false
QUICK_LOOK_VMAX=1
QUICK_LOOK_VMIN=-0.1

CALIBRATION_COLUMN="CORRECTED_DATA"

start_time=$(date +%s)

opts=$(getopt \
     -o :hd:O:r:s:f:m:Mn:a:yzF:wST:A: \
     --long help,processing_dir:,obsid_list:,robust:,scale:,field:,minuv:,multiscale,niter:,obsnum:,noimage,nocorrect,flagfile:,nominw,subbands,flagtiles:,array_offset: \
     --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piipimage
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done


# after configuration file loading:
set -x

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipimage ;;
        -d|--processing_dir) INDIR=$2 ; shift 2 ;;
        -O|--obsid_list) obsid_list=$2 ; shift 2 ;;
        -r|--robust) robust=$2 ; shift 2 ;;
        -s|--scale) scale=$2 ; shift 2 ;;
        -f|--field) field=$2 ; shift 2 ;;
        -u|--minuv) IMAGE_MINUV=$2 ; shift 2 ;;
        -m|--multiscale) MULTISCALE=true ; shift ;;
        -n|--niter) NITER=$2 ; shift 2 ;;
        -a|--obsnum) obsnum=$2 ; shift 2 ;;
        -y|--noimage) DOIMAGE=false ; shift ;;
        -z|--nocorrect) DOCORRECT=false ; shift ;;
        -w|--nominw) MINW=false ; shift ;;
        -S|--subbands) SUBBANDS=true ; shift ;;
        -F|--flagfile) flagfile=$2 ; shift 2 ;;
        -T|--flagtiles) flagtiles=$2 ; shift 2 ;;
        -A|--array_offset) ARRAY_OFFSET=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR: " ; usage_piipimage ;;
    esac
done

check_required "--processing_dir" "-d" $INDIR
check_required "--robust" "-r" $robust

if ! $DOIMAGE && $DOCORRECT; then
    CORRECT_ONLY=true
else
    CORRECT_ONLY=false
fi

if [ -z ${TMP_DIR} ] || [ "${TMP_DIR}" == "./" ]; then
    TMP_DIR="./"
    USING_TMP=false
else
    TMP_DIR="${TMP_DIR}/"
    if $USE_JOB_FOR_TMP; then
        TMP_DIR="${TMP_DIR}/${SLURM_JOB_ID}/"
    fi 
    if [ ! -e ${TMP_DIR} ]; then
        mkdir -p ${TMP_DIR} || exit 1
    fi
    USING_TMP=true
fi

if ${USING_TMP}; then
    BIND_PATH="${TMP_DIR},${BIND_PATH}"
fi

if [ -z ${CONTAINER} ]; then
    container=""
else
    if [ ! -z ${BIND_PATH} ]; then
        BIND_PATH="-B ${BIND_PATH}"
    fi
    container="singularity exec ${BIND_PATH} ${CONTAINER}"
fi 

# Set obsid from array:
if [ -z ${SLURM_ARRAY_TASK_ID} ]; then
    check_required "--obsnum" "-a" ${obsnum}
else
    obsnum=$((${SLURM_ARRAY_TASK_ID} + ${ARRAY_OFFSET}))
    
fi

if [ ${#obsnum} = 10 ]; then
    obsid=${obsnum}
else
    check_required "--obsid_list" "-o" $obsid_list
    obsid=$(sed "${obsnum}q;d" ${obsid_list} | awk '{print $1}')
fi

check_obsid ${obsid}

if [ ! -e ${INDIR}/${obsid} ]; then
    echo "ERROR: missing working directory - has calibration been done?"
    exit 1
fi

if [ ! -z ${config} ]; then
    cp ${config} ${INDIR}/${obsid}/
fi
cd ${INDIR}/${obsid} || exit 1

if [ ! -e ${obsid}.metafits ]; then
    wget http://ws.mwatelescope.org/metadata/fits/?obs_id=${obsid} -O ${obsid}.metafits
fi

chan=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "CENTCHAN")
freq=$(echo "${chan}*1.28" | bc -l)

if [ ! -z ${field} ] && [ ! -z ${DBFILE} ] && $DOIMAGE; then
    d_format=$(date +%F:%H:%M:%S)
    if [ ${#obsnum} -lt 10 ]; then
        extraOptions="--selection=${obsnum}"
    else
        extraOptions=""
    fi
    $container ${PIIPPATH}/field_manager.py -m "update_snapshot_status" \
        -d ${DBFILE} \
        --field=${field} \
        --chan=${chan} \
        --status="start-image-6r${robust}-[${d_format}]" \
        --overwrite \
        --obsid=${obsid} ${extraOptions}
fi

# Allow overwriting of DATA column:
if [ ${CALIBRATION_COLUMN} = "DATA" ]; then
    datacolumn="data"
else
    datacolumn="corrected"
fi


# ---------------------------------------------------------------------------- #
# Each robust weighting imaging set will have its own directory:

if [ ! -z ${TAPER} ]; then
    taper="-taper-gaussian ${TAPER} "
else
    taper=""
fi

# TODO: check if weighting in this 'list'!
# add function to piipfunctions.sh as part of the "check required" stuff?
if [ "${robust}" == "uniform" ]; then
    weight="uniform"
    weight_dir="uniform"
elif [ "${robust}" == "natural" ]; then
    weight="natural"
    weight_dir="natural"
else
    weight="briggs ${robust}"
    weight_dir="r${robust}"
fi

if ! $CORRECT_ONLY; then

    if [ -e ${INDIR}/${obsid}/image_${weight_dir} ]; then
        rm -rf ${INDIR}/${obsid}/image_${weight_dir}
    fi

    mkdir ${INDIR}/${obsid}/image_${weight_dir}

else
    if [ ! -e ${INDIR}/${obsid}/image_${weight_dir} ]; then
        echo "ERROR: imaging needs to be done for corrections to be applied!"
        exit 1
    fi
fi

if [ ! -z ${config} ]; then
    mv ${config} ${INDIR}/${obsid}/image_${weight_dir}/ || exit 1
fi
cd ${INDIR}/${obsid}/image_${weight_dir}


if ! $CORRECT_ONLY; then

    if [ ! -z ${REMOTE_PROCESSED} ]; then
        rclone copy --local-no-set-modtime ${REMOTE_PROCESSED}/${obsid}_ms.tar ${TMP_DIR}
        tar -mxvf ${TMP_DIR}${obsid}_ms.tar -C ${TMP_DIR} && rm ${TMP_DIR}${obsid}_ms.tar

    else
        cp -r ../${obsid}.m* ${TMP_DIR}
    fi

fi

cp ../${obsid}.metafits . 

subbands=("0000" "0001" "0002" "0003" "MFS")   

scale=$(echo "${SCALE}/${chan}" | bc -l)
radius=$(echo "${scale}*${IMSIZE}/2.0" | bc -l)

# Work out where the peak of the primary beam mainlobe response is, then
# shift to that location - this will give better results across the board
# unless you are interested in a single tiny spot on the sky and only
# plan to image that single tiny spot. 
PERCENT=0.1   
ra=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "RA")
dec=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits "DEC")

if [ -z ${RA_DIR} ]; then
    RA_DIR=$ra
fi
if [ -z ${DEC_DIR} ]; then
    DEC_DIR=$dec
fi

$container get_beam_lobes ${obsid}.metafits -p ${PERCENT} -mM  --pnt ${RA_DIR} ${DEC_DIR} > ${obsid}_beamlobes.txt
mainlobe=$(grep "main" < ${obsid}_beamlobes.txt)
ra_beam=$(echo "$mainlobe" | awk '{print $2}')
dec_beam=$(echo "$mainlobe" | awk '{print $3}')
radec="${ra_beam} ${dec_beam}"


if ! $CORRECT_ONLY; then

    radec_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${radec} -d "hms")
    $container chgcentre ${TMP_DIR}${obsid}.ms ${radec_hmsdms}
    
fi

# ---------------------------------------------------------------------------- #
# Once more flag anything that moves! I mean, anything that needs flagging.

if ! $CORRECT_ONLY; then
    if ${FLAG} && ${FLAG_IMAGE}; then

        if [ ! -z ${flagtiles} ]; then
            flagExtras="--tiles=${flagtiles}"
        fi 

        ${PIIPPATH}/piipflag.sh ${config} --ms=${TMP_DIR}${obsid}.ms \
            --datacolumn=${CALIBRATION_COLUMN} \
            --obsid_list="${obsid_list}" \
            --nobaseline ${flagExtras} \
            --obsid=${obsid}

    fi
fi
# ---------------------------------------------------------------------------- #


# TODO rework the output naming scheme to avoid piiprename stuff
outbase=${obsid}_${freq%.*}MHz_${weight_dir}

if ! $CORRECT_ONLY; then

   
    if ${APPLYBEAM}; then
        applybeam="-apply-primary-beam"
    fi

    if $MULTISCALE; then
        mscale="-multiscale -multiscale-gain 0.1 -multiscale-scale-bias ${MULTISCALE_BIAS} -multiscale-max-scales ${MULTISCALE_MAX_SCALES}"
    else
        mscale=""
    fi

    $container wsclean -name ${TMP_DIR}${obsid}_deep \
        -size ${IMSIZE} ${IMSIZE} \
        -niter ${NITER} \
        -nmiter ${NMITER} \
        -auto-threshold 1 \
        -auto-mask 3 \
        -pol I \
        -padding ${PADDING} \
        -weight ${weight} \
        -scale ${scale:0:8} \
        -abs-mem ${ABSMEM} \
        -j ${NCPUS} \
        -mgain ${MGAIN} \
        -log-time \
        -channels-out 4 \
        -join-channels \
        -no-mf-weighting \
        -mwa-path ${BEAM_PATH} \
        -data-column ${CALIBRATION_COLUMN} \
        -use-wgridder  \
        -minuv-l ${IMAGE_MINUV} \
        -temp-dir ${TMP_DIR} \
        -no-update-model-required \
        ${applybeam} ${mscale} ${taper} ${TMP_DIR}${obsid}.ms &> ${obsid}_deep_wsclean1.log

    # check for divergence:
    DIV_CHECK=$(${container} ${PIIPPATH}/check_for_wsclean_divergence.py ${obsid}_deep_wsclean1.log)

    if [ ! -z ${DIV_CHECK} ]; then

        DIV_ITERATION=$(echo "${DIV_CHECK}" | awk '{print $2}')
        NMITER=$((${DIV_ITERATION}-1))
      
        $container wsclean -name ${TMP_DIR}${obsid}_deep \
            -size ${IMSIZE} ${IMSIZE} \
            -niter ${NITER} \
            -nmiter ${NMITER} \
            -auto-threshold 1 \
            -auto-mask 3 \
            -pol I \
            -padding ${PADDING} \
            -weight ${weight} \
            -scale ${scale:0:8} \
            -abs-mem ${ABSMEM} \
            -j ${NCPUS} \
            -mgain ${MGAIN} \
            -log-time \
            -channels-out 4 \
            -join-channels \
            -no-mf-weighting \
            -mwa-path ${BEAM_PATH} \
            -data-column ${CALIBRATION_COLUMN} \
            -use-wgridder  \
            -minuv-l ${IMAGE_MINUV} \
            -temp-dir ${TMP_DIR} \
            -no-update-model-required \
            ${applybeam} ${mscale} ${taper} ${TMP_DIR}${obsid}.ms &> ${obsid}_deep_wsclean2.log
    
    fi


    if [ ! -e "${obsid}_deep-MFS-image.fits" ]; then

        mv ${TMP_DIR}*.fits .

    fi

    # So many beam images!
    rm ./${obsid}_deep-*-beam-*.fits

    if ${STOKESV_IMAGE}; then

        $container wsclean -name ${TMP_DIR}${obsid}_v \
            -size ${IMSIZE} ${IMSIZE} \
            -niter ${NITERV} \
            -nmiter ${NMITERV} \
            -auto-threshold 1 \
            -auto-mask 3 \
            -pol V \
            -padding ${PADDING} \
            -weight ${weight} \
            -scale ${scale:0:8} \
            -abs-mem ${ABSMEM} \
            -j ${NCPUS} \
            -mgain ${MGAIN} \
            -log-time \
            -channels-out 4 \
            -join-channels \
            -no-mf-weighting \
            -apply-primary-beam \
            -mwa-path ${BEAM_PATH} \
            -data-column ${CALIBRATION_COLUMN} \
            -use-wgridder  \
            -minuv-l ${IMAGE_MINUV} \
            -temp-dir ${TMP_DIR} \
            -no-update-model-required \
            ${mscale} ${taper} ${TMP_DIR}${obsid}.ms

        if [ ! -e "${obsid}_v-MFS-image.fits" ]; then

            mv ${TMP_DIR}*.fits .

        fi

        # So many beam images!
        rm ./${obsid}_v-*-beam-*.fits
        
    fi



    ${container} ${PIIPPATH}/quick_look.py "${obsid}_deep-MFS-image.fits" \
        --cmap cubehelix \
        --inset \
        --vmin ${QUICK_LOOK_VMIN} \
        --vmax ${QUICK_LOOK_VMAX} \
        --rms
   

    

    narrowBands=("0000 0001 0002 0003")
    freqMFS=$($container ${PIIPPATH}/get_MFS_freq.py ${obsid}_deep-????-image.fits)
    for image in "image-pb" "image" "model" "model-pb" "residual" "residual-pb"; do
        if [ -e ${obsid}_deep-MFS-${image}.fits ]; then
            $container ${PIIPPATH}/update_header_key.py ${obsid}_deep-MFS-${image}.fits CRVAL3 ${freqMFS}
            $container ${PIIPPATH}/update_header_key.py ${obsid}_deep-MFS-${image}.fits FREQ ${freqMFS}
        fi
    done

fi

if [ ! -e ${obsid}.ms ] && ${PREPARE_FOR_IDG}; then

    # if preparing MSes for an IDG run, leave them in the working directory
    # this means all flagging will have been done already
    rsync -av ${TMP_DIR}${obsid}.ms .
    rm -r ${TMP_DIR}${obsid}.ms

elif [ -e ${TMP_DIR}${obsid}.ms ] && ! ${PREPARE_FOR_IDG}; then 

    rm -r ${TMP_DIR}${obsid}.ms

fi


# ---------------------------------------------------------------------------- #
# Make some image-based corrections:
if $DOCORRECT; then


    if $SUBBANDS; then
        subbands=("0000 0001 0002 0003 MFS")
    else
        subbands=("MFS")
    fi

    narrowImages=""

    for subband in ${subbands[@]}; do


        # add more flexible support for non-standard channels?
        if [[ "${chan}" == "69" ]]; then
            get_69_subbands ${subband}
        elif [[ "${chan}" == "93" ]]; then
            get_93_subbands ${subband}
        elif [[ "${chan}" == "121" ]]; then
            get_121_subbands ${subband}
        elif [[ "${chan}" == "145" ]]; then
            get_145_subbands ${subband}
        elif [[ "${chan}" == "169" ]]; then
            get_169_subbands ${subband}
        fi

        root=${obsid}_deep
        rootv=${obsid}_v
        image=${root}-${subband}-image-pb.fits
        imagev=${rootv}-${subband}-image-pb.fits
        model=${root}-${subband}-model-pb.fits

        if [ ! -e ${image} ] || ! ${APPLYBEAM}; then
            
            # make pb corrected image using the mwa_pb FEE beam
            ${container} get_beam_image ${obsid}.metafits \
                --image=${root}-${subband}-image.fits \
                -o ${root}-${subband}-FEE.fits

            ${container} pbcorrectMWA.py ${root}-${subband}-image.fits \
                ${root}-${subband}-FEE.fits \
                --cutoff ${CLIP_PB_LEVEL} \
                --outname ${image}

            ${container} pbcorrectMWA.py ${root}-${subband}-model.fits \
                ${root}-${subband}-FEE.fits \
                --cutoff ${CLIP_PB_LEVEL} \
                --outname ${root}-${subband}-model-pb.fits

            ${container} pbcorrectMWA.py ${root}-${subband}-residual.fits \
                ${root}-${subband}-FEE.fits \
                --cutoff ${CLIP_PB_LEVEL} \
                --outname ${root}-${subband}-residual-pb.fits
        fi


        # newname=$($container ${PIIPPATH}/get_new_name.py ${model} -r ${weight_dir})
        if [ -e ${model} ]; then
            newname=$($container ${PIIPPATH}/get_new_name.py ${model} -r ${weight_dir})
            newnamev=$($container ${PIIPPATH}/get_new_name.py ${model} -r ${weight_dir} -v)
        elif [ -e ${image} ]; then
            newname=$($container ${PIIPPATH}/get_new_name.py ${model} -r ${weight_dir})
            newnamev=$($container ${PIIPPATH}/get_new_name.py ${model} -r ${weight_dir} -v)
        else
            echo "ERROR: no images - wsclean may have failed!"
            exit 1
        fi

        stokesIbase=$(echo ${newname} | sed "s/.fits//")
        stokesVbase=$(echo ${newnamev} | sed "s/.fits//")
        warpname=${stokesIbase}_warp.fits
        warpnamev=${stokesVbase}_warp.fits


        # Do the corrections:
        if [ ! -e ${image} ] && [ ! -e ${newname} ] && [ ! -e ${warpname} ]; then
            continue
        else
            if [ -e ${image} ]; then
                cp ${image} ${newname}
            fi

            if [ -e ${imagev} ]; then
                cp ${imagev} ${namenamev}
            fi

            if [ ${subband} == "MFS" ]; then
                if $SUBBANDS; then
                    freqHz=$($container ${PIIPPATH}/get_MFS_freq.py ${narrowImages})
                else
                    if [ -e ${image} ]; then 
                        freqHz=$($container ${PIIPPATH}/get_fits_freq.py ${image} -pa)
                    else
                        freqHz=$($container ${PIIPPATH}/get_fits_freq.py ${newname} -pa)
                    fi
                fi
            fi


            if [ -e ${newname} ]; then
                if [ ${subband} == "MFS" ]; then
                    if $SUBBANDS; then
                        freqHz=$($container ${PIIPPATH}/get_MFS_freq.py ${narrowImages})
                        $container ${PIIPPATH}/update_header_key.py ${newname} FREQ ${freqHz}
                    else
                        freqHz=$($container ${PIIPPATH}/get_fits_freq.py ${newname} -pa)  # updates header
                    fi
                fi
                freq=$($container ${PIIPPATH}/get_fits_freq.py ${newname} -pma)
                freqHz=$(echo "${freq}*1000000" | bc -l)
            else
                if [ ${subband} == "MFS" ]; then
                    if $SUBBANDS; then
                        freqHz=$($container ${PIIPPATH}/get_MFS_freq.py ${narrowImages})
                        $container ${PIIPPATH}/update_header_key.py ${warpname} FREQ ${freqHz}  
                    else
                        freqHz=$($container ${PIIPPATH}/get_fits_freq.py ${warpname} -pa)  # updates header
                    fi
                fi
                freq=$($container ${PIIPPATH}/get_fits_freq.py ${warpname} -pma)
                freqHz=$(echo "${freq}*1000000" | bc -l)
            fi
            threshold=$($container ${PIIPPATH}/get_scaled_flux.py ${freq} -f 88.0 -S 0.5 -a -0.77)

            if [ "${SOURCE_FINDER}"  == "aegean" ]; then
                imageCatalogue=${stokesIbase}_masked_comp.vot
                raKey="ra"
                decKey="dec"
                fluxKey="int_flux"
                pfluxKey="peak_flux"
                efluxKey="err_int_flux"
                epfluxKey="err_peak_flux"
                fluxKeys="int_flux peak_flux"
                rmsKey="local_rms"
            else
                imageCatalogue=${stokesIbase}.pybdsm.gaul.fits
                raKey="RA"
                decKey="DEC"
                fluxKey="Total_flux"
                pfluxKey="Peak_flux"
                efluxKey="E_Total_flux"
                epfluxKey="E_Peak_flux"
                fluxKeys="Total_flux Peak_flux"
                rmsKey="Isl_rms"
            fi

            if [ -z "${MATCH_CATALOGUES_OPTIONS}" ]; then
                MATCH_CATALOGUES_OPTIONS="--ra2 ra --dec2 dec"
            fi

            if [ -z "${FLUX_WARP_OPTIONS}" ]; then
                FLUX_WARP_OPTIONS="-A alpha_c -a beta_c -c gamma_c"
            fi
                

            # Do positional warping to correction 1st order ionosphere effects:
            

            if [ ${subband} == "MFS" ]; then
                $container ${PIIPPATH}/update_header_key.py ${newname} FREQ ${freqHz}
            fi

            current_imsize=$($container ${PIIPPATH}/get_header_key.py ${newname} NAXIS1)

            radius=$(echo "${scale}*${current_imsize}/2.0 - 0.5" | bc -l)

            $container ${PIIPPATH}/update_header_key.py ${stokesIbase}.fits "FREQ" ${freqHz}

            if [ "${SOURCE_FINDER}" == "aegean" ]; then
                $container ${PIIPPATH}/piipaegean.sh ${stokesIbase}.fits ${radec} ${radius} ${config}
            else
                # assume PyBDSF:
                $container simple_bdsf.py ${stokesIbase}.fits 
            fi

            # Allow for a 5 pixel offset. It shouldn't really be this much
            # offset, but just in case. 
            maxsep=$(echo "${scale}*10" | bc -l)
            smooth=$(echo "1.0/${scale}" | bc -l)
            echo "INFO: using smoothing parameter of ${smooth} pixels"

            bmaj=$($container ${PIIPPATH}/get_header_key.py ${stokesIbase}.fits "BMAJ")
            zone=$(echo "${bmaj}*2" | bc -l)


            # TODO: if using non-standard model need to specify same keys as create_skymodel
            $container match_catalogues ${imageCatalogue} ${SKYMODEL} \
                                --separation ${maxsep} \
                                --exclusion_zone ${zone} \
                                --threshold 0.0 \
                                --nmax ${MAX_MATCHED_SOURCES} \
                                --outname ${stokesIbase}_matched.fits \
                                --coords ${radec} \
                                --radius ${radius} \
                                --ra1 ${raKey} \
                                --dec1 ${decKey} \
                                --flux_key ${fluxKey} \
                                --eflux_key ${efluxKey} \
                                -f1 ${fluxKeys} \
                                --localrms ${rmsKey} \
                                --ratio 1.2 \
                                ${MATCH_CATALOGUES_OPTIONS}

            $container fits_warp.py \
                            --xm ${stokesIbase}_matched.fits \
                            --infits ${stokesIbase}.fits \
                            --suffix warp \
                            --ra1 old_ra \
                            --dec1 old_dec \
                            --smooth ${smooth} \
                            --plot \
                            --cores ${NCPUS} \
                            ${MATCH_CATALOGUES_OPTIONS}

            if ${STOKESV_IMAGE}; then
                $container fits_warp.py \
                    --xm ${stokesIbase}_matched.fits \
                    --infits ${stokesVbase}.fits \
                    --suffix warp \
                    --ra1 old_ra \
                    --dec1 old_dec \
                    --smooth ${smooth} \
                    --plot \
                    ${MATCH_CATALOGUES_OPTIONS}
            
                ${container} ${PIIPPATH}/get_leakage.py ${imageCatalogue} \
                    ${stokesVbase}.fits \
                    ${stokesVbase}_leakage.fits

            fi

            # Perform fluxscale multiplicative corrections:

            freqHz=$(echo "${freq}*1000000.0" | bc -l)
            $container ${PIIPPATH}/update_header_key.py ${warpname} "FREQ" ${freqHz}
            if ${STOKESV_IMAGE}; then
                $container ${PIIPPATH}/update_header_key.py ${warpnamev} "FREQ" ${freqHz}
            fi

            naxis1=$($container ${PIIPPATH}/get_header_key.py ${warpname} NAXIS1)
            naxis2=$($container ${PIIPPATH}/get_header_key.py ${warpname} NAXIS2)
            current_imsize=$($container python -c "print(max([int(${naxis1}), int(${naxis2})]))")
            radius=$(echo "${scale}*${current_imsize}/2.0 - 0.5" | bc -l)


            if [ "${SOURCE_FINDER}" == "aegean" ]; then
                $container ${PIIPPATH}/piipaegean.sh ${warpname} ${radec} ${radius} ${config}
                imageCatalogue=${stokesIbase}_warp_masked_comp.vot
            else
                $container simple_bdsf.py ${warpname} 
                imageCatalogue=${stokesIbase}_warp.pybdsm.gaul.fits
            fi

            bmaj=$($container ${PIIPPATH}/get_header_key.py ${stokesIbase}_warp.fits "BMAJ")
            if [ -z ${maxsep} ]; then
                maxsep=${bmaj}
            fi
            zone=$(echo "${bmaj}*2" | bc -l)

            $container match_catalogues ${imageCatalogue} ${SKYMODEL} \
                                 --separation ${maxsep} \
                                 --exclusion_zone ${zone} \
                                 --threshold 0.0 \
                                 --nmax ${MAX_MATCHED_SOURCES} \
                                 --outname ${stokesIbase}_matched_int.fits \
                                 --coords ${radec} \
                                 --radius ${radius} \
                                 --ra1 ${raKey} \
                                 --dec1 ${decKey} \
                                 --flux_key ${fluxKey} \
                                 --eflux_key ${efluxKey} \
                                 -f1 ${fluxKeys} \
                                 --localrms ${rmsKey} \
                                 --ratio 1.2 \
                                 ${MATCH_CATALOGUES_OPTIONS}


            


            if [ -e ${obsid}_deep-MFS-model-pb.fits ]; then
                addImage="--additional_images ${obsid}_deep-${subband}-model-pb.fits "
                addOutname="--additional_outnames ${obsid}_deep-${subband}-model-pb_${FLUXMODE}.fits "
                if [ -e ${obsid}_deep-MFS-residual-pb.fits ]; then
                    addImage="${addImage} ${obsid}_deep-${subband}-residual-pb.fits"
                    addOutname="${addOutname} ${obsid}_deep-${subband}-residual-pb_${FLUXMODE}.fits "
                fi
                if ${STOKESV_IMAGE}; then
                    addImage="${addImage} ${warpnamev}"
                    addOutname="${addOutname} ${warpnamev/.fits/}_${FLUXMODE}.fits"
                fi
            fi

            if [ -z ${SMOOTHING} ]; then

                $container get_beam_lobes ${obsid}.metafits -p 0.05 -Mm > ${obsid}_beamlobes.txt
                
                if (( $( wc -l < ${obsid}_beamlobes.txt ) )); then

                    while read line; do
                        beam_size=$(echo "$line" | awk '{print $4}')
                        SMOOTHING=$(echo "$beam_size / 4.0" | bc -l)
                    done < ${obsid}_beamlobes.txt

                fi

            fi



            $container flux_warp ${stokesIbase}_matched_int.fits \
                       ${warpname} \
                       --threshold ${threshold} \
                       --nmax ${MAX_MATCHED_SOURCES} \
                       --smooth ${SMOOTHING} \
                       --test_subset \
                       --test_fraction 0.25 \
                       --residual \
                       --mode ${FLUXMODE} \
                       --outname ${stokesIbase}_warp_${FLUXMODE}.fits \
                       --order ${ORDER} \
                       --ra_key old_ra \
                       --dec_key old_dec \
                       -l ${rmsKey} \
                       --plot \
                       --nolatex ${FLUX_WARP_OPTIONS} ${addImage} ${addOutname}


            

            # this is no longer needed as all recent runs have the freq info
            $container ${PIIPPATH}/update_header_key.py ${stokesIbase}_warp_${FLUXMODE}.fits "FREQ" ${freqHz}
            for ext in "model" "residual"; do
                $container ${PIIPPATH}/update_header_key.py "${obsid}_deep-${subband}-${ext}-pb.fits" "FREQ" ${freqHz}
            done


            if [ ${subband} != "MFS" ]; then
                narrowImages="${narrowImages} ${warpname}"
            fi

            if [ ! -z ${CLIP_PB_LEVEL} ]; then
                
                ${container} get_beam_image ${obsid}.metafits \
                    --image ${stokesIbase}_warp_${FLUXMODE}.fits \
                    --outname ${stokesIbase}_warp_${FLUXMODE}_beam.fits

                ${container} ${PIIPPATH}/fits_clip_by_beam.py \
                    ${stokesIbase}_warp_${FLUXMODE}.fits \
                    ${stokesIbase}_warp_${FLUXMODE}_beam.fits \
                    --clip ${CLIP_PB_LEVEL}

                if ${STOKESV_IMAGE}; then
                    ${container} ${PIIPPATH}/fits_clip_by_beam.py \
                        ${stokesVbase}_warp_${FLUXMODE}.fits \
                        ${stokesIbase}_warp_${FLUXMODE}_beam.fits \
                        --clip ${CLIP_PB_LEVEL}
                fi

            fi

            if [ "${SOURCE_FINDER}" == "aegean" ]; then
                $container ${PIIPPATH}/piipaegean.sh ${stokesIbase}_warp_${FLUXMODE}.fits ${radec} ${radius} ${config}
            else
                # assume PyBDSF:
                $container simple_bdsf.py ${stokesIbase}_warp_${FLUXMODE}.fits 
            fi

            

            ${container} ${PIIPPATH}/quick_look.py "${stokesIbase}_warp_${FLUXMODE}.fits" \
                --cmap cubehelix \
                --inset \
                --vmin ${QUICK_LOOK_VMIN} \
                --vmax ${QUICK_LOOK_VMAX} \
                --rms

            if ${STOKESV_IMAGE}; then
                ${container} ${PIIPPATH}/quick_look.py "${stokesVbase}_warp_${FLUXMODE}.fits" \
                    --cmap "RdBu_r" \
                    --inset \
                    --rms
            fi


            # keep non-pb image? For use in creation of diagonal screens.
            # if [ -e ${newname} ]; then
            #     # if no newname then we need to keep this old image
            #     files=$(find . -type f \( -name "*deep-${subband}-*image*.fits" \))
            #     for file in ${files[@]}; do
            #         if [ -e ${file} ]; then
            #             rm ${file}
            #         fi
            #     done
            # fi

            files=$(find . -type f \( -name "*-${subband}-*beam*.fits" -or -name "*-${subband}-*dirty*.fits" -or -name "*-${subband}-*psf.fits" \))
            for file in ${files[@]}; do
                if [ -e ${file} ]; then
                    rm ${file}
                fi
            done
        fi

    done

    if ! $SUBBANDS; then

        subbands=("0000" "0001" "0002" "0003")
        
        for subband in ${subbands[@]}; do

            if [ -e ${obsid}_deep-${subband}-image-pb.fits ]; then
                files=$(find . -type f \( -name "*-${subband}-*beam*.fits" -or -name "*-${subband}-*dirty*.fits" -or -name "*-${subband}-*residual*.fits" -or -name "*-${subband}-*psf.fits" \))
                for file in ${files[@]}; do
                    if [ -e ${file} ]; then
                        rm ${file}
                    fi
                done
            fi

            if ${REMOVE_SUBBANDS}; then
                if [ -e ${obsid}_deep-${subband}-image-pb.fits ]; then
                    files=$(find . -type f \( -name "*-${subband}-*image*.fits" -or -name "*-${subband}-*model*.fits" \))
                    for file in ${files[@]}; do
                        if [ -e ${file} ]; then
                            rm ${file}
                        fi
                    done
                fi
            fi
            
        done
    fi


    status="imaging-7"
else
    status="imaging-6"
fi


# Remap crval1 values to be positive:
allImages=*.fits
for image in ${allImages[@]}; do
    ${container} ${PIIPPATH}/fits_fix_crval1.py ${image}
done

# ---------------------------------------------------------------------------- #

end_time=$(date +%s)
duration=$(echo "${end_time}-${start_time}" | bc -l)
echo "Total time taken: ${duration}"

if [ ! -z ${SLURM_ARRAY_TASK_ID} ]; then
    mv ${LOG_LOCATION}/${SLURM_JOB_NAME}.o${SLURM_ARRAY_JOB_ID}_${obsnum} \
        ${LOG_LOCATION}/${SLURM_JOB_NAME}.e${SLURM_ARRAY_JOB_ID}_${obsnum} .
fi

if [ ! -z ${field} ] && [ ! -z ${DBFILE} ]; then
    d_format=$(date +%F:%H:%M:%S)
    $container ${PIIPPATH}/field_manager.py -m "update_snapshot_status" \
        -d ${DBFILE} \
        --field=${field} \
        --chan=${chan} \
        --obsid=${obsid} \
        --status="${status}r${robust}-[${d_format}]" \
        --overwrite ${extraOptions}
fi

exit 0

 #! /bin/bash -l

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

# aoflag -A --aoflag
# antenna flag -T --tileflag
# channel flag -c --channel
# channel flag auto -c
# baseline flag
# quack flag
# rflag
# tfcrop

FLAG_AO=false
FLAG_TILE=false
FLAG_CHANNEL=false
FLAG_CHANNEL_AUTO=false
FLAG_QUACK=false
FLAG_BASELINES=false
FLAG_RFLAG=false
FLAG_TFCROP=false
FLAG_EDGES=false
FLAG_EDGES_N=80
FLAG_EDGES_FRES=40

TMP_DIR=
BIND_PATH=

opts=$(getopt \
     -o :hm:o:M:d:O:ATcCbqrtezyxwZYXWuS: \
     --long help,ms:,obsid:,calms:,datacolumn:,obsid_list:,aoflag,tileflag,channelflag,\
autochannelflag,quackflag,baselineflag,rflag,tfcrop,edgeflag\
noao,notile,nochannel,noautochannel,noquack,\
nobaseline,norflag,notfcrop,noedge,tiles: \
     --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piipflag
    exit 1
}

set -x

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipflag ;;
        -m|--ms) ms=$2 ; shift 2 ;;
        -o|--obsid) obsid=$2 ; shift 2 ;;
        -M|--calms) calms=$2 ; shift 2 ;;
        -d|--datacolumn) datacolumn=$2 ; shift 2 ;;
        -O|--obsid_list) obsid_list=$2 ; shift 2 ;;
        -A|--aoflag) FLAG_AO=true; shift ;;
        -T|--tileflag) FLAG_TILE=true; shift ;;
        -c|--channelflag) FLAG_CHANNEL=true; shift ;;
        -C|--autochannelflag) FLAG_CHANNEL_AUTO=true; shift ;;
        -q|--quackflag) FLAG_QUACK=true ; shift ;;
        -b|--baselineflag) FLAG_BASELINES=true; shift ;;
        -r|--rflag) FLAG_RFLAG=true ; shift ;;
        -t|--tfcrop) FLAG_TFCROP=true ; shift ;;
        -e|--edgeflag) FLAG_EDGES=true ; shift ;;
        -z|--noao) FLAG_AO=false ; shift ;;
        -y|--notile) FLAG_TILE=false ; shift ;;
        -x|--nochannel) FLAG_CHANNEL=false ;  shift ;;
        -w|--noautochannel) FLAG_CHANNEL_AUTO=false ; shift ;;
        -Z|--noquack) FLAG_QUACK=false ; shift ;;
        -Y|--nobaseline) FLAG_BASELINES=false ; shift ;;    
        -X|--norflag) FLAG_RFLAG=false ; shift ;;
        -W|--notfcrop) FLAG_TFCROP=false ; shift ;;
        -u|--noedge) FLAG_EDGES=false ; shift ;;
        -S|--tiles) tile_list=$2; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR:" ; usage_piipflag ;;
    esac
done

check_required "--ms" "-m" $ms
check_required "--datacolumn" "-d" $datacolumn

if [ -z ${TMP_DIR} ]; then
    TMP_DIR="./"
    USING_TMP=false
else
    TMP_DIR="${TMP_DIR}/"
    USING_TMP=true
fi

if ${USING_TMP}; then
    BIND_PATH="${TMP_DIR},${BIND_PATH}"
fi

if [ -z ${CONTAINER} ]; then
    container=""
else
    if [ ! -z ${BIND_PATH} ]; then
        BIND_PATH="-B ${BIND_PATH}"
    fi
    container="singularity exec ${BIND_PATH} ${CONTAINER}"
fi 

if [ -z ${CASA_LOCATION} ]; then
    CASA_LOCATION="${container} casa"
fi

t=$(date +%s)

if [ "${datacolumn}" = "DATA" ]; then
    casa_datacolumn="data"
elif [ "${datacolumn}" = "CORRECTED_DATA" ]; then
    casa_datacolumn="corrected"
else
    echo "Only DATA and CORRECTED_DATA are possible - exiting."
    exit 1
fi

$container ${PIIPPATH}/ms_get_flag_stats.py ${ms}

if $FLAG_AO; then
    echo "INFO: running AOFlagger on ${datacolumn} of ${ms}..."
    ${container} aoflagger -v -column $datacolumn $ms &> aoflagger.${t}.log
    $container ${PIIPPATH}/ms_get_flag_stats.py ${ms}

    if [ ! -z ${calms} ]; then
        echo "INFO: running AOFlagger on ${datacolumn} of ${calms}..."
        ${container} aoflagger -v -column $datacolumn $calms &> aoflagger.${t}.log
        $container ${PIIPPATH}/ms_get_flag_stats.py ${calms}
    fi

fi

if $FLAG_CHANNEL; then
    
    if [ ! -z ${obsid_list} ]; then

        check_required "--obsid" "-o" $obsid
        channel_list=$(grep "${obsid}" "${obsid_list}" | awk '{print $3}')
        if [ ! -z ${channel_list} ]; then
            echo "INFO: flagging user-specified channels ${channel_list} in ${datacolumn} of ${ms}..."
            ${container} ${PIIPPATH}/ms_flag_channels.py "${ms}" -c ${channel_list} -d "${datacolumn}"
            $container ${PIIPPATH}/ms_get_flag_stats.py ${ms}
        fi
    fi

fi

if $FLAG_CHANNEL_AUTO; then
    echo "INFO: auto-flagging channels in ${datacolumn} of ${ms}..."
    ${container} ${PIIPPATH}/ms_flag_channels.py "${ms}" -a -d "${datacolumn}"
    $container ${PIIPPATH}/ms_get_flag_stats.py ${ms}
fi

if $FLAG_TILE; then
    if [ -z ${tile_list} ]; then
        check_required "--obsid" "-o" $obsid
        check_required "--obsid_list" "-O" $obsid_list
        tile_list=$(grep "${obsid}" "${obsid_list}" | awk '{print $2}')
    fi
    if [ -z ${tile_list} ]; then
        echo "INFO: no tiles to flag for ${ms}"
    else
        tile_list=$(echo ${tile_list//,/ })
        echo "INFO: flagging tile(s) ${tile_list} for ${ms}..."
        ${container} flagantennae "${ms}" ${tile_list}
        $container ${PIIPPATH}/ms_get_flag_stats.py ${ms}
  
        if [ ! -z ${calms} ]; then
            echo "INFO: flagging tile(s) ${tile_list} for ${calms}..."
            ${container} flagantennae "${calms}" ${tile_list}
            $container ${PIIPPATH}/ms_get_flag_stats.py ${calms}
        fi

    fi
fi

if $FLAG_BASELINES; then
    echo "INFO: flagging baselines in ${datacolumn} of ${ms}..."
    ${container} ${PIIPPATH}/ms_flag_by_uvdist.py ${ms} ${datacolumn} -a
    $container ${PIIPPATH}/ms_get_flag_stats.py ${ms}
fi

if $FLAG_QUACK; then
    echo "INFO: flagging beginning and end 4 seconds of ${ms}."
    for mode in "beg" "endb"; do
        ${CASA_LOCATION} --nogui \
            --agg \
            --nologger \
            -c \
            "flagdata(vis='${ms}', mode='quack', quackinterval=4, quackmode='${mode}')"
    done
    $container ${PIIPPATH}/ms_get_flag_stats.py ${ms}

    if [ ! -z ${calms} ]; then
        echo "INFO: flagging beginning and end 4 seconds of ${calms}."
        for mode in "beg" "endb"; do
            ${CASA_LOCATION} --nogui \
                --agg \
                --nologger \
                -c \
                "flagdata(vis='${calms}', mode='quack', quackinterval=4, quackmode='${mode}')"
        done
        $container ${PIIPPATH}/ms_get_flag_stats.py ${calms}
    fi

fi

if $FLAG_RFLAG; then
    echo "INFO: running CASA rflag mode on ${ms}..."
    ${CASA_LOCATION} --nogui \
        --agg \
        --nologger \
        -c \
        "flagdata(vis='${ms}', mode='rflag', timedevscale=4.0, datacolumn='${casa_datacolumn}', action='apply')"
    $container ${PIIPPATH}/ms_get_flag_stats.py ${ms}
fi

if $FLAG_TFCROP; then
    echo "INFO: running CASA tfcrop mode on ${ms}..."
    ${CASA_LOCATION} --nogui \
        --agg \
        --nologger \
        -c \
        "flagdata(vis='${ms}', mode='tfcrop', freqcutoff=3.0, usewindowstats='sum', datacolumn='${casa_datacolumn}', action='apply')"
    $container ${PIIPPATH}/ms_get_flag_stats.py ${ms}
fi

if $FLAG_EDGES; then
    # Birli doesn't flag these?
    echo "INFO: ${FLAG_EDGES_N} edge channels for ${ms}..."
    # flag 1 time interval and 80 channels
    edge=$(echo "${FLAG_EDGES_N}/${FLAG_EDGES_FRES}" | bc -l)
    $container flagmwa ${ms} 1 24 1 $edge
    $container ${PIIPPATH}/ms_get_flag_stats.py ${ms}

    if [ ! -z ${calms} ]; then
        echo "INFO: ${FLAG_EDGES_N} edge channels for ${calms}..."
        # flag 1 time interval and 80 channels
        edge=$(echo "${FLAG_EDGES_N}/${FLAG_EDGES_FRES}" | bc -l)
        $container flagmwa ${calms} 1 24 1 $edge
        $container ${PIIPPATH}/ms_get_flag_stats.py ${calms}

    fi

fi

echo "Flagging tasks finished."

exit 0



#!/bin/bash -l

if [ -z ${PIIPPATH} ]; then
    PIIPNAME=$(realpath -s "${0}")
    export PIIPPATH="$(dirname ${PIIPNAME})/"
fi

echo "INFO: PIIPPATH=${PIIPPATH}"

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

IMAGE_STUB=_300MHz_r0.0_warp_rbf.fits
MIN_RMS=10

opts=$(getopt \
     -o :hd:D:o:O:i:I:R:\
     --long help,indir1:,indir2:,outid_list:,obsid_list:,image_stub:,image_dir:,min_rms: \
     --name "$(basename "$0")" \
     -- "$@"
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_piipcal
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_piipcal ;;
        -d|--indir1) INDIR1=$2 ; shift 2 ;;
        -D|--indir2) INDIR2=$2 ; shift 2 ;;
        -o|--outid_list) OUTID_LIST=$2 ; shift 2 ;;
        -O|--obsid_list) OBSID_LIST=$2 ; shift 2 ;;
        -i|--image_stub) IMAGE_STUB=$2 ; shift 2 ;;
        -I|--image_dir) IMAGE_DIR=$2 ; shift 2 ;;
        -R|--min_rms) MIN_RMS=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR:" ; usage_piipcal ;;
    esac
done


set -x

if [ -z ${CONTAINER} ]; then
    container=""
else
    container="singularity exec ${CONTAINER}"
fi

while read line; do

    GOOD_IMAGE=
    OBSID=$(echo "${line}" | awk '{print $1}')
    IMAGE=${OBSID}${IMAGE_STUB}
    if [ -z ${IMAGE_DIR} ]; then
        IMAGE1=${INDIR1}/${IMAGE}
        IMAGE2=${INDIR2}/${IMAGE}
    else
        IMAGE1=${INDIR1}/${OBSID}/${IMAGE_DIR}/${IMAGE}
        IMAGE2=${INDIR2}/${OBSID}/${IMAGE_DIR}/${IMAGE}
    fi

    if [ -e ${IMAGE1} ] && [ -e ${IMAGE2} ]; then

        # ${CONTAINER} ${PIIPPATH}/quick_look_2panel.py ${IMAGE1} ${IMAGE2} ${IMAGE/.fits/_comparison.png} --cmap "cubehelix" --inset --rms
        GOOD_IMAGE=$(${CONTAINER} ${PIIPPATH}/compare_images_for_selection.py ${IMAGE1} ${IMAGE2} --min_rms ${MIN_RMS})


    elif [ -e ${IMAGE1} ]; then

        GOOD_IMAGE=${IMAGE1}

    elif [  -e ${IMAGE2} ]; then

        GOOD_IMAGE=${IMAGE2}

    fi

    if [ ! -z ${GOOD_IMAGE} ]; then 
        echo ${GOOD_IMAGE} >> ${OUTID_LIST}
    fi

done < ${OBSID_LIST}

exit 0

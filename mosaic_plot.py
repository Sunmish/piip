#! /usr/bin/env python

from __future__ import print_function, division

import numpy as np
from astropy.io import fits
from astropy.wcs import WCS
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.wcs.utils import proj_plane_pixel_scales
from astropy.visualization import AsymmetricPercentileInterval, simple_norm
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from matplotlib import rc

import sys
from matplotlib.gridspec import GridSpec, SubplotSpec

# rc("font", **{"family":"serif"})
# rc("text", usetex=True)

mpl.rcParams["xtick.direction"] = "in"
mpl.rcParams["ytick.direction"] = "in"


def auto_v(pmin, pmax, data, stretch="linear"):
    """Determine vmin and vmax from AsymmetricPercentileInterval.

    A mirror of aplpy's old auto_v function.
    """

    interval = AsymmetricPercentileInterval(pmin, pmax)
    try:
        vmin, vmax = interval.get_limits(data)
    except IndexError:
        vmin = vmin = 0  # problem?

    normalizer = simple_norm(data, stretch=stretch, min_cut=vmin, max_cut=vmax)

    vmin = -0.1 * (vmax - vmin) + vmin
    vmax = 0.1 * (vmax - vmin) + vmax

    normalizer.vmin = vmin
    normalizer.vmax = vmax

    return vmin, vmax    


def main():
    """
    """

    image = sys.argv[1]
    psfmap = sys.argv[2]
    rmsmap = sys.argv[3]
    summap = sys.argv[4]

    cmaps = ["gray", "gnuplot2", "gray", "afmhot"]

    with fits.open(rmsmap) as f:
        rms = f[0].data[int(f[0].header["CRPIX1"]), int(f[0].header["CRPIX2"])]
        rms_v = auto_v(10., 99., f[0].data)
    with fits.open(psfmap) as f:
        psf_v = auto_v(10., 99., f[0].data[0, ...])
    with fits.open(summap) as f:
        sum_v = auto_v(10., 99., f[0].data)


    v_settings = [
        (-3*rms, 20*rms),
        psf_v,
        rms_v,
        sum_v,
    ]

    # labels = [
    #     r"(a) Stokes \emph{I}",
    #     r"(b) Effective \verb|BMAJ|",
    #     r"(c) RMS map", 
    #     r"(d) Effective Stokes \emph{I} beam",
    # ]

    labels = [
        "(a) Stokes I", 
        "(b) Effective BMAJ",
        "(c) RMS map",
        "(d) Effective Stokes I beam"
        ]

    # colorbar_labels = [
    #     r"Surface brightness [mJy\,beam$^{-1}$]",
    #     r"Effective \verb|BMAJ| [$^{\prime\prime}$]",
    #     r"Root-mean-square [mJy\,beam$^{-1}$]",
    #     r"Sum Stokes \emph{I} response"
    # ]

    colorbar_labels = [
        "Surface brightness [mJy/beam]",
        "Effective BMAJ [arcsec]",
        "RMS [mJy/beam]",
        "Sum Stokes I response"
        ]

    colorbar_multipier = [
        1000., 3600., 1000., 1.
    ]

    tick_colors = [
        "white", "black", "white", "white"
    ]

    font_labels = 16
    font_ticks = 14

    figsize = (12, 10.5)
    fig = plt.figure(figsize=figsize)

    R = 12./10.

    gs = GridSpec(2, 2, wspace=0.03*R, hspace=0.1, left=0.1, right=1-0.1, 
                  top=0.98, bottom=0.05)


    images = [fits.open(i)[0] for i in [image, psfmap, rmsmap, summap]]

    AXES = []

    for i in range(4):
        sb = SubplotSpec(gs, i)
        try:
            sp = sb.get_position(figure=fig).get_points().flatten()
        except TypeError:
            sp = sb.get_position(fig=fig).get_points().flatten()
        x = sp[0]
        y = sp[1]
        dx = sp[2]-x
        dy = sp[3]-y
        AXES.append([x, y, dx, dy])

    header = fits.getheader(image)
    w = WCS(header).celestial

    image_centre = SkyCoord(ra=header["CRVAL1"], dec=header["CRVAL2"],
                            unit=(u.deg, u.deg))

    for i in range(4):
        norm = mpl.colors.Normalize(vmin=v_settings[i][0]*colorbar_multipier[i],
                                    vmax=v_settings[i][1]*colorbar_multipier[i],
                                    )
        dx = 0.015
        dy = AXES[i][3]
        y = AXES[i][1]
        if i in [0, 2]:
            # colorbar on left:
            x = AXES[i][0] - dx - 0.01
        else:
            x = AXES[i][0] + AXES[i][2] + 0.01

        cbax = [x, y, dx, dy]

        if i in [1, 3]:
            cmaps[i] = plt.get_cmap(cmaps[i], 21)

        colorbar_axis = fig.add_axes(cbax)
        colorbar = mpl.colorbar.ColorbarBase(colorbar_axis, 
                                             cmap=cmaps[i],
                                             norm=norm)

        colorbar.set_label(colorbar_labels[i],
                           fontsize=font_labels,
                           labelpad=5.)
        colorbar.ax.tick_params(which="major",
                                length=3.,
                                color=tick_colors[i],
                                labelcolor="black",
                                width=1.,
                                labelsize=font_ticks)

        if i in [0, 2]:
            colorbar.ax.yaxis.set_ticks_position("left")
            colorbar.ax.yaxis.set_label_position("left")

        ax = plt.axes(AXES[i], projection=w)
        if len(images[i].data.shape) == 3:
            data = images[i].data[0, ...]
        else:
            data = images[i].data 
        ax.imshow(np.squeeze(data),
                  vmin=v_settings[i][0],
                  vmax=v_settings[i][1],
                  cmap=cmaps[i],
                  origin="lower",
                  aspect="auto")

        ax.grid(color=tick_colors[i], ls="--")

        for j in range(2):
            ax.coords[j].set_ticks(color=tick_colors[i], size=5)
            ax.coords[j].display_minor_ticks(True)
            ax.coords[j].set_minor_frequency(2)
            ax.coords[j].set_ticklabel_visible(False)

        ax.text(0.5, -0.06, labels[i],
                horizontalalignment="center",
                transform=ax.transAxes,
                color="black",
                fontsize=font_labels)

    fig.savefig("{}_multiplot.pdf".format(image.replace(".fits", "")),
                transparent=True, dpi=720)

    plt.close("all")

if __name__ == "__main__":
    main()
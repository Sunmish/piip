#! /usr/bin/env python

from argparse import ArgumentParser

import os
import numpy as np
from astropy.table import Table
from astropy.io import fits


def get_args():
    ps = ArgumentParser()
    ps.add_argument("outname", type=str)
    ps.add_argument("images", type=str, nargs="*")
    return ps.parse_args()


def make_table(outname : str, images : list):


    obsids = np.full((len(images),), 0, dtype=int)
    ras = np.full((len(images),), np.nan)
    decs = np.full((len(images),), np.nan)

    for i in range(len(images)):

        print(f"Adding {images[i]}")
        hdr = fits.getheader(images[i])
        obsids[i] = int(os.path.basename(images[i])[0:10])
        ras[i] = hdr["CRVAL1"]
        decs[i] = hdr["CRVAL2"]


    table = Table()
    table["OBSID"] = obsids
    table["RA"] = ras
    table["DEC"] = decs

    table.write(outname, format="fits", overwrite=True)


def cli(args):
    make_table(args.outname, args.images)


if __name__ == "__main__":
    cli(get_args())

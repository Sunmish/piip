#! /usr/bin/env python

from __future__ import print_function, division

from argparse import ArgumentParser
from astropy.io import fits

def main():
    """Find frequency in FITS image header."""  

    ps = ArgumentParser()
    ps.add_argument("image", type=str, help="FITS image.")
    ps.add_argument("-p", "--print", "--echo", dest="echo", action="store_true", 
                    help="Print frequency instead of adding to header.")
    ps.add_argument("-M", "--MHz", "-m", "--mhz", dest="mhz", action="store_true",
                    help="Print in MHz.")
    ps.add_argument("-a", "--add", "-A", dest="add", action="store_true",
                    help="Add found frequency to FITS header.")
    args = ps.parse_args()

    image = fits.open(args.image, mode="update")
    freq = None

    if freq is None and "FREQ" in image[0].header.keys():
        freq = image[0].header["FREQ"]
    if freq is None and "CRVAL3" in image[0].header.keys():
        if "FREQ" in image[0].header["CTYPE3"]:
            freq = image[0].header["CRVAL3"]
    if freq is None and "CRVAL4" in image[0].header.keys():
        if "FREQ" in image[0].header["CTYPE4"]:
            freq = image[0].header["CRVAL4"]
    if freq is None and "CENTCHAN" in image[0].header.keys():
        freq = 1.28 * image[0].header["CENTCHAN"]*1.e6
    
    if freq is None:
        raise ValueError("no frequency information found for {}".format(
                         args.image))

    if freq < 1000:
        freq *= 1.e6

    if args.add:
        image[0].header["FREQ"] = freq
    if args.echo and args.mhz:
        print(freq/1.e6)
    elif args.echo:
        print(freq)

    image.flush()
    image.close()


if __name__ == "__main__":
    main()
#! /usr/bin/env python

from __future__ import print_function, division

from argparse import ArgumentParser

import numpy as np
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.io import fits
from astropy.wcs import WCS

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def strip_wcsaxes(hdr):
    """Strip extra axes in a header object."""


    remove_keys = [key+i for key in 
                   ["CRVAL", "CDELT", "CRPIX", "CTYPE", "CUNIT", "NAXIS"]
                   for i in ["3", "4"]]

    for key in remove_keys:
        if key in hdr.keys():
            del hdr[key]

    return hdr


def radec_to_lm(ra, dec, ra0, dec0):
    """Convert RA,DEC to l,m."""

    l = np.cos(dec)*np.sin(ra-ra0)
    m = np.sin(dec)*np.cos(dec0) - np.cos(dec)*np.sin(dec0)*np.cos(ra-ra0)

    return l, m


def dOmega(ra, dec, ra0, dec0):
    """Calculate dOmega from RA,DEC.

    dOmega = dldm / (1-l^2-m^2)^1/2
    """

    ra = np.radians(ra)
    dec = np.radians(dec)
    ra0 = np.radians(ra0)
    dec0 = np.radians(dec0)

    l, m = radec_to_lm(ra, dec, ra0, dec0)
    n = np.sqrt(1 - l**2 - m**2)

    return 1. / n


def check_projection(hdr, projections=["SIN", "ZEA"]):
    """Check that projection == 'SIN'."""

    projection = hdr["CTYPE1"].split("-")[-1]

    if  projection not in projections:
        raise ValueError("projection is not {}: projection={}".format(
            projections, projection))
    else:
        return projection



def get_new_pa(ra, dec, coords0):
    """Get position angle between new coordinates and reference coordinates.
    """

    coords = SkyCoord(ra=ra*u.deg, dec=dec*u.deg)
    pa = np.degrees(coords0.position_angle(coords).value)
    return pa


def make_sinfactor_map(fitsimage, stride=16000000, ra0=None, dec0=None,
                       outname=None):
    """Make a map of dOmega for a given image and reference coordinates.
    """

    hdu = fits.open(fitsimage)
    try:
        projection = check_projection(hdu[0].header, projections=["SIN"])
    except ValueError:
        logger.warning("dOmega factors only valid for SIN projection.")
        raise
    else:

        shape = np.squeeze(hdu[0].data).shape
        arr = np.full(shape, np.nan)

        w = WCS(hdu[0].header).celestial

        if ra0 is None:
            ra0 = hdu[0].header["CRVAL1"]
        if dec0 is None:
            dec0 = hdu[0].header["CRVAL2"]

        indices=  np.indices(shape)
        y = indices[0].flatten()
        x = indices[1].flatten()
        n = len(x) 

        for i in range(0, n, stride):

            r, d = w.all_pix2world(x[i:i+stride], y[i:i+stride], 0)
            factors = dOmega(r, d, ra0, dec0)

            arr[y[i:i+stride], x[i:i+stride]] = factors

    if outname is None:
        outname = fitsimage.replace(".fits", "_dOmega.fits")

    fits.writeto(outname, arr, strip_wcsaxes(hdu[0].header), overwrite=True)



def make_ratio_map(fitsimage, ra0, dec0, stride=16000000, outname=None, projection=None):
    """Make a map of ratio of dOmega."""

    hdu = fits.open(fitsimage)
    if projection is None:
        try:
            projection = check_projection(hdu[0].header)
        except ValueError:
            logger.warning("dOmega ratios only valid for SIN and ZEA projections.")
            raise
        else:
            logger.info("Found projection {}".format(projection))

    shape = np.squeeze(hdu[0].data).shape
    arr = np.full(shape, np.nan)

    w = WCS(hdu[0].header).celestial

    ra0_n = hdu[0].header["CRVAL1"]
    dec0_n = hdu[0].header["CRVAL2"]

    indices=  np.indices(shape)
    y = indices[0].flatten()
    x = indices[1].flatten()
    n = len(x) 

    for i in range(0, n, stride):

        r, d = w.all_pix2world(x[i:i+stride], y[i:i+stride], 0)
        factors = dOmega(r, d, ra0, dec0)

        if projection == "SIN":
            factors_new = dOmega(r, d, ra0_n, dec0_n)
        elif projection == "ZEA":
            # equal area projection, so no solid angle change over the map:
            factors_new = 1.
        else:
            raise ValueError("Only SIN and ZEA projection supported.")

        arr[y[i:i+stride], x[i:i+stride]] = factors_new / factors

    if outname is None:
        outname = fitsimage.replace(".fits", "_dOmega.fits")

    fits.writeto(outname, arr, strip_wcsaxes(hdu[0].header), overwrite=True)


def make_pa_map(fitsimage, ra0, dec0, stride=16000000, outname=None, projection=None,
                bpa=0.):
    """Make a map of PA."""

    hdu = fits.open(fitsimage)
    if projection is None:
        try:
            projection = check_projection(hdu[0].header)
        except ValueError:
            logger.warning("PA only valid for SIN and ZEA projections.")
            raise
        else:
            logger.info("Found projection {}".format(projection))

    shape = np.squeeze(hdu[0].data).shape
    arr = np.full(shape, np.nan)

    w = WCS(hdu[0].header).celestial

    ra0_n = hdu[0].header["CRVAL1"]
    dec0_n = hdu[0].header["CRVAL2"]

    indices=  np.indices(shape)
    y = indices[0].flatten()
    x = indices[1].flatten()
    n = len(x) 

    coords0 = SkyCoord(ra=ra0*u.deg, dec=dec0*u.deg)
    coords0_n = SkyCoord(ra=ra0_n*u.deg, dec=dec0_n*u.deg)

    for i in range(0, n, stride):

        r, d = w.all_pix2world(x[i:i+stride], y[i:i+stride], 0)
        pa_old = get_new_pa(r, d, coords0)
        pa_new = get_new_pa(r, d, coords0_n)

        # arr[y[i:i+stride], x[i:i+stride]] = np.degrees(np.arctan2(np.sum([np.sin(np.radians(pa_old)),
        #                                                           np.sin(np.radians(pa_new))])/2., 
        #                                                           np.sum([np.cos(np.radians(pa_old)),
        #                                                           np.cos(np.radians(pa_new))])/2.))

        arr[y[i:i+stride], x[i:i+stride]] = pa_old

    arr += bpa
    arr = np.mod(arr, 360.)

    if outname is not None:
        fits.writeto(outname, arr, strip_wcsaxes(hdu[0].header), overwrite=True)

    return arr


def make_psf_map(reference, bmaj, bmin, bpa, outname=None):
    """Make a PSF map with input BMAJ, BMIN, and BPA, based on reference image.

    This is a convenience function.
    """

    with fits.open(reference) as f:

        shape = np.squeeze(f[0].data.shape)
        psf_map = np.full((3, shape[0], shape[1]), np.nan)
        psf_map[0, ...] = bmaj
        psf_map[1, ...] = bmin
        psf_map[2, ...] = bpa

        psf_header = strip_wcsaxes(f[0].header)

    if outname is None:
        # Naming convention suitable for Aegean's autoload feature:
        outname = reference.replace(".fits", "_psf.fits")

    fits.writeto(outname, psf_map.astype(np.float32), psf_header, overwrite=True)



def make_effective_psf(dOmega_map, bmaj, bmin=None, bpa=0., outname=None):
    """
    """

    if bmin is None:
        bmin = bmaj

    hdu = fits.open(dOmega_map)
    shape = hdu[0].data.shape
    psf_map = np.full((3, shape[0], shape[1]), np.nan)
    psf_map[0, ...] = bmaj / hdu[0].data
    psf_map[1, ...] = bmin
    psf_map[2, ...] = bpa


    if outname is None:
        outname = dOmega_map.replace(".fits", "_psfmap.fits")

    fits.writeto(outname, psf_map.astype(np.float32), hdu[0].header, overwrite=True)


def main():
    """
    """

    ps = ArgumentParser(description="Calculate ratio of effective solid angle "
                                    "change across reprojected radio maps for "
                                    "determining how the point spread function "
                                    "varies over the new map.",
                        )

    ps.add_argument("image", type=str,
                    help="Reprojected FITS image in SIN or ZEA projection.")
    ps.add_argument("-i", "--original-image", "--original_image", 
                    dest="original_image", default=None, type=str,
                    help="Original FITS image in SIN projection. Used to get "
                         "original CRVAL1/CRVAL2 values.")
    ps.add_argument("-r", "--ra", default=None, type=float,
                    help="CRVAL1 of original image. Not used if original image "
                         "supplied.")
    ps.add_argument("-d", "--dec", default=None, type=float,
                    help="CRVAL2 of original image. Not used if original image "
                         "supplied.")
    ps.add_argument("-p", "--projection", default=None, choices=["SIN", "ZEA"])
    ps.add_argument("--pa", action="store_true")

    args = ps.parse_args()

    hdr = fits.getheader(args.image)
    bmaj = hdr["BMAJ"]
    try:
        bmin = hdr["BMIN"]
    except KeyError:
        bmin = bmaj
    try:
        bpa = hdr["BPA"]
    except KeyError:
        bpa = 0.

    if args.original_image is not None:

        with fits.open(args.original_image) as f:

            original_projection = check_projection(f[0].header,
                projections=["SIN"])
            args.ra = f[0].header["CRVAL1"]
            args.dec = f[0].header["CRVAL2"] 

    outname = args.image.replace(".fits", "_dOmega")
    make_ratio_map(args.image, args.ra, args.dec, outname=outname+".fits",
                   projection=args.projection)

    if args.pa:
        bpa = make_pa_map(args.image, args.ra, args.dec, outname=outname+"_pa.fits",
                   projection=args.projection, bpa=bpa)


    make_effective_psf(outname+".fits", bmaj, bmin, bpa)


if __name__ == "__main__":
    main()


    
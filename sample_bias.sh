#!/bin/bash

# uv-sampling bias factor test

#SBATCH --account=mwasci
#SBATCH --partition=workq
#SBATCH --time=06:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=12
#SBATCH --mem=120GB
#SBATCH --output=/astro/mwasci/duchesst/G0045/processing/logs/%x.o%A_%a
#SBATCH --error=/astro/mwasci/duchesst/G0045/processing/logs/%x.e%A_%a
#SBATCH --mail-type=ALL
#SBATCH --mail-user=stefanduchesne@gmail.com

source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

set -x

GAUSS=${PIIPPATH}/gaussian_model.txt
POINT=${PIIPPATH}/point_model.txt

PADDING=2
STEP=60
NAME="samplebias"
CALIBRATION_COLUMN="CORRECTED_DATA"
compare_only=false
endSize=600
snr=10
ZERO=false

NAME="samplebias"
IMAGE_MINUV=0
MULTISCALE_BIAS=0.7
MULTISCALE_MAX_SCALES=7

opts=$(getopt \
    -o :hd:o:r:n:s:bcS:w:ZI:\
    --long help,processing_dir:,obsid:,robust:,name:,maxsize:,beam,compare_only,snr:,weight:,zero,increment: \
    --name "$(basename "$0")" \
    -- "$@" 
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_dirty_bias
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_dirty_bias ;;
        -d|--processing_dir) INDIR=$2 ; shift 2 ;;
        -o|--obsid) obsid=$2 ; shift 2 ;;
        -r|--robust) robust=$2 ; shift 2 ;;
        -w|--weight) weight=$2 ; shift 2 ;;
        -n|--name) NAME=$2 ; shift 2 ;;
        -s|--maxsize) endSize=$2 ; shift 2 ;;
        -b|--make_beam) beam=true ; shift ;;
        -c|--compare_only) compare_only=true; shift ;; 
        -S|--snr) snr=$2 ; shift 2 ;; 
        -Z|--zero) ZERO=true ; shift ;;
        -I|--increment) STEP=$2 ; shift 2 ;;
        --) shift ; break ;;
        *) echo "ERROR: " ; usage_dirty_bias ;;
    esac
done

check_required "--processing_dir" "-d" $INDIR
check_required "--obsid_list" "-o" $obsid
check_required "--robust" "-r" $robust

if [ -z ${CONTAINER} ]; then
    container=""
else
    container="singularity run ${CONTAINER}"
fi  

if [ -z ${weight} ]; then
    weight=${robust}
fi

if [ "${weight}" == "uniform" ]; then
    weight=$weight
    sample_dir="uniform"
elif [ "${weight}" == "natural" ]; then
    weight=$weight
    sample_dir="natural"
else
    sample_dir="r${weight}"
    weight="briggs ${weight}"
    
fi

if [ "${robust}" == "uniform" ]; then
    weight_dir="uniform"
elif [ "${robust}" == "natural" ]; then
    weight_dir="natural"
else
    weight_dir="r${robust}"
fi

cd ${INDIR}/${obsid}/image_${weight_dir}/ || exit 1

outdir=${NAME}_${sample_dir}

if [ ! -e $outdir ]; then
    mkdir $outdir
fi

chan=$(${container} ${PIIPPATH}/get_header_key.py ${obsid}.metafits "CENTCHAN")
freq=$(echo "${chan}*1.28" | bc -l)
scale=$(echo "${SCALE}/${chan}" | bc -l)

if [ -e ${INDIR}/${obsid}/image_${weight_dir}/${obsid}_deep-MFS-model.fits ]; then
    if [ ! -e ${outdir}/${obsid}.ms ]; then
        cp -r ../${obsid}.ms ${outdir}/
        originalimsize=$(${container} $PIIPPATH/get_header_key.py ${obsid}_deep-MFS-model.fits NAXIS1)
        originalscale=$(${container} $PIIPPATH/get_header_key.py ${obsid}_deep-MFS-model.fits CDELT2)
        ${container} wsclean \
            -scale ${originalscale} \
            -size ${originalimsize} ${originalimsize} \
            -predict \
            -channels-out 4 \
            -name ${obsid}_deep \
            -use-wgridder \
            ${outdir}/${obsid}.ms
        
        ${container} subtrmodel -usemodelcol -datacolumn $CALIBRATION_COLUMN "" ${outdir}/${obsid}.ms
        
    fi
# elif ! ${ZERO}; then
else
    echo ">>> Imaging must have been done so that we can subtract model to get residuals..."
    exit 1
# else
    # if [ ! -e ${outdir}/${obsid}.ms ]; then
        # cp -r ../${obsid}.ms ${outdir}/
    # fi
fi

cd $outdir || exit 1

if [ -e ../${obsid}.metafits ]; then
    cp ../${obsid}.metafits .
elif [ ! -e ${obsid}.metafits ]; then
    wget http://ws.mwatelescope.org/metadata/fits/?obs_id=${obsid} -O ${obsid}.metafits
fi


# Subtract model after imaging to get residual data:
# assume we worked on the CORRECTED_DATA column

ms=${obsid}.ms



if [ ! -z ${TAPER} ]; then
    taper="-taper-gaussian ${TAPER} "
else
    taper=""
fi

if $MULTISCALE; then
    mscale="-multiscale -multiscale-gain 0.1 -multiscale-scale-bias ${MULTISCALE_BIAS} -multiscale-max-scales ${MULTISCALE_MAX_SCALES}"
else
    mscale=""
fi

mgain=0.8


ra=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits RA)
dec=$($container ${PIIPPATH}/get_header_key.py ${obsid}.metafits DEC)
radec_hmsdms=$($container ${PIIPPATH}/dd_hms_dms.py ${ra} ${dec} -d "hms")

${container} chgcentre ${obsid}.ms ${radec_hmsdms}

sizes=$(seq 60 $STEP ${endSize})

if [ -e ${obsid}_sbf_SNR${snr}_models.txt ]; then
	rm ${obsid}_sbf_SNR${snr}_models.txt
fi


# make a template empty image:
${container} wsclean -name ${obsid}_template \
        -size 1000 1000 \
        -niter 1 \
        -auto-mask 3 \
        -auto-threshold 1 \
        -pol I \
        -padding ${PADDING} \
        -use-wgridder \
        -weight ${weight} \
        -nmiter 1 \
        -scale ${scale:0:8} \
        -abs-mem ${ABSMEM} \
        -j ${NCPUS} \
        -mgain ${mgain} \
        -log-time \
        -channels-out 4 \
        -join-channels \
        -no-mf-weighting \
        -data-column $CALIBRATION_COLUMN \
        -no-update-model-required \
        ${minuv} ${mscale} ${taper} ${ms}

rm *-000?*-*.fits
rm *psf*.fits
rm *residual*.fits 

rms=$(${container} $PIIPPATH/get_std_of_image.py ${obsid}_template-MFS-image.fits -p)
bmaj=$(${container} $PIIPPATH/get_header_key.py ${obsid}_template-MFS-image.fits BMAJ)
bmin=$(${container} $PIIPPATH/get_header_key.py ${obsid}_template-MFS-image.fits BMIN)
bmaj=$(echo "${bmaj}*3600" | bc -l)
bmin=$(echo "${bmin}*3600" | bc -l)
 
for size in ${sizes[@]}; do

	peak=$(echo "${snr}*${rms}" | bc -l)
	echo ${peak}
	size1=$(echo "sqrt(${size}*${size} + ${bmaj}*${bmaj})" | bc -l)
	size2=$(echo "sqrt(${size}*${size} + ${bmin}*${bmin})" | bc -l)
	integrated=$(echo  "${peak}*(${size1}*${size2})/(${bmaj}*${bmin})" | bc -l)
	echo ${integrated}
	cat $GAUSS | sed -e "s:YY:${size}:g" \
                     -e "s:XX:${freq}:g" \
                     -e "s:Jy 1.0:Jy ${integrated}:g" \
                     -e "s:ZZ:${radec_hmsdms}:g" > ${obsid}_SNR${snr}_${size}.txt

    size3=$(echo "${size1}/60" | bc -l)
    echo "${size3},${peak},${integrated}" >> ${obsid}_sbf_SNR${snr}_models.txt

    if [ -e ${ms}.${size} ]; then
    	rm -r ${ms}.${size}
    fi
    cp -r $ms ${ms}.${size}

    if $ZERO; then
        ${container} $PIIPPATH/ms_set_column_zero.py ${ms}.${size} ${CALIBRATION_COLUMN}
        midname="zero"
    else
        midname="sbf"
    fi

    ${container} subtrmodel -datacolumn $CALIBRATION_COLUMN -r ${obsid}_SNR${snr}_${size}.txt ${ms}.${size}

    if $ZERO; then
        stop_criteria="-threshold ${rms}"
    else
        stop_criteria="-auto-mask 3 -auto-threshold 1"
    fi

    ${container} wsclean -name ${obsid}_${midname}_SNR${snr}_${size} \
            -size 1000 1000 \
            -niter 10000 \
            -pol I \
            -padding ${PADDING} \
            -use-wgridder \
            -weight ${weight} \
            -scale ${scale:0:8} \
            -abs-mem ${ABSMEM} \
            -j ${NCPUS} \
            -mgain ${mgain} \
            -log-time \
            -channels-out 4 \
            -join-channels \
            -no-mf-weighting \
            -data-column $CALIBRATION_COLUMN \
            -no-update-model-required \
            -minuv-l $IMAGE_MINUV \
            ${stop_criteria} ${mscale} ${taper} ${ms}.${size}

    rm *-000?*-*.fits
    rm *psf*.fits
    rm *residual*.fits

    rm -r ${ms}.${size}

done


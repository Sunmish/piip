#!/bin/bash

# clean bias factor test

#SBATCH --account=mwasci
#SBATCH --partition=workq
#SBATCH --time=06:00:00
#SBATCH --nodes=1
#SBATCH --output=/group/mwasci/duchesst/logs/%x.o%A_%a
#SBATCH --error=/group/mwasci/duchesst/logs/%x.e%A_%a
#SBATCH --mail-type=ALL
#SBATCH --mail-user=stefanduchesne@gmail.com



source ${PIIPPATH}/piipconfig.cfg
source ${PIIPPATH}/piipfunctions.sh
source ${PIIPPATH}/piiphelp.sh

set -x

GAUSS=${PIIPPATH}/gaussian_model.txt
POINT=${PIIPPATH}/point_model.txt

PADDING=2
STEP=30
NAME="dirtybias"
compare_only=false


opts=$(getopt \
    -o :hd:o:r:n:s:bc \
    --long help,processing_dir:,obsid:,robust:,name:,maxsize:,beam,compare_only \
    --name "$(basename "$0")" \
    -- "$@" 
)

[ $? -eq 0 ] || {
    echo "ERROR: unknown option provided - please check inputs!"
    usage_dirty_bias
    exit 1
}

for opt in ${opts[@]}; do
    if [[ "${opt}" == *".cfg"* ]]; then
        config=$(echo "${opt}" | sed "s/'//g")
        echo "INFO: using config file ${config}"
        source ${config}
        break
    fi
done

eval set -- "$opts"

while true; do
    case "$1" in
        -h|--help) usage_dirty_bias ;;
        -d|--processing_dir) INDIR=$2 ; shift 2 ;;
        -o|--obsid) obsid=$2 ; shift 2 ;;
        -r|--robust) robust=$2 ; shift 2 ;;
        -n|--name) NAME=$2 ; shift 2 ;;
        -s|--maxsize) maxsize=$2 ; shift 2 ;;
        -b|--make_beam) beam=true ; shift ;;
        -c|--compare_only) compare_only=true; shift ;; 
        --) shift ; break ;;
        *) echo "ERROR: " ; usage_dirty_bias ;;
    esac
done

check_required "--processing_dir" "-d" $INDIR
check_required "--obsid_list" "-o" $obsid
check_required "--robust" "-r" $robust


if [ "${robust}" == "uniform" ]; then
    weight="uniform"
    weight_dir="uniform"
elif [ "${robust}" == "natural" ]; then
    weight="natural"
    weight_dir="natural"
else
    weight="briggs ${robust}"
    weight_dir="r${robust}"
fi

if [ ! -e ${INDIR}/${obsid}/image_${weight_dir}/ ]; then
    mkdir ${INDIR}/${obsid}/image_${weight_dir}/
fi

if [ ! -e ${INDIR}/${obsid}/image_${weight_dir}/ ]; then
    exit 1
fi

cd ${INDIR}/${obsid}/image_${weight_dir}/

outdir=${NAME}_${weight_dir}

if [ ! -e $outdir ]; then
    mkdir $outdir
fi
cd $outdir

if [ ! -z ${maxsize} ]; then
    endSize=$maxsize
elif [ ! -z ${MINANGULAR} ]; then
    endSize=$(echo "${MINANGULAR}*3600" | bc -l)
    endSize=${endSize%.*}
else
    # 10 arcmin
    endSize=600
fi

if ! $compare_only; then

if [ -e ${INDIR}/${obsid}/${obsid}.ms ]; then
    cp -r ${INDIR}/${obsid}/${obsid}.ms .
elif [ -e ${INDIR}/${obsid}/${obsid}_ms.zip ]; then
    cp ${INDIR}/${obsid}/${obsid}_ms.zip . && unzip ${obsid}_ms.zip && rm ${obsid}_ms.zip
elif [ -e ${INDIR}/${obsid}/image_${weight_dir}/${obsid}_ms.zip ]; then
    cp ${INDIR}/${obsid}/image_${weight_dir}/${obsid}_ms.zip . && unzip ${obsid}_ms.zip && rm ${obsid}_ms.zip
else
    mv ${INDIR}/${obsid}/image_${weight_dir}/${obsid}.ms .
fi

if [ -e ../${obsid}.metafits ]; then
    cp ../${obsid}.metafits .
elif [ ! -e ${obsid}.metafits ]; then
    wget http://ws.mwatelescope.org/metadata/fits/?obs_id=${obsid} -O ${obsid}.metafits
fi
chan=$(${PIIPPATH}/get_header_key.py ${obsid}.metafits "CENTCHAN")
freq=$(echo "${chan}*1.28" | bc -l)
scale=$(echo "${SCALE}/${chan}" | bc -l)

if [ ! -z ${TAPER} ]; then
    taper="-taper-gaussian ${TAPER} "
else
    taper=""
fi

if $MULTISCALE; then
    if [ -z ${MINANGULAR} ]; then
        maxangular=2.0
    else
        maxangular=${MINANGULAR}
    fi
    mscales=$(${PIIPPATH}//get_mscales.py ${freq} ${scale} ${maxangular})
    mscale="-multiscale -multiscale-gain 0.1 -multiscale-scale-bias 0.65 -multiscale-scales ${mscales}"
else
    mscale=""
fi

if [ ! -z ${MINUV} ]; then
    minuv="-minuv-l ${MINUV}"
elif [ ! -z ${MINANGULAR} ]; then
    wl=$(python -c "from astropy.constants import c; wl = c.value/(${freq}*1.e6); print('{:.2f}'.format(wl))")
    uv=$(python -c "import numpy as np; uv = ${wl} / np.radians(${MINANGULAR}); print('{:.0f}'.format(uv))")
    minuv="-minuvw-m ${uv}"
    echo "INFO: setting ${minuv}"
fi

mgain=0.8

sizes=$(seq 5 $STEP ${endSize})

# Get peak of the primary beam to phase rotate to:
# if [ ! -e ../${obsid}_beamlobes.txt ] || $beam; then
#     get_beam_lobes ./${obsid}.metafits -p 0.05 -mM > ../${obsid}_beamlobes.txt
# fi
# mainlobe=$(grep "main" < ../${obsid}_beamlobes.txt)
# ra_beam=$(echo "$mainlobe" | awk '{print $2}')
# dec_beam=$(echo "$mainlobe" | awk '{print $3}')
# radec="${ra_beam} ${dec_beam}"
ra=$(${PIIPPATH}/get_header_key.py ${obsid}.metafits RA)
dec=$(${PIIPPATH}/get_header_key.py ${obsid}.metafits DEC)
radec_hmsdms=$(${PIIPPATH}/dd_hms_dms.py ${ra} ${dec} -d "hms")

chgcentre ${obsid}.ms ${radec_hmsdms}


size=0
cat $POINT | sed -e "s:XX:${freq}:g" \
                 -e "s:ZZ:${radec_hmsdms}:g" > ${obsid}_${size}.txt

${PIIPPATH}//ms_set_column_zero.py ${obsid}.ms DATA
${PIIPPATH}//ms_set_column_zero.py ${obsid}.ms MODEL_DATA

subtrmodel -datacolumn DATA -s ${obsid}_${size}.txt ${obsid}.ms

wsclean -name ${obsid}_cbf_${size} \
        -size 1000 1000 \
        -niter 10000 \
        -threshold 0.001 \
        -pol I \
        -padding ${PADDING} \
        -nwlayers-factor 4 \
        -weight ${weight} \
        -scale ${scale:0:8} \
        -abs-mem ${ABSMEM} \
        -j ${NCPUS} \
        -mgain ${mgain} \
        -log-time \
        -channels-out 4 \
        -join-channels \
        -no-mfs-weighting \
        -datacolumn DATA \
        -no-update-model-required \
        ${minuv} ${mscale} ${taper} ${obsid}.ms

rm *-000?*-*.fits
rm *psf*.fits
rm *residual*.fits


for size in ${sizes[@]}; do


    cat $GAUSS | sed -e "s:YY:${size}:g" \
                     -e "s:XX:${freq}:g" \
                     -e "s:ZZ:${radec_hmsdms}:g" > ${obsid}_${size}.txt

    ${PIIPPATH}//ms_set_column_zero.py ${obsid}.ms DATA
    ${PIIPPATH}//ms_set_column_zero.py ${obsid}.ms MODEL_DATA

    subtrmodel -datacolumn DATA -s ${obsid}_${size}.txt ${obsid}.ms


    wsclean -name ${obsid}_cbf_${size} \
            -size 1000 1000 \
            -niter 10000 \
            -threshold 0.001 \
            -pol I \
            -padding ${PADDING} \
            -nwlayers-factor 4 \
            -weight ${weight} \
            -scale ${scale:0:8} \
            -abs-mem ${ABSMEM} \
            -j ${NCPUS} \
            -mgain ${mgain} \
            -log-time \
            -channels-out 4 \
            -join-channels \
            -no-mfs-weighting \
            -datacolumn DATA \
            -no-update-model-required \
            ${minuv} ${mscale} ${taper} ${obsid}.ms

    rm *-000?*-*.fits
    rm *psf*.fits
    rm *residual*.fits

done

fi

sigma=20
${PIIPPATH}/compare_dirty_to_clean.py --radius 3600 88 ${obsid}_cbf 0 `seq 5 $STEP ${endSize}` --name ""  -A 1.4 --snr $sigma --sigma 0

rm -r ${obsid}.ms

exit 0
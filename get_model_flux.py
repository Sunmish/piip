#! /usr/bin/env python

import sys
from astropy.io import fits
import numpy as np

def model_flux(filename):
    """
    """
    image = fits.open(filename)[0]
    data = np.squeeze(image.data)
    total_flux = np.nansum(data[np.where(data > 0.)])
    return total_flux
    
def main():
    """
    """
    print(model_flux(sys.argv[1]))

if __name__ == "__main__":
    main()
